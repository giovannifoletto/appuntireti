# Capitolo 3: Livello di Trasporto

L’obiettivo per questa sezione è capire i principi che sono alla base dei servizi del livello di trasporto, come ad esempio:

* multiplexing/demultiplexing
* trasferimento dati affidabile

* controllo di flusso
* controllo di congestione

Per raggiungere questo obiettivo si descriveranno i protocolli del livello di trasporto di internet: 

* UDP: trasporto senza connessione
* TCP: trasporto orientato alla connessione
* controllo di congestione TCP

## 3.1 Servizi e protocolli di trasporto

I servizi e i protocolli di trasporto forniscono la parte logica della comunicazione tra processi applicativi di host differenti, facendo in modo che i processi ottengano un tunnel risultante come se i processi interagissero direttamente tra loro. 

Di fatto il transport layer fa in modo che due processi si parlino attraverso la rete come fossero direttamente collegati.

I protocolli di questo layer servono sia dal lato invio, sia dal lato ricezione:

* all’invio tutti i messaggi vengono rotti in chunk e forniti di header di questo layer della pila di rete. Dopo questo chunk viene passato al protocollo di rete e inviato all’altro host.
* alla ricezione di questi messaggi il protocollo di rete si occupa di ottenere i dati e li fornisce al livello di trasporto, che si occupa di fornire i dati richiesti.

Questo livello e i protocolli a esso collegati lavorano completamente sugli host, e sono i responsabili dello “smistamento” dei pacchetti in arrivo al protocolli di rete. Questi pacchetti infatti contengono le informazioni nell’header che identificano il processo che sta ricevendo informazioni. Per questo motivo questo protocollo non ha la necessità di utilizzare e quindi conoscere l’indirizzo IP della macchina.

Il libro presenta la rete come il servizio postale, dove i postini che si occupano di ricevere e mandare mail tra le varie abitazioni sono i protocolli di rete, mentre le persone che all’interno della casa raccolgono tutte le buste in arrivo e le distribuiscono ai vari inquilini è il protocollo di trasporto. Di fatto quindi questi due livelli, lavorando molto similmente hanno una particolare caratteristica che li distingue: uno lavora sull’host (protocollo di trasporto) , l’altro lavora tra gli host (protocollo di rete).



Il livello di trasporto dispone di due protocolli:

* protocollo **TCP**: fornisce un servizio affidabile e garantisce che nella consegna il messaggio non venga modificato rispetto all’originario. Questo protocollo fornisce:
  * controllo di congestione
  * controllo di flusso
  * setup della connessione
  * affidabilità della connessione. Si può pensare come ci sia un vincolo di consegna.
* protocollo **UDP**: fornisce un servizio di consegna inaffidabile e senz’ordine.
  * estensione del servizio di consegna *best-effort* con nessun tipo di applicazione aggiunta
  * questo servizio non fornisce molte garanzie, ma allo stesso modo fornisce una grande velocità di trasferimento data la mancanza di overload di operazioni da effettuare prima di mandare il pacchetto sulla rete. Per questo motivo è molto utilizzato per mandare grandi quantità di dati oppure dati piuttosto piccoli ma che hanno la necessità di essere recapitati velocemente.
* in entrambi questi protocolli non si ha:
  * la garanzia sui ritardi
  * garanzia sull’ampiezza di banda
  * queste due funzioni possono essere implementate dagli sviluppatori, sia dal lato client che dal lato server (spesso da entrambe le parti). Ma questo non significa che la rete fornisca uno strumento efficace per effettuare queste operazioni direttamente.

Per questo motivo il servizio di consegna fornito dal protocollo IP viene definito non affidabile, siccome non riesce a garantire un servizio consistente sempre nella rete, ma lavora solo con il concetto di **best-effort**.



## 3.2 Multiplexing e demultiplexing

Queste operazioni permettono il corretto invio dei pacchetti e la corretta ricezione, facendo in modo di identificare in modo univoco il processo che richiede questi dati.

Di base il funzionamento molto ad alto livello è questo:

* il layer sottostante passa al layer di trasporto i datagrammi in arrivo sulla macchina
* il layer di trasporto analizza gli header e fornisce a ciascun processo tutte le informazioni a lui indirizzate

Il **demultiplexing** sono tutte le operazioni che permettono la ricezione dei singoli pacchetti ai rispettivi processi applicativi, mentre il **multiplexing** sono tutte quelle operazioni di aggiunta di header al pacchetto e di modifica che permettono il corretto passaggio al layer successivo e di conseguenza l’invio dei dati.

Per fare queste operazioni possiamo partire dal fatto che le socket hanno la necessità di avere indicazioni univoche sulla posizione a cui vanno indirizzati i pacchetti diretti a questi processi. Queste due informazioni sono chiamate **porta di destinazione e porta di origine**. Ogni porta è un valore di 16bit, che quindi può variare da 0 a 65535. Le porte da 0 a 1023 sono chiamate *porte conosciute* e servono per applicazioni importanti e di solito riservate al sistema operativo (infatti per usarle in Linux serve di solito avere i permessi di root). Queste porte sono conosciute attraverso l`RFC 1700` e costantemente aggiornate su [questo sito](http://www.iana.org).

Da notare come l’**indirizzo IP non è in questo header,** siccome non sono nell’ambito di competenza del livello di trasporto.

Ci sono due tipi di multiplexing e demultiplexing nel caso si voglia una connessione con persistenza o senza. Logicamente le casistiche e i protocolli che stanno alla base di questi tipi di connessioni sono diversi e le tecniche di demultiplexing e multiplexing che ne conseguono logicamente sono modificata a loro volta.

Tratteremo prima di queste due operazioni con connessioni senza e con connessione, poi tratteremo i protocolli che stanno alla base di queste operazioni.



### Demultiplexing senza connessione: UDP (User Datagram Protocol)

Il multiplexing e demultiplexing operato dal protocollo UDP è particolarmente lineare. La socket UDP è identificata da due parametri: IP destinazione e porta di destinazione. Il buffer UDP, non avendo connessione, “scarica” tutto sullo stesso buffer, come se consegnasse tutto alla stessa porta. Poi in seguito ogni segmento UDP viene inviato alla socket con il numero di porta di destinazione uguale a quello dell’header del pacchetto.

L’header contiene quindi anche la destinazione, questa opzione permette di avere un pacchetto di ritorno molto più veloce, infatti nel caso basta impostare come tuple `(IP, port)` di destinazione quella di origine nel messaggio ricevuto.



### Demultiplexing con connessione: TCP (Transmission Control Protocol)

La socket necessaria al funzionamento del TCP è identificato da 4 parametri: indirizzo IP di origine, numero di porta di origine, numero IP di destinazione, numero di porta di destinazione.

L’host che riceve questo messaggio utilizza questi parametri per inviare il segmento alla socket appropriata. Inoltre si può notare la prima grande differenza: due pacchetti in arrivo sullo stesso host ma con due IP diversi vengono direzionati su due socket diverse da subito, a differenza di come gestiva i pacchetti in arrivo UDP. Di fatto quindi un host server può servire contemporaneamente più socket TCP identificate da questi quattro parametri. Ogni connessione client con questo server web avrà una diversa socket di riferimento.

Il funzionamento tipo della connessione tramite protocollo TCP è:

* il server TCP ha un messaggio di accoglienza, che aspetta una connessione client con un IP e una porta
* il client TCP crea una socket TCP e invia un messaggio di richiesta di connessione al server
* un messaggio di connessione al server non è altro che un pacchetto TCP con una struttura di bit particolare nell’header (visti più avanti).
* Quando il server viene raggiunto da questa richiesta di connessione, si connette alla socket TCP in ascolto creata precedentemente e accetta la connessione. A questo punto crea una nuova socket per questo collegamento.
* I 4 valori che identificano questa nuova socket vengono condivisi con il client e ogni messaggio in arrivo sul server TCP con questi 4 valori (IP destinazione e origine, porta di destinazione e origine) sono uguali a una delle socket presenti sul server allora il processo di demultiplexing le reinderizza a quel determinato processo.

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107130307279.png" alt="image-20211107130307279" style="zoom:33%;" />

Nei più moderni server TCP si utilizzano i thread al posto di effettivi programmi diversi, in modo da lasciare sempre la porta occupa costante e inoltre ridurre la necessità di tempo e potenza della macchina per creare diversi processi.

Le connessioni HTTP non persistenti invece non si comportano come spiegato prima, ma ottengono una socket che viene aperta e chiusa per ogni messaggio inviato o ricevuto. Il problema di chiudere e aprire socket così velocemente può seriamente modificare le performance del server, ma alcuni sistemi operativi hanno possibilità di mitigare questo problema.



## 3.3 Trasporto senza connessione: UDP

Si basa sul FRC `768` ed è un protocollo non affidabile e senza fronzoli ovvero aggiunge ben poco rispetto al livello di rete (cioè di base solo le informazioni delle porte della socket). Per questo motivo le applicazioni che si possono basare su questo protocollo sono diverse e possono avere diversi scopi. Le caratteristiche di base dell’UDP sono che:

* riesce a trasmettere dati rapidamente e in pacchetti molto semplici (infatti non mantiene lo stato e altre informazioni)
* ha un header ridotto al minimo
* offre una latenza data da overlay di software minimizzata
* è molto utile quando si vogliono passare dati direttamente al livello successivo (di trasporto) oppure al precedente (di applicazione). In questo modo si lascia il compito di controllare i messaggi e fare tutte le operazioni sui pacchetti all’host finale lasciando il passaggio nella pila TCP/IP il più lineare e semplice possibile.

Il protocollo UDP quindi, a parte una piccola sezione di controllo degli errori, non fa nulla che non fa già il protocollo IP: aggiunge un piccolo header al pacchetto prima di mandarlo oppure lo rimuove al momento della ricezione.

Un esempio di applicazione che utilizza il protocollo UDP è il DNS, che sfrutta la mancanza di overlay di questo protocollo per inviare dati e risposte più velocemente. Inoltre il server DNS non dovendo mantenere la connessione con l’host dopo che ha risposto, sfrutta proprio questa caratteristica del protocollo. Il client inoltre se non riceve una risposta può decidere di rimandarla.

Adesso si potrà pensare che sia sempre meglio il TCP, date le sue molte garanzie. Invece con l’UDP si possono ottenere alcuni use case particolarmente efficaci:

* più granulare controllo da parte dell’applicazione di quanti, quali e dove i dati sono mandati. Questo nel TCP non è sempre disponibile, siccome il meccanismo di controllo delle congestioni impone di seguire un rigoroso protocollo che non sempre premette ai pacchetti finali di arrivare a destinazione.
* non si crea una connessione. TCP utilizza una tecnica per ottenere una connessione sicura e confermarla, cosa che l’UDP non esegue. Per questo motivo l’UDP non inserisce nessun delay nel mandare il pacchetto.
* connessioni senza stato. Mentre il TCP utilizza il controllo dello stato per effettuare una serie di controlli sui pacchetti in uscita e in entrata, UDP non fa nessuno di questo mantenimento. Di fatto questo permette solitamente che i server UDP permettano un maggior numero di connessioni in entrata.
* poco overhead sui pacchetti. TCP manda circa 20byte di dati in più ogni pacchetto, mentre UDP solo 8byte.

Molto spesso si utilizza l’UDP in tutte quelle applicazioni che richiedono un grande traffico, ma che può mantenere un buon livello di pacchetti persi.

Un pacchetto UDP invece segue una certa struttura, definita dal `RFC 768`:

* tupla contenente il numero della porta e l’IP

* dati dell’applicazione trasportati nel pacchetto

* lunghezza del messaggio che specifica il numero di bytes nel segmento UDP (header + data). Questo campo è particolarmente importante siccome i pacchetti UDP non hanno una dimensione predefinita.

* checksum: utilizzato per controllare che nel segmento ricevuto non ci siano errori. Questi possono avvenire nel trasporto oppure mentre aspettavano su un router.

  Calcolando che ogni “frase” del pacchetto UDP sia lungo 16bit, allora si incolonnano questi bit del pacchetto tra di loro.

  <img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107160908673.png" alt="image-20211107160908673" style="zoom:33%;" />

  Dopo si esegue una semplice operazione XOR, che essendo operazione direttamente sui bit è estremamente efficiente per il processore. Questa processione dei dati può essere vista come un metodo di hashing, ma allo stesso tempo non utilizza nessuna formula matematica, soltanto una formula logica di bit per bit.

  Di solito i pacchetti il cui checksum non corrisponde vengono eliminati e si aspetta che l’host di partenza lo riinvii, infatti le tecniche di recupero errori sono piuttosto complicate. Nonostante questo ci sono alcuni casi in cui invece conviene utilizzare una modalità di recupero errori, ad esempio nelle connessioni satellitari geostazionarie. Queste connessioni hanno un tempo di risposta di circa 3 secondi, motivo per cui conviene impiegare un secondo per recuperare un pacchetto perso piuttosto che aspettarne 6 che questo venga rimandato.

  L’argomento del recupero pacchetti persi o contenenti errori è molto complicato e spesso non applicato solamente alla rete, che preferisce (sia per ragioni di tempo sia per mantenere le cose semplici) di rimandare i pacchetti.

  Queste operazioni di controllo errori nel percorso vengono più volte ripetute in più di un livello, siccome non ci sono garanzie che protocolli sottostanti offrano questo tipo di controllo.

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107081923081.png" alt="image-20211107081923081" style="zoom:33%;" />





## 3.4 Principi del trasferimento dati affidabile

Il problema dell’affidabilità della rete (**reliable data service** o **rdt**) è molto importante e molto difficile da ottenere. Si torna sempre al solito problema, ovvero che la rete è nata dall’unione di molte reti diverse con diversa struttura e componenti. Per questo motivo avere la garanzia sul trasferimento effettivo dei dati è molto difficile.

Ma in questa azione noi vogliamo essere molto sicuri che: 1) i nostri dati arrivino e 2) i nostri datti arrivino *corretti*. Per fare questo server un tubo che operi questa operazione. Questo però come si diceva prima è un canale inaffidabile. Serve quindi un processo per ottenere questa tecnica, e serve anche che sia accessibile da tutti, siccome non si può pensare che si reimplementino tutti queste tecniche ogni volta che si vuole una connessione affidabile. Per questo motivo si lavora tramite un interfaccia, che permette di interfacciarsi a alto livello e produrre gli effetti desiderati.

Logicamente i luoghi fisici o logici in cui si possono ottenere errori sulla rete sono molteplici e quindi ci sono molteplici casualità che devono essere sempre controllate. Per questo motivo si utilizzano gli automi a stati finiti che connettono degli oggetti con uno stato (situazione del pacchetto, della rete, etc) definiti. Utilizzando questi schemi si ottiene uno schema con la semplificazione di tutte le operazioni che il protocollo effettua nei vari casi.

Logicamente per avere accesso a questa serie di protocolli si devono seguire una serie di passi. Da lato invio abbiamo bisogno di una funzione che invia i dati sul protocollo e consegna i dati da mandare a livello superiore del ricevente. Poi si passa attraverso le funzioni di controllo del protocollo affidabile. Il pacchetto in uscita viene ricevuto dalla funzione `udt_send()` che si occupa del trasferimento dei dati sul canale inaffidabile. 

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107165100602.png" alt="image-20211107165100602" style="zoom:33%;" />



Dal lato ricezione i dati vengono ottenuti attraverso la funzione `rdt_rcv()` che si occupa di recuperarli dal canale inaffidabile e passarli al protocollo di trasferimento affidabile. Una volta che questi protocolli vengono eseguiti e il pacchetto è stato controllato, allora la funzione `deliver_data()` si occupa di passare il pacchetto o i dati al livello superiore.

Ci sono più modelli di trasferimento affidabile, definiti rdt e che introducono miglioramenti partendo dal caso di base a modelli che introducono più errori.



### Rdt1.0: trasferimento affidabile su canale affidabile

Il canale di collegamento è perfettamente affidabile (completamente ideale), non si verificano errori nei bit mandati o ricevuti e non si ha nessuna perdita di pacchetti.

Il mittente invia i dati al canale sottostante, mentre il ricevente ottiene solamente i dati dal canale sottostante. 

Di fatto in questo caso non avviene nulla, da una parte si aspetta che i dati arrivino, dall’altra che arrivi la richiesta dei dati (di solito si dice che il sistema è in “ascolto” per un’eventuale chiamata).

![image-20211026141102197](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211026141102197.png)

L’unico evento in questo caso è che l’applicazione invochi la primitiva `rdt_send()`. Questo è un automa che aspetta dati e pacchettizza sulla rete. Le azioni che vengono svolte in modo continuo sono: creo un nuovo pacchetto con una primitiva, poi chiamo `udt_send(packet)` per inviare il pacchetto.

Al livello di ricezione si ottiene la primitiva che ottiene i dati (`rdt_rcv(packet)`) e poi, siccome il canale è affidabile, toglie l’intestazione e restituisce il pacchetto al livello superiore.



### Rdt2.0: canale con errori nei bit

Il canale preso in considerazione permette una comunicazione affidabile (quindi *non* si perdono pacchetti) ma con errori all’interno dei pacchetti (*errori ai bit del pacchetto*). Questo è il primo *rdt* che introduce delle operazioni sui pacchetti. 

Di base questa versione permette una trasmissione affidabile, introducendo un checksum che permetta di capire se il pacchetto ricevuto è corretto. 

La parte di correzione degli errori non è valida perché bisogna avere un pacchetto ridondante che utilizza di conseguenza tantissima banda. Per questo motivo non viene utilizzato questa funzione di recupero, eccetto su alcuni canali caratterizzati da particolare ritardo di trasporto o propagazione (o con un RTT molto elevato, tipo la connessione satellitare). Normalmente in questi casi si tende a rimandare il pacchetto. 

Logicamente il mittente deve sapere se il pacchetto è arrivato correttamente o no (quindi deve sapere se rimandarlo o meno), per cui si sono introdotti due messaggi di risposta (che sono di fatto dei **feedback** per il mittente):

* mandare una **notifica positiva (ACK)** (acknowledgement): il ricevente comunica espressamente che il pacchetto ricevuto è arrivato correttamente
* mandare una **notifica negativa (NAK)** (negative acknowledgement): il ricevente comunica espressamente al mittente che il pacchetto contiene errori. Dopo questo messaggio il mittente deve ritrasmettere il pacchetto.

![image-20211026141851149](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211026141851149.png)

Questo modello ha un funzionamento a blocchi definibile in questo modo:

* per il mittente (doppio stato):
  * A) ricevo dati al livello applicazione
    * creo il pacchetto da inviare e calcolo in checksum, poi lo invio
  * B) entro nello stato di `attesa di ACK o NACK`, ovvero aspetto la risposta del ricevitore
    * quando ricevo un pacchetto, controllo se è di risposta `NAK`. 
    * se il pacchetto è `NAK` allora rimando il pacchetto
    * se il pacchetto è `ACK` allora torno allo stato di `ATTESA DI CHIAMATA DALL'ALTO`
* per il ricevitore (stato singolo):
  * vado in `ATTESA DI CHIAMATA DAL BASSO` e poi elaboro due transizioni. 
    * Se il pacchetto arrivato non è corretto, allora mando un `ACK` al mittente
    * Se il pacchetto arrivato è corretto, allora mando `NAK` al mittente



### Rdt2.1: errore nei pacchetti di NAK o ACK

Il modello rdt2.0 ha un errore importante perché lascia la comunicazione di ritorno come *sempre* affidabile. Invece come posso avere errori nei dati posso avere errori nella trasmissione dei feedback (ACK, NAK).

Per applicare questo rdt ci si deve dotare di un protocollo di **stop & wait**, ovvero bisogna aspettare la risposta alla comunicazione di un pacchetto prima di poter mandare il prossimo. 

Per avere a disposizione tutte le informazioni per determinare uno stato finito in questo modello è necessario introdurre più stati.

IQuesto modello evita la **produzione di pacchetti duplicati**: trasmettendo un pacchetto che è già stato mandato perché il messaggio di avvenuta consegna non è arrivato correttamente fa in modo che il destinatario restituisca al livello successivo due volte gli stessi dati, producendo effetti potenzialmente indesiderati. Si introduce il concetto di **numerazione dei pacchetti** (in questo caso può essere 0 o 1).

![image-20211026143210458](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211026143210458.png)

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107172718209.png" alt="image-20211107172718209"  />

I due automi (nelle due slide, prima mittente poi ricevente):

* mittente:

  * creo un pacchetto numerato o 0 o 1. Il pacchetto viene mandato sulla rete

  * aspetto la ricezione del ricevente

  * Questo pacchetto di risposta può essere ACK/NAK valido (ovvero pacchetto corretto con lo stesso valore numero del pacchetto). 

    Come prima in questo stato si decide se rimandare o meno il pacchetto. 

* ricevente

  * questo valore deve sapere se aspettare il pacchetto 0 o il pacchetto 1
  * se ricevo un pacchetto corretto (il checksum non rivela errori), allora estraggo i dati e li consegno
  * creo il pacchetto nell’ACK con il valore di 0. 
  * Inizio a aspettare per il pacchetto 1

  Il mittente può avere un pacchetto ACK che confermi la corretta ricezione dello 0, perchè se no il trasmettitore non avanza. 

Ancora una volta però si ha un problema infatti aggiungere solo il numero 1 o 0 al pacchetto per definire la sequenza non è sufficiente per ottenere un ordine ben definito. Inoltre si inserisce anche il problema che bisogna controllare se i pacchetti di ACK e NAK sono danneggiati. 

Altro problema è che lo stato deve terne conto dello stato dei pacchetti, aggiungendo overhead di memoria per il determinato pacchetto. 

Dalla parte del ricevente invece si hanno degli altri problemi: si deve controllare se il pacchetto ricevuto è duplicato, e l’unica informazione disponibile per questo controllo è se la sequenza debba essere 1 o 0. Inoltre non può sapere se il il suo ultimo ACK/NAK è stato ricevuto correttamente dal mittente.



### Rdt2.2: un protocollo senza NAK

Questo protocollo ha le stesse funzionalità del rdt2.1, utilizzando soltanto gli ACK, usando il numero per rappresentare il NAK. Viene applicato al caso specifico in cui né il mittente né il ricevente ricevono pacchetti completi, ma solo frammenti. 

Se io mando un pacchetto 0 e ricevo un ACK 0, allora è intuibile che è stato ricevuto tutto correttamente. Se invece si manda un pacchetto 1 e si riceve un ACK 0, allora è come mandare un NAK, e di conseguenza il mittente può rimandare il pacchetto.

Ci sono quindi due tipi di ACK, quello cor retto e quello sbagliato (uno con il numero di sequenza corretto e l’altro con il numero di sequenza sbagliato).

Questo protocollo viene rappresentato a stati come: 

![image-20211107181512766](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107181512766.png)

Le modifiche in rosso permettono di inserire la possibilità che sia indice 1 o indice 



### Rdt3.0: canali con errori e perdite

Si lavora a questo punto su una nuova ipotesi, ovvero che il canale sottostante può anche smarrire i pacchetti (sia di dati che di ACK). Questo blocca il sistema rdt2.2. Nessuna azione viene attivata se non si riceve risposta, ma anzi si continua a aspettare, congelando il sistema.

Per ovviare questo problema si modifica il grafo a stati finiti da parte del mittente introducendo il meccanismo di **countdown timer**. (Questa pratica è sempre utile quando si lavora con la rete o in generale quando si parla di eventi, in modo da sbloccare lo stato anche se non avvengono delle operazioni).

Di solito si parla di perdita di pacchetti molto casuali, perché le perdite ci sono ma di solito sono abbastanza randomico. Alcune tecnologie di comunicazione permettono una minore perdita di pacchetti, mentre invece altre tecnologie hanno più problemi di questo tipo (soprattuto nei protocolli wireless). Di solito comunque i pacchetti persi di una linea considerata “buona” è di $10^{-3}$ solitamente. Se si ha una perdita media maggiore di questo valore il canale non è ritenuto ottimale.

Esiste anche il caso in cui il paccheto viene bloccato per congestione su un router che ha un buffer completamente pieno, ma di solito questi avvenimenti non vengono presi in considerazione dalla statistica.

Il funzionamento di questo protocollo è:

* per il mittente:
  * ogni pacchetto viene creato con numero di controllo e checksum.
  * una volta che il pacchetto viene inviato viene fatto partire un counter. In questo tempo posso:
    * ricevere pacchetti con errori o NAK (ACK con numero sbagliato) allora rinvio il pacchetto
    * se ricevo un pacchetto ACK corretto, allora mando il prossimo pacchetto e resetto il timer.
    * Se il countdown si ferma, allora faccio ripartire il pacchetto.
* per il ricevente viene mantenuto il diagramma a stati di prima



![image-20211107182117298](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107182117298.png)

![image-20211107182131564](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107182131564.png)

![image-20211107182148253](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107182148253.png)

Questa secondo schema evidenzia come il timer livello ricezione non è necessario, perchè si ha un controllo duplicati, che fa in modo di garantire il corretto arrivo dei pacchetti.

Nell’esempio *d)* invece si può notare che un timer troppo stretto rispetto al tempo di percorrenza del pacchetto fa in modo di bloccare tutto il diagramma a stati finiti siccome continuerà a mandare lo stesso pacchetto e il ricevente continuerà a tagliare pacchetti all’arrivo perché trova un duplicato.



### Rdt3.0: Prestazioni

Il protocollo è corretto dal punto di vista logico, siccome inserisce tutti i controlli necessari a avere un tipo di connessione e di trasmissione dei pacchetti corretto. Il problema che ci sono delle prestazioni molto basse, siccome ci si basa solamente sul concetto di *stop&wait*. Quindi il sistema è **robusto** ma **non efficace**.

Il protocollo infatti rischia di fornire un troughtput molto basso. Se si nota, dalle slides sopra, questo valore è rappresentato dai pacchetti blu e il fatto di aspettare la conferma fa bloccare tutta la comunicazione.

Assumento di avere una linea di 1Gbps, con ritardo di 15ms e pacchetti da 1KB. Con questi valore si ha:
$$
T_{trasm} = \frac{L(\text{lunghezza pacchetto in bit})}{R (\text{tasso trasmissivo in bps})} = \frac{8 \space Kb/pacc}{10^9 \space b/sec} = 8 \space microsec
$$
A questo valore di tempo di trasmissione si deve aggiungere la frazione di tempo in cui l’utente è impiegato nell’invio dei bit:
$$
U_{mitt} = \frac{L/R}{RTT + L/R} = \frac{0,008}{30,008} = 0,00027
$$
Questo vuol dire che si invia un pacchetto di 1KB ogni 30 msec, che di fatto rende il valore del throuthput di 33kB/sec in un collegamento da 1Gbps.

Questo significa che il protocollo di trasporto **limita le risorse fisiche**. Questo concetto è fondamentale, siccome su tutta la pila protocollare si può introdurre un valore che rallenta tutta la connessione. Logicamente il protocollo TCP fornisce un obbligo piuttosto grande sulla quantità di pacchetti scambiati e inviati sulla rete.

Questo problema è accettabile, perché si sta dando la precedenza alla sicurezza, ma non in questa quantità. Di fatto quindi servono altre tecniche per ovviare a questo basso bitrate di trasmissione lavorando sul concetto di *stop&wait*. Questo impone che il RTT sia:

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111180313993.png" alt="image-20211111180313993" style="zoom:50%;" />

### Protocolli con il pipeline

Un metodo per risolvere il problema della poca banda risultante dovuta all’overhead di comunicazione del TCP è il **pipeline**. Il concetto che sta alla base di questa idea è che ci sia più di un pacchetto in transito sulla linea di comunicazione, in modo da occupare il tempo sul canale di attesa di risposta con l’invio di altri pacchetti. I pacchetti al ricevitore potranno arrivare con errori o essere corretti, ma intanto si ha diminuito drasticamente il packet-rate della connessione, aumentando di fatto il collo di bottiglia che il protocollo *stop&wait* opera.

Logicamente questo tecnica è migliore perchè permette di avere un RTT sul canale del pacchetto singolo praticamente uguale, ma la banda risulterà divisa in questo modo:

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111181023022.png" alt="image-20211111181023022" style="zoom:50%;" />

In questo esempio si utilizzano solo 3 pacchetti in sequenza, e solo questa aumenta considerevolmente il tempo di emissione dei pacchetti. 

Logicamente, se riuscissimo a mandare tanti pacchetti da riempire completamente il canale, si ottiene un valore di $U_{mitt}$ notevole. Per questo motivo si eseguono dei controlli ogni certo tempo per capire quanti pacchetti si possono mandare oppure si utilizza un valore medio standard.

Da questo concetto risultano due applicazioni (tutti due derivati dal protocollo rdt3.0):

1. **Go-back-N**:

   $N$ sono il numero di pacchetti in sequenza senza attendere i rispettivi ACK. Aspetto il pacchetto di conferma solo dopo che questi siano stati inviati. 

   Il ricevente risponde solo con **ACK comulativi**: se si inviano $N=3$ pacchetti, e si ottiene una risposta con un ACK di valore 3, allora significa che tutti e tre i pacchetti sono stati ricevuti correttamente.

   Il mittente ha un timeout associato al più vecchio pacchetto senza che sia stato tornato un ACK. Questo significa che se il timer scade allora verrano rimandati tutti i pacchetti sequenzialmente dopo questo. Questo metodo è più facile da implementare.

2. **Ripetizione selettiva**:

   Il mittente può trasmettere fino a $N$ pacchetti diversi senza richiedere un ACK di risposta. Il ricevente fornisce un pacchetto ACK per ogni singolo pacchetto. 

   Il mittente stabilisce un timer per ogni pacchetto inviato che non ha ancora ricevuto un ACK, quando scade allora ritrasmette solo il pacchetto che non ha ricevuto ACK.

Di solito queste due tecniche sono entrambe valide e correttamente funzionanti. Inoltre hanno una notevole quantità di sotto-protocolli derivati. Spesso si ha una soluzione di questo tipo: il primo pacchetto indicava al server che protocollo utilizzare e veniva mantenuto per tutta la connessione. Fino a poco tempo fa il default era il *Go-back-N*. Adesso sia per questioni di efficienza che di costo (soprattutto nel wifi, che ha un livello di perdita pacchetti notevole) si preferisce la ripetizione selettiva. 

Entrambi i modelli hanno punti di forza e criticità e per questo vengono mantenuti entrambi per avere una migliore applicazione nei diversi use-cases.

Una visualizzazione di questi pacchetti si può trovare [qui](https://www2.tkn.tu-berlin.de/teaching/rn/animations/gbn_sr/).

##### Go-Back-N:

Dal lato del mittente si devono emettere un valore $N$ continuo di pacchetti. Questo valore poi può essere stimato in vari modi che verranno visti più avanti.

Dal punto di vista del **mittente** si ha una coda così costituita:

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111185120689.png" alt="image-20211111185120689" style="zoom:50%;" />

Il funzionamento di base è che il puntatore a `base` indica il pacchetto più vecchio senza una conferma, il puntatore a `nextseqnum` indica il prossimo pacchetto da mando dei pacchetti ACK duplicati.

I pacchetti arrivati fuori sequenza vengono scartati, quindi non c’è buffering.are, invece il puntatore che conta `base+N` oggetti serve per obbligare il mittente a non mandare troppi pacchetti sulla rete senza aspettare conferma. 

Di fatto questa coda indica che: 

* alcuni pacchetti sono stati mandati e è stato ricevuta conferma della ricezione.

* poi seguono $N$ pacchetti che si dividono in:

  * gialli: pacchetti che sono stati inviati ma non è ancora stata ricevuta la conferma. Hanno un timer attivo sul pacchetto inviato per primo e vengono mantenuti nel buffer finché non si ottiene conferma. Questo per evitare di doverli ricaricare nel caso di timer scaduto.
  * blu: pacchetti pronti per essere inviati e in attesa di lasciare l’host.

  Se questa sezione di sequenza è completamente gialla, allora si sta aspettando per un messaggio di conferma e si ferma. Infatti questo è anche un controllo sul numero di pacchetti emessi.

* poi la coda continua con i pacchetti in sequenza che aspettano di essere preparati per l’invio.

Il funzionamento generale è che nella finestra di controllo degli $N$ pacchetti si mantiene il puntatore `nextseqnum​` che indica il prossimo pacchetto da mandare, mentre invece si ha il puntatore `base` che indica il pacchetto di cui si sta mantenendo il timer.

Quando un pacchetto ACK comulativo arriva tutta la finestra gialla viene confermata, diventando verde. La sezione di $N$ pacchetti si sposta avanti di tanti pacchetti quanti sono stati confermati (ovvero il primo pacchetto giallo sarà il `nextseqnum`). 

In questo caso si presume che ci sia un timer separato per ciascun pacchetto (l’implementazione è più faciel e permette una semplicità maggiore nella gestione del reset del counter), che impone allo scoccare del *timeout* di rimandare tutti i pacchetti successivi a quello a cui è scaduto il tempo. 

L’automa a stati di questo protocollo dalla parte del **mittente** è quindi il seguente (che può sembrare più facile, ma in realtà ha molta più logica):

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111190833599.png" alt="image-20211111190833599" style="zoom:50%;" />

Dalla parte del **ricevente**:

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111213306363.png" alt="image-20211111213306363" style="zoom:50%;" />

Questo modello dalla parte del ricevente permette di capire che il sistema emette un ACK corretto solo se si ottiene un numero più alto in sequenza, rispetto al contatore `expectedseqnum`, ovvero il contatore dei pacchetti arrivati correttamente. Questo permette una corretta gestione dei pacchetti ricevuti correttamente, ma c’è la possibilità che si creino dei pacchetti ACK duplicati.

I pacchetti arrivati fuori sequenza vengono scartati, quindi non c’è buffering.

Ecco un esempio di questo protocollo in azione:

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111214142466.png" alt="image-20211111214142466" style="zoom:50%;" />

Si può notare che:

* ci sono più pacchetti ACK1 mandati, infatti il mittente continua a rispondere con questo pacchetto finchè il mittente rimanda il `pkt1`.
* Questo modello inoltre fa vedere come tutti i pacchetti arrivati dopo il `pkt1` siano stati scartati siccome fuori ordine, e di come poi il mittente abbia dovuto mandare anche pacchetti che erano arrivati correttamente al ricevente.

##### Ripetizione selettiva:

Il modello della ripetizione selettiva è molto simile a quello del GBN, ma con la differenza che si crea un pacchetto di riscontro specifico per ogni pacchetto ricevuto correttamente. Questa pratica permette di controllare singolarmente la correttezza dei pacchetti e, in caso, salvarli in un buffer se sono fuori sequenza. Questa tecnica permette di salvare tempo e banda, in modo che non si debba rimandare tutti i pacchetti dopo un singolo errore come avveniva nel GBN.

IL mittente riceve quindi molti ACK, e ritrasmette solamente i paccheti di cui non riceve ACK. Logicamente, per applicare questa tecnica, è necessario utilizzare un timer su ogni pacchetto inviato. In questo modo se avviene il timeout, il pacchetto viene rimandato direttamente.

Questo protocollo utilizza comunque la tecnica di multipli invii di pacchetti (sempre $N$). Il problema che si può avere in questo caso è che il primo pacchetto non abbia ricevuto conferma, mentre tutti gli altri della finestra invece si. Questo blocca completamente la sequenza di invii, riducendo l’effettività di questo protocollo.

Lo schema di mittente (sopra) e ricevente(sotto) è riporato in questo lucido:

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111220002146.png" alt="image-20211111220002146" style="zoom:50%;" />

Il funzionamento generale è così spiegabile:

* dalla parte del mittente e del ricevente si crea una finestra di $N$ pacchetti che verranno mandati/ricevuti in contemporanea.
* allo stesso modo si crea una finestra di $N$ pacchetti in attesa di essere ricevuti

Di fatto le azioni principali fatte dal **mittente** sono:

* se si dispone di pacchetti successivi pronti per l’invio all’interno della finestra di $N$ elementi, allora si può procedere all’invio
* se un pacchetto inviato va in timeout, allora si procede a rimandare il pacchetto. La finestra non avanza se l’ultimo pacchetto ha ancora attivo il timer.
* se riceve un ACK, marca il numero di pacchetto corrispondente a come arrivato correttamente e fa avanzare la finestra se questo è l’ultimo

Dalla parte del **ricevente** invece avviene che:

* se si ottiene un pacchetto corretto allora invia il pacchetto ACK corrispondente
* se si ottiene un pacchetto fuori sequenza si bufferizza il contenuto, ma si invia lo stesso il pacchetto ACK. Se questo pacchetto è l’ultimo della finestra si ottiene un’avanzamento di tutta la sezione di $N$ elementi.
* se si ricevono pacchetto fuori dalla finestra di controllo allora vengono ignorati



La ripetione selettiva però ha un **problema** che ne limita l’affidabilità. Questa azione però, siccome è conosciuta può essere limitata e arginata.

Se si utilizzano dei numeri di frequenza di un certo valore e si ottiene che, per errori vari le sequenze di controllo non siano mai state recapitate al mittente e quindi si ottiene che le due finestre siano sincronizzate con il numero di sequenza ma in realtà sono sbagliate di una finestra completa.

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211114163554551.png" alt="image-20211114163554551" style="zoom:50%;" />

In questo modo il pacchetto con numero di sequenza 0 viene inteso dal mittente come pacchetto iniziale, mentre dal lato ricezione viene inteso come il secondo 0 della serie. 

Facendo così si distrugge completamente l’affidabilità del trasferimento, siccome si sta di fatto duplicando un oggetto rispetto al valore di partenza. 

Di fatto questa situazione fa capire che bisogna necessariamente **utilizzare una numerazione di controllo opportuna**, ovvero di solito almeno due volte il valore della finestra di controllo.



In teoria la ripetizione selettiva è quella che offre le performance maggiori. 

Nella versione di TCP studiata in questo corso di solito si utilizza il GBN, ma sempre più spesso è facile trovare sistemi (soprattutto wireless, che hanno una grande perdita di pacchetti) utilizzare la ripetizione selettiva, in modo da minimizzare il tempo di trasferimento file.

In realtà tutti i protocolli TCP sono retrocompatibili, in modo da rendere la comunicazione sempre disponibile in base al protocollo utilizzato, che viene di solito imposto dal server e che viene corrisposto dal client. Questo impone che ogni pila disponga di più protocolli TCP al suo interno, tutti però con lo stesso funzionamento ad alto livello, ovvero garantire una comunicazione sicure e affidabile dei file scambiati sulla rete.

Nella realtà dimensione della finestra e del timeout quindi sono dei parametri modificabili. Di base comunque il TCP ha dei contatori di finestra un valore numerico anche fino a 32bit.



## 3.5 Trasporto orientato alla connessione: TCP

Il protocollo per garantire l’affidabilità e la comunicazione garantita sulla rete si utilizza il TCP. Questo protocollo implementa un misto delle tecniche viste precedentemente per garantire il corretto scambio di pacchetti. Per questo motivo il TCP è un protocollo “poliedrico” (ovvero che non è un unico protocollo, ma ci sono diverse opzioni e implementazioni). Gli RFC di base sono `RFC 792, 1122, 1323, 2018, 2581`, dove la RFC principale stabilisce il protocollo di base, poi le altre o utilizzano delle varianti e o introducono dei miglioramenti. Tutte le varianti però sono retrocompatibili e utilizzano l’intestazione standard del TCP studiato e implementato 30 anni fa.

In generale questo protocollo è l’implementazione dell’**Rdt3.0 con il pipelining**, ovvero utilizza:

* numerazione dei segmenti

* riscontri dei segmenti
* timeout
* pipeline (GBN o ripetizione selettiva)

Inoltre il TCP:

* fornisce connessione punto a punto: si può fare il collegamento solo fra una coppia di host

* è affidabile, ovvero crea un flusso che porta al trasporto di tutti i byte dal server al client in modo che non ci sia nessun tipo di modifica da quello originale. Si può pensare come un meccanismo che prende dal buffer di partenza e copia sul buffer di ricezione senza nessun tipo di perdita.

* utilizza la pipeline:

  * due algoritmi che sono responsabili della creazione della finestra di scambio dati dalla parte client e dalla parte server. Questi algoritmi spesso si scontrano, e servono per ottenere il controllo di flusso e il controllo di congestione, due caratteristiche tipo della connessione TCP.
  * il **controllo del flusso** dati serve per adattare la propria frequenza trasmissiva alla propria capacità e a quelle della rete, in modo da non sovraccaricare mai né destinatario né router.

* è **full duplex**: fornisce un protocollo completamente bidirezionale. Di solito si stima un client e un server che comunicano tra di loro attraverso la stessa linea e quindi le risposte del server sono mandate attraverso lo stesso canale/socket. 

* ha una dimensione massima di pacchetto (**MSM, maximum segment size**)

* è orientato alla connessione. Per garantire questo fatto all’apertura della connessione si utilizzano una serie di messaggi di controllo tra mittente e ricevente che servono a garantire la sicurezza degli scambi di file successivi. Questo scambio è chiamato **handshake** e viene sempre fatto prima dell’inizio dell’invio dei file. 

  Queste azioni servono per concordare il numero di sequenza dei messaggi (che logicamente deve essere uguale) e il metodo di comunicazione (Go-back-N o selective repeat, oppure varianti).

Il protocollo TCP è molto comodo sulla rete e da utilizzare perché fornisce una serie di controlli di default (come visto sopra). Questa cosa è molto utile se si vuole avere una connessione sicura e affidabile su un applicazione, senza dover reimplementare tutti i controlli su UDP, rendendo più veloce l’applicazione. Allo stesso modo però limita il controllo della connessione da parte dell’utente, con possibili ritardi dovuti a dei controlli sui pacchetti o sul flusso dei dati, che può portare a un ritardo di invio o ricezione dei dati.

Inoltre il procollo TCP crea dei pacchetti con un header molto più grande di un più semplice UDP (32bit rispetto a 8bit).

### Struttura dei pacchetti TCP

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211116162756764.png" alt="image-20211116162756764" style="zoom:50%;" />

Il pacchetto TCP è costituito da almeno 5 righe da 32bit ciascuna. Poi seguono dei campi opzionali che possono contenere una serie di informazioni che utente e ricevente si devono scambiare utili al protocollo. I dati sono l’ultima sezione e hanno lunghezza variabile fino a raggiungere la lunghezza massima del pacchetto. Tutti i valori inseriti all’interno di un pacchetto sono costituiti da righe di 32bit.

La struttura dell’header di un pacchetto TCP è così costituito:

* *I riga*:  **numero porta di origine e numero porta di destinazione** (due campi da 16bit)

  Questo valore è uguale al pacchetto UDP e indica le informazioni per trasferire i dati da livello applicativo di un host a un’altro. 

* *II  e III riga*: **numero di sequenza e numero di riscontro** (due campi da 32bit)

  Questi due valori servono al protocollo per garantire lo scambio affidabile dei dati, ovvero indicano con questi valori che dato dovrà comunicare il successivo ACK di conferma.

  La numerazione di questi valori è trattata nel paragrafo successivo.

* *IV riga*: (da tre sezioni: una da 10bit, una da 6bit e una da 16)

  La seconda sezione è chiamata anche **FLAG field**. Questi sono dei valori che possono essere veri o falsi, in base a se il bit è 1 o 0.

  * *prima sezione:* **lunghezza dell’intestazione** (4bit)

    Indica il numero di righe dell’intestazione (chiamate word e costituite da 32bit), per informare del numero di campi opzionali presenti nell’header. In questo modo il ricevente sa già la grandezza del payload in ricezione. 

    Se gli header opzionali sono stati lasciati vuoti, allora questo valore è 20byte.

  * *prima sezione*: **unused** (bit variabili)

    Per mantenere le righe del pacchetto sempre di lunghezza 32bit questo valore viene riempito di bit nulli.

  * *flag field*: **ACK**

    Indica se il numero di riscontro nell’intestazione è valido o meno, cioè se si sta strasportando un ACK valido (*peaky-bekking*, ovvero la pratica di utilizzare un pacchetto dati come ACK di un altro pacchetto)

  * *flag field*: **RST**, **SYN** e **FIN**

    Sono pacchetti utilizzati per il setup della connessione TCP e successivamente per la chiusura.

  * *flag field*: **CWR** e **ECE**

    Sono utilizzati in esplicite situazioni di congestione. Raramente utilizzati (infatti con questi due parametri il flag field diviene di 8bit).

  * *flag field*: **PSH**

    Indica che i dati ricevuti vanno passati subito al layer successivo

  * *flag field*: **URG**

    Indica che il pacchetto mandato è stato segnalato dall’applicazione mandante o ricevente come urgente.

  * *terza sezione*: **finestra di ricezione**

    Questo parametro indica il numero di byte che il ricevitore conferma di poter ricevere, ovvero la dimensione di $N$ in byte che il ricevitore ha a disposizione per la connessione.

    Questo viene fatto in byte perchè permette una dimensione dinamica della finestra di recezione del ricevitore oppure può indicare la capacità di buffering del server.

* *V riga*:

  * *prima sezione*: **checksum** 

    Questa sezione indica il valore di controllo (calcolato come il pacchetto UDP) con la funzione di controllare l’integrità dei dati scambiati.

  * *seconda sezione*: **puntatore ai dati urgenti**

    Questa non è una funzionalità molto utilizzata, ma serve a indicare che una certa parte del payload è urgente. 

    Questa segnalazione non serve a TCP, ma serve semplicemente alle applicazioni che utilizzano questo protocollo, che otterranno un valore di dati segnalato come urgenti. 

    I dati di questo pacchetto viaggiano allo stesso modo e vengono trattati dal TCP allo stesso modo, con l’unica differenza che l’applicazione che riceve questo pacchetto ha un puntatore diretto a questo tipo di informazioni.

* **options**: 

  Campo di lunghezza variabile di valore `lenght` nella *IV riga*.

  Questo campo è utilizzato da mittente e ricevente per negoziare il MSS (maximum segment size) o come fattore di scala per delle reti ad altra performance.

  Questo segmento contiene anche il time-stamp. 

  Ulteriorio informazioni di questo parametro sono definite nel `RFC 854` e `RFC 1323`.

* **data**

  Contiene tutti i dati che il pacchetto vuole trasportare.

  Ad esempio nel caso di un pacchetto TCP di una comunicazione HTTP a questo punto inizia l’header di HTTP.

#### Numeri di sequenza e ACK nel TCP

Il numero di sequenza e il numero di ACK in realtà non sono sequenze come indicate negli schemi (quindi ad esempio 1-2-3 ...). Questi valori di solito indicano dei valori di byte.

Ad esempio, prendiamo un pacchetto di 400byte diviso in 4 segmenti da 100byte, il numero di sequenza consecutiva sarà:

1. `#100`, perché decido di chiamarlo così. Questo pacchetto ha lunghezza di `1000byte`.
2. `#1100` (perchè è il valore `#100` del primo e il dato di lunghezza del pacchetto), con lunghezza ancora di `1000byte`.
3. `#2100` (perchè `1100+1000`), sempre di lunghezza di `1000 byte`
4. `#3100` per concludere.

Allo stesso modo gli ACK:

1. Ricevo il pacchetto `#100`, con una lunghezza di `1000byte`. Il corrispondente ACK è `ACK=1100`.
2. Questo sarà `#2100` (`1100` di prima, più la lunghezza del payload ricevuto)
3. Sarà `#3100` 
4. `#4100`

Questo esempio quindi rappresenta uno scambio di questo tipo (dove il *flag: A* è disattivato, quindi si ottiene un pacchetto di ACK singolo per confermare ogni risposta, a differenza dell’esempio successivo che lo scambio di dati deve essere bidirezionale e quindi l’ACK di conferma è sempre inserito in un pacchetto di dati con un *flag: A* attivo):

```mermaid
%% Example of sequence diagram
  sequenceDiagram
    HostA->>HostB: SeqNum=100, ACK=0, lenght=1000
    HostB->>HostA: SeqNum=0, ACK=1100, lenght=1000
    HostA->>HostB: SeqNum=1100, ACK=0, lenght=1000
    HostB->> HostA: SeqNum=0, ACK=2100, lenght=1000
    HostA->>HostB: SeqNum=2100, ACK=0, lenght=1000
    HostB->>HostA: SeqNum=0, ACK=2100, lenght=1000
    HostA->>HostB: SeqNum=3100, ACK=0, lenght=1000
    HostB->>HostA: SeqNum=0, ACK=3100, lenght=1000
    HostA->>HostB: SeqNum=3100, ACK=0, lenght=1000
    HostB->>HostA: SeqNum=0, ACK=4100, lenght=1000
```





Di fatto quindi si ottiene uno schema di sequenze numeriche e ACK di questo tipo (Calcolando la lunghezza dei pacchetti sempre di `1000byte`:

```mermaid
%% Example of sequence diagram
  sequenceDiagram
    HostA->>HostB: SeqNum=42, ACK=79, data=`C`
    HostB->>HostA: SeqNum=79, ACK=43, data=`C`
    HostA->>HostB: SeqNum=43, ACK=80
```

Da questo schema si riesce a capire come si ottengono i valori di sequenza dei pacchetti e i rispettivi ACK:

1. Si ha un ACK, un SeqNum e un payload (data)

   I numeri che andiamo a utilizzare li inseriamo nell’ACK e nel numero di riscontro. Il primo indica dove è posizionato il buffer (SeqNum), il secondo indica fino a che punto il ricevitore ha ottenuto i dati in maniera corretta (ACK). In fondo al pacchetto logicamente c’è il payload.

   Inoltre il dato di ACK indica anche se il ricevitore è in difficoltà, siccome indica qual’è ultimo pacchetto che ha ricevuto correttamente.

2. Il secondo pacchetto ha attivo il *flag: A*, ovvero si indica che questo pacchetto sta anche confermando un pacchetto precedente. 

   Questo significa che per convertire il pacchetto come risposta, si inverte ACK e SeqNum e poi si aggiunge `+1` all’ACK.

3. Il terzo pacchetto non vuole più pacchetti, allora riinvia lo stesso senza problemi



La gestione dei **segmenti out-of-order** dipende completamente dall’implementazione del ricevitore, siccome il suo comportamento non è in nessun modo definito da RFC. Chi trasmette logicamente non deve occuparsi di questa problematica.

Le differenze di gestione di questi pacchetti si avvertono solo nel caso di applicazione del protocollo di *selective repeat*, che sfrutta proprio la bufferizzazione dei pacchetti arrivati precedentemente fuori ordine per ottenere una rete risultante più veloce. Altre implementazioni invece possono optare per distruggere questi pacchetti e mandare ACK negativo come risposta, mantenendo quindi solo i pacchetti arrivati nell’ordine corretto.



### Stima del RTT (round trip time)

La stima di questo valore serve per avere un valore di **timeout** più corretto possibile (questa funzione è necessaria per il corretto funzionamento della rete, siccome permette il riinvio dei pacchetti in caso di perdita o mancata consegna), infatti se questo valore viene impostato senza questa stima:

* se valore troppo piccolo: ritrasmetto il pacchetto nonostante (forse) non sia stato perso il pacchetto, che potrebbe essere ancora in transito
* se il pacchetto è troppo grande: potrebbe andare bene (time out di partenza di solito sono un paio di secondi), ma attenzione che più si aumenta questo valore si aumenta il tempo di reazione per la perdita di pacchetti (quindi ci metto più tempo a ottenere il valore che voglio)

Questo valore può cambiare anche sulla stessa rete nella stessa condizione, anzi è un comportamento che il TCP si aspetta siccome utilizza anche questo valore come modo per limitare il traffico in uscita (controllo del flusso). Di base, questo tempo aumenta se la rete è congestionata o se l’host è molto lontano.

Il TCP utilizza i pacchetti di dati che sta inviando sulla rete come modello, senza inserire dei pacchetti ad hoc e quindi evitando di utilizzare banda per pacchetti “inutili”. 

Per sapere quanto impiega un pacchetto a essere ricevuto dal server (durata del RTT) si può dedurre attraverso la finestra impiegata dal client a ricevere il pacchetto di conferma da quando ha inviato il pacchetto inizialmente. Tutti i calcoli vegono fatti da lato client. Questo valore viene chiamato `SampleRTT`.

Chiaramente questo modello di misurazione non garantisce un valore preciso, ma permette comunque di avere un idea abbastanza corretta di un RTT normale. Inoltre se si cerca di capire l’RTT i pacchetti ritrasmessi vanno ignorati. Questo valore viene poi utilizzato come primo valore del RTT, e in seguito corretto per renderlo più vicino al reale possibile con la tecnica della **media mobile esponenziale ponderata**, che è rappresentata in questa formula:
$$
\text{EstimateRTT} = (1- \alpha) * \text{EstimatedRtt} + \alpha * \text{SampleRTT}
$$
Questo calcolo indica che la stima del RTT è la stima precedente (quindi la formula è *ricorsiva*) e quindi significa che faccio la media tra il campione di RTT creato adesso e il valore precedente, poi ci aggiungo $\alpha * SampleRTT$. Questo valore viene aggiunto per evitare delle oscillazioni molto grosse dovute alla media tra valori possibilmente molto variabili. In questo modo invece ottengo una media mobile (perchè ho una storico di tutta la connessione), ma i pacchetti precedenti hanno sempre meno peso.

Il valore tipico di $\alpha = 0,125$.

Un esempio di questo valore come cambia (e soprattutto come variano i valori nello storico è):


$$
EstimatedRTT(t)=(7/8)*EstimatedRTT(t-1)+1/8*SampleRTT(t) \\ EstimatedRTT[0]=SampleRTT[0] \\ 
EstimatedRTT[1]=7/8*EstimatedRTT[0]+1/8*SampleRTT[1]= =7/8*SampleRTT[0]+1/8*SampleRTT[1] 
\\ EstimatedRTT[2]=7/8*EstimatedRTT[1]+1/8*SampleRTT[2]= =7/8*(7/8*SampleRTT[0]+1/8*SampleRTT[1])+1/8*SampleRTT[2]
$$
Da questi calcoli si capisce come il peso dei valori rimanga sempre, ma di fatto più il campione diventa vecchio più il suo valore si approssima allo zero. Di solito si può stimare il valore alla media dei primi 15 valori, mentre gli altri nello storico sono stati resi approssimabili allo zero.

Chiaramente in tutti questi calcoli si ignorano i valori di ritrasmissione, in modo da non ottenere dei valori di RTT troppo bassi rispetto all’effettiva operabilità della rete.

Di base questo valore è molto variabile, quindi si cerca di ottenere la *deviazione del RTT*, ovvero di quanto il valore ottenuto si discosta dal valore atteso. Questo valore si ottiene con questa formula:

$$
DevRTT= (1 - \beta)*DevRTT + \beta*|SampleRTT - EstimatedRTT|
$$
Tipicamente $\beta = 0,25 = 1/4 = 2^{-2}$ (si utilizza questo valore perché essendo multiplo di due è molto comodo).

Per impostare l’**intervallo di timeout** quindi:
$$
TimeOutInterval = EstimatedRTT + 4* DevRTT
$$
Questa formula è *empirica* e con questi test la formula si è dimostrata efficace al massimo.

Questa formula è piuttosto interessante siccome in modo indiretto ottiene una serie di informazione sulla rete (in particolare al flusso di pacchetti e allo stato di congestione), senza aver bisogno di pacchetti aggiuntivi. In questo modo si permette di avere una serie di statistiche necessarie al funzionamento della rete senza però introdurre ritardi oppure diminuire banda (siccome si introducono pacchetti sulla rete).

Inoltre questo valore è interessante siccome si ottiene una buona media tra un valore che permette di reagire prontamente alla perdita di un pacchetto senza però introdurre troppo ritardo  di recupero della connessione nel caso di perdita di pacchetto. Di fatto in questo modo:

* se si ha un timeout troppo grande non si reagisce alle perdite di pacchetto abbastanza prontamente, quindi la rete risulterà rallentata
* se si ha un timeout troppo piccolo allora si tenderà a rimandare una serie di pacchetti più volte, senza aspettare la necessaria conferma, e di nuovo quindi ottenendo una rete risultante rallentata

![image-20211206211620058](image/image-20211206211620058.png)

Di base tutto il calcolo del timeout presentato è lo standard di calcolo per il TCP, mentre invece si può impostare un valore di base deciso dall’utente, ma si otterrà probabilmente una perdita di prestazione.



### Principi di trasferimento dei dati affidabile

 Il protocollo che permette il trasferimento affidabile di dati su una rete inaffidabile fornita dal protocollo IP è chiamato **TCP**, e ha diverse caratteristiche:

* pipeline dei segmenti

* ACK cumulativi

* TCP utilizza un solo timer di ritrasmissione

* Le ritrasmissioni sono avviate da: 

  * eventi di timeout (GBN e altre tecniche)

  * ACK duplicati

    Se si utilizzano ACK cumulativi, ricevere un ACK duplicato è come ricevere un NAK. Questo di fatto fa assumere al protocollo TCP che il pacchetto non sia stato correttamente consegnato e quindi il mandante cerca di rimandarlo prima di ottenere un timeout.

* inizialmente utilizziamo un mittente TCP semplificato: 

  * ignoriamo gli ACK duplicati
  * ignoriamo il controllo di flusso e il controllo di congestione (queste tecniche servono per non creare blocchi e scarso funzionamento dal lato server)

* si introduce il controllo di flusso e di congestione. In questo modo si cerca di evitare di distruggere o rallentare la rete con l’immissione di troppi pacchetti.

Di fatto il TCP nelle prime versioni aveva solo il controllo di flusso, poi nelle versioni successive è stato introdotto anche il controllo della congestione. Di base in queste prime sezioni si stanno prendendo in considerazione solo le prime specifiche del protocollo.

#### TCP: eventi del mittente

Il caso più semplice è quando un applicazione chiede a TCP di creare dati. In questo caso il TCP considera la stringa di dati che ottiene e crea dei segmenti in sequenza (è il valore del primo byte del segmento nel flusso di byte). Una volta che il pacchetto viene mandato allora si avvia il timeout (se non è già in funzione).

In questo caso l’operazione di invio dei pacchetti viene interrotta solamente se si ottiene un `TimeOutInterval`, che ha l’effetto di resettare il timer e ritrasmettere il segmento che ha causato l’evento. 

Questo evento non rimanda tutti i segmenti consecutivi al pacchetto che ha ottenuto il timeout, perché se è successo che solo quel pacchetto è andato perso, allora rimandarli tutti non è efficiente.



Di base quindi l’algoritmo che si ottiene rispetto a questo modello è così formulato (pseudocodice):

```c++
while(true){
    switch(evento){
            evento: "i dati dell'applicazione crano il segmento TCP con numero di sequenza NextSeqNum",
            	if( "timer not working yet"){
                	startTimer();
            	}
            	passa il segmento al livello inferiore (IP);
            	NextSeqNum = NextSeqNum+(lunghezza dati);
        	
            evento: "timeout del timer",
            	"rtrasmetti il segmento non ancora riscontrato con il più piccolo numero di sequenza"
                startTimer();
            
            evento: "ACK ricevuto, campo ACK pari a y",
            	if(y > SendBase){
                    SendBase = y;
                    if("esistono ancora segmenti non riscontrati"){
                        startTimer();
                    }
                }
    }
}// end loop

/*	In questo esempio:
	SendBase -1 : ultimo byte comulatamente riscontrato
*/  
```



#### TCP: Scenari di connessione

<img src="image/image-20211206215827582.png" alt="image-20211206215827582" style="zoom:50%;" />

<img src="image/image-20211206215648099.png" alt="image-20211206215648099" style="zoom:50%;" />

In questi scenari si può vedere come è possibile l’applicazione del protocollo TCP nel caso di perdita del riscontro, nel caso di timeout prematuro, nel caso di riscontro cumulativo, e nel caso non si ottenga nulla dall’altra parte.

Di fatto questi schemi indicano come la funzione timeout permetta di risolvere principalmente tutti i problemi che si possono riscontrare durante una connessione.

Inoltre tutti questi controlli vengono fatti dalla parte del client, di fatto quindi si ottiene un modello della gestione completamente staccato dal server che punta a ridurre la banda necessaria per il trasferimento dei pacchetti e l’accumulo dei pacchetti nella rete.

#### TCP: generazione di ACK

Questi scenari sono descritti nel protocollo dal`RFC 1122` e dal `RFC 2581`.

Queste specifiche permettono di ridurre il numero di ACK emessi per evitare di non avere un’accumulo di pacchetti sulla rete, ma allo stesso modo di non perdere l’affidabilità della rete.

In questa sezione del protocollo vengono definiti una serie di eventi dalla parte del destinatario che corrispondono a un azione intrapresa:

* Arrivo di un segmento con **numero di sequenza atteso**. Tutti i dati fino al numero di sequenza ricevuto sono stati riscontrati. 

  * Si suggerisce l’**ACK ritardato**: aspettare 500ms affinché arrivi il prossimo segmento. Se il segmento non arriva, allora si invia un ACK per il pacchetto riscontrato.

    Questo significa che otterrò un ACK bufferizzato per 500ms, ma se si ottiene un secondo ACK allora si può mandare un unico ACK comulativo, ottenendo una minor occupazione della banda.

    Non si può tenere l’ACK più di questo tempo nel buffer o si otterrebbe un notevole errore nel calcolo del RTT del client, che risulterebbe in una rete più lenta.

* Si ottiene un pacchetto con numero di sequenza atteso e un altro ACK è in attesa di essere mandato.

  * Si crea un pacchetto cumulativo che indichi l’avvenuta ricezione di entrambi i pacchetti.ù

* Arrivo **non ordinato** con un numero di sequenza superiore a quello atteso. Viene rilevato un buco.

  * Invia un ACK duplicato indicando il valore del pacchetto atteso.

* Arrivo di un pacchetto che colma parzialmente o completamente il buco

  * invia immediatamente un ACK, ammesso che il frammento inizi dall’estremità inferiore del buco



C’è un algoritmo che permette di avere una ritrasmissione rapida dei pacchetti con ACK sbagliato. Questo pseudocodice non fa altro che aggiungere un evento di questo tipo:

```c++
evento: "ACK ripetuto, con valore del campo ACK pari a y"
    if(y > SendBase){
        SendBase = y;
        if("esistono attualmente segmenti non ancora riscontrati"){
            startTimer();
        }
    } else { // ACK duplicato per un segmento già riscontrato
        "incrementa il numero di ACK duplicati ricevuti per y"
            if("numero di ACK duplicati ricevuti per y=3"){ // ritrasmissione in caso di 3 ACK duplicati, ottenendo quindi una ritrasmissione rapida
                "rispedisci il numero di sequenza y"
            }
    }
```



#### TCP: controllo di flusso

Il controllo di flusso nella rete serve per evitare che il buffer del ricevitore non vada mai in overflow per colpa dei pacchetti dei mittente.

Il concetto di base di questo tipo di controllo è che il buffer della socket fornita al livello applicazione non sia mai pieno, e che quindi possa continuamente accettare pacchetti in arrivo. Di fatto quindi serve che le applicazioni che devono leggere questi dati li elaborino più velocemente di quanti questi arrivino, in modo che non si trovi mai un accumulazione esagerata (situazione non funzionale, siccome si perdono dati all’interno della pila protocollare, che è orribile, siccome di solito si da per scontato che si perdano pacchetti solo sulla rete, mai sugli host).

Per ottenere questo controllo si utilizza il campo del pacchetto TCP chiamato *receive windows* (se vuoi vedere la sezione clicca [qui](#Struttura dei pacchetti TCP)). Di base quindi il ricevitore dice al mittente il valore di dati che ancora può ricevere, permettendo quindi di non superare mai la soglia critica. (Ancora una volta quindi si utilizza un valore definito nell’header per permettere una comunicazione tra ricevente e mittente).

Il buffer di questa socket è creata dal sistema operativo e quindi ha uno specifico spazio a disposizione. Il calcolo di questo valore quindi è semplicemente la differenza tra il buffer TCP e la finestra in attesa.

Di base quindi questo meccanismo è funzionale siccome indica al mittente quanto spazio può ancora utilizzare.

Il ricevitore può indicare anche valore zero di questo pacchetto e in questo caso il mittente:

* spegne il pipelining
* smette di mandare pacchetti sulla rete indirizzati a quella socket
* inizia a aspettare il primo ACK (per forza duplicato) che indicherà una finestra di socket di nuovo libera.

Questa protezione è necessaria siccome sulla rete sono presenti tutti i tipi di host disponibili, e quindi non si ha il controllo o la percezione di quanto la socket ricevente sia grande. In questo modo il TCP automatizza il controllo assicurando ancora una volta un corretto scambio di pacchetti.

Questo modello è molto efficace, ma di fatto non è privo di errori. Infatti si ha una latenza dovuta allo scambio di pacchetti, ma in qualsiasi caso se il ricevitore perde dei pacchetti allora si ottiene un effetto risultante come quello di perdita di pacchetti sulla rete (ovvero non si ottiene un ACK di conferma).

Di base questo protocollo ha la capacità di stimare la dimensione del buffer in qualsiasi momento con un semplice calcolo, utilizzando solo la dimensione della finestra disponibile ottenuta nel pacchetto di risposta:
$$
RcvWindow = RcvBuffer - [LastbyteRcvd - LastByteRead]
$$


#### TCP: gestione della connessione (apertura)

La gestione della connessione nel TCP è la parte principale dell’affidabilità della connessione. Infatti i pacchetti che vengono persi è sempre un lato negativo, ma ci sono modalità per limitarne gli effetti e ridurre l’impatto che questo ha sulla connessione. Il fatto principale della connessione TCP invece è proprio che mittente e ricevente si accordino su dei parametri utilizzabili poi per implementare i meccanismi di affidabilità. 

Tutte queste azioni e variabili vengono definite nella primissima parte della connessione, attraverso l’**handshake a tre vie** (“una stretta di mano con tre messaggi”). 

Il **3-way handshake** viene svolto in questo modo:

```mermaid
%% Sequence diagram	
	sequenceDiagram
Client->>Server: SYN;
Server->>Client: SYNACK;
Client->>Server: ACK;
```

* *primo passo*:
  * Il client inizializza la connessione, manda un messaggio `SYN` al server
  * specifica il numero di sequenza iniziale (il numero da cui inizia a contare i pacchetti)
  * ha attivo il bit di flag `SYN`.
* *secondo passo*:
  * Il server riceve il pacchetto del client, e risponde con un segmento `SYNACK`
  * specifica il numero di sequenza iniziale che utilizzerà il server
  * ha attivi due bit di flag: `SYN` e `ACK`
* *terzo passo*:
  * il client riceve il pacchetto, risponde con un `ACK`
  * specifica la conferma della ricezione del numero di sequenza in utilizzo dal server
  * ha attivo solo il bit di flag `ACK`.
  * questo pacchetto, per aumentare l’efficienza della connessione (siccome dopo 3 pacchetti non è ancora stato mandato nessun dato) può contenere anche un payload di dati.

Questa azione è stata implementata con tre messaggi perché c’è la possibilità di avere un apertura di connessione non corretta se si utilizzassero solo due messaggi. (Il problema sta nella ricezione del pacchetto `SYNACK` che potrebbe portare alla conferma di un numero di ricezione errato del SeqNum del server da parte del client).

Un esempio di handshake a tre vie può essere:

```mermaid
%% Example of sequence diagram
  sequenceDiagram
    Client->>Server: Syn 3546
    Server->>Client: Syn 7654, ACK 3547
    Client->>Server: ACK 7655
```

Da notare che il client conta da `3546` e il server conta da un altro numero, di solito non collegati e quindi fuori da ogni successione. Il valore di *SeqNum* del client è confermato dal server attraverso il messaggio `ACK 2547`.

Il motivo per cui questo modo per impostare il setup della connessione si ha la necessità di avere 3 messaggi viene dimostrato con questo esempio:

```mermaid
%% Example of sequence diagram
  sequenceDiagram
  	Client->>Server: SYN 9999
    Client->>Server: Syn 3546
    Server->>Client: Syn y, ACK 10000
    Client->>Server: ACK y+1, RST
    Server->>Client: SYN 7654, ACK 3547
    Client->>Server: ACK 7655
```

* nel passato il client ha mandato un pacchetto `SYN` numerato `9999` che viene ricevuto ora dal server
* il client riceve anche il pacchetto `SYN` dallo stesso host numerato diversamente.
* A questo punto il client risponde con il suo SeqNum, ma aggiunge anche il bit flag `RST` ovvero il bit che indica il reset della connessione. Questo avviene perché il client ha notato che il server non sta rispondendo al messaggio corretto.
* Il pacchetto `RST` indica al server che sto rifiutando il pacchetto `SYN y, ACK 10000`. 
* Dopo il messaggio di reset inviato dal client il server riinvia il secondo messaggio, aspettando risposta positiva

Di fatto il pacchetto che contiene il flag `RST` annulla tutti i pacchetti che non seguono l’ultima richiesta di connessione, senza valutarne il contenuto.

Un pacchetto si può ottenere anche a metà di una connessione, ma di solito è associato a un problema non prevedibile con la connessione e viene solitamente risolto con un annullamento della connessione e un nuovo handshake.



#### TCP: gestione della connessione (chiusura)

Di solito il client è il responsabile della chiusura della connessione, come l’apertura. Di base questo modello è molto simile all’apertura della connessione.

La connessione non essendo affidabile può anche saltare in qualsiasi momento. In questo caso la disconnessione avviene in modo non controllato e si lasciano tutti i pacchetti pendenti come persi. 

Nel caso normale invece che si ha finito di aver bisogno di una connessione e si vuole procedere a una disconnessione si può operare una serie di azioni che permettono di essere sicuri di chiudere la comunicazione dopo che tutti i pacchetti sono transitati.

```mermaid
sequenceDiagram
Client->>Server: FIN
Server->>Client: ACK
Server-->Client: chiusura della connessione del server
Server->>Client: FIN
Server-->Client: attesa temporizzata
Client->>Server: ACK
```

Di fatto quindi le azioni possono essere così descritte:

* *primo passo*: il client invia un pacchetto `FIN` al server, indicando che si ha intenzione di chiudere la connessione.
* *secondo passo*: il server conferma la ricezione del pacchetto del client con un `ACK`. Una volta che il server è pronto a chiudere la connessione allora invia a sua volta un pacchetto `FIN`
* *terzo passo*: il client riceve un `FIN` dal server quando questo ha terminato la connessione
  * inizia un attesa temporizzata che serve a recuperare tutti gli eventuali segmenti che possono essere ancora in traffico sulla rete. Il client risponde `ACK` a tutti i `FIN` che riceve.
* *quarto passo*: il server riceve un `ACK` dal client e chiude la connessione



Un riassunto attraverso diagrammi a stati per l’apertura la chiusura può essere rappresentata così:

![image-20211207153608391](image/image-20211207153608391.png)



## 3.6 Principi del controllo di congestione

La rete ha da sempre il problema della congestione siccome è creata da molte tecnologie e capacità diverse. Questo significa che si possono creare degli accumuli di pacchetti in passaggio per una determinata zona della rete. Nel caso gli host o i router non riescano più a contenere i pacchetti in arrivo allora può avvenire una situazione di congestione critica, ovvero possibilità di aver perdita di pacchetti e tempo di trasmissione aumentato notevolmente.

Questa situazione può essere risolta in due modi: la rete smette di accettare pacchetti oppure gli host che immettono pacchetti “controllano” lo stato di salute della rete prima di iniziare una comunicazione. Come più volte visto l’approccio applicato è quello di risolvere i problemi ai bordi, quindi si utilizza un controllo da parte degli host attraverso il protocollo TCP.

Le prime versioni del TCP non aveva controllo di congestione, ma solo di flusso (che è un’altra cosa).

Solitamente una situazione di congestione critica viene anticipata da un periodo di tempo in cui:

*  pacchetti vengono persi (per l’overflow dello spazio a disposizione sugli host)
* lunghi ritardi nella trasmissione

Spesso i router preferisce avere sempre la possibilità di poter scegliere di tenere o buttare via il pacchetto. Per ottenere questa possibilità fa in modo di avere sempre il buffer abbastanza vuoto da poter accettare sempre i pacchetti in arrivo, ovvero iniziando a eliminare i pacchetti prematuramente per evitare che al bisogno ci sia necessità di spazio. Spesso inoltre si possono avere delle regole che stabiliscono che in determinate situazioni alcuni tipi di pacchetti vengano eliminati di default.

Il funzionamento del controllo di congestione si basa sul concetto che i pacchetti vengano buttati via. Questo può avvenire sia per una congestione critica, sia per triggerare il controllo sulle congestioni degli host quando il nodo decide di aver raggiunto una quantità di pacchetti nel buffer abbastanza consistente per iniziare a “proteggersi”.

Quando il nodo si accorge di iniziare a perdere dei pacchetti l’host attiva il controllo di congestione e quindi riduce i pacchetti inseriti nella rete nel tentativo di migliorare la situazione di congestione.

Il problema della stabilità e della congestione della rete è uno dei 10 grandi problemi della rete, e viene tuttora studiato. Di solito l’approccio più utilizzato è quello di iniziare a “sfiatare” i pacchetti prima che si raggiunga la soglia critica di congestione, in modo da mantenere sempre la rete operativa sopra un certo livello.

Inoltre il controllo di congestione è la funzione che da la forma al traffico internet, infatti i pacchetti verranno emessi conseguentemente a questo controllo, che regola non solo il numero di pacchetti emesso, ma anche la finestra di controllo del protocollo. Questo flusso non è consistente, ma è molto spezzato, inoltre non cerca neanche mai di ottenere un livello di finestra costante siccome la rete è molto variabile.

Di fatto questi comportamenti si possono notare sulla rete soprattutto quando ci sono degli down/up-load di file molto grandi, che hanno appunto un comportamento di questo tipo.

Inoltre va notato che il mittente è completamente responsabile della limitazione sulla connessione, attraverso questa operazione:
$$
LastByteSent - LastByteAcked \le CongesionWindow
$$
E di fatto quindi si ottiene una frequenza d’invio dei pacchetti di approssimativamente:
$$
SendFreq = \frac{CongestionWindow}{RTT} byte/sec
$$
Un esempio di utilizzo di questo valore può essere:

<img src="image/image-20211208101828087.png" alt="image-20211208101828087" style="zoom:50%;" />

Per forza di cose da questa formula si capisce diminuire il $RTT$ permette di aiutare il TCP a funzionare meglio. Da questo si capisce anche come le CDN siano molto utili e permettano un sostanziale aumento delle performance. 

Inoltre notare che la funzione che indica la finestra di congestione è dinamica rispetto al valore della congestione percepita dalla rete.



### Cause e costi della congestione DA FARE

Si possono avere degli approcci alla risoluzione della congestione diversi, ad esempio:

* Il throughtput non può mai superare la capacità

  <img src="image/image-20211207161427857.png" alt="image-20211207161427857" style="zoom:50%;" />

  In questo caso si ha una quantità di pacchetti sempre in aumento linearmente. In questo modello il tempo d

* il ritardo aumenta all’aumentare della congestione (si ottiene un grafico con un asindoto, non si arriverà mai al valore di arrivo). Il ritardo di consegna in questo caso è che il tempo di aspetto per il pacchetto è infinitamente grande (nonostante sia un asindoto nella visione teorica, ma prima o poi arriva lo stesso o viene droppato)

* perdite e ritrasmissioni riducono il trhoughtput. Questo significa che si utilizza tutta la capacità ma alcuni pacchetti vengono mandati più volte, quindi si ottiene un valore di dati scambiati minore.

Nella realtà si ottiene un grafico diverso (grafico 4 sulla slide di cause e costi della congestione).

Nella realtà si sommano una serie di effetti e in più si aggiunge il concetto che superata una certa soglia non i recupera più.

C’è un livello che è un filo prima del gomito in cui si arriva al limite della congestione, a questo punto si ottiene il valore massimo del throughput, ma se si continua a questo livello si potrebbe congestionare la rete, quindi si ottiene una congestione. 

I router non riescono più a gestire il traffico, ovvero non possono più scegliere di accettare un pacchetto rispetto al rifiuto di un altro. Questo è lo stesso effetto Un pacchetto si può ottenere anche a metà di una connessione, ma di solito è associato a un problema non prevedibile con la connessione e viene solitamente risolto con un annullamento della connessione e un nuovo handshake.

Se si ottiene un pacchetto del genere a metà della connessione di solito è avvenuto un problema non prevedibile e viene riannullata la connessione e si rifà di nuovo l’handshake.che avviene nell’idraulica. Si ottiene che si può buttare più acqua in un certo tubo, se si butta troppa acqua il tubo non riesce più a svuotare la vasca.

Questo è uno dei motivi per cui la rete IP è complicata e sensibile.

Ci sono due cose da dire, una è che chi costruisce un isola della rete deve dimensionarla nel modo giusto, e se il traffico tipo è modellizzato correttamente, allora questa opzione non avviene facilmente (che si arrivi al gomito). di soltio è molto più facile che si ottenga uno strozzamento a un certo punto nel sending dei pacchetti.

Questo valori sono più pensate a una rete IP pura, infatti in questo caso questo grafico è quello che meglio sintetizza il comportamento di questa rete. 



## 3.7 Controllo di congestione TCP

A questo problema si cerca di risolvere in due modi: controllare la congestione dall’interno della rete o distribuire il controllo ai lati della rete. Come al solito si è scelto di portare la complessità ai lati della rete. Questi due modelli sono:

* **controllo di congestione punto-punto**

  è un modello di controllo di congestione che cerca di capire tutte le informazioni necessarie a evitare la congestione senza nessun aiuto dalla rete. Di fatto quindi tutti gli host e i nodi sono autonomi e da soli riescono a capire in che stato di congestione si trova la rete e di conseguenza a gestire l’immissione dei pacchetti sulla rete. Questo è il metodo applicato dal protocollo TCP.

  Il funzionamento di questo modello è molto facile: utilizza una valutazione su tempi di risposta e sulle perdite per valutare lo stato della rete.

  

* **controllo di congestione assistito dalla rete**

  Questo meccanismo è stato studiato perché teoricamente possibile. Di fatto non è mai stato applicato perché serviva che tutti i nodi della rete fossero predisposti a avere delle funzionalità di collaborazione tra di loro. Di fatto l’implementazione di questo metodo è stata teorizzata troppo tardi e troppo hardware era già in circolazione per permettere a questo modello di essere applicato su larga scala.

  Il funzionamento di questo processo di controllo è attraverso dei flag inseriti nei pacchetti modificabili dai nodi attraverso cui passano. Questi flag possono essere di più tipi (`SNA`, `DECbit`, `TCP/IP ECN` e `ATM`). Di fatto però questo metodo infrange la stratificazione della pila protocollare, siccome di fatto il pacchetto viene modificato.

  Ma di fatto questo modello non è funzionale per due motivi in particolare:

  * i router sono stateless: non dovrebbero ricordare i pacchetti che sono passati.
  * Questo principio necessita di sapere che flusso va avvisato, e per questo motivo avvisa solo il flusso che sta creando problemi

Tutte funzioni responsabili del controllo congestione si basano su delle informazioni ottenute dalla rete in modo indiretto, questi possono essere:

* un *evento di timeout*: scatta un timeout su un pacchetto. Questo evento viene sempre preso dal client come indicazione di una congestione e agisce subito per limitarla.
* *ricezione di 3 ACK duplicati*: il client in questo modo capisce che c’è stata una perdita, e di fatto ritrasmette dall’ultimo pacchetto confermato. Questo evento è un warning per il protocollo, infatti vengono effettuate operazioni anche in questo caso.

Ci sono **tre meccanismi** utilizzati per ridurre la finestra di congestione:

* partenza lenta
* AIMD
* reazione agli eventi di timeout

(tutti definiti più avanti)

La versione utilizzata di TCP è **newReno**, che è il riferimento di standard e viene utilizzato come limite minimo delle performance. Di fatto ad oggi il migliore è stato presentato da Google chiamato **BBR**. Questo modello è stato ottimizzato utilizzando tantissimi dati elaborati sulle performance di rete.

Tutto il controllo di trasmissione viene ottenuto con il controllo delle finestre di invio dei messaggi. Quindi questa finestra viene utilizzata nello stesso momento dal controllo di congestione e dal controllo di flusso. Di fatto prende il valore minimo tra i due ottenuti dai due controlli.



### Partenza lenta

Quando la connessione viene effettuata si ottiene un valore di dimensione massima del pacchetto (**MSS, maximum segment size**), solitamente $1500 byte$. Con questo valore si ottiene il primo frammento che viene mandato, e di fatto si può mandare questa quantità di dati in un RTT. 

Questa prima parte della connessione è caratterizzata da un frequenza iniziale di invio dei pacchetti molto ridotta e di fatto la finestra è molto piccola. Ogni RTT però il valore della finestra viene raddoppiato, ottenendo una funzione esponenziale rispetto al numero di pacchetti mandati sulla rete.

Questo modello di aumento pacchetti però è particolarmente aggressivo e quindi ha bisogno di essere fermato appena si verifica un evento particolare che provoca il raggiungimento della fase di affinamento.

Questo cambio può essere causato da due motivi principalmente, che hanno degli effetti diversi:

* Dopo 3 ACK duplicati:

  * la finestra di `CongWin` viene dimezzata
  * da ora in poi la finestra viene fatta aumentare linearmente (metodo di regime, che punta a continuare a oscillare sempre intorno a questo valore)

* Dopo un evento di timeout:

  * la finestra di `CongWin` viene impostato a 1 MSS

  * la finestra viene nuovamente aumentata in modo esponenziale.

    Questo evento però fa anche in modo di avere un valore di soglia impostato alla metà della frequenza raggiunta prima di un evento di timeout. In questo modo quando si ottiene nuovamente questo valore si passa alla fase di affinamento, prima di rischiare di stressare la rete ulteriormente. 

Queste azioni vengono diversificate siccome un evento di 3 ACK duplicati indica che la capacità massima della rete è stata ottenuta, mentre invece un timeout è considerato un evento grave di congestione. Infatti questo evento indica che il client non ha più ricevuto nulla, indicando di fatto che la rete è completamente bloccata. Per questo motivo si ritorna alla tecnica di partenza lenta. 

<img src="image/image-20211208111316351.png" alt="image-20211208111316351" style="zoom:67%;" />

In questo grafico si ottiene il valore di finestra massimo ottenuto nelle varie fasi della connessione. Inoltre si vede anche il confronto con due protocolli TCP diversi: *Tahoe* e *Reno*. Il secondo più avanzato implementa la crescita lineare dopo il valore di soglia, come indicato nelle specifiche scritte qui sopra.

Nelle connessioni brevi (chiamate anche *mouse connection*) non si riesce a terminare questa fase, e di fatto si ottiene un comportamento molto aggressivo per tutta la durata della connessione, mantenendo sempre il comportamento a crescita esponenziale.

Normalmente si potrebbe pensare che in questo caso sarebbe meglio avere un punto di partenza per il primo pacchetto più alto, in modo da avere subito tutti i pacchetti da mandare. Questo nella realtà poi non funziona, siccome con un comportamento del genere questo flusso si riesce a “infilare” in mezzo alle altre connessioni, sfruttando tutti i buchi lasciati dalle altre connessioni e di fatto ottenendo una comunicazione risultante più veloce.

Nelle connessioni più lunghe invece (chiamate anche *elephant connection*) si termina la fase di affinamento tranquillamente e si procede nelle fasi successive, ottenendo un andamento diverso nel corso del tempo.

Il grafico inoltre rappresenta il caso teorico in cui si valuta un funzionamento della rete completamente corretto sempre e con sempre dati disponibili a essere mandati.

Un riassunto di questi comportamenti e eventi è rappresentata in questa tabella:

![image-20211208112615969](image/image-20211208112615969.png)

Invece il funzionamento di questo modello è descritto con questo modello a stati:

![image-20211208113246833](image/image-20211208113246833.png)



### AIMD: incremento additivo e decremento moltiplicativo

Questa sezione della connessione viene ottenuta dopo la sezione di affinamento e viene applicata solo durante delle connessioni lunghe. Lo scopo di questo comportamento è quello di avere un valore di frequenza di pacchetti inviati sempre ondeggiante sul limite di capacità della connessione.

La soluzione per risolvere questo problema è l’AIMD, ovvero **additive increase multiplicative decrease**. Questo modello viene applicato in una connessione prolungata, non viene applicata subito.

<img src="image/image-20211207183206710.png" alt="image-20211207183206710" style="zoom:67%;" />

Idealmente il concetto che è dietro questa applicazione è che vogliamo trovare il throughput esattamente uguale alla nostra capacità di trasmissione per ottenere le migliori performance. Di fatto quindi si ottiene una spezzata che ha dei valori all’interno di un range vicino a questo valore (che non conosciamo perché la rete non ce lo fornisce). 

Questo modello fa in modo di:

* crescere linearmente: quando non si hanno perdite di pacchetti e non si hanno ritardi (ovvero quando la connessione viene rilevata come in buone condizioni)

* decresce dimezzandosi: c’è stato un evento che ha fatto capire al client di aver superato il valore di capacità. Allora a questo punto per prevenire la congestione viene dimezzata la finestra di invio. 

  La finestra viene brutalmente dimezzata perché in questo modo si ha la possibilità che i buffer dei router si svuotino o che comunque abbiano alcuni momenti per recuperare un pochino e rimettersi in pari sulla quantità di pacchetti in entrata (infatti se si è avvertita una perdita, vuol dire che la congestione è già in corso).

Questo modello permette quindi di fare **probing** della rete: fornisce alla rete una quantità di pacchetti sempre maggiore, annotandosi se riesce questa riesce a accettarli. Appena si accorge che la rete è satura allora dimezza completamente la quantità di pacchetti inoltrati. In questo modo si lascia alla rete il tempo di ricominciare a scorrere e per questo motivo il grafico prende questa forma a dente di sega.

Questa modalità è molto utile anche perché la capacità non è mai costante, e in questo modo quindi si punta a ottenere il massimo throughput rispetto alla capacità disponibile.

Questo modello è anche il primo presentato per risolvere questo problema. Di fatto ad oggi si utilizza un protocollo più avanzato sviluppato da Google che cerca attivamente di prevenire la congestione.



### TCP: throughput

Siccome la frequenza dei pacchetti immessi sulla rete è completamente dipendente dal protocollo TCP. Questo significa che il valore di throughput è completamente definibile da TCP stesso.

I protocolli TCP, eccetto le finestre di apertura della connessione, tende a mantenere un valore medio massimizzato, in modo da ottenere il massimo numero di pacchetti inviati sulla rete senza disturbare la rete. 

Di fatto se prendiamo $W$ la dimensione della finestra quando si verifica una perdita, allora il throughput è quantificabile come:
$$
\frac{W}{RTT}
$$
Subito dopo la perdita invece il throughput, dipendente dalla finestra dimezzata (quindi $W/2$), prende il valore di:
$$
\frac{W}{2RTT}
$$
Questo non è il modello migliore, infatti per capire il limite della rete si ottiene la finestra $W$ quando si sta già parzialmente creando una congestione. Ci sono metodi che puntano a stressare meno possibile la rete, come il nuovo standard di TCP presentato da Google, ottenendo quindi una prestazione migliore e più stabile. 

Chiaramente questa modifica non cambia radicalmente le performance di una semplice azienda, invece possono cambiare notevolmente in applicazioni come streaming e video consuming, infatti applicando il nuovo protocollo si ottengono meno perdite di pacchetti, siccome ci si ferma prima di ottenere una congestione.

Il throughput medio è $0,75 W/RTT$, generalmente.



### TCP: il suo futuro

Si vuole cercare di massimizzare i pacchetti mandati su una rete da $10Gbps$, con dimensione di segmenti da $1500byte$ e  RTT di $100ms$.

La finestra per utilizzare tutta questa capacità di rete è $W = 83.333$ di segmenti in transito.

La perdita di pacchetti nella realtà invece modifica notevolmente quando si applica in funzione dello smarrimento dei pacchetti:
$$
L = \frac{1.22*MSS}{RTT\sqrt{L}} = 2 \times 10^{-10}
$$
calcolato con una serie di modelli avanzati e calcoli specifici e, in alcuni casi, empirico-statistico. Questo valore è la probabilità di perdita di un pacchetto necessario per ottenere il throughput abbastanza grande da saturare la connessione con i valori precedentemente definiti. Questa probabilità di errore è infinitesima, ma non si può ottenere neanche sulla rete migliore possibile (la fibra ha la proprietà di errore intorno a $10^{-8}/10^{-9}$). 

Da questi calcoli si capisce che il problema di queste versioni di TCP ha un tempo di reazione alla perdita troppo grande e quindi è necessario studiare **nuove versioni di TCP**, che siano pensate per migliorare la connessione, studiando il caso in cui avvengono perdite nella connessione.

Lo studio si basa principalmente su questi due grafici che rappresentano il funzionamento della connessione (ideale):

<img src="image/image-20211208120059382.png" alt="image-20211208120059382" style="zoom:50%;" />

Nel grafico vediamo 3 fasi rappresentate al variare tra il numero di pacchetti immessi sulla rete e il throughput:

* *nella parte blu*:
  * il RTT è costante perché non ho congestione e la rete è in buono stato, e di fatto tutti i pacchetti vengono consegnati nello stesso tempo.
  * il throughput invece prende la forma lineare siccome è l’inverso del RTT
* *nella parte gialla*:
  * il throughput non riesce più a aumentare siccome si raggiunge la capacità massima della connessione
  * il RTT aumenta, siccome si sta appesantendo la rete. infatti la banda è completamente occupata e si otterrà un accodamento sempre maggiore nei buffer (i pacchetti non vengono buttati via, ma la componente di tempo di accodamento diventa non indifferente)
* *nella parte rossa*: ottengo un blocco della rete, siccome ho raggiunto la capacità massima e anche il valore massimo dei buffer dei router, quindi non riesco più a accumulare pacchetti.
  * il throughput rimane controllato, siccome ero già al valore massimo
  * il RTT potenzialmente è molto maggiore siccome si hanno molto pacchetti persi.

Il protocollo **TCP NewReno** trova la sua applicazione tra il settore giallo e quello rosso, siccome sollecita al limite della perdita e a quel punto viene dimezzata la finestra. Questo modello si chiama **lost-based congestion control**, ovvero si ottiene una modifica della finestra dovuta all’evento di perdita creato apposta per ottenere il valore di congestione della rete.

Il protocollo TCP proposto da Google (chiamato **BBR**) si impone di lavorare tra la sezione blu e gialla, ovvero nel momento di massimo funzionamento della rete, siccome si ha il minor RTT possibile e il maggiore throughput. L’unico modo per capire quando questa soglia viene superata però è il controllo sul RTT, valore che inizia a aumentare proprio dopo la sezione blu. Il protocollo inizia a diminuire la finestra quando si accorge che il RTT sta iniziando a aumentare mentre il valore di throughput rimane costante. 

In passato questa tecnica era stata già presentata e studiata, ma di fatto tutti gli altri protocolli, essendo più aggressivi, facevano in modo di “fagocitare” le connessioni in uscita da questo nuovo approccio. Infatti solitamente sulla rete se due protocolli competono per la banda, allora vince quella che diventa più aggressiva. Questo concetto crea il problema di applicazione di questo protocollo, che non essendo applicabile su ogni host, allora tutte le volte che si applica questo modello si ottiene di fatto una connessione più lenta. Google per la prima volta ha ottenuto un protocollo che lavora in questa zona tra blu e giallo ma nello stesso modo non si fa distruggere dagli altri protocolli e a sua volta non è troppo superiormente avanzato da essere lui il protocollo che prevale. Tutto ciò significa che Google ha presentato un protocollo che funziona meglio di TCP NewReno, che tenta di evitare completamente la perdita di pacchetti con lo scopo di avere una connessione più reattiva e che allo stesso modo riesce a convivere con tutte le versioni di TCP esistenti.



La tendenza di oggi su questi protocolli è quello di abbandonare il TCP. Infatti i prerequisiti di throughput della rete fa in modo che sia necessario un controllo diverso della frequenza di pacchetti inoltrati. La tendenza è quindi quella di abbandonare TCP e iniziare a utilizzare UDP con delle accortezze. 



### TCP: il passato

Prima che Google presentasse il suo protocollo evidentemente superiore, c’erano due versioni utilizzate maggiormente.

Il primo è **TCP Cubic** che è di default in Linux. Ha un comportamento di questo tipo:

<img src="image/image-20211208122128394.png" alt="image-20211208122128394" style="zoom:50%;" />

Su Windows invece è presente un approccio diverso (**TCP Compund**), che utilizza dei moltiplicatori quando si trova una finestra migliore di trasmissione. Di fatto questo protocollo non è completamente studiabile, siccome è uno standard chiuso, ma ha un comportamento di questo tipo:

<img src="image/image-20211208122249674.png" alt="image-20211208122249674" style="zoom:50%;" />

Di fatto questi protocolli poi vengono chiaramente battuti come efficienza dal protocollo di Google, qui sotto in confronto:

<img src="image/image-20211208122340837.png" alt="image-20211208122340837" style="zoom:50%;" />

Chiaramente il grafico è idealizzato e server a rappresentare la capacità end-to-end, e di fatto si può capire che:

* il BBR (verde) tende a stabilizzarsi *esattamente* sul limite della capacità. Questo significa che tende sempre a stare sul limite di dimensione dei buffer senza stressarli.

  Di fatto dal grafico sembra che il protocollo si stabilizzi sempre su quel valore, siccome la capacità non sarai mai così stabile. In base a questo valore allora si ottiene un grafico leggermente diverso, in cui la parte di *platue* coincide alla capacità massima.

  Inoltre si noti che ci sono delle sezioni che comunque diminuiscono, dando lo spazio ai buffer di svuotarsi.

  Per rendere questo protocollo competitivo rispetto agli altri inoltre hanno aggiunto alcuni trucchetti molto sviluppati, non trattati in questo corso.

* il CUBIC (rosso) tende sempre a stare molto sopra il limite di capacità. Questo significa che tende sempre a dare un certo stress alla rete, lavorando sempre al limite, nonostante il protocollo funzionasse molto bene.

* il COMPOUND invece ha un comportamento leggermente diverso, che lavora sulla linea del limite e ha crescita lineare finché non arriva al limite della connessione, portando quindi sempre dello stess alla rete.



### TCP: Equità

Il problema del TCP di oggi è che avendo tantissimi protocolli disponibili con un sacco di applicazioni e implementazioni diverse si ha il problema che non tutti i protocolli daranno delle performance diverse.

Su una connessione ci sono dei link condivisi. Quando la rete ha equità ogni connessione ha esattamente $Banda/numeroHostConnessi$ come valore di banda.

Di solito quindi si capisce se un protocollo è equo attraverso delle simulazioni o dei test, siccome dimostrazioni matematiche di questi concetti è molto complicato.

Il TCP NewReno è equo, siccome permette a tutti di condividere più o meno le performance. Chiaramente questo fatto dipende notevolmente in funzione del RTT, siccome logicamente queste connessioni si comporteranno in modo più aggressivo.

Di fatto ci sono dei protocolli che hanno un metodo di decisione della finestra di controllo calcolata attraverso delle formule matematiche e che quindi riesce a ottenere un modello equo anche nel caso di variazione del RTT.

<img src="image/image-20211209115127103.png" alt="image-20211209115127103" style="zoom:50%;" />



Ci sono due altri aspetti di equità che sono ugualmente importanti: l’equità tra UDP e TCP. Questi due flussi di dati tendono a avere un comportamento vincente rispetto a UDP. Infatti, siccome UDP non ha controlli automatizzati, la perdita di pacchetti non triggera nessun evento che limita il throughput del canale. 

Nel TCP alla prima perdita di un pacchetto si ottiene un throughput dimezzato. 

Ad oggi le applicazioni di solito vengono fatte su TCP perché le applicazioni si adattano alla capacità della connessione senza sovraccaricare la rete. Inoltre spesso i router commerciali hanno delle regole per bloccare i flussi UDP, siccome gli host che emettono UDP non hanno azioni secondarie. Questo fa in modo che i router droppino i pacchetti quando ci si avvicina a uno stato di rischio di connessione. 

Per evitare problemi con la rete comunque, le applicazioni che utilizzano UDP so applicano con un modello definito TCP friendly, ovvero aggiungono una serie di controlli dal lato mittente che permette di ridurre/bloccare l’emissione di pacchetti in casi particolari.

Per tutti questi motivi si può dire che UDP non è equo rispetto TCP.



Un altro modello è quello utilizzato alle applicazioni che permettono di avere un accelleratore di download. Questi client aprirono più connessioni in parallelo in modo da ottenere il file in spezzoni separati. In pratica il comportamento di questo host però è un comportamento di ogni connessioni uguale al TCP:

*  slow start,
* crescita lineare e perdita moltiplicativa

Il throughput di tutti i canali sarà costante e equo. Ma di fatto in questo modello il client che emette più connessioni parallele avrà un throughput maggiore per forza di cose (ad esempio: se si vuole scaricare lo stesso file con due host diversi sulla stessa condizione di rete, uno dei quali però impiega 9 connessioni parallele, allora questo sarà 9 volte più veloce).

Questo modello permette si di sfruttare l’equità del TCP (che quindi assegna un valore di banda diviso tra le connessioni che richiedono) per aumentare il throughput personale. Però è molto rischioso avere un applicazione di questo protocollo in parallelo molto spesso, perché mina la stabilità della rete. Infatto si stressa molto più del previsto il buffer dei router, perché la crescita lenta avviene $n$ volte più velocemente. Questo significa che un host cerca di prendere più posto nei buffer possibile per avere la precedenza. 

Inoltre questo modello funziona solo in un momento transitorio (non continuativo), siccome poi è un gioco al rialzo che porta a una congestione sulla rete tra gli host (se il secondo host inizia a aprire più connessioni in parallelo delle nostre si ottiene un ulteriore rallentamento). Il risultato finale è una serie di connessioni sempre più aggressive sulla stessa rete che stressano esageratamente il buffer di un router e quindi un appesantimento di tutta la rete.



### TCP: versioni specifiche

Ci sono alcune applicazioni che non riescono ad utilizzare le versioni disponibili di TCP, perché si hanno dei problemi sul modo in cui è studiata la rete. 

Ad esempio sulle reti satellitari nessuna connessione TCP funziona completamente bene, e quindi si utilizza un TCP modificato appositamente. Un altro esempio è nei datacenter. Di solito si potrebbe pensare che quello fosse il modello più ottimale per il funzionamento dei protocolli di rete. Di fatto però la bassima latenza che questi luoghi offrono mettono in crisi il TCP, siccome il datacenter ha dei pattern di traffico molto particolari e quindi si possono ottenere delle congestioni molto spinte in casi molto particolari.

Di base ad oggi si utilizza però il TCP in quasi tutte le applicazioni di base, mentre invece UDP viene utilizzato in contesti particolari.

##### QUIC

Le ultime proposte per ridurre alcuni problemi sulla rete, soprattutto sul web in generale. Quindi hanno proposto un nuovo modello che richiede di non utilizzare il TCP per le connessioni HTTP. Questa scelta è stata fatta anche in visione del HTTPS, dove c’è anche bisogno di gestire la crittografia e quindi molto scambi client-server per fare il setup di queste funzioni.

Il nuovo protocollo HTTP/3 si propone di utilizzare l’UDP. Questa scelta però non permette né l’introduzione del layer di sicurezza, né quello di controllo di congestione e di flusso.

La soluzione è stata trovata nel protocollo **QUIC (quick udp internet connection)**, sviluppato da Google e in fase di testing. Questo protocollo permette di utilizzare UDP per l’HTTPS in modo da gestire meglio le connessioni in parallelo, la gestione della sicurezza e del congestion control. Questo modello lavora su uno stack applicativo di questo tipo:

<img src="image/image-20211209121806752.png" alt="image-20211209121806752" style="zoom:50%;" />

Il protocollo quindi implementa alcuni tipi di applicativi:

* **controllo di errori e congestione**: algoritmi e controlli molto simili a quelli del TCP

* modello di **apertura della connessione**: questo processo si occupa di aprire la connessione, per assicurare l’affidabilità della connessione, controllo della congestione, autenticazione, crittografia. 

  Il protocollo inoltre utilizza una tecnica per aprire la connessione e la connessione sicura in un modello chiamato $0RTT$, ovvero che inizializza tutto in un solo RTT.

* inoltre si ha la possibilità di **multiplexing sulla singola applicazione**, ovvero avere un unica connessione che permette di avere una serie di flussi che consentano lo scambio di pacchetti multipli sulla stessa connessione.

* no **HOL blocking**: di fatto questo protocollo permettendo il multiplexing, permette di non avere il problema del HOL blocking, siccome un errore di comunicazione di un pacchetto causa lo stop di un solo flusso di dati.

La differenza di questi modelli si può trovare in questi schemi:

<img src="image/image-20211209122519623.png" alt="image-20211209122519623" style="zoom:50%;" />

