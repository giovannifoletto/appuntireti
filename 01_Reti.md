---
author: Giovanni Foletto
title: Appunti di Reti
subtitle: Sept. 2021 - Nov. 2021
rights: see LICENSE on gitlab.com/giovannifoletto/appuntireti
book: true
ibooks:
  version: 2.0
titlepage: true
---



# Corso di  Reti 

## Abstract

>  Started: Martedì 14/09/2021

> Fabrizio Granelli - II anno ICE

Corso di base riguardante la rete moderna. Questo è un corso rimodulato e serve a conoscere *solo* le basi. 

Libro di riferimento: `Computer Networking: A Top-Down Approach, 7th Edition`.


# Introduzione

<img src="image/image-20210929102946179.png" alt="image-20210929102946179" style="zoom:33%;" />

Internet è nato negli anni ‘80 con i primi protocolli e la prima forma di struttura. Alla sua nascita, la rete Internet non prevedeva la compravendita online o qualsiasi cosa si possa fare ora su di essa; ora internet fornisce un accesso a circa 4 miliardi di persone. Vedremo quindi anche come si sono innovate le tecnologie per consentire a questa rete di lavorare con tecnologie diverse da quella per cui è stato creato.

Da molti anni la persona non è più il principale utente di internet, ma ci sono sempre più oggetti che popolano la rete. Ad esempio un tostapane intelligente, oppure una cornice web over-the-ip. Si possono avere dei web-server miniaturizzati che lavorano per la rete nella rete.

Internet si sta comportando come la rete elettrica: tutti la utilizzano mentre solo pochissimi sanno come effettivamente funziona. Di fatto, tutti accedono alla rete come se fosse una *black box*, ovvero che non si sa come lavori all’interno. Inoltre è uno strumento che cambia la vita.

Con Internet si apre anche la discussione sul *digital divide* ovvero la differenza tra le popolazioni che possono accedere a internet e quelle invece che non lo possono fare. Google aveva provato a proporre una soluzione a questo problema, attraverso il *Google Project Loon*, che ha fallito come progetto iniziale di portare internet in zone distanti dalla connettività a costi favorevoli, ma è servito ad esempio per fornire connessione durante i soccorsi al terremoto in Perù.

In Italia non c’è una struttura molto elaborata di internet, ma nonostante questo c’è un gruppo abbastanza fornito di esperti che si occupa di innovazione in questo campo, rendendo il nostro stato meta abbastanza favorevole per questo tipo di studi.

Le applicazioni di internet sono imprevedibili: era nata con scopo militare, poi passata alla ricerca e infine oggi viene utilizzata da tutti. Quindi non si sa mai come una struttura fallimentare in un certo campo, possa essere riscoperta perfettamente funzionante per altri scopi.



## Obiettivi

* introdurre la terminologia e i concetti di base sulle reti di telecomunicazioni. Questa parte può sembrare ripetitiva, ma vanno dichiarate per avere un campo comune. Si definirà la pila protocollare, ovvero il modo di organizzare la rete internet.
* fornire conoscenza su struttura e protocolli di internet
* approccio top-down, con lezioni e esercitazioni. Si parte da livello applicazione (l’utente finale) e si studieranno i protocolli di comunicazione che si rifanno a dei servizi standard. Questa sezione tratterà anche funzionamento di applicazioni (che siano peer-to-peer, server-client, o il cloud). C’è la necessità di conoscere questi protocolli, per consentire il corretto funzionamento con il massimo della resa, fare la scelta per il miglior protocollo per lo scopo cercato.



## Argomenti

* che cos’è Internet? 

  il suo funzionamento, cosa comporta e tutto il collegato.

* Livello di applicazione: studio dei servizi che si appoggiano alla rete

* Livello di trasporto: in particolare TCP e UDP

* Livello di Rete: come si trasmettono i dati nei canali

* Livello Fisico: (non trattato in questo corso) e serve a studiare la sequenza di bit che viene mandata attraverso la rete. Di solito questa parte è molto specializzata nel campo delle comunicazioni, richiedendo un corso unico e con tutta una teoria, sviluppata dagli anni ‘40 per trasformare un informazione in segnale capace di propagarsi. Di fatto questo layer di comunicazione trasforma le informazioni in onde elettrostatiche. 

  In questo argomento rientrano anche tutte le tecniche di comunicazione wireless, che sono molto più raffinate e complesse. 

Molti dei concetti che si trattano nella rete sono molto astratti, ma da alcuni anni si svolge un laboratorio per capire il funzionamento di base attraverso la virtualizzazione di alcuni apparati di rete (attraverso kathara.org). Questo software sviluppato da UniRoma3 attraverso Linux per simulare le reti. Soprattutto riesce a simulare il traffico tra router interni alla rete, ovvero non un router casalingo.

Questo software è un emulatore, quindi si può installare del software come su router reali e testare cosa entra e cosa esce.



(*Lezione 2*)

# Capitolo 1: Introduzione e nomenclatura

## 1.0 Panoramica e obiettivi

* introdurre la terminologia e i concetti di base
  * che cos’è Internet? Che cos’è un protocollo? 
  * ai confini della rete: host, reti di accesso e mezzi trasmissivi
  * nucledo della rete: commutazione di circuito e commutazione di pacchetto, struttura di Internet
  * prestazioni: ritardi, perdite e throughput
  * sicurezza
  * livelli di protocollo, modelli di servizio (sono necessari perché risolvendo un problema in piccoli problemi si riesce poi a risolvere problemi più grandi in modo più semplice)
  * un po’ di storia
* gli approfondimenti arriveranno nei capitoli successivi
* approccio: capire Internet attraverso degli esempi

Alcune scelte che sono presenti nella rete oggi dipendono dalla storia e dall’evoluzione, siccome non è mai stata spenta, e di conseguenza ha dovuto sempre rimanere funzionante. 

## 1.1 Termini Principali di internet

Per iniziare a spiegare internet, spieghiamo alcuni termini.

> **Host**
>
> Sistema terminale, il dispositivo che viene collegato alla rete. Ad esempio lo smartphone o il pc, ..
>
> La sua caratteristica principale è che gli host sono dei dispositivi reali e di uso comune (appunto, come un computer) e sono gli unici dispositivi sui quali girano le applicazioni (quindi può essere il mio computer o una macchina virtuale in un datacenter). 
>
> Di fatto sempre più spesso gli host ospitano le applicazioni, o la parte client o la parte server, e sono sempre meno sotto il controllo delle persone (ad esempio l’avvento degli IOT devices).
>
> Gli host sono distaccati da come lavora l’applicazione, che può avere impostazioni di tipo client-server, oppure essere parte attiva del collegamento. Inoltre, di solito i server sono leggermente diversi dai normali computer perché hanno una capacità di calcolo superiore e una banda di accesso più elevata per garantire servizio a più persone.
>
> Questi oggetti si diversificano moltissimo per oggetti che si inseriscono in rete e di tecnologie di accesso alla rete.



> **Collegamenti**
>
> Sono le strutture, radio (onde elettromagnetiche o satellite) o fisiche (attraverso cavi di rame, fibra ottica o altro), che collegano più strutture fra loro. 
>
> I collegamenti sono importanti perché forniscono un salto (“*hop*”), ovvero un collegamento tra due segmenti di un percorso (Che può essere anche più lungo di un solo salto). Questi collegamenti non sono tutti uguali e dipendono completamente da chi costruisce questi collegamenti. Tutte le caratteristiche di questi collegamenti dipendono da come sono stati costruiti e da quali tecniche hanno deciso di utilizzare (ad esempio: la fibra di solito ha una certa banda di accesso a internet e un certo ping, ...).



> **Router**
>
> Sono i nodi che hanno come compito principale di inoltrare i dati da un punto a un altro. Sono gli effettivi smistatori del traffico di pacchetti creato nella rete. 
>
> La rete a questo punto è composta in maniera abbastanza uniforme. 



La rete di accesso è molto diversificabile sia per tecnologie utilizzate per accedere alla rete (rete mobile, rete aziendale, rete pubblica, rete telefonica, ...) sia per dispositivi che ci accedono.

<img src="image/image-20211001002809106.png" alt="image-20211001002809106" style="zoom:50%;" />

Le isole blu su questo schema di internet sono un concetto importante, infatti la rete nasce da una serie di gruppi di collegamento, che di solito sono uniti attraverso una serie di Internet Service Provider (**ISP**) via via più grosso (quindi internet è la fusione di diverse reti con un *sacco di proprietari*, e ogni isola è gestita in un modo con un certo grado di libertà, rispettando alcuni limiti che permettono alla propria “isola” di collegarsi alle altre). Si noti anche che il concetto di rete come collegamento di più host non viene meno, semplicemente che gli host rimangono collegati in piccola scala e collegati in gruppi sempre più grandi.

Ogni isola può essere gestita da chiunque la possieda con il solo vincolo di avere il router esterno (*gateway*, responsabile al collegamento degli host con la rete) compatibile con il resto della rete (per funzionare), ma lasciando il resto dell’organizzazione interna della rete completamente a discrezione del proprietario.

> **Internet**
>
> Internet è la rete delle reti, che attraverso una struttura ad albero si collega in tutta se stessa passando dal cuore (i rami principali e maggiori) fino all’utente finale.
>
> *internet* e *Intranet* sfruttano la stessa tecnologia, ma viene applicata a reti con scopi diversi (ad esempio: rete privata o rete aziendale o ancora una rete a accesso pubblico). Si differisce appunto il fatto che questa rete è “privata”/ha un proprietario dalla maiuscola. Non tutte queste reti sono collegate completamente con l’esterno, costruendola in modo che non tutto sia visibile dall’esterno normalmente.



> **Protocollo**
>
> Definizione del formato e dell’ordine dei messaggi scambiati fra due o più entità in comunicazione, così come le azioni intraprese in fase di trasmissione e/o ricezione di un messaggio o un altro evento (anche ad esempio nel caso di fallimento di comunicazione e le tecniche di riconnessione).
>
> Molto spesso la prima operazione è l’*hand-shake*, ovvero la stretta di mano tra i due componenti della comunicazione.
>
> [Come scrivere i messaggi, come devo ordinare e cosa vogliono dire e soprattutto se la serie di comunicazioni sono corrette]



Su Internet, **IEFT (Internet Engineering Task Force)** è un gruppo di controllo completamente democratico. Chiunque può lavorare in un loro working group per proporre un **RFC (Request for Comments)**, ovvero un documento aperto a tutti risultato di una serie di consultazioni che può portare alla creazione di un nuovo protocollo. 

Questi RFC sono in formato testo e sono pubblici, oltre che estremamente tecnici sui dati di riferimento forniti, siccome devono spiegare tutte le fasi di ogni protocollo. Questi protocolli sono aperti *ai commenti* perché si cerca sempre di migliorarsi. 

Alcuni protocolli hanno più di un RFC perché hanno avuto la necessità di più modifiche del protocollo.

Quando si sceglie di essere sulla rete, si possono ottenere principalmente due servizi:

* **affidabile/reliable** (nulla a che vedere con la sicurezza): il servizio fornisce la garanzia che la comunicazione non perda nessun bit e che si attenga una copia bit-per-bit. 

  Questo servizio garantisce che tutto quello che viene ricevuto è la copia perfetta di quello che sto cercando.

  * Non viene indicato il tempo di questo trasferimento, infatti questo tipo di meccanismo perderà del tempo per garantire l’affidabilità

* **best-effort**: mando dei dati senza nessuna garanzia dell’effettiva ricezione dei dati. Questo servizio ha una serie di lati positivi rispetto all’altro servizio (anche se potrebbe non sembrare):
  
  * permette di bloccare tutti i servizi che garantiscono l’affidabilità, quindi di base ottenendo una perdita di tempo minima
  * per questo motivo si utilizza nella applicazioni che necessitano di un ritardo di risposta molto veloce (ad esempio i videogiochi online, oppure la voce). A questi sistemi non cambia molto la perdita del 5%-10% dei dati.
  * L’applicazione poi potrebbe compensare a questa perdita di dati/correzione di errori con delle tecniche di controllo implementate ad hoc via software.

Ci sono due tipi di connessioni: in banda e fuori banda. Le connessioni **in banda** vengono utilizzate da tutti quei protocolli che scambiano dati e valori di controllo sulla stessa linea (esempio: http, ma in realtà quasi tutti i protocolli di livello 2 visti più avanti). Le connessioni **fuori banda** forniscono invece due linee al servizio, (solitamente) una che permette la gestione e una seconda che permette lo scambio di dati (esempio: ftp). 



## 1.2 Architettura di Internet

La rete è costituita da più tipi di elementi che si possono dividere come dispositivi di accesso, dispositivi fisici e dispositivi nel centro della rete.

### 1.2.1 Accesso alla rete

L’accesso alla rete viene definito come ultimo salto (o anche *last mile* o ultimo host) e si tratta dell’insieme di elementi che ci permette di essere presenti sulla rete.

La rete di solito rende visibili solo questi dispositivi: la rete effettiva creata da isole e da router rimane “oscurata”, mantenendo le tecnologie e il funzionamento di questi componenti nascosti e sempre funzionanti senza bisogno di un implementazione ad hoc.

Al bordo della rete sono presenti degli elementi che possono essere:

* sistemi terminali (**host**): responsabili dell’esecuzione dei programmi applicativi situati all’estremità di Internet.
* architettura **client/server**: l’host *client* richiede e riceve un servizio da un programma *server* in esecuzione su un altro terminale.

* servizio **peer-to-peer**: il servizio è direttamente collegato al computer di destinazione, che spesso è un host di tipo simile al nostro. Di fatto a questo tipo di tecnologia appartengono i torrent oppure (circa) anche il protocollo di Skype. Di solito questa modalità permette di avere un uso limitato o inesistente di server dedicati.

(Lezione 3)

L’applicazione di queste architetture di rete dipende soltanto dall’applicazione utilizzata.

Spesso però ad oggi si parla di architettura client-server, oppure l’architettura **cloud** che è un server ancora più flessibile e grande. Infatti questi computer lavorano in modo da poter servire milioni di persone attivando più o meno computer in base alla richiesta e smistando il traffico attraverso tutti i server disponibili.



Quando si possiede un host, c’è la necessità di portare il collegamento al *primo nodo della rete*. Si parla in questo caso di **tecnologia di accesso**. Queste reti si differenziano spesso nella capacità di accesso (che in realtà è limitata alla capacità dell’ultimo miglio), e non dipende per nulla dalle altre performance della rete. 

A seconda della rete di accesso che si sceglie si può avere un collegamento **condiviso** o **dedicato**. Si parla di collegamento condiviso quando si utilizzano tecnologie wireless (es. Wi-Fi) come rete di accesso, oppure una rete aziendale. Si parla di collegamento dedicato quando ci si collega tramite ADSL, fibra FTTH e simili (quando il collegamento è privato).



#### Reti di Accesso: accesso basato su cavo

Ci sono più modelli di accesso tramite cavo.

<img src="image/image-20211001084623911.png" alt="image-20211001084623911" style="zoom:50%;" />



Molto più comune in America, questo metodo permetteva a una singola centralina di fornire collegamento internet e televisione (la cosiddetta televisione via cavo) attraverso una sola connessione. Per trasmettere tutti questi dati assieme si utilizza il **FDM (Frequency Division Multiplexing)**, ovvero la tecnologia che permette di trasmettere in più frequenze (come la radio) e poi in base a questa frequenza si può ottenere il contenuto del canale. Per ottenere queste prestazioni si devono avere delle frequenze divise completamente, e devono essere anche segnali estraibili/divisibili rispetto agli altri. Al momento di ricezione si recupera questo segnale e si decodifica. Il funzionamento è uguale alla radio, con la differenza che la radio fornisce questo collegamento in modo analogico.



![image-20211001085226359](image/image-20211001085226359.png)

Lo stesso metodo di prima può essere migliorato utilizzando tecnologie come **HFC (Hybrid Fiber Coax)** che è un collegamento in fibra più performante, capace di trasmissioni fino a 40Mbps-1.2Gbps in downstream, 30-100Mbps in upstream.

Gli appartamenti condividono sempre la rete di accesso, siccome c’è un unico collegamento tra il CMTS e il servizio internet fornito dall’ISP.

 

![image-20211001085517918](image/image-20211001085517918.png)

Questo tipo di accesso è chiamato **DSL (Digital Subscriber Line)**. Ogni cavo che raggiunge la propria casa è del privato. Più connessioni private vengono poi aggregate nel *DSLAM (DSL Access Multiplexer)* che passa i dati alla rete dell’ISP. Questo metodo è utile perché i cavi utilizzati erano già in loco per la comunicazione telefonica, ma non può raggiungere alte prestazioni siccome il cavo non era originariamente pensato per svolgere questo compito. Si può comunque utilizzare per questo scopo, siccome il router di accesso casalingo sa adattare quello trasmissibile e capire il ricevuto.

Questo metodo però è molto limitato dal *digital divide*, infatti se il doppino è in brutte condizioni di manutenzione, o semplicemente si è troppo distanti dal DSLAM la linea DSL non funziona. Infatti questi collegamenti erano costruiti per far correre frequenze più basse, mentre trasmettendo frequenze così alte si ottiene uno smorzamento dell’onda più grande, riducendo la banda effettivamente distribuita.

I dati telefonici sono lo stesso presenti su questa linea, attraverso il multiplexing e al DSLAM vengono estratti e mandati sulla rete telefonica.

Il range della frequenza di trasmissione di questa connessione è tra i 24-52Mbps in downstream, 3.5-16Mbps in upstream.

Spesso questo servizio è offerto come **ADSL (Asymmetric DSL)**, chiamato in questo modo perché fornisce un banda in downstream molto maggiore di quella che fornisce in upstream.



<img src="image/image-20211001090653258.png" alt="image-20211001090653258" style="zoom:67%;" />

Anche le reti casalinghe sono ritenute reti di accesso, e sono molto comuni. In questo caso ho la necessità di tre apparecchi (di solito contenuti in un unico scatolotto) per avere connessione:

* modem via cavo o DSL
* componente router (di solito compreso di firewall e NAT)
* wifi access-point



Tipo di accesso **Wireless** fornisce una rete senza fili che può connettere più dispositivi. Tipicamente è un tipo di accesso preferibile all’interno degli edifici.

Si basa su uno standard chiamato *IEEE 802.11* con delle lettere a seguito che indicano la versione (siccome ancora attivamente sviluppato). L’ultima versione ad oggi è la *an* capace di circa 300/400Mbps. Le prestazioni migliorano ogni qualche anno perché le nuove tecnologie riescono a raggiungere frequenze più alte. Inoltre si riesce a tenere più bit/Hz, ovvero più informazioni condivise nella stessa frequenza. 

Un altra tecnologia di accesso wireless è il **Wide-area Cellular Access Networks**, ovvero la rete di accesso telefonica. Questa viene fornita dagli operatori telefonici con un raggio di anche qualche km (per questo si differisce dal wifi). Le reti cellulari fornivano una rete di accesso con una banda molto scadente, dopo il 4G-LTE è diventata un buon sostituto del wifi dato che la banda è compatibilmente accettabile (di solito 50-100Mbps).

 ![image-20211001092632330](image/image-20211001092632330.png)

Le reti **enterprise networks**, ovvero le reti aziendali sono delle reti leggermente diverse. Di solito si ha un collegamento diretto con l’ISP, attraverso il router gateway, che si occupa di dividere dentro e fuori dalla rete. Un altra serie di router permettono di portare internet ovunque, compreso nel datacenter. SI connettono agli switch anche gli access-point wifi, oppure i laptop e le prese ethernet.

Come effetto finale si ottiene che nella rete aziendale ci si collega come fossimo a casa, quindi o con l’ethernet o il wifi.



Le **data center networks** si hanno degli accessi di solito ridondanti e con molti Gbps di banda di accesso, siccome si deve dividere questo collegamento in molte sezioni. Queste reti servono a collegare centinaia di server tra di loro e alla rete. Di solito questi centri hanno come collo di bottiglia la linea di accesso alla rete, per questo di solito si studia dove posizionarli in base alla posizione di accesso sulla rete, che deve essere vicina a più ISP con alta banda. La potenzialità dei datacenter è la virtualizzazione, ovvero la capacità di avere un certo numero di risorse computazionali (anche elasticamente modificabili) e facilmente scalabile (modificare la richiesta di server in base all’effettivo utilizzo dell’applicaizone).



#### Mezzi trasmissivi

Per trasportare i dati su una rete, sono necessari dei segnali diversi che cambiano anche in base alla tecnologia a cui si appoggiano le connessioni.

Questi segnali possono essere **mirati**, ovvero il segnale si propaga in un mezzo fisico, come la fibra ottica o il filo di rame o cavo coassiale. Questo permette di avere un controllo più efficace sulla propagazione del segnale. 

Utilizzando dei **mezzi a onda libera** invece, i segnali si propagano nell’atmosfera e nello spazio esterno in modo tridimensionale, facendo in modo che la stessa potenza venga spalmata in un area molto più grande, e di conseguenza avendo una controllo sulla propagazione e ricezione del segnale minore.

Attraverso questi mezzi si trasmettono 

> **Bit**
>
> dato che viaggia da un terminale a un altro, passando per una serie di coppie trasmittente-ricevente.



> **Mezzo Fisico**
>
> L’effettivo tramite tra chi trasmette e chi riceve.



Il **cavo coassiale**, utilizzato prima per le televisioni, poi anche per l’ethernet. Costituito da due conduttori di rame concentrici, con capacità di trasmissione bidimensionale e la possibilità di avere una banda di base (canale singolo, capacità Ethernet), oppure una banda larga (con più canali su un unico cavo e possibilità di segnale video).

Ad oggi la migliore tecnologia per la comunicazione internet invece è la **fibra ottica**, costituita da un mezzo sottile e flessibile che conduce impulsi di luce. Questa linea è costituita da un'alta frequenza trasmissibile da punto a punto (anche fino 10-100Gbps). Il segnale è caratterizzato da un basso tasso di errore ed è immune all’interferenza elettromagnetica. Necessità però di ripetitori ed è molto sensibile alle piegature o danni durante i lavori. Di solito su ogni linea ci possono essere cavi contenenti anche centinaia di fibre singole.  



Un altro metodo di comunicazione abbastanza conosciuto sono i **canali radio**, che permettono di trasportare segnali sfruttando lo spettro elettromagnetico. Questo metodo di comunicazione ha il lato positivo che non richiede la posa fisica di cavi e la bidirezionalità. Ha come contro la sensibilitò agli effetti dell’ambiente, quindi sono influenzati da riflessione, ostruzione e interferenza.

Ci sono diversi tipi di canali radio:

* collegamenti a **microonde terrestri**: sono dei collegamenti veloci fino a 45Mbps. Viene utilizzato come backup su delle linee in fibra oppure per studiare una successiva creazione di fibra e temporaneamente creare il collegamento.

* internet **satellitare**: fornisce una banda di accesso intorno ai 45Mbps, il problema di questa connessione è che il ritardo di propagazione è di circa 270-300 millesimi di secondo (quando il collegamento è geostazionario). Hanno il lato positivo che possono servire un intera metà del pianeta con un unico satellite (sempre geostazionario).

  Per risolvere questo problema si può o fornire un collegamento a più bassa quota, ma per fare questo è necessaria una costellazione di satelliti oppure la connessione non può essere continua.

  Il vantaggio di questo metodo è che può portare abbastanza semplicemente internet in ogni posizione del mondo.

  I costi al momento sono ancora molto alti.

* **LAN** (es. Wifi), fornisce una rete di accesso di circa 11-54-300+Mbps. 

* **Wide-Area** (es: rete cellulare) fornisce un collegamento fino a 100Mbps (Rete 4G-LTE), in continuo sviluppo.



Spesso se si fa un test per controllare la velocità del collegamento, ad esempio con *speedtest.com*, si ottiene una velocità decisamente più lenta di quelle esposte in questi schemi. Questo dipende dal collo di bottiglia, che rallenta le prestazioni della rete.



### 1.2.2 Nucleo della rete

Nonostante i confini della rete siano abbastanza diversificati fra di loro, la parte interna della rete è molto più simile e costante. 

Di solito più si aggrega traffico, più la rete è “magliata”, ovvero sono presenti router che permettono la connessione.

Ci sono due tecnologie che permettono il passaggio di dati in questa architettura, basate entrambe sulla **commutazione** ovvero l’azione di prendere in ingresso i dati da una porta e redirezionarli su un'altra, per indirizzare il traffico e, quindi, mandare i pacchetti dove effettivamente devono andare. Queste tecnologie servono a capire come smaltire il traffico della rete.

Ci sono due tecnologie per fare queste azioni, commutazione di circuito e commutazione di pacchetto.

Questi due metodi si basano su due concetti diversi: la commutazione di circuito si basa sul fatto che ogni circuito ha una data banda e una serie di altre informazioni che conosco. La commutazione di pacchetto invece lascia la libertà di come i dati vengano mandati, e di conseguenza rende adattabile la rete a tutti i tipi di dati che la popolano.

Logicamente queste due tecnologie non possono convivere nella stessa rete.

#### commutazione di circuito: 

tecnologia nata con la rete telefonica. Si creava un circuito tra ricevente e trasmittente e si pagava una quota al minuto perché in quel momento si teneva impegnata la rete. Questo concetto funzionava bene perché questa rete serviva solo per lavorare con le frequenze vocali.

Questo tipo di comunicazione fornisce un collegamento che impiega qualche secondo a essere creato, ma che rimane costante e invariato fino a che non si termina.

Di solito questa tecnologia si utilizza solamente per la voce. 

Questo metodo inoltre riserva il circuito finché non si termina la chiamata. Questo concetto di fatto impone uno spreco di dati nel caso non si stesse completamente occupando la linea. 

#### commutazione di pacchetto:

 è una tecnologia molto diversa nata per la comunicazione digitale, dove i bit possono essere mandati in gruppo o meno e sono sempre caratterizzati dal *non* essere continui (al contrario della voce). Per questo motivo è stata sviluppata questa soluzione. I dati in questa tecnologia sono incapsulati in pacchetti, scambiati tra host senza limiti d'invio o ricezione. Questa rete tratta ogni pacchetto in modo uguale, di fatto quindi senza controllo sui flussi di pacchetti in entrata e in uscita. In teoria si autobilancia fornendo più spazio per mandare i pacchetti di chi ne sta mandando di più.

Questo modello permette di avere una velocità che si adatta alle necessità degli host che la stanno usando. Di fatto questo significa che la rete è molto performante se si è in pochi ad utilizzarla, meno performante quando tanti condividono lo stesso link. 

Questo metodo fornisce il lato positivo di avere una rete che non rifiuta mai una connessione e che quindi permette sempre di mandare un pacchetto, ma può accadere che una parte della rete non riesca a smaltire più il traffico se gli host ne creano troppo e quindi la rete si blocca.

Questa rete ha la caratteristica che necessita di meno cavi e permette di dividere una banda molto grande in capacità in piccoli “pezzi”. Queste tecniche si chiamano di **suddivisione del canale** (suddivisione dell’ampiezza di banda, *bandwidth*). Per trasmettere posso dividere nella frequenza e fare una connessione continua nel tempo utilizzando diverse frequenze. Nel caso invece io potessi dividere solo nel tempo potrei fornire una certa quantità di tempo a ogni pacchetto e in questo modo fornire a rotazione la possibilità di trasmettere. Quest’ultima tecnica può essere fatta solo con dati numerici, la voce per definizione è continua e non può essere trattata in questo modo.

(lezione 4)

Per la suddivisione del canale si hanno due possibili tecniche (spiegate sopra), chiamate **FDM (Frequency Division Multiplexing)** e **TDM (Time Division Multiplexing)**. 

In ogni caso la FDM viene utilizzata sempre nelle tecniche di comunicazione wireless e nelle chiamate telefoniche. Il TDM viene utilizzata ogni canale ogni spot di tempo e è utilizzabile con la commutazione di pacchetto. In questo caso tutto il flusso viene diviso in pacchetti e vengono messi in seguito per condividere le risorse di rete. Ogni volta che un pacchetto viene mandato utilizza completamente la banda del collegamento. Per questo motivo nella rete si crea la **contesa per le risorse**, dove i pacchetti aspettano di avere lo spazio per avere tutta la banda disponibile per essere mandato. In questo modo però si crea il problema della **congestione**, ovvero quando la pila di richieste diventa troppo lunga per eseguire le operazioni di commutazione in tempi rapidi e di conseguenza l’attesa per l’utilizzo del collegamento continua. Se la congestione peggiora al punto da riempire il buffer della coda del router, allora non c’è più spazio per ricevere pacchetti e quindi si parla di **congestione grave** e si verificano perdite di pacchetti oppure blocco totale della comunicazione lungo questo nodo. 

Per ovviare a questo problema di solito il router identifica una rete di riserva meno congestionata e reindirizza i pacchetti attraverso un altra rete che porta i pacchetti allo stesso obiettivo per un altro percorso.

Ogni pacchetto segue la tecnica del **store and forward** ovvero il nodo deve ricevere completamente il pacchetto prima di poterlo reindirizzare. La memoria del router non è illimitata perché ha la necessità di avere una memoria più veloce possibile e di conseguenza non può essere troppo grande.

La congestione non è semplicemente eliminabile, siccome la rete non ha una modalità interna di autoregolarsi, ma ci sono alcune tecniche per limitare questi problemi o quantomeno aggirarle.

Il comportamento della rete a pacchetto è piuttosto complesso e prende il nome di **multiplexing statistico**. Questo metodo permette la divisione dello spazio diviso nella banda in modo che ogni host abbia una velocità finale di invio dei pacchetti più o meno costante. In questo modo l’host che invia più traffico è anche quello che ottiene più spot per mandare i suoi pacchetti. Di fatto quindi la velocità finale dei vari host per ottenere/mandare i suoi pacchetti è più o meno costante. 

Inoltre di solito i collegamenti non vanno sempre in capacità crescente, ma ogni sezione si basa sul concetto di avere solo la capacità media che passa in quel tratto di percorso. Questo permette di avere il massimo della resa nei periodi “normali” di traffico e di avere una rete leggermente congestionata nei rari momenti in cui tutti gli host sono collegati, ma un costo di realizzazione dell’opera non esageratamente grande.



Per calcolare il tempo di un percorso di store-and-forward si calcola che:

* occorrono L/R secondi per trasmette (push out) un pacchetto di `L` bit su un collegamento in uscita da `R` bps. 
* L’intero pacchetto deve arrivare completamente al router per essere ritrasmesso al successivo
* il *ritardo* quindi si calcola semplicemente come `3L/R` (supponendo che il ritardo di propagazione sia nullo)

Di fatto ci sono degli accorgimenti pensati per rendere la rete stabile, ad esempio il controllo di congestione in entrata (quando l’interfaccia di rete stessa si rifiuta di mandare un pacchetto, ritenendo la rete molto congestionata, oppure uno sharper all'imboccatura, che fa cadere tutte le connessioni che ritiene avessero troppe risorse).



#### Commutazione di pacchetto e commutazione di circuito

La commutazione di pacchetto è di fatto diventata la scelta vincente perché anche la più “comoda”, siccome fornisce un ottima elasticità sui dati che possono essere mandati. Questo metodo a pacchetti è ottimo per i dati a raffica, dove le risorse sono condivise. Inoltre sono necessarie pochissime impostazioni di chiamata. 

L’eccessiva congestione di pacchetti può portare alla perdita di pacchetti o a un ritardo. Per questo sono necessari protocolli per il trasferimento affidabile dei dati e per il controllo della congestione.

Si può anche ottenere un circuito *circuit-like*, ovvero che imita la commutazione di circuito attraverso una rete a commutazione di pacchetti, ma ci sono molto problemi e sono tecniche molto avanzate, non viste in questo corso di base.  



### 1.2.3 Struttura della rete Internet

Come si fa quindi a connettere tutte le reti di accesso tra loro? Ci sono ISP più grandi che servono ISP sempre più piccoli, fino ad avere un servizio capillare e globale. Per costruire la rete internet questa rete di ISP ha la necessità degli **IXP(Internet Exchange Point)**, ovvero luoghi dove gli ISP si collegano fra loro. La presenza di questi IXP non è casuale, ma dipende dagli accordi commerciali tra gli ISP. Di solito queste connessioni sono multiple e con delle linee di backup. 

Inoltre si può pensare a ISP regionali, nazionali e internazionali. Tutti questi si servono a vicenda per fornire la rete di accesso. 

Allo stesso modo i grandi content provider che necessitano di una grande rete di distribuzione del loro servizio (es. Google, Microsoft, Akamai, Amazon, Netflìcs ..) possono sviluppare una loro rete per potersi collegare ai nodi più grandi e meglio posizionati. Questa rete può essere fisica, quindi costruita ad hoc (es. Google Fiber), oppure costruita come rete di overlay, ovvero una rete virtuale, gestita dal content provider, che si appoggia ad un ISP, sfruttando il servizio “affittato” come soluzione della loro rete.

Di fatto quindi, nella rete posso trovare:

<img src="image/image-20211002092707567.png" alt="image-20211002092707567" style="zoom:50%;" />

Al “centro” un piccolo numero di grandi reti con “molte” interconnessioni:

* **”tier-1” commercial ISPs** che offrono copertura nazionale e internazionale (es. Level 3, Sprint, AT&T, NTT)

* **Reti dei content provider**: reti private che connettono i propri data center a internet , spesso by-passando ISP tier-1 e regionali, e collegandosi direttamente agli ISP finali. Questo serve perché i contenuti in questo modo sono più vicini, e quindi ha by-passato una serie di colli di bottiglia che possono rallentare il servizio. Inoltre in questo modo si possono creare dei nodi di distribuzione per i servizi che devono essere più accessibili, in modo da velocizzare la qualità del servizio. Spesso queste reti utilizzano la creazione di reti fisiche proprie o reti overlay per avvicinare i dati all’utente finale. 

  (lezione 5)

  Molti content provider hanno la propria rete di delivery, spesso fatta in overlay con accordi con i singoli ISP. 



I nodi nella rete aumentano più si è vicini all’utente finale, mentre al cuore della rete si hanno pochi ma grandi collegamenti. Altra differenza si ha nel consumo energetico di internet: l’utilizzo energetico di un oggetto dipende dalla sua capacità di switchare il traffico. Un router che consuma di più di solito riesce a smistare più pacchetti o più velocemente.

I collegamenti intercontinentali solitamente sono fatti con cavi in fibra ottica, e è interessante pensare che sfruttino le vecchie rotte di navigazione transoceanica del 500. ![https://submarine-cable-map-2015.telegeography.com/]

Altre informazioni sulla struttura della rete è la struttura della *NSFnet*, ovvero la rete degli istituti di ricerca americani.

In Europa abbiamo la rete **GEANT** per la ricerca, ma a livello nazionale. Questa rete quindi permette di collegare i punti di collegamento fondamentale internazionale. 

<img src="image/image-20211002101942846.png" alt="image-20211002101942846" style="zoom: 33%;" />

Quando questo collegamento arriva in Italia (a Milano) si crea la rete del consorzio **GARR**, ovvero l’ISP della ricerca, che collega gli istituti di ricerca in Italia e poi si innesta come collegamento per gli stati confinanti. Di solito questo consorzio affitta una “dark-fiber”, ovvero una rete non utilizzata e viene collegata alla rete attraverso dei router fornito dal consorzio GARR. 

Questa rete fornisce un accesso a internet, ma per avere questo collegamento devo poi uscire da questi percorsi.

<img src="image/image-20211002102132527.png" alt="image-20211002102132527" style="zoom:33%;" />

## 1.3 Perdite e ritardi

Sulla rete internet a commutazione di pacchetto, ogni pacchetto viene gestito singolarmente ed elaborato seguendo le direttive dello store-and-forward fino al raggiungimento della destinazione. Di fatto quindi il pacchetto rimane nel buffer di un router finché non arriva a destinazione.

Quando i pacchetti arrivano nel buffer, si accodano agli altri pacchetti, in attesa al proprio turno. Di fatto quindi il ritardo può avvenire nel buffer (mentre i pacchetti sono in attesa di essere trasmessi), in accodamento (aspettando che tutto il pacchetto venga ricevuto) oppure può avvenire una perdita di pacchetto se questo arriva e trova un buffer pieno.

Di solito si possono identificare 4 principali cause di ritardo del pacchetto:

1. Ritardo di **elaborazione del nodo**: questa fase avviene nel router o nel nodo di scambio e serve per eseguire una serie di controlli sul pacchetto, ovvero il controllo di errori sui bit (controllo della correttezza del pacchetto) e la determinazione del canale di uscita.

   Questo tempo è da considerabile costante, siccome i router commerciali sono ottimizzati al massimo in queste operazioni.

2. Ritardo di **accodamento**: quando il pacchetto viene smistato, si mette in attesa di trasmissione nel buffer.

   Questo contenuto è il più difficile da calcolare perché dipende dagli altri pacchetti, dal funzionamento della rete in generale e anche come è congestionata la linea in utilizzo. Di solito si stima questo valore in un range elaborato solo tramite valori statistici.

3. Ritardo di **trasmissione**: quando il pacchetto finisce la coda, allora viene trasmesso. Questo è il tempo che l’interfaccia di rete impiega a creare la forma d’onda necessaria per il trasporto del pacchetto, ovvero quanto tempo impiega il segnale nel quale è stato tradotto il pacchetto ad essere completamente eseguito. 

   Questo è un valore facilmente quantificabile, infatti sapendo quanto il segnale di un certo valore di bit impiega a essere trasmesso, allora si può calcolare per qualsiasi valore di bit. Serve quindi la *lunghezza del pacchetto* $L$, la *frequenza di trasmissione del collegamento (in bps)* $R$, allora il ritardo di trasmissione è $L/R$.

4. Ritardo di **propagazione**: si calcola semplicemente facendo $d = \text{lunghezza del collegamento fisico}$ e $s = \text{velocità di propagazione del collegamento}$, si ottiene il ritardo di propagazione con $d/s$. 

   Questo calcolo non valuta, come si vede, la dimensione del pacchetto e la frequenza di trasmissione, che invece vengono calcolati nel ritardo di trasmissione.

   Di solito queste indicazioni si trovano specificatamente su ogni mezzo utilizzato, e con questa costante avendo le indicazioni della lunghezza del collegamento allora si ottiene questo valore di ritardo.

   Questo tempo viene calcolato per una tratta, e rimane costante per ogni pacchetto che viene inviato attraverso quel nodo. Questo valore può variare in casi particolari, ad esempio con una comunicazione satellitare, dove il ricevitore non è fermo in orbita geostazionaria e quindi la distanza (e di conseguenza il percoso) cambiano.



Il **ritardo di nodo** quindi si può semplificare con questa formula $d_{nodal} = d_{proc} + d_{queue} + d_{trans} + d_{prop}$:

* $d_{proc}$ ovvero ritardo di elaborazione (*processing delay*).

  Di solito pochi microsecondi, anche meno

* $d_{queue}$ ovvero ritardo di accodamento (*queuing delay*)

  Dipende dalla congestione della rete. Si può stimare attraverso la teoria delle code: analizzando il tempo di attesa di un oggetto nel buffer, si ottiene un grafico di questo tipo

  <img src="image/image-20211004110215996.png" alt="image-20211004110215996" style="zoom:67%;" />

  Dove si identificano $R = \text{frequenza di trasmissione in bps}$, $L= \text{lunghezza del pacchetto in bit}$ e $a = tasso medio di arrivo dei pacchetti$. $La/R$ è definito come *intensità di traffico*. Con questo fattore tendente a zero, il ritardo è pressoché nullo, se si arriva a 1 il ritardo si fa consistente, ma se questo valore diventa maggiore di 1 vuol dire che il lavoro in arrivo è più di quando possa essere svolto, quindi il ritardo medio può tendere all’infinito. 

  Questo calcolo significa che progettando la rete si deve far in modo da non avere mai un canale completamente saturo, perché così facendo si ottiene una rete più lenta.

  Di fatto nella realtà, questo ritardo viene avvertito solo in caso di congestione, in caso contrario è inavvertibile o inesistente (se la rete è scarica).

* $d_{trans}$ ovvero ritardo di trasmissione (*transmission delay*)

  Varia con valore $L/R$ e è significativo solo sui collegamenti a bassa velocità.

* $d_{prop}$ ovvero ritardo di propagazione (*propagation delay*)

  Può variare da un valore di pochi microsecondi a centinaia di millisecondi. 



Per avere una diagnostica su questi dati si utilizza un programma chiamato `traceroute`. Questo programma crea dei pacchetti che hanno un parametro, chiamato *tempo di vita*, ordinati in modo crescente. Questo serve per avere l’eliminazione di questi pacchetti in un certo router. Quando viene effettuata questa eliminazione, il nodo risponde al terminale che è stato eliminato questo pacchetto. Facendo così si ottengono le informazioni sui nodi del percorso e anche le informazioni sulla velocità del collegamento.

Un esempio di questo comando può essere così:

```bash
su un massimo di 30 punti di passaggio:

  1     4 ms     2 ms     1 ms  10.196.160.1
  2     *        *        *     Richiesta scaduta.
  3     *        *        *     Richiesta scaduta.
  4     4 ms     5 ms     2 ms  193.205.210.253
  5     5 ms     2 ms     3 ms  ru-unitn-povo-re1-tn1.tn1.garr.net [193.206.143.77]
  6    23 ms     6 ms     9 ms  re1-tn1-rx1-mi2.mi2.garr.net [90.147.81.113]
  7     9 ms     6 ms     8 ms  garr.mx1.mil2.it.geant.net [62.40.125.180]
  8    14 ms    14 ms    15 ms  ae6.mx1.gen.ch.geant.net [62.40.98.80]
  9    23 ms    22 ms    21 ms  ae6.mx1.par.fr.geant.net [62.40.98.183]
 10    98 ms    94 ms    92 ms  hundredge-0-0-0-22.102.core1.newy32aoa.net.internet2.edu [198.71.45.236]
 11   100 ms    93 ms    92 ms  163.253.1.43
 12    97 ms    99 ms    97 ms  nox300gw1-i2-re.nox.org [192.5.89.221]
 13   100 ms    98 ms    98 ms  192.5.89.58
 14    99 ms    98 ms    99 ms  nox-mghpcc-gw1-umassnet-re2.nox.org [18.2.8.90]
 15   102 ms    97 ms    97 ms  69.16.1.0
 16    99 ms    98 ms    99 ms  core2-rt-et-8-3-0.gw.umass.edu [192.80.83.113]
 17   101 ms    98 ms   100 ms  n5-rt-1-1-et-10-0-0.gw.umass.edu [128.119.0.10]
 18   101 ms    98 ms    99 ms  cics-rt-xe-0-0-0.gw.umass.edu [128.119.3.32]
 19   103 ms   124 ms   101 ms  nscs1bbs1.cs.umass.edu [128.119.240.253]
 20    99 ms    97 ms    98 ms  gaia.cs.umass.edu [128.119.245.12]

Traccia completata.
```

Di importante in questa traccia è il passaggio da nodo 9 al 10, dove avviene un drastico aumento del tempo di passaggio. Questo indica ad esempio (in questo caso, non sempre) che è stato fatta una connessione transoceanica.

Un altro particolare è il risultato `* * *`. Questo avviene (ormai) spesso perché alcune reti per sicurezza non lasciano “vedere” dentro, oppure hanno configurato i gateway o i firewall in modo da “droppare” i pacchetti nel caso di un `traceroute`.

(lezione 6)

I pacchetti vengono persi di solito perché una coda ha capacità finita, e il pacchetto non era memorizzabile nel buffer, ovvero per *congestione*. Quando il buffer è completamente pieno allora si va in **congestione grave**. Questi pacchetti vengono persi, ma non si occupa la rete di recuperare i pacchetti persi, ma l’host.

Siccome la rete è l’unica che conosce il suo stato, ma gli host hanno il compito di mandare o rimandare i pacchetti in caso vengano persi, allora c’è bisogno di una sottile modalità di gestione per fare in modo che non si intasi la rete, mentre cerco di mandare dei pacchetti mentre la rete è congestionata. 

La rete è delicata, infatti c’è un bilanciamento tra affidabilità (ridurre la possibilità di dati errati e la capacità di recuperarli) e la sostenibilità della rete (mantenere il collegamento funzionale al meglio).

La rete moderna di solito non ha problemi seri in un tempo continuato, ma di solito ha un picco di connessioni e poi il traffico viene smaltito.



## 1.4 Throughput: velocità della rete

Questo parametro valuta gli effettivi bit/byte per unità di tempo che si possono ricevere/mandare da mittente o ricevente. Si può calcolare il valore **istantaneo** e il valore **medio**.

Il throughput è istantaneo se si calcola quello di un valore predefinito, oppure il valore legato a un singolo pacchetto. Di solito per questo motivo si utilizza il valore medio. 

Questo valore può dipendere anche dalla quantità di utenti che hanno un accesso su un determinato collegamento. Inoltre se la rete è condivisa, e non dedicata, allora bisogna calcolare che non si può utilizzare tutta la banda di quella linea.

Questo valore è importante in modo da segnalare a delle applicazioni se il loro collegamento è fattibile, in base alla richiesta di velocità della rete per il funzionamento. 

Si possono fare delle assunzioni abbastanza significative attraverso il concetto di **collo di bottiglia**: la prestazione che si ottiene nel collegamento con capacità più piccola si ottiene calcolando il minimo tra le capacità dei collegamenti che creano il mio percorso. Questo significa che il rallentamento peggiore è quello che alla fine regola la velocità del trasporto.

Viene fatto un calcolo di questo tipo per capire che prestazione fornisce una rete, in base a quanto io abbia bisogno di rete in un determinato momento. Di fatto dopo un analisi di questo tipo si possono spostare o modificare delle connessioni sulla rete per migliorare questo blocco o by-passarlo. 

Le grandi aziende utilizzano questo valore per identificare le zone migliori dove posizionare i data-center.

Per migliorare il throughput si possono pensare a vie diverse per raggiungere quella posizione, oppure portare un miglioramento alla linea di contatto peggiore.

Es:

<img src="image/image-20211004113912899.png" alt="image-20211004113912899" style="zoom:67%;" />

Il throughput si calcola trovando il minimo tra $R_c, R_s, R/6$.



## 1.5 Livelli di protocollo

Le reti sono molto complesse e comprendono un sacco di device, per questo motivo dagli anni 70 hanno iniziato a introdurre una tassonomia della rete. 

Questa organizzazione della rete si basa su una serie di operazioni sequenziali che hanno lo scopo di fornire valori alla prossima operazione. Tutte queste azioni in ordine un pochino più furbo. 

Queste operazioni si identificano come dei livelli, e ognuno di questi può parlare solo con il precedente o il successivo.

Inoltre questo modo di dividere le operazioni serve anche per semplificare quello che, nel suo complesso, sarebbe un problema troppo grande e troppo specifico da risolvere. In questo modo, come l’architettura dei computer, si ottiene un modello che fornisce una grandissima elasticità di applicazioni senza perdere nessuna parte del percorso.

In alcuni casi però la generalizzazione è un lato negativo. Ad esempio nel caso io voglia un sistema completamente off-shore, con la necessità di avere la batteria più ottimizzata possibile e è necessario avere dei vincoli prestazionali non indifferenti, questa struttura non è la soluzione migliore. Di solito in questi casi si tende a sviluppare una tecnologia apposita per garantire questi dati, quindi di fatto questa struttura per un elemento del genere non fornisce un aiuto, ma crea più un problema che altro. (Ci sono degli elementi in cui questo modello a strati viene “schiacciato” per garantire i vincoli prestazionali).



## 1.6 Pila di protocolli internet: Modello TCP/IP

Questa astrazione è la pila responsabile per il funzionamento di internet.

A ogni livello appartiene una funzionalità particolare specifica, in modo che ognuno abbia il suo compito principale. Di solito l’utente o il software che utilizza la rete si appoggia a questa pila, ovvero entra in contatto con l’astrazione della rete. 

L’accesso alle funzioni di questa pila sono offerti all’interno di ogni host e sono responsabili di fornire le funzionalità di base dei servizi per collegarsi a internet. Le parti di questa pila sono direttamente inserite nel sistema operativo, non entrano mai in contatto diretto con l’utente finale. 

Ogni livello di questa pila si può interfacciare **solo** con i livelli successivi o precedenti. Inoltre, lasciando questa struttura così generalizzata, si possono aggiungere o rimuovere eventuali altri livelli per aggiungere funzionalità o altro. 

La **pila TCP/IP** è costruita in questo modo:

1. **fisico**: questo livello è responsabile di convertire la sequenza di bit creata nel link come una frequenza d’onda che si può propagare. Esempio: la luce della fibra che segnala 1 se luce e 0 se non luce.

   Questa sezione è importante soprattutto perché ci sono delle teorie per trasportare più dati nella stessa frequenza d’onda utilizzando delle tecniche sviluppate e molto avanzate.

2. **link (collegamento)**: instradamento dei pacchetti attraverso la serie di commutatori di pacchetto. Questo permette di avere un percorso fatto anche con tecnologie diverse, e si può utilizzare attraverso l’instradamento di pacchetto. Di fatto questo livello gestisce il collegamento da un nodo all’altro.

3. **rete**:instradamento dei datagrammi dall’origine al destinatario, ovvero crea il collegamento per il livello di trasporto, creando la strada svolgendo una ricerca tramite grafo e di fatto creando la “strada” da sorgente a destinatario. 

4. **trasporto**: il livello di trasporto costruisce quella connessione virtuale che mi permette di vedere direttamente il server, in cui non si percepisce la rete, ma si ottiene semplicemente il contatto che viene tipo “oscurato” come fosse un unico tunnel. 

   Costruisce uno o più percorsi attraverso la rete per raggiungere l’host cercato. Questo layer lavora con il più basso layer di rete per fornire la connessione. (fornisce il collegamento host-to-host)

5. **applicazione**: supporta l’applicazione di rete, ovvero applicazioni che si interfacciano con la rete. Questo livello mi permette di avere un linguaggio univoco comprensibile a ogni macchina. 

   Il sistema operativo fornisce questa interfaccia per comunicare con la rete a alto livello, ma è anche l’unico layer che si può bypassare, interagendo direttamente con il layer di trasporto.

   Esempi di applicazioni di rete sono i numerosi protocolli, ad esempio: FTP, SMTP, HTTP, HTTPS, ...

![image-20211004181929759](image/image-20211004181929759.png)

Questi livelli sono tutti necessari per la comunicazione e siccome ogni livello si interfaccia solo con il layer sopra o quello sotto, è necessario un approccio top-down-bottom-up per la comunicazione tra due host (per garantire **servizio, stratificazione e incapsulamento**).

Ogni livello non fa altro che prendere i dati ottenuti dal layer superiore e incapsularli in un pacchetto creato in modo che quando questi dati verranno forniti allo stesso livello dell’altro host, allora il primo è in grado di comunicare in maniera univoca con il secondo. L’intestazione fornita dal livello di trasporto, chiamata *header* viene creata e “aggiunta” al pacchetto.

 Il messaggio dello strato applicazione insieme all’header di trasporto fornito nello strato trasporto si chiama **segmento** di trasporto.  

Il protocollo di rete trasferisce il segmento da un host all’altro trovando il percorso. Il tipo di dati che vengono scambiati però viene chiamato **datagramma di rete**. Questo datagramma ha due intestazioni: una esterna che è a livello di rete, e una interna del livello di trasporto (ma questa non è importante per il suo scopo).

Il protocollo di link incapsula il datagramma di rete con l’header di link H per creare un **frame/trama**, responsabile poi di viaggiare verso la destinazione.

![image-20210928144728616](image/image-20210928144728616.png)

Una volta che il secondo host riceve questo frame, allora i dati ricevuti risalgono la pila protocollare (vengono “spacchettati”), ovvero vengono **de-incapsulati**. Ogni strato toglie il suo “strato” di dati e così facendo  fa i suoi controlli e passa la mole di dati al livello successivo.

Nonostante l’incapsulamento, ogni layer omologo ha l’impressione di parlare sullo stesso livello. Questi livelli riescono con questa tecnica a operare la comunicazione orizzontale, e quindi a comprendersi a vicenda.



I 5 livelli sono implementati in **ogni host** e funzionano in serie, un livello interagisce solo con il livello sottostante o il livello sovrastante. Una volta che si incapsula completamente, per recuperare il messaggio serve de-incapsulare completamente.



Questa pila viene chiamata **pila TCP/IP**, e è quella utilizzata normalmente.



## 1.7 Modelli di riferimento ISO/OSI

Questo è un modello di standardardizzato all’incirca nello stesso periodo del modello TCP/IP. Questo modello fornisce due layer in più, dando alcune informazioni e utilizzi utili ma non necessarie a tutti per lo stato di applicazione.

Nella pratica questo modello non è utilizzato, nonostante questo sia l’unico protocollato e studiato. Il modello TCP/IP è il modello *de facto*.

<img src="image/image-20211004183831775.png" alt="image-20211004183831775" style="zoom:50%;" />



## 1.8 Comunicazione tra host non direttamente collegati

Questo è come la comunicazione avviene tra due host direttamente collegati, ma come funziona il collegamento tra due host non direttamente collegati tra loro? 

<img src="image/image-20210928145303344.png" alt="image-20210928145303344" style="zoom:50%;" />

Gli host sono gli unici che hanno tutti e 5 i livelli. Gli switch e i router di rete non hanno tutta la pila, perché non hanno la necessità di “spacchettare” tutti i livelli del messaggio ma lavorano solo a livello 3, quindi instradando il traffico attraverso il minor numero di informazioni necessarie. Il fatto che il router non abbia il livello 4 e il 5 è dimostrazione del fatto che non deve interferire nei dati effettivamente comunicati. Se un router svolge questa azione non è più definibile router (non nel senso tradizionale almeno).

Inoltre sul router per costruzione non ha applicazioni a cui fornisce supporto, motivo per cui non gli serve tutta la pila TCP/IP.

Lo switch può ricevere fino a livello 2, motivo per cui viene anche chiamato commutatore.



## 1.9 Sicurezza di Rete

Quando sono strati creati i primi protocolli le persone collegate non si preoccupavano per la sicurezza, perché gli unici che la utilizzavano erano quelli a cui serviva, ed era impensabile che queste persone la attaccassero.

Questo punto di vista tenuto durante l’ideazione della rete internet ha poi dato problemi quando, diventando pubblica, c’è stato sempre più la necessità di confidenzialità delle informazioni, protezione della privacy o controlli sulla modifica del pacchetto.

Per mettere qualche soluzione a questi problemi sono state predisposte delle “patch”, ovvero delle sezioni di codice modificate per aggiungere queste funzionalità e risolvere in parte questi problemi.

Nonostante questo la sicurezza rimane un problema, perché ogni modifica permette la possibilità di errore nel codice.

I problemi più comuni sono:

* Malware che possono raggiungere host in modo nascosto e aprire la comunicazione per controllare il computer obiettivo (possono essere virus, worm o cavalli di troia)

  Sempre più utilizzato per spionaggio con il fine di memorizzare le informazioni del computer e mantenute sul computer (questi modelli cercano di essere il più passivi possibili, per non essere scoperti).

* L’attacco più comune sulla rete, che sfrutta la rete stessa è il DOS (Denial of Services), che punta a bloccare un server “bombardardolo” di richieste. Questo attacco è facilmente risolvibile attraverso firewall più controllati e black-list degli ip. 

  Più complicato invece sono gli attacchi DDoS(Distruibuited Denial of Service). Questi attacchi creano una rete di “bot”, ovvero computer infettati con uno specifico virus, che viene attivato/si attiva per fare richieste verosimili al server. Logicamente in questo caso è più difficile porre rimedio, perché si parla di milioni di IP e non si riesce a distinguere il normale utilizzo dall'effettivo attacco da parte di ogni host collegato.

  Spesso questi gruppi di bot vengono raggruppati in una vera e propria rete virtuale chiamata *botnet*.

* Il malware è spesso autoreplicante, ovvero un host infettato può passare lo stesso virus a altri host sulla stessa rete.

  <img src="image/image-20211004185549353.png" alt="image-20211004185549353" style="zoom:50%;" />

* Packet sniffing: si tratta di una tecnica per intercettare i pacchetti sfruttando il fatto che in una rete wifi tutte le comunicazioni (potenzialmente) sono ascoltate da ogni host sulla rete, e i pacchetti vengono accolti solo se destinati all'host in ascolto (questa tecnica di comunicazione “in comune” si chiama broadcast). 

  Di solito queste tecniche sono molto utili soprattutto nella fase di reconassance, per capire che tipo di tecnologie sono utilizzate ed eventualmente ottenere dei dati comunicati in chiaro.

  Ad oggi ogni connessione viene “spinta” per essere privata, adottando il protocollo **https** che fornisce un layer di sicurezza solitamente abbastanza buono per proteggersi da una scansione casuale.

  Questo attacco permette il *packet spoofing* ovvero l’imitazione di una serie di pacchetti con il fine di garantire l’accesso.

Di fatto molti software utilizzati in questi attacchi diventano vettori di attacco solo se utilizzati per questi scopi, quindi tutto diventa cattivo se lo si utilizza per gli scopi sbagliati.



## 1.10 Storia di Internet

* $1961$: Kleinrock nella sua tesi di laurea dimostra l’efficacia delle reti a commutazione di pacchetto rispetto a quelle a commutazione di circuito

* $1964$: Baran, visti questi studi, propone l’utilizzo delle tecniche di commutazione di pacchetto nelle reti militari, siccome erano più sicure ed era tempo di guerra fredda.

* $1967$: Nasce ARPAnet, sviluppato dalla ricerca avanzata per l’esercito statunitense (ARPA: Advance Research Project Agency). L’applicazione doveva servire per un login remoto.

* $1969$: il primo nodo operativo di ARPAnet viene creato

* $1972$: viene fatta la dimostrazione pubblica di ARPAnet, con un primo protocollo elementare per gestire il traffico tra i nodi e il primo programma di *posta elettronica* (programma che diventerà predominante nella rete per qualche tempo). ARPAnet cresce fino a 15 nodi.

* **Iniziano a nascere le prime reti proprietarie**

* $1970$: viene creata la prima connessione internet satellitare chiamata ALOHAnet per collegare le università delle Hawaii.

* $1974$: Cerf e Kahn creano l’architettura per l’interconnessione delle reti, utilizzando una serie di basilari concetti come:

  * Per avere una rete più aperta possibile, si accetta di avere una rete *best-effort*, ovvero che cerca di erogare un servizio al meglio possibile in base alla sua capacità. 
  * Tutti i collegamenti vengono fatti in modo da lasciare ogni nodo il più autonomo e minimale possibile, in modo da evitare una serie di collegamenti interni. 
  * L’introduzione nella rete dei router *stateless* ovvero che non ricordano i pacchetti che ci sono passati attraverso, fornendo un controllo decentralizzato.
  * questi principi definiranno poi l’architettura della rete che tuttora abbiamo.

* $1976$: Ethernet allo Xerox PARC

* con la fine degli anni ‘70 si elaborano una serie di architetture proprietarie per il funzionamento della rete.

* $1979$: ARPAnet festeggia i 200 nodi.

* **Vengono elaborati nuovi protocolli, modelli e le reti prolificano**

* $1983$: rilascio del controllo protocollare della pila TCP/IP (protocolli di rete tutt’ora validi).

* $1982$: creazione del protocollo `SMTP` per la posta elettronica (anche questo tutt'ora utilizzato).

* $1983$: definizione del DNS per la traduzione degli indirizzi IP.

* $1985$: definizione del protocollo `FTP`

* $1988$: introduzione del controllo di congestione TCP.

  Questa operazione viene svolta dando agli host il controllo sullo stato della rete, impedendo quindi l’immissione di troppi dati su un solo nodo. Molte di queste scelte sono fatte per mantenere la rete nella situazione più facile possibile nel core, portando tutti i servizi sugli host e ai margini della rete e quindi mantenendo l’interno molto flessibile e agile.

* Si iniziano a creare nuove reti nazionali e di ricerca. Si contano circa 100.000 hosts collegati.

* **Commercializzazione di internet, Web e nuove applicazioni**

* $\text{Primi anni '90}$: ARPAnet viene dismessa. Viene creato il **WEB**:

  * ipertestualità come modello di comunicazione internet per le pagine web
  * Berners-Lee elabora HTML e HTTP per questo scopo
  * Si creano i primi browser (al tempo solo visualizzatori web) come Netscape.

* $1991$: NSF (la rende rete di ricerca) fa cadere i diritti per l’utilizzo commerciale. Da questo punto poi si iniziano a creare una serie di protocolli per la comunicazione web e interazione client-server (protocollo http) e la commercializzazione della rete internet e delle applicazioni a essa collegata.

* $\text{fine anni ‘90 - 2007}$ Iniziano le prime applicazioni che svolgono applicazioni fortemente collegate a internet:

  * Prime applicazioni *“killer”* che implementano la messaggistica istantanea e condivisione di file peer-to-peer. 
  * Le dorsali di fibra transoceaniche vengono rafforzate e migliorate notevolmente (nell’ordine del Gbps).
  * Si inizia a parlare di sicurezza di rete
  * Si contano 50 milioni di hosts e oltre 100 milioni di utenti

* $\text{Dal 2005 al tempo presente}$: Scalabilità, SDN, mobilità e Cloud:

  * $2008$: Si inizia a sviluppare il **sofware-define networking**, metodo di creare reti molto avanzato.
  * Si ha sempre più spesso una rete AAA (anyone, anytime, anywhere) con le tecnologie WIFI e 4G ad alta velocità.
  * I grandi fornitori di servizi (Google, Facebook, Microsoft) iniziano a creare le loro reti, bypassando le reti commerciali per portare i dati più vicino all’utente, evitando problemi di congestione, migliorando la qualità del servizio, avendo più banda disponibile e staccandosi dal concetto di servizio centralizzato. Si creano quindi degli **accessi** sempre più **istantanei**.
  * Le applicazioni puntano sempre più ai servizi in **cloud**: più semplici da sviluppare delle tecnologie client-server, molto più convenienti e facili da gestire, oltre che essere molto più flessibili e scalabili.
  * Iniziano a essere collegati sempre più dispositivi mobili che fissi: si punta sempre più una connessione in mobilità. Si stimano circa 18B di dispositivi collegati a internet. 
  * Gli oggetti collegati a internet hanno anche un utilizzo dei dati e un utilizzo della banda molto diverso dagli utenti normali, imponendo delle variazioni delle caratteristiche della rete e della banda di ingresso. Adesso l’internet è ovunque, con i dispositivi **IoT**.

* Dopo il 2018 c’è sempre più la tendenza ad utilizzare tecnologie che portano verso il  **cloud-edge continuum**,  ovvero l’utilizzo di una struttura cloud di base, con un metodo per fornire i dati in modo non centralizzato e quindi più vicino agli utenti finali, garantendo in questo modo un servizio migliore e più rapido. 

  Questo concetto estremizzato forse porterà nel futuro un nuovo concetto di rete basato sull’integrazione integrazione continua tra edge-cloud e CDN che avvicinano sempre più i dati all’utente finale.



# Capitolo 2: Livello applicazione

## 2.1 Principi delle applicazioni di rete

Nonostante il nome, questo livello non si occupa di applicazioni che *usano* internet per funzionare, ma quello strato che permette alle applicazioni di comunicare in modo facile e generico.

Creare applicazioni di rete significa creare programmi che girano su sistemi terminali diversi e comunicano attraverso la rete, come ad esempio le comunicazioni di un software web, oppure un applicazione di messaggistica piuttosto che *youtube*.

Per fare questo è necessario che questi programmi possano girare su più macchine differenti, in modo da garantire la continuità del servizio e soprattutto la completa compatibilità con gli altri oggetti sulla rete. Per ottenere questo risultato si è appunto creata questa astrazione generica che riesce a interfacciarsi con tutti i dispositivi possibili.

Inoltre il fatto che si sfruttano questi programmi permette che non si debba pensare a tutte le macchine nel percorso fino all’utente finale, permettendo quindi uno sviluppo più facile.

Inoltre il lato positivo di avere un sistema stratificato come il modello TCP/IP è che lo studio di un singolo livello ci consente di concentrarci solo su quello che questo livello compie, sapendo che può al massimo chiedere informazioni al livello sottostante (il livello di trasporto) o a quello successivo (il livello di rete). Il livello sottostante fornisce dei servizi esposti a questo.

Ci possono essere più **architetture delle applicazioni di rete**:

1. *client-server*

   Con questa architettura si dispone di un server sempre attivo, mentre il client si può accendere on-demand in base all’utente per richiedere degli elementi presenti sul server. 

2. *peer-to-peer*

   In questa architettura non ci sono dei server sempre attivi, ma le connessioni sono sempre tra due dispositivi alla pari, ossia che fungono da client e da server nello stesso momento entrambi. Questa rete riesce a aumentare di capacità all’aumentare del numero di nodi che ne fanno parte. 

   Viene ritenuto il metodo migliore per diffondere contenuti nel modo più rapido possibile, perché si decentralizza il luogo da cui ottenere risposta e quindi si riesce sempre ad appoggiarsi al più vicino nodo della rete. Inoltre è facilmente scalabile, siccome è necessario solamente aggiungere nodi alla rete.

   Logicamente questo tipo di rete non ha solo lati positivi, infatti c’è un problema in particolare che ne ha impedito la diffusione e la standardizzazione per tutti i servizi: la manutenzione. Per implicita costruzione di questa rete infatti tutti i nodi sono “dispersi” nella rete (questa rete è caratterizzata infatti dalla caoticità dei nodi), rendendo difficile e costosa la manutenzione. Inoltre, essendo spesso i peer (gli host che si connettono alla rete peer-to-peer) si possono anche staccare da una rete o non fornire una banda accettabilmente alta da consentire lo scambio di informazioni, rendendo quindi alcune informazioni (se non ancora rilasciate su tutta la rete) inacessibili.

   Esistono dei modi per cercare delle informazioni nella rete peer-to-peer senza indice, ma per il momento questi metodi funzionano, ma senza garanzie di prestazioni.

3. *architetture ibride* (client-server e P2P): architetture che utilizzano entrambi i tipi in funzioni differenti (di solito per ottenere il meglio di entrambe le architetture).

   Spesso è utilizzata nelle applicazioni per parti diverse della connessione o utilizzi specifici. Oppure è utile in alcune applicazioni particolari tipo la messaggistica istantanea, che utilizza un server centralizzato per conoscere gli indirizzi degli altri contatti, per poi avere una connessione P2P con loro.

4. *cloud computing*

   Questa architettura è un aggiunta al tipo client-server. 

   Questa architettura è molto scelta ad oggi per diversi motivi:

   * la applicazione di questo modello a qualsiasi applicazione è molto semplice

   * permette la decentralizzazione del servizio completamente a carico del fornitore del servizio di cloud, permettendo quindi a un semplice utilizzatore di avere un applicazione molto performante in (in un certo modo) pochissimi passi.

   * fornisce all’acquirente una potenza di calcolo facilemente scalabile, ovvero possiede una capacità computazionale on-demand. Si fornisce la potenza necessaria al minimo funzionamento, ma poi si può tranquillamente espandere nel momento in cui si ottiene troppo lavoro da dividere solo su quella macchina.

     Questa opzione di solito è mediata completamente dal data-center a cui ci si appoggia per questo servizio, lasciando all’utente finale una configurazione minima, e un ottimo servizio in caso di picchi di traffico. 

     Questa operazione viene fatta dai fornitori semplicemente vedendo il data-center come un “aggregato” di CPU e memoria, e avendo quindi la modalità per accedere a queste risorse in blocco.

   * L’insieme di tutte queste tecniche permette di avere una serie di operazioni completamente semplificate, lasciando tantissime operazioni di gestione al data-center.

   * operazioni di backup sono automatici e completamente disponibili online

   Di fatto però questa architettura ha anche alcuni lati negativi, ad esempio non posso mai sapere dove fisicamente i miei dati sono conservati, ma sono nella posizione poco specifica di “tutto” il data-center.



Di fatto esiste anche una rete interna al computer che permette al connessione di servizi all’interno del computer, ma di solito queste comunicazioni vengono svolte a basso livello dal sistema operativo, non attraverso una rete effettiva, e vengono chiamati **schemi interprocesso**. Nonostante questo si può anche impostare un servizio che parla con la macchina attraverso un interfaccia internet (ad esempio `localhost` o `127.0.0.1`).

Di solito c’è scambio di messaggi di due host diversi attraverso la rete.

Al giorno d’oggi si tende quasi sempre a fornire applicazioni client-server, ovvero una connessione creata da due host, uno responsabile di fare le richieste e l’altro a risponderle. Spesso questi servizi poi sono posizionati in un infrastruttura del cloud.



Le **socket** sono un architettura software che serve per dare un interfaccia di programmazione o comunque di accesso alla pila di rete. Di solito ci sono librerie che forniscono questa applicazione già pronta a essere istanziata.

Questa interfaccia è analoga a una porta:

* se un processo vuole inviare un messaggio lo fa inviare attraverso le sue applicazioni con il sistema operativo.
* presuppone l’esistenza di un’infrastruttura esterna che fornirà il servizio per trasportare il messaggio fino alla destinazione.

Il socket di solito fornisce semplicemente un’API che si connette direttamente con l’implementazione della pila di internet fornita dal sistema operativo.

Per fare in modo che un messaggio possa essere inviato a un altro host il mittente deve identificare il processo destinatario. Il problema è che entrambi possono avere più servizi che stanno lavorando o che hanno necessità di un connessione con un altro host. Per questo motivo l’identificativo della macchina **IP** univoco a 32 bit è sempre associato a un **numero di porta** che a sua volta è associato a un processo in esecuzione sull’host.

```bash
indirizzo=128.119.10.10 porta=80
```

Alcuni numeri di porte sono state impostate di default, ad esempio HTTP lavora sulla porta 80, i server mail sulla 25. Le prime 1000 porte sono riservate e spesso pre-assegnate a dei servizi. Il client ha la possibilità di accesso solo alle porte con numero superiore a 1000.



Ci sono dei **protocolli a livello applicazione** che servono a definire:

* i **tipi** di messaggi scambiati
* la **sintassi** dei messaggi scambiati, ovvero quali e come sono fatti i campi che sono necessari al messaggio
* **semantica** dei campi, ovvero il significato delle informazioni presenti in ogni campo
* **regole** per determinare come e quando un processo invia e risponde ai messaggi

Ci sono **protocolli di pubblico dominio** che vengono definiti (pubblicamente) nelle RFC (Request For Comments), che descrivono tutti i punti importanti di funzionamento del protocollo da un punto di vista puramente tecnico, specificando ogni minimo particolare. Questi protocolli consentono interoperabilità (essendo liberi tutti possono utilizzarli) e sono ad esempio HTTP o SMTP.

Altri tipi di protocollo possono essere **proprietari**, e si ottengono informazioni sul loro utilizzo solo attraverso *reverse engineering* o con documenti pubblici forniti dalle aziende.

Il **servizio di trasporto** da scegliere per un’applicazione dipende solo da che utilizzo ho di questa applicazione e di che compito devo svolgere (quindi che necessità ha). In base a questo concetti si possono avere delle necessità, riassumibili come:

* **perdita di dati**: bisogna capire se l’applicazione può tollerare qualche perdita di dati, oppure se ha la necessita di garantire un trasferimento affidabile di tutti i file passati attraverso la rete.

  In base a questo concetto di solito si propende per il servizio TCP che fornisce una garanzia sul ricevimento effettivo dei pacchetti.

* **throughtput**: alcune applicazioni hanno la necessità di una grande larghezza di banda, oppure tutta la larghezza disponibile. Altre invece necessitano di un ampiezza di banda minima per essere efficaci.

  Di solito un browser web non ha grandi limitazioni da questo punto di vista, cercando sempre di ottenere i dati in qualsiasi situazione di capacità della rete, mentre alcune applicazioni di streaming necessitano di un certo throughput per essere consistenti o utilizzabili.

* **sicurezza**: alcune applicazioni particolarmente sensibili hanno la richiesta della sicurezza, che ad oggi si può ottenere aggiungendo un elaborazione a livello software applicata ai dati ottenuti o offerti dalla/alla pila protocollare. Oppure si può utilizzare nuovi protocolli che permettono una connessione sicura aggiungendo di default alcuni strati alla pila.
* **temporizzazione**: alcune applicazioni hanno la necessita di essere “realistiche”, ovvero richiedono il più basso valore di ritardo possibile. Questa necessità però non è coperta da nessun tipo di servizio, nonostante si utilizzi la UDP siccome, non avendo una serie di controlli propri del servizio TCP, permette di essere più rapida nel collegamento.



Il livello di trasporto offre due servizi, in particolare, TCP e UDP. Il loro utilizzo dipende dal modello di applicazione che ho la necessità di creare: se io voglio navigare su internet necessito che la maggioranza dei dati siano corretti (anche a scapito della velocità). Altre applicazioni invece permettono di perdere dei dati (ad esempio la telefonia), riuscendo però a continuare la fornitura del servizio (infatti la comunicazione di voce non viene così rovinata da una perdita fino al 10%dei dati originali). 

![image-20211005182259302](image/image-20211005182259302.png)



### TCP

Il servizio TCP è utilizzato da gran parte delle connessioni a Internet, siccome ha come punto di forza la sicurezza della ricezione delle informazioni.

è caratterizzato da:

* **orientamento verso la connessione**: è richiesto un setup minimo tra client e server con il fine di avere un canale di comunicazione fisso e stabile, ovvero la verifica che il ricevente è attivo e disponibile alla ricezione per avere la garanzia di un traffico efficace.
* **trasporto affidabile**: controlla i processi di invio e ricezione per assicurare il corretto trasferimento dei pacchetti.
* capacità di **controllare il flusso**: il mittente (chi manda i dati) ha un meccanismo per cui riesce a conoscere lo stato del buffer del destinatario e quindi a non sovraccaricarlo.
* capacità di **controllare la congestione**: riesce a ottenere informazioni sullo stato di carico della rete, e quindi agire sui pacchetti in uscita per evitare di congestionare ulteriormente la comunicazione (eventualmente strozzando il traffico in uscita). Questo processo ha l’effetto collaterale di togliere il controllo sull’invio dei pacchetti all’utente.

Questo servizio però **non fornisce garanzie di temporizzazione** (anzi, spesso può subire dei rallentamenti per il controllo anticongestione in uscita) .

Di solito viene utilizzato nel browsing web o nella posta elettronica. Nonostante sembri sbagliato, anche lo streaming video sfrutta il TCP, siccome si vuole che questa grande quantità di dati non congestioni la rete. Per ovviare al questo problema si mettono dei convertitori che riescono a mandare il video nella rete a qualità differenti in base alla larghezza di banda disponibile.



### UDP:

Il servizio di UDP non fornisce nessun controllo fornito dal TCP, ma permette un **trasferimento dati inaffidabile** fra i processi d’invio e ricezione.

Questo servizio è nato per la necessità di avere un traffico a livello più basso possibile, infatti dovendo passare meno layer di controllo si ha la possibilità di avere più controllo sul pacchetto in uscita. Di fatto è come un protocollo più semplice, senza nessuna garanzia e nessun controllo.

Altre applicazioni sono ad esempio nella sensoristica, che utilizza questi servizi per evitare qualche gradino della pila TCP, che influirebbe sulle prestazioni o sulla batteria.

Viene anche utilizzato nella telefonia su internet, che ha la necessità di avere il ritardo più basso possibile.

Date le sue particolari caratteristiche del TCP (soprattutto senza il controllo di congestione) alcuni pacchetti UDP (troppo numerosi) possono essere droppati da nodi interni di controllo.

![image-20211005184222763](image/image-20211005184222763.png)

### TCP sicuro

Siccome le connessioni sicure sono sempre più importanti, ma la possibilità di avere un layer che fornisce sicurezza non è definito in nessun servizio (né TCP, né UDP), si è creato un nuovo servizio standard applicando degli aggiuntivi layer al servizio (preesistente) TCP.

Le applicazioni vanilla TCP/UDP non hanno crittografia. Quindi vuol dire che tutti i dati mandati e ricevuti sono mandati in chiaro (comprese le password).

Si utilizza quindi il **TLS (Transport Layer Security)** che si occupa di fornire connessioni TCP criptate per la comunicazione sicure di dati. Inoltre si occupa di *data integrity*, ovvero controlla che il pacchetto non sia stato in nessun modo modificato nel percorso dal mittente a ricevente, e fornisce il servizio di *end-point authetication*, ovvero la verifica che l’host con cui si sta parlando sia effettivamente chi dichiara di essere (questa misura rende gli attacchi di impersonificazione molto improbabili). 

Questo servizio è utilizzato da protocolli tipo l’`https` o l’`ssh` che  utilizzano il `TLS` proprio in modo da avere tutte le garanzie di questo servizio.

Questo layer è il responsabile di blocco in caso di rejecting site, ovvero quando viene annullata la connessione a un server che non dispone di certificati riconosciuti oppure inaffidabili (spesso ormai queste richieste vengono fatte direttamente dal browser). Questo tipo di blocco può capitare anche a siti che non hanno nessun problema di sicurezza, ma che semplicemente non hanno servito i certificati aggiornati o verificati. 

Il TLS può essere anche implementato a livello applicazione, utilizzando delle apposite librerie che si appoggiano al TCP, facendo in modo che in questo socket il traffico in uscita sia criptato. (Argomento molto complicato, che necessita di numerose conoscenza pregresse. Viene trattato nel capitolo 8 del libro).



## 2.2 Web e HTTP

Una pagina web è costituita da molti oggetti diversi, che possono essere un file HTML (Hypertext Markup Language, responsabile della descrizione della struttura della pagina, ovvero indica cosa contiene e una struttura indicativa), e una altra serie di file che possono essere foto, video e altri tipi di supporto:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Questo spazio indica il titolo della pagina</title>
  </head>
  <body>
    All'interno di questa sezione ci sono i testi e gli elementi che compaiono nella pagina.
    Queste invece sono immagini e altri documenti che possono essere inseriti e verranno richiesti al server:
    <img src="image.png" alt="Image" align="middle" />
    <img src="image.png" alt="Image" width="100" />
    Audio
    <audio controls>
  	<source src="tutorial.ogg" type="audio/ogg" />
  	<source src="tutorial.mp3" type="audio/mpeg" />
  	Your browser does not support the audio element.
		</audio>

  </body>
</html>

```

Questo file HTML è la base della pagina, che contiene diversi oggetti referenziati. Ogni oggetto è referenziato da un **URL (Universal Resource Locator)**, ovvero il modello universale per indicare la localizzazione di un oggetto sulla rete.

L’URL di solito è formato da: (esempio con `www.someschool.edu/someDept/home.html`):

* `www.someschool.edu`: indica il nome dell’host
* `/someDept/home.html`: indica il nome del percorso

Il file HTML viene poi dato al browser che fornisce la rappresentazione della pagina con gli elementi descritti al suo interno (effettua il rendering).

(Sia chiaro che l’HTML è il formato con cui si scrivono come sono fatte le pagine web, non il protocollo con cui questi file vengono scambiati, che invece si chiama HTTP).

### HTTP

Questo protocollo si chiama **HyperText Transfer Protocol** e serve per scambiare file HTML tra due host. Di solito questa connessione è di tipo client-server, siccome uno scarica i file dall’altro. Dopo questa operazione questi file vengono visualizzati dal browser. 

Questo protocollo si appoggia al servizio TCP.

Di solito TCP fa uno scambio di pacchetto per assicurarsi la connessione e prosegue solo dopo a aprire la connessione logica con il server web. Tutte queste operazioni vengono fatte attraverso dei messaggi tra client e server. Questa socket utilizza la porta 80.

HTTP è dice protocollo **stateless**: ogni richiesta è a se e non viene mantenuto lo stato nel server che ha risposto a una richiesta. Questo vuo dire che per il server HTTP ogni richiesta è come un nuovo messaggio, senza avere una modifica delle pagine in base alla memoria della mia connessione. La scelta di creare questo protocollo in questo modo è stata fatta perché mantenere lo stato è più complesso e dispendioso di risorse per chi vuole fornire una risposta ottimale in un tempo accettabile. Inoltre nel caso di crash o errori da entrambe le parti si può avere la possibilità di avere due stati diversi e quindi non avere le connessioni sincronizzate (operazione non sempre facile soprattutto se si deve effettuare un unione dei due stati diversi).

Ci sono tecniche che permettono di fare delle pagine web a stato diverso in base all’utente utilizzatore, ma non attraverso il protocollo (ad esempio attraverso i cookie).

Di questo protocollo esistono diverse versioni, che forniscono gli host che utilizzano questi protocolli per comunicare con maggiore flessibilità o più velocemente.

Di base si hanno due tipi diversi di connessioni fornite dall’HTTP:

* **connessioni non persistenti**: forniscono la possibilità di scambio di un solo pacchetto e poi chiudono la connessione.

  Ottimale se si vuole ottenere solo un oggetto dalla zona richiesta.

* **connessioni persistenti**: forniscono una connessione con l’host che viene mantenuta per più pacchetti. Questa connessione verrà chiusa in condizioni particolari o in accordo con l’host connesso.

  Molto più comune nel web moderno, siccome uno stesso file HTML contiene molti più file al suo interno, che devono essere a loro volta scaricati, questo consente di scaricarli tutti utilizzando lo stesso “tunnel” e quindi non perdendo tempo a creare una nuova connessione con il server ogni pacchetto necessario.

Gli schemi di connessione HTTP con queste due modalità sono:

![image-20211005144701444](image/image-20211005144701444.png)

![image-20211005193823720](image/image-20211005193823720.png)

I passaggi *1a* e *1b* non avvengono a livello di protocollo, ma a livello di rete (sono create dal client TCP). Nel punto 3 il server analizza la risposta e se i dati sono disponibili e corretti risponde al messaggio, mentre qualsiasi altra risposta da queste presenti porteranno a una chiusura del collegamento. 

Alla fine di queste azioni la connessione termina (perché connessione non persistente). 

Alla necessità di un’altra richiesta al server si dovranno rifare tutti questi processi un’altra volta, siccome la rete è stateless e quindi non si ricorda la risposta precedentemente fornita.

![image-20211005145020605](image/image-20211005145020605.png)

A caratterizzare il tipo di protocollo utilizzato è anche **RTT (Round Trip Time)**, ovvero il tempo necessario da quando viene mandata la richiesta a quando si ottiene la risposta.

Il tempo totale di risposta poi si calcola, in connessione non persistente, come $2RTT + \text{tempo di trasmissione}$.

Infatti le connessioni non persistenti hanno, oltre alla lunghezza sempre finita di connessione per ottener un pacchetto (ma che viene sempre ripetuta completamente) la caratteristica di aggiungere overhead al sistema operativo per ogni connessione TCP. Questo però permette di avere connessioni multiple e parallele con il server per scaricare oggetti (o lo stesso oggetto più velocemente).

Le *connessioni persistenti* invece hanno un tempo di risposta per pacchetto di $RTT + tempo di trasmissione$, siccome il server lascia la connessione aperta dopo la ricezione del primo segnale, lasciando i successivi pacchetti viaggiare nello stesso “tunnel”. Il client quindi è libero di inviare richieste ogni volta che incontra un oggetto referenziato senza la necessità di aspettare la creazione di un nuovo layer di connessione.



Logicamente il lato positivo del secondo tipo di connessione si ottiene solo se si ha la necessità di fare più connessioni consecutive, perché in caso contrario si ha lo stesso tempo di risposta per ottenere i pacchetti.

Il lato negativo di connessioni continuative sono che le risorse sono bloccate dal lato server e in casi particolari il server può non riuscire più a soddisfare le richieste di tutti gli host in connessione.

Esistono anche dei pacchetti chiamati *keep-alive* con lo scopo di mantenere una rete attiva. Di solito viene utilizzato questo metodo per mantenere una connessione operativa anche se è presente scarso traffico su quella linea. Per questo motivo è molto spesso utilizzata da collegamenti tra strumenti non con richieste di utenti. Questo tipo di pacchetto è necessario siccome ogni rete ha un tempo oltre al quale droppa la connessione e se si vuole continuare a comunicare la si deve ricreare. Nell’utilizzo normale il servizio TCP si occupa di questa gestione.

#### Messaggi HTTP

Il protocollo HTTP è diviso in due tipi di messaggi: **richiesta** e **risposta**, inoltre sono tutti in **formato testo ASCII**, ovvero leggibili dagli utenti.

##### Richiesta HTTP

```http
GET /somedir/page.html HTTP/1.1
Host: www.someschool.edu
User-agent: Mozilla/4.0
Connection: close
Accept-language: fr

<body>
```

Dove nella prima riga sono presenti:

* la definizione del **METODO** (in questo caso `GET`)
* indicazione delle location **URL**
* **versione protocollo HTTP** (che può essere `1`, `1.1` o `2`) 

Seguono le **righe di intestazione**:

* `Host`: conferma l’host alla quale siamo collegati.
* `User-agent`: segnala l’engine che gestisce il browser, in modo da fornire il miglior formato HTML per il supporto richiesto.
* `Connection`: indica il tipo di connessione desiderata. Questo valore può essere `keep-alive` se si vuole mantenere aperta oppure `close` se invece si vuole chiudere la connessione. (`close` è default nelle richieste HTTP/1.0).
* `Accept-language`: indica la lingua della risposta, in modo da fornire dei dati più soddisfacenti al client (se disponibili). 
* infine per indicare la fine, si inserisce un `carriage return` e `line feed` extra che indicano che le informazioni della richiesta sono state inviate.
* `<body>`: indica la possibilità di dati associati alla richiesta fatta. Il fatto che questa parte ci sia o meno dipende dalla prima riga, che indica di conseguenza se aspettarsi o meno la ricezione di questo eventuale pacchetto.



##### Metodi HTTP

Ci sono diversi metodi di richiesta verso il server, che possono indicare il fatto che si vuole aggiungere della informazioni al server o che si vogliono fare altre operazioni. Questi metodi sono (dal protocollo `HTTP/1.0`):

* `GET`: indica che si vuole riceve un valore contenuto nel server
* `POST`: indica che si vuole aggiungere un valore al server (di solito utilizzato per fare l’upload di file e mandare form)
* `HEAD`:  si richiedono al server *solo* gli header del file, senza l’effettivo corpo.

dal protocollo `HTTP/1.1` in poi hanno aggiunto:

* `PUT` : si vuole fare una richiesta al server (ma a differenza di `POST`, più richieste uguali verranno scartate e se ne manterrà una sola)
* `DELETE`: si utilizza per cancellare dati dal server



*Per mandare un form perè ci sono altri metodi:*

* o utilizzo il metodo *`POST`* attraverso HTTP

* o utilizzo metodo `GET` attraverso `URL` 

  si utilizzava più un tempo, ma anche oggi si può incontrare questo metodo in qualche caso.

  ```http
  www.somesite.com/animalsearch?monkeys&banana
  ```

  Questo significa che se faccio un `GET` di questo URL allora sto facendo una ricerca e due parametri ricercati sono `mokeys` e `banana`. Di solito si utilizza spesso questo metodo se la pagina è “attiva”, ovvero che ci sono alcuni valori della pagina che possono cambiare in base alle richieste.




##### Messaggio di risposta HTTP

```http
HTTP/1.1 200 OK 
Connection close
Date: Thu, 06 Aug 1998 12:00:15 GMT 
Server: Apache/1.3.0 (Unix) 
Last-Modified: Mon, 22 Jun 1998
Content-Length: 6821
Content-Type: text/html

<data>
```

La prima riga indica:

* il **protocollo utilizzato**
* il **codice di stato** della richiesta

Poi seguono la serie di informazioni degli header, terminati da un `carriage return` e un `line feed` che indicano la fine degli header. Poi seguono tutti i dati di risposta.

I *codici di stato* della risposta HTTP sono diversi, e hanno tutti un significato e un uso ben preciso:

* `200 OK`: richiesta ha avuto successo, l’oggetto richiesto viene inviato
* `301 Moved Permanently`: l’oggetto richiesto è stato trasferito; la nuova posizione è specificata nell’intestazione `Location:` della risposta
* `400 Bad Request`: Il messaggio di richiesta non è stato compreso dal server 
* `404 Not Found`: Il documento richiesto non si trova su questo server
* `505 HTTP Version Not Supported`: Il server non ha la versione di protocollo HTTP

Questo collegamento si può provare a lato client con `telnet`, attraverso questo comando si può aprire una connessione con un server e una porta `telnet www.unitn.it 80`. Inserendo e mandando degli header HTTP si otterranno le risposte, come si può vedere da questo esempio:

```bash
telnet www.unitn.it 80
GET / HTTP/1.1
Host: www.unitn.it


HTTP/1.1 301 Moved Permanently
Server: nginx
Date: Mon, 11 Oct 2021 13:42:13 GMT
Content-Type: text/html
Content-Length: 162
Connection: keep-alive
Location: https://www.unitn.it/

<html>
<head><title>301 Moved Permanently</title></head>
<body>
<center><h1>301 Moved Permanently</h1></center>
<hr><center>nginx</center>
</body>
</html>
```

In questo caso ritorna un codice `301 Moved Permanently` siccome è un sito `https` e quindi dovrebbe essere sulla porta `443` definitivamente.



##### Cookies

Nonostante HTTP non permetta il mantenimento dello stato della connessione, si può ottenere questa funzione utilizzando i **cookies**. Questo valore viene inserito all’interno dell’header di qualche messaggio e contiene un valore univoco, conosciuto dal client e dal server. Questo valore incrociato con i risultati di un database permette la personalizzazione di una pagina web, fornendo gli stessi dati allo stesso valore di un determinato cookies.

I cookies sono utilizzati da molti dei più importanti siti web.

Un esempio di utilizzo:

![image-20211011154607082](image/image-20211011154607082.png)

Queste informazioni vengono chiaramente salvate sia da parte client e server. I dati salvati dalla parte client  sono facilmente accessibili dall’user e quindi facilmente eliminabili. Le informazioni salvate sul server invece sono tutte completamente sotto il controllo dell’amministratore del server e quindi il loro utilizzo può cambiare in base a che utilizzo ne fa l’azienda che lo controlla (ad esempio si possono sfruttare con il fine di profilare l’utente finale del servizio).

Questa funzione è molto utile, soprattutto nel caso di molte azioni sullo stesso sito che necessitano di una connessione autenticata, infatti permette di eseguire una serie di richieste al server senza la necessità di ripetere tutte le volte le informazioni di login.

Di solito i cookies hanno un tempo di vita, ma si può decidere di cancellarli manualmente.

I cookies funzionano attraverso 4 componenti principali:

1. una riga di intestazione nel messaggio di *risposta* HTTP
2. una riga di intestazione nel messaggio di *richiesta* HTTP
3. un file cookie mantenuto sul sistema terminale dell’utente e gestito dal browser dell’utente
4. un database sul sito

Al loro interno possono contenere una serie di informazioni, come:

* autorizzazione
* carta per acquisti 
* raccomandazioni 
* stato della sessione dell’utente (e-mail) 

Lo stato di queste informazioni, trasportato dalle richieste HTTP, servono per mantenere costante dei valori condivisi tra client e server che sono necessari alla comunicazione a stato costante.

Per la creazione di cookies sono necessarie delle azioni ben precise:

* il server risponde alla richiesta includendo nell’header HTTP il valore univoco di identificazione:

  ```http
  Set-cookie: <value>
  ```

* Il browser vedendo questo valore nell’header e aggiunge il valore di questo cookie nel suo gestore.

* ogni volta che il browser invia una richiesta al server, allora richiede al gestore dei cookies il valore cercato e lo inserisce nella richiesta HTTP attraverso l’header, con:

  ```http
  Cookie: <value>
  ```



##### Cache Web o server proxy

Un server proxy o un server di cache serve per mantenere una copia del sito che si vuole visitare più vicino all’utente finale, in modo da velocizzare la richiesta e/o oltrepassare dei colli di bottiglia che ci possono essere sulla rete esterna o direttamente sul collegamento principale.

Questo server funziona intercettando tutte le richieste HTTP (perché questo avvenga deve essere configurato sull’host o sul gestore della rete, in modo che ogni computer che si collega sappia di dover far passare tutto il traffico attraverso questo server). Una volta ricevute queste richieste cerca di esaudirle in locale, attraverso dati precedentemente scaricati (per questo motivo si deve avere una grande disponibilità di memoria).

Questa funzione ha una serie di lati positivi, ma lascia anche una serie di questioni irrisolte, ad esempio la distcrepanza che può crearsi tra sito reale e sito “copiato” in locale.

Infatti questo sistema permette da un lato di avere una connessione (risultanti) più performante, in modo da 1) non sovraccaricare la rete scaricando la stessa pagina molte volte, 2) riduce il tempo di risposta, 3) minimizzando l’utilizzo del collegamento alla rete, scaricando solo quello necessario alla risposta delle richieste.

Il funzionamento generale di questo server è:

1. il browser stabilisce una connessione TCP con il proxy, fornendo le indicazioni del sito che si vuole visitare
2. Il proxy controlla se è presente tra le copie memorizzate localmente, se lo è allora ritorna l’oggetto con un pacchetto HTTP
3. Se il server non ha l’oggetto, allora il proxy apre una connessione con il server finale, ottiene il valore cercato, ne salva una copia in locale e poi ritorna il pacchetto via HTTP al richiedente.

Di solito questi server sono mantenuti dagli ISP o da grandi centri di distribuzione, fornendo un modo per “salvare” delle connessioni particolarmente poco connesse o con una banda ridotta. In questo permettono di aumentare la banda disponibile e anche di ridurre i costi di gestione (siccome i pacchetti sono più vicini, il server e le comunicazioni saranno più veloci).

Questi utilizzi di server di cache sono molto utilizzati anche nelle infrastrutture di CDN (Content Distribution Networks), che utilizzano queste tecniche per velocizzare il servizio e la latenza media.

Un esempi di utilizzo server di cache in modo utile:

![image-20211011160844590](image/image-20211011160844590.png)

![image-20211011191042019](image/image-20211011191042019.png)

![image-20211011191057324](image/image-20211011191057324.png)

Nonostante questo operazioni portino moltissimi lati positivi per l’utente finale, il server si deve occupare di mantenere la copia aggiornata rispetto al valore reale. Per favorire questa operazione nell’header del HTTP c’è un valore che serve a verificare se il valore di cache è aggiornato. Questo meccanismo di controllo è chiamato **conditional GET** (lo scopo logicamente è quello di fare meno richieste possibili all’esterno della rete).

Questa richiesta HTTP chiamata appunto conditional GET richiede al server finale solo queste informazioni:

```http
GET / HTTP/1.1
Host: www.get-this-host-down.com
If-modified-since: Wed, 9 Sep 2015 09:23:24 
```

Ovvero la data che il proxy ha salvato la pagina in locale. Se il server nota un cambiamento della pagina tra questa data e la versione attuale, allora risponde con un oggetto, in caso contrario risponde con un codice `304 Not Modified` e quindi il proxy mantiene la copia originale.

Altri accorgimenti da parte del server possono essere introdotti con questi altri valori inseriti nella risposta:

```http
Cache-Control: max-age-<seconds>
Cache-Control: no-cache
```

che informano l’host se consentire di effettuare la cache, e in caso per quanto è consigliato mantenerla/dopo quanto è consigliato aggiornarla.

Il proxy può fare questo tipo di controllo tutte le volte che si desidera in base a quanto sia importante avere dei dati aggiornati piuttosto che veloci. 

Con un proxy di cache comunque ci può essere un piccolo intervallo di tempo in cui i dati sono “antichi”, ovvero non sono coerenti con il buffer temporale di adesso (si aggiornano semplicemente aggiornando la pagina, perché una volta che si richiede nuovamente questo sito, il server avrà fatto il controllo al server reale e, in caso, scaricato la copia originale e aggiornata).

Esempio di connessione *conditional GET*:

![image-20211011162620804](image/image-20211011162620804.png)

### Aggiornamenti e miglioramenti di HTTP

#### HTTP/1.1

Rispetto al protocollo HTTP/1 introduce la possibilità di avere una connessione aperta per tutta la durata di multipli trasferimenti, per velocizzare il download di contenuti di un unica pagina. (Get multipli in pipeline, ovvero su una singola connessione TCP).

Si inseriscono dei metodi e dei codici nuovi, come PUT e DELETE.



#### HTTP/2

Rispetto al protocollo HTTP/1.1, in questa versione, si cerca di diminuire il ritardo in caso di richieste di oggetti HTTP multipli. Tutti i comandi, codici e metodi del HTTP/1.1 rimangono invariati.

Questo protocollo cerca di risolvere un problema della rete per l’utilizzo di dati sempre più pesanti e grandi. Di solito i server funzionano con il metodo **FCFS (First Come First Served)**, ovvero viene servito il primo oggetto in cima alla coda di richieste. Questo può portare disfunzioni per gli altri utenti della rete, se uno di questi richiede un oggetto molto grande e quindi che necessita di molto tempo per la comunicazione, ancora superiore se connessione richiesta come TCP, che dovrebbe recuperare eventuali errori di trasmissione.

Questo problema si chiama **HOL blocking**, ovvero **head-of-line blocking**. 

Per risolverlo, si utilizza la divisione di oggetti molto grandi in frame, che vengono mandati con frequenza diversa, lasciando quindi lo spazio agli altri pacchetti di circolare. L’effetto finale è che il pacchetto più grande sembrerà leggermente più lento, ma la rete nella totalità sarà più reattiva.

Di fatto questo protocollo modifica l’ordine di importanza dei pacchetti e quindi l’ordine con cui vengono inviati.

![image-20211011163129591](image/image-20211011163129591.png)



#### HTTP/3

Il protocollo HTTP/2 viene presto soppiantato dal protocollo HTTP /3 che inserisce alcuni miglioramenti rispetto al predecessore (soprattutto sul lato sicurezza) e lavora sul protocollo (più veloce UDP, ma con il controllo della congestione dei pacchetti).

In realtà molte modifiche di questo protocollo non avvengono a livello applicazione, ma a livello di trasporto (e quindi verranno trattate più avanti nel corso).



### Evoluzione dal HTTP: protocollo QUIC

Data la difficoltà nell’aggiornare il protocollo HTTP, si è studiato un modo migliore per scambiare dati, avendo una serie di caratteristiche necessarie nel mondo di oggi.

Google lo ha inizialmente disegnato e proposto nel 2012, implementato subito come protocollo di connessione di Chrome per connettersi ai server Google (subito implementato anche da Firefox, Edge chrome version e Safari). 

Questo protocollo si basa sull’UDP, ma viene anche spesso nominato TCP/2 per le numerose somiglianze che alla fine si ottengono, siccome sono stati poi implementati una serie di controlli tipici del TCP.

QUIC implementa la possibilità di ridurre la latenza, attraverso la stima della banda disponibile da entrambe le direzioni, implementando algoritmi di nuova generazione per il controllo delle congestioni (sviluppato dal punto di vista users, che permette uno sviluppo più rapido). 

Questo protocollo supporta le connessioni sicure e multiplexate, in modo da fornire banda e sicurezza per una connessione sicura il più veloce possibile.



## 2.3 FTP

Il protocollo per trasferire i file, in maniera affidabile (in modo che i file siano uguali da server a client). Questo protocollo si appoggia a TCP.

Le informazioni di questo protocollo sono contenute nel `RFC: 959`.

Questo protocollo consente la connessione per scaricare dei file attraverso una connessione doppia: a porta `21` un gestore si interfaccia con il server, mentre i file veri e propri vengono spostati sfruttando la porta `22` (che viene aperta solo in caso di spostamento di file). 

Questo protocollo si utilizza tra client e server, ma spesso si utilizza un’interfaccia di più alto livello che permette di svolgere azioni senza interagire direttamente con questi due servizi.

Il fatto che le connessioni per la gestione e il passaggio di dati vengono divise tra due porte si oppone direttamente alla tecnica di HTTP di mandare dati (in unico pacchetto che contiene tutti i valori necessari).

Il server FTP mantiene lo stato della connessione: riesce a mantenere la directory corrente, le autenticazioni e altre informazioni utili.

I comandi più comuni sono inviati come test ASCII sulla connessione di controllo, e possono essere:

```http
USER username ; manda informazioni dell'user
PASS password ; manda informazioni della password
LIST 		  ; elenca gli elementi di una directory
RETR filename ; recupera il file nella direcoty corrente
STOR filename ; memorizza file nell'host remoto
```

mentre i codici di ritorno più comuni sono:

* `331 Username Ok password required`
* `125 data connection alredy open; transfer starting`
* `425 Can't open data connection`
* `452 Error writing file`



## 2.4 Posta Elettronica

Per il funzionamento della posta elettronica sono necessari 3 componenti principali: 

* **agente utente**: 
  * detto anche “mail reader”
  * ha la possibilità di comporre, editare e leggere i messaggi di posta elettronica (su un agente dedicato, come Outlook, Thunderbird, oppure il browser attraverso un interfaccia)
  * i messaggi in arrivo e in uscita sono memorizzati su un server
* **server di posta**: contiene la 3 componenti:
  * *casella di posta* (mailbox): contiene i messaggi in arrivo per l’utente
  * *coda di messaggi* da trasmettere
* **protocollo SMTP (Simple Mail Transfer Protocol)**: protocollo di base per consegnare le mail tra server di posta elettronica.
  * Sono lo stesso costituiti da server e client, (uno che manda e uno che riceve). Nonostante questa caratteristica i server SMTP possono essere bi-direzionali.
  * Utilizza il servizio TCP.
  * Il protocollo è definito dal `RFC 5321`

Le mail sono dei mezzi di comunicazione asincrona (le persone mandano e ricevono messaggi quando più gli conviene, senza doversi organizzare con altri server e users). Le mail sono veloci, facili da mandare e senza particolari costi rispetto alle poste. Inoltre le nuove versioni permettono anche l’invio di dati molto complicati, che possono includere allegati, testo in formato HTML e foto inserite all’interno. 

Nonostante sia un protocollo molto utilizzabile e a cui è stato aggiunto molto supporto nel corso degli anni, ci sono comunque una serie di restrizioni che sono portate dalla sua creazione, principale su tutto: trasmisione fissa a 7-bit ASCII char. Questa limitazione è particolarmente sentita oggi, siccome tutte le foto e le immagini, oltre che gran parte dei contenuti multimediali sono contenuti in un buffer di 8-bit.

Questo impone che il server codifichi gli allegati al momento di mandare la mail, e li decodifichi al momento di ricezione del messaggio. Questo problema si nota dalla dimensione dei messaggi che prima della codifica sono più piccoli.

Le connessioni SMTP di solito sono mandate direttamente, senza passare per nessun server secondario, costituendo quindi di fatto una connessione peer-to-peer per il passaggio di dati.

Le caratteristiche di connessione inoltre dispongono che, in caso di server irraggiungibile, il messaggio verrà mandato in un altro momento, riprovando finché non si ottiene un esito positivo.

La porta predefinita per queste connessioni è la `25`.

Un esempio di una connessione SMTP può essere:

```http
S: 220 hamburger.edu
C: HELO crepes.fr
S: 250 Hello crepes.fr, pleased to meet you
C: MAIL FROM: <alice@crepes.fr>
S: 250 alice@crepes.fr ... Sender ok
C: RCPT TO: <bob@hamburger.edu>
S: 250 bob@hamburger.edu ... Recipient ok
C: DATA
S: 354 Enter mail, end with ”.” on a line by itself
C: Do you like ketchup?
C: How about pickles?
C: .
S: 250 Message accepted for delivery
C: QUIT
S: 221 hamburger.edu closing connection
```

Anche questo servizio può essere testato con `telnet serverName 25`.

I comandi più utilizzati di questo protocollo sono `HELO`, `MAIL`, `FROM`, `RCPT TO` , `DATA`, `CRLF`, `QUIT`. 

Questo protocollo è abbastanza complicato da implementare a mano, siccome tutti i comandi devono essere mandati nel momento giusto con la giusta sintassi.



In questo protocollo ci sono alcune somiglianze e altre differenze con il protocollo HTTP:

* HTTP serve a passare oggetti dal server al client, oggetti chiamati pacchetti.  Di solito la connessione è tra un server e client(browser).
* SMTP si connette direttamente da un server a un altro e fornisce una comunicazione diretta.
* HTTP e SMTP utilizzano connessioni persistenti.
* con HTTP è molto più utile utilizzare la modalità di PULL (ovvero richiedere dati dal server), mentre con SMTP è molto più comune fare operazioni di PUSH.
* SMTP impone la condizione di avere caratteri ASCII a 7-bit, mentre HTTP non impone limiti
* SMTP gestisce diversamente gli hypertext, a differenza di HTTP che li inserisce in un pacchetto e li manda al richiedente.



### Messaggi SMTP

Il protocollo SMTP crea dei messaggi che seguono un formato ben definito. Queste caratteristiche sono tutte descritte nel `RFC: 822`, e di base contiene due “parti” di messaggio:

* le righe di intestazione: `TO/a`, `FROM/da`, `Subject/oggetto` e un altra serie di parametri che servono a indicare la presenza (e in questo caso il tipo) degli allegati, e il tipo di encoding utilizzato per mandare i dati (di solito base64, che è lo standard per fare conversione da 8 a 7 bit).
* il corpo: contiene il messaggio con le caratteristiche identificate prima

```html
From: alice@creps.fr
To: bob@hamburger.edu
Subject: Just-Eat order
MIME-Version: 1.0
COntent-Transfer-Encoding: base64 // Codifica per i 7 bit
Content-Type: image/jpeg 		  // indica i sottotipi di dati, oppure i dati multimediali presenti come allegati

base64 encoded data ....

// se più di un oggetto ci sono dei marcatori che separano gli oggetti mandati fra di loro
```



### Protocolli di accesso alla posta

Mentre SMTP si occupa di tutta la sezione di invio, trasporto e consegna. Una volta che il messaggio viene recapitato però ci sono più modi per gestire l’effettiva ricezione del destinatario. Questi 3 modelli sono:

* **POP (Post Office Protocol)**: segue `RFC 1939`
* **IMAP (Internet Mail Access Protocol)**: segue `RFC 1730`
* **HTTP**: per accedere alla posta via browser (ad esempio Gmail).

Questi modelli si fondano sul fatto che, una volta mandato il messaggio, il ricevente può decidere quando riceverlo (lasciando quindi la possibilità al mittente di inviare il messaggio quando è più comodo, senza occuparsi del ricevente).

Al momento il più utilizzato è l’accesso HTTP, siccome molto servizi di posta forniscono un’interfaccia per accedere, modificare e organizzare le mail dal browser.



#### POP:

Questo protocollo è generalmente utilizzato con dei gestori di posta. Si occupa di gestire l’autorizzazione di agente e server per il download della posta. Questo protocollo non mantiene lo stato sul server (ma lo può mantenere sul client) e per questo motivo utilizza il metodo *read-and-delete*, ovvero le mail scaricate vengono rimosse dal server.

Nonostante queste impostazioni di default, il protocollo può anche essere impostato per mantenere tutte le mail sul server, che siano state lette o meno, ma questo impone che il client abbia un metodo per selezionare solo quelle da leggere per rendere comprensibile all’utente quali mail siano ancora da aprire. Inoltre la massa di dati che si genera in questo modo, avendo una casella di posta molto grande, rende lungo e difficile il download di tutti i dati tutte le volte che si richiedono.

Questo protocollo ha inoltre il problema che se si utilizza il metodo *read-and-delete* sul server, per avere un database consistente di mail, nel quale compaiano tutte le mail ricevute, allora è necessario avere un unico punto di download delle mail che è unico per questo scopo (concetto anacronistico al mondo d’oggi).

Questo protocollo è stato il più utilizzato nei primi di anni di utilizzo della posta elettronica.



#### Funzionamento del Protocollo POP3:

Il POP3 è il protocollo più nuovo del modello POP. Sarà l’unico protocollo per la gestione del download della posta visto in questo corso.

Tutta la comunicazione utilizza solo caratteri alfanumerici. Si da per scontato sia la configurazione di base, quindi i download e la cancellazione della mail.

Questo protocollo segue `RFC: 1939`.

```html
telnet mailServer 110
+OK POP3 server ready
user bob
+OK
pass hungry
+OK user successfully logged on
```

Nel caso di errori, il server risponderà con `- ERR`.

Se l’autenticazione va bene invece, si procede al download-and-delete, effettuato con questi tre comandi `list`, `retr` e `dele`.  Un esempio di questa connessione può essere:

```http
C: list
S: 1 498
S: 2 912
S: .
C: retr 1
S: (blah blah ...
S: .................
S: ..........blah)
S: .
C: dele 1
C: retr 2
S: (blah blah ...
S: .................
S: ..........blah)
S: .
C: dele 2
C: quit
S: +OK POP3 server signing off
```

In realtà questo protocollo ha uno “stato parziale”, infatti tutte le modifiche di eliminazione e modificazione vengono effettivamente svolte solo una volta chiusa la connessione. Per questo motivo i comandi effettivamente portano un valore di stato a indicare se sono state scaricate o meno.

Grazie a questa mancanza di stato è molto più facile l’implementazione e il suo funzionamento in generale, nonostante porti a una mancanza sostanziale del supporto multipiattaforma per un servizio che, ad oggi, lo richiede.

#### IMAP

Il protocollo IMAP ha lo stesso scopo del protocollo POP, ovvero permettere il download delle mail dal server di ricezione, ma si comporta in maniera completamente diversa.

Questo protocollo ha la maggiore differenza nel fatto che mantiene lo stato, e quindi si ricorda quali messaggi sono stati inviati, letti e non letti. Si può anche ricordare di regole di smistamento, di cartelle per organizzare le mail archiviate e in arrivo.

Nonostante sia stato sviluppato prima del POP questo protocollo è più moderno per le caratteristiche che offre, permettendo la sincronizzazione su più dispositivi di tutte le mail ricevute. 

Inoltre questo protocollo ha il successivo lato positivo che non si impone come gestore delle mail, ma fa in modo di rilasciare tutto il compito di gestione al server di ricezione.



## 2.5 DNS (Domain Name System)

Un sistema di computer, una volta arrivato alla mano della comunità ha dovuto cercare un modo migliore per nominare gli host, siccome i singoli indirizzi IP potevano essere molto difficili da comprendere. Per questo motivo hanno introdotto **hostname** (ad esempio: `www.facebook.com`, `www.google.it`). 

Nonostante questa convenzione sia molto più comoda per la lettura e la comprensione umana degli indirizzi, non lo era altrettanto per i router, che invece lavorano meglio con i singoli indirizzi IP (un indirizzo IP è un numero costituito in questo modo: `112.134.243.199`, dove ogni valore separato dal punto è compreso tra 0 e 255).

Siccome queste due necessità devono esistere nello stesso momento, allora serviva creare un servizio che procedesse a questa “traduzione” (da IP a hostname, oppure da hostname a IP). Questo servizio è stato chiamato **DNS (Domain Name System)** e è completamente implementato sui nodi esterni della rete.

Questo servizio è un database distribuito implementato su una gerarchia di server DNS che lavora insieme a un protocollo che permette di effettuare una query distribuita su questi database.

Di solito il DNS viene utilizzato molto per applicazioni che utilizzano HTTP e SMTP. Le azioni che portano a ottenere una risposta DNS sono:

* l’applicazione sul computer richiede un servizio e passa la richiesta al DNS client, perché la traduca.
* il DNS client manda una richiesta a un server DNS.
* il DNS client riceve una risposta con l’IP dell’hostname ricercato.
* una volta ricevuto questo indirizzo, allora si inizia a creare la connessione TCP

E’ interessante pensare che come si è iniziato a fare negli anni 70, la complessità di un servizio del genere non viene relegata all’interno della rete, ma si appoggia solo sui nodi di accesso e quelli perimetrali. In particolare questo modo di fare è stato molto utile nel caso dei server DNS, siccome è un servizio che può spesso cadere (per le moltissime richieste che può ricevere), e in questo caso nessun host che necessita di una “traduzione” (operazione di conversione IP-hostname, oppure hostname-IP) è più possibile e quindi diventa difficile accedere a qualsiasi punto della rete di cui non si conosce esplicitamente l’IP.

Per limitare questi avvenimenti si sono sviluppati degli accorgimenti negli anni che mirano a rendere questo servizio il più stabile e efficace possibile, aggiungendo dei livelli di cache sempre più vicino all’utente finale con un indice degli indirizzi più cercati in locale.



### Servizi DNS:

Il DNS fornisce diversi servizi:

* tradurre hostname a IP, e inverso.

* **host aliasing**: avendo un sistema di traduzione, si può anche pensare che un hostname molto utile si può anche salvare con una variante diversa di questo nome e fatta puntare allo stesso server. Si otterrà quindi che quel server ha un hostname multiplo, ma che riporta alla stessa macchina alla fine.

  Ad esempio, questo tipo di servizio può servire per nomi di server molto lunghi: `enterprise-server.com` serve la stessa macchina che internamente è chiamata `relay1.west-coast.enterprise.com`.

* Un importantissima applicazione di questo servizio è il **load balancing**, ovvero quando diversi server che servono la stessa applicazione si dividono il carico di richieste in entrata per fornire delle risposte più veloci e evitare un eventuale crash del sistema per un carico di dati esageratamente alto. Inoltre questa modalità di organizzazione server è molto utile per mantenere sempre attiva un applicazione, siccome si avrà sempre a disposizione un’altra macchina per fornire una risposta.

  Questa applicazione dei servizi DNS è molto utilizzata nei server cloud, che permettono di aver lo stesso servizio replicato più volte, in base al traffico di ingresso. Per collegarsi a questi servizi però si utilizzerà lo stesso *hostname*, anche se questo valore riporta a indirizzi diversi in base a quale macchina il *load balancer* (programma per direzionare il traffico, dividendolo tra le macchine) decide che è più scarica di richieste.

  Altre aziende, come Akamai, hanno trovato modi per utilizzare questa azione del DNS in modo molto più sofisticato, per permettere la scalabilità dei sistemi e il funzionamento di altri servizi che offrono.

* **mail server aliasing**: allo stesso modo del name-server aliasing si ottiene un servizio che possa restituire lo stesso indirizzo di posta, anche se questo è molto scomodo e lo si è sostituito con uno più facile.



### Funzionamento: richieste e risposte del DNS

Questo servizio di traduzione è utilizzato dal 1970, per questo motivo ha diversi RFC che lo riguardano. Di base ci si può riferire al `1034` e il `1035` in particolare.

Il sistema è molto complesso, quindi si noteranno solo i punti chiavi delle azioni che svolgono questi server riferite solo all’azione di traduzione hostname-IP.

Intanto bisogna notare che il DNS è un **application-layer protocol** (quindi come HTTP, FTP, ...). Tutte le richieste sono mandate attraverso UDP sulla porta 53.

Il modo più semplice per implementare questo sistema è quello di avere un server centralizzato che possa rispondere a tutte le richieste, ma questa architettura (anche se alletta il fatto che sia così semplice) ha molti lati negativi:

* **single point-of-failure**: se questo serve va in crash, allora tutti gli hostname esistenti sono irraggiungibili da tutti.
* **volume di traffico**: questo server da solo gestirebbe un volume di traffico che serve per milioni di utenti.
* **database centralizzato localizzato**: un database centralizzato per definizione avrà un area di vicinanza e una di lontananza. Questa organizzazione dei server quindi fornirebbe un servizio che ha prestazione molto varianti in base alla posizione del client.
* **update e maintenance**: questo server sarebbe grandissimo, richiederebbe una complicata e lunga operazione di manutenzione (con la possibilità di lasciare tutti gli host senza accesso alle sue informazioni). Inoltre sarebbe molto spesso in update, siccome dovrebbe ogni volta aggiungere le informazioni degli host DNS tutte le volte che vengono aggiornate dai singoli user.

Di fatto questi lati negativi si possono riassumere con il concetto che un server centralizzato non può essere scalabile.

La soluzione a questi problemi arriva con la costruzione di un sistema decentralizzato e non gerarchico. Ci sono 3 livelli di server DNS:

* **root DNS server**: sono circa 400 server disposti in tutto il mondo in modo da fornire più server dove c’è più richiesta (dove ci sono più utenti), gestiti da 13 organizzazioni.

* **TLD (top level domain) DNS server**: sono ad esempio i server che mantengono le informazioni per gli hostname che terminano per `.com`, `.edu`, etc., ma anche `.it`,`.uk`, etc.. Di solito questi server sono gestiti da privati che hanno interessi a mantenere il loro server, oppure da organizzazioni nazionali o internazionali.

* **authoritative DNS server**: sono i server di una qualsiasi organizzazione che necessita di avere host accessibili dalla rete necessita. Può decidere di gestire questi server internamente (se ad esempio è un azienda molto grande e necessita di molti hosts sulla rete), oppure di affidarsi a un azienda che offre questo servizio. 

  Spesso le grandi aziende mantengono e supportano il server primario e secondario (server di backup) che mantiene le informazioni per i loro hosts.

Organizzati a albero, con come radice i *root DNS server*. Un *hostname* è scritto in questo modo: `www.unitn.it`, e si possono trovare al suo interno questi server diversi:

* top-level domain: `it`
* authoritative server: `unitn`, che in realtà contiene anche le informazioni per `www.unitn`.

Durante una normale DNS query, si server **DNS radice** è il primo che si va a contattare (anche se non viene richiesto direttamente dall’utente). Questi server non hanno mai tutte le informazioni necessarie a fare già l’associazione hostname-IP, ma sono l’entry-point per inziare la query. Siccome ci sono 13 associazioni che gestiscono questi server, si può pensare che ci siano 13 punti di ingresso logici per iniziare la ricerca.

Siccome le informazioni non sono quasi mai presenti sui *root* server, allora si passa la richiesta ai *top-domain* server. Questi server possono restituire le indicazioni per trovare il server di competenza (*authoritative*) che sappia rispondere alla query. Questi server sono spesso gestiti e mantenuti dagli ISP regionali.

Tutta queste operazioni servono solo per recuperare l’indirizzo fisico del server, quindi possono essere bypassate se si ricorda direttamente l’indirizzo IP. La pratica di ricordare gli indirizzi risulta molto utile sia in caso di caduta del server, sia per evitare di dover accedere tutte le volta al server root e svolgere l’intera query tutte le volte. La funzione di ricordare gli indirizzi viene mantenuta di solito da un server di cache (con un funzionamento uguale al cache proxy server del protocollo HTTP). Di solito tutte le intranet hanno un DNS proxy locale, molto spesso anche il router casalingo mantiene un semplice proxy DNS ormai.

Questa usanza inoltre permette di ridurre il tempo necessario alle richieste DNS, siccome si avvicinano le informazioni all’user finale, permettendo una navigazione più veloce.

Un attacco DDoS contro un server DNS sarebbe molto effettivo per tutti gli utenti che utilizzano quel server per tradurre gli hostname, motivo per cui è un attacco che spesso si nota. Per ridurre il livello di impatto di questo tipo di attacchi si utilizza spesso un load-balancer prima dell’accesso del server, che redireziona a una serie di server DNS uguali che lavorano in parallelo. Inoltre molto spesso si hanno numerosi server di backup.

Ciascun ISP ha un DNS locale, detto anche **default name server**. Questo server serve a tradurre localmente se si possiedono le informazioni, oppure a chiedere a dei server di più alto livello. Questa pratica è utilizzata ancora una volta per velocizzare l’accesso al servizio e per togliere carico ai server root. DNSQuesto server non compare completamente nella gerarchia dei server DNS.

Un altra usanza molto comune è quella di mantenere la cache in locale degli indirizzi dei server *top level domain*, in modo da non dover sempre chiedere la risoluzione al server radice.



### DNS query

Ci sono due metodi per fare una DNS query:

* **query iterativa**: il server risponde con le informazioni se le possiede, se non le ha risponde con le informazioni per dove recuperarle. Spesso queste tecniche permettono al server anche di imparare questo valore in una sorta di cache.

  Questa richiesta risulterà come una serie di richieste fatte dallo stesso server in direzioni diverse, finché non ottiene la risposta cercata.

  Le informazioni in cache vengono invalidate dopo un certo valore di tempo.

  Spesso la macchina che esegue questa query ha un indice aggiornato di tutti i TLD server DNS per eseguire la ricerca più velocemente.

  I meccanismi di notifica e aggiornamento sono regolati nel RFC `2136`.

  Questo metodo è il default per le richieste DNS, data la facilità di funzionamento.

  <img src="image/image-20211020142453442.png" alt="image-20211020142453442" style="zoom:33%;" />

  

* **query ricorsiva**: questa modalità fa in modo che ogni server a cui si richiede porti una risposta, ovvero se il server la possiede comunichi quel valore, se non possiede la risposta allora si incarica di fare un’altra richiesta a un server che potrebbe sapere la risposta. In questo modo si crea una catena di richieste che produce un risultato solo all’ultimo passaggio, proprio come la definizione di chiamata ricorsiva.

  Questa modalità permette di salvare questo record di dati su tutta la serie di server passati per ottenere la risposta, fornendo potenzialmente a più server la possibilità di una risposta in più.



### Database DNS

Siccome il DNS è un database, studiamo insieme le informazioni che contiene, chiamate **RR (Resources Record)**. Questo record di risorsa ha 4 parametri: `(NAME, VALUE, TYPE, TTL)`, dove `TTL` è il *time to live* parameters, che indica dopo quanto tempo la risorsa deve essere tolta dalla cache. Il valore di `NAME` e `VALUE` dipendono dal parametro `TYPE`:

* `TYPE=A`: `name` is the *hostname* e `value` è l’indirizzo IP dell’host.

  Questo tipo di record quindi fornisce il tipico link tra indirizzo e hostname. Esempio:

  ```http
  (relay1.bar.foo.com, 145.37.93.126, A) 
  ```

* `TYPE=NS`:  `name` è il dominio, mentre invece `value` è l’indirizzo di un authoritative server che può sapere dove recuperare la risposta.

  Questo tipo di record serve quindi a far continuare la catena di query per portare una risposta all’host richiedente. Esempio:

  ```http
  (foo.com,dns.foo.com, NS) 
  ```

* `TYPE=CNAME`: `value` è alias di qualche nome canonico (ovvero `name`, il vero nome), `CNAME` appunto.

  Questo tipo di record permette aliasing dell’hostname, ovvero il fatto di nascondere un nome dietro un altro di più immediata comprensione. Esempio:

  ```http
  (foo.com,relay1.bar.foo.com, CNAME)
  ```

* `TYPE=MX` : `value` è il nome canonico del server di posta associato a `name`.

  Questo tipo di record permette ai server di posta di avere dei semplici alias. Esempio:

  ```http
   (foo.com, mail.bar.foo.com, MX)
  ```

  

### Messaggi DNS:

I messaggi scambiati con il protocollo DNS sono impostati con lo stesso formato, sia che siano richieste (query), sia che siano risposte.

![image-20211020152314135](image/image-20211020152314135.png)

![image-20211020152401509](image/image-20211020152401509.png)



### Inserire record nel database DNS

Si presuma che tu abbia un nome con cui vorresti fare un sito per la tua nuova startup, chiamato `new-soc.it`. Per inserire questo record ci si appoggia a un’entità chiamata **registrar**, che si occupa di controllare l’unicità del nome del dominio e in caso aggiungerlo ai database. Per questo servizio spesso si chiede di pagare una piccola parcella.

I registrar sono accreditati dall’Internet Corporation for Assigned Names and Numbers (ICANN), che dopo gli anni 90 ha permesso a più privati la partecipazione a queste azioni.

Quando si inserisce un valore nel DNS, si deve anche inserire il TLD della compagnia, sia il valore predefinito, sia il backup. Di questi compiti di solito se ne occupa il registrar.

Di fatto quindi, il registrar creerà 3 tipi di RR:

```http
(dns1.networkutopia.com, 212.212.212.212, A)
(networkutopia.com, dns1.networkutopia.com, NS)
(networkutopia.com, dns2.networkutopia.com, NS)
```

Chiaramente se si vogliono anche gli alias per mail e hostnames, allora si devono aggiungere altri dati (azione sempre effettuabile attraverso registrar) e inserire i valori `CNAME` e `MX`.

 

## 2.6 Applicazioni P2P

Le applicazioni P2P (peer-to-peer) sono state viste brevemente precedentemente [qui](#accesso-alla-rete), ma si possono riassumere con:

* nessun server è sempre attivo
* coppie arbitrarie di host e peer comunicano direttamente tra loro a livello applicazione, ovvero i router che permettono di effettuare gli “hop” fisici non si “notano”, ma si ottiene soltanto una scatola nera che entra da una parte e che esce dall’altra
* i peer non sono sempre online, non sono sempre nella stessa posizione in rete e non hanno sempre lo stesso indirizzo IP. La rete funziona anche se non tutti i peer sono online (entro certi limiti dovute al livello di richieste in entrata e potenza dei nodi attivi logicamente).
* La rete P2P funziona attraverso la creazione di una rete overlay, costruita da coppie di host che comunicano direttamente tra loro.

La connessione P2P è diventata molto utilizzata negli anni a cavallo del 2000 e per una serie di condizioni che la rendevano migliore per le caratteristiche della rete di allora. Dopo negli anni le caratteristiche di gestione hanno fatto in modo che il sistema preferito diventasse via via quello del client server, fino ad arrivare al cloud elastico.

Per capire i lati positivi di questa rete, analizziamo dei casi diversi.

Mettiamo che un server abbia da consegnare un file molto grosso, mettiamo il kernel di Linux. Se si applica la tecnica client-server allora la copia di questo file deve essere scaricata singolarmente da ogni client. Questo vuol dire che il server ha la necessità di avere una banda condivisa e di conseguenza per svolgere queste operazioni anche delle risorse condivise tra i client che fanno richiesta. 

In un classico caso di connessione P2P il server fornisce il file completamente solo a pochi peer (scelti in base a una serie di regole predefinite). Una volta che il file è stato distribuito a più host, allora anche questi possono partecipare alla condivisione del file aiutando il server fornendo delle parti di file. Questo processo è vantaggioso siccome un peer può essere più vicino dal punto di vista geografico e quindi avere la necessità di meno *hop* per concludere la trasmissione. Un altro punto di vantaggio è che si toglie la possibilità che un collo di bottiglia limiti tutta la condivisione: l’algoritmo di gestione dei peer in questo caso semplicemente redirezionerà la connessione attraverso un peer che fornisce una banda di accesso maggiore.

Queste immagini riassumono queste due condizioni e aggiungono i calcoli matematici che provano questa teoria:

![image-20211018154717895](image/image-20211018154717895.png)

In questo modo possiamo prendere un tempo per distribuzione di un file tra **client e server** di dimensioni $F$ a $N$ client come  $d_{cs} = max(NF/u_s, F/min(d_i))$. Questa relazione indica che il tempo massimo impiegato per questo trasferimento è il massimo tra il tempo del server per inserire i dati in rete e il tempo massimo che il client impiega a scaricare il file. Chiaramente in un caso ideale in cui tutti i valori si riducono a valori infinitesimi si otterrà che la crescita di questo valore dipende solamente da $N$. Per questo motivo il modello di fornitura di un file con l’approccio client-server è lineare.

Con il modello **peer to peer** la discussione è diversa: il server non è mai responsabile di tutta l’operazione di scaricamento (a meno che non sia la prima copia) oppure fornisce solo una frazione del file (che quindi vuol dire che il blocco dati mandato è più piccolo del file totale). Il server deve inviare una copia nel tempo $F/u_s$, mentre il client impiega un tempo $F/d_i$ per il download. Il tasso di upload quindi diventa distribuito su tutti i peer, avendo valore: $u_s + \sum u_i$. Questo sta indicare che il server condivide lo scaricamento del file con altri utilizzando connessioni multiple e fatte con hosts diversi, riuscendo a scaricare il file teoricamente più velocemente.

Il calcolo temporale in questo caso diventa: $d_{P2P} = max(F/U_s, F/min(d_i), NF/(u_s + \sum u_i))$. Questo calcolo significa che il termine massimo di aumento è la sommatoria (somma di termini dipendenti da $n$). Questo valore aumenta meno linearmente, al limite diventa asintotica alla funzione logaritmica. 

Confrontando le funzioni di incremento del tempo necessario per lo scaricamento di un file, avendo definito le costanti della relazione, si ottengono questi due modelli:

![image-20211018155714722](image/image-20211018155714722.png)

Di fatto quindi il P2P si ottiene un tempo di distribuzione asintotico in base all’aumentare di $n$. Questa analisi server a far vedere che in teoria il P2P è molto più adeguato ad un interner scalare: è capace di fornire un servizio con una velocità minima costante a tutti gli host, dopo un certo valore limite di peer connessi.

Ma il P2P ha anche dei lati negativi:

* il numero di peer necessario a ottenere dei tempi confrontabili con l’approccio client server nella vita reale è piuttosto importante e non sempre è raggiunto da queste reti (rendendo l’accesso ai file molto più lento alla fine, diventando molto simile al modello lineare)
* si presume che i peer siano sempre connessi e pronti, che forniscano un aiuto alla rete dopo aver terminato il collegamento. Queste pratiche di solito non vengono rispettate, spesso infatti ci si ritira dalla rete prima di ricondividere il file a sua volta, annullando quindi il concetto principale dell’esistenza di questa rete. Questa pratica di solito viene indicata con la rottura della catena di connessione.
* la manutenzione di questo modello è molto complicata, perché un aggiornamento del server implicherebbe il contattare tutti i peer.



### BitTorrent: un esempio di rete P2P

BitTorrent è un tipo di connessione P2P molto vantaggioso, siccome ha applicato una serie di modifiche utili per semplificare l’accesso e la gestione della rete. In primo luogo utilizza un server, chiamato **tracker** che fornisce una lista di peer che partecipano alla connessione. Gli altri peer che partecipano alla connessione si chiamano torrent.

Il funzionamento ad alto livello è questo:

*  il file viene diviso in più pezzi (chiamati **chunk** e di massimo 256kb), disposti in nodi della rete e duplicati in altri.

* si scarica un file chiamato manifesto: `manifest.torrent` che contiene l’indice dei peer su cui sono presenti le parti di cui è composto il file richiesto.

* attraverso una serie di controlli e parametri, il client torrent capisce a quali parti del file dare la precedenza per il download e quali nodi preferire per lo stesso chunk (anche in questo caso fa delle scelte in base alla velocità di accesso al file e l’eventuale presenza di un collo di bottiglia su uno dei percorsi). Questo algoritmo deve tenere conto di molti fattori:

  * Il numero di connessioni attive tra host e vari peer deve essere bilanciato: avere più connessioni attive nello stesso istante permette di continuare a scaricare il file e di essere veloce anche se mentre si sta scaricando da un nodo questo va offline,  ma allo stesso modo di mantenere abbastanza banda ancora utilizzabile. Un ultimo punto di cui tiene conto è la potenza del computer, infatti non tutte le macchine hanno uguale potenza e di conseguenza non possono servire tutte lo stesso numero di hosts.

    Nel caso di questo algoritmo di solito si mantengono sempre 4 connessioni, chiamate *favorite* decise in base a questi parametri e molti altri. A lato di queste si mantiene una sottolista di possibili nodi di backup nel caso uno dei 4 favoriti si facesse indietro.

  * i nuovi algoritmi di solito preferiscono chi offre maggiore capacità, rispetto a chi mette a disposizione poco. Questo è fatto in modo che le informazioni siano sempre più disperse per la rete e sempre più comuni a più peer.

  * l’algoritmo di solito preferisce i pacchetti disponibili su meno peer possibili: da una parte bisogna scaricarli subito in modo che se questi nodi in un momento siano offline io non mi trovi senza nodi da cui scaricalo. Dall’altra parte in questo modo, l’host su cui sto scaricando questo *chunk* *raro* può a sua volta iniziare a distribuirlo, rendendo più grande la rete di nodi disponibili a fornire questo pacchetto. Questo approccio è chiamato **rarest-first**.

* quando un peer ottiene un file (se decide continuare a partecipare alla rete), allora gli viene assegnato il compito di fornire un determinato chunk a chi si dovesse collegare richiedendolo.

Questo modello di servizio P2P punta a risolvere il problema di come trovare le informazioni sulla rete. Questo problema è stato particolarmente studiato e ancora ora viene studiato per fornire un’implementazione ottimale (che comunque è difficile). Nessun metodo di ricerca riesce a scalare bene in qualsiasi caso comunque. Per questo motivo avere un server che mantiene tutte le informazioni sul dove trovare i chunk necessari per scaricare il file è una possibile soluzione.

Dopo questa applicazione moltissime altre hanno seguito questo modello: centralizzato per la parte generale e di gestione della rete, distribuito per fornire i file richiesti.

Sempre più spesso la parte di login alla rete viene contralizzata, mentre la fornitura di elementi multimediali viene distribuita attraverso peer.



### Napster

Questa applicazione molto diffusa tra gli anni 2000 è stato forse una delle prime applicazioni della connettività P2P con ottimi risultati. Di più [qui](https://en.wikipedia.org/wiki/Napster).

Il funzionamento generale si basava su un server principale che manteneva le informazioni sulle informazioni memorizzate sui peer su un *server centrale*. Ogni volta che un peer si collegava alla rete, informava il server centrale e riceveva informazioni sui i file che stava cercando.

Questo servizio quindi utilizzava il P2P solo per la distribuzione dei file musicali che offriva, ma sfruttava l’approccio client-server per tutti i servizi necessari, come il servizio di localizzazione delle risorse. 

Questo servizio però poi ha avuto problemi perchè:

* si formava un collo di bottiglia sul server centrale (che aveva difficoltà a scalare, rispetto al resto della rete)

* il server centrale aveva le identità di tutti i peer partecipanti alla rete (questo elemento è anche quello che dopo ha permesso la chiusura di molti dei sui servizi in modo rapido, siccome erano stati accusati di promuovere il pirataggio della musica).

  

### Query Flooding

Questo nuovo protocollo si basa su un sistema completamente distribuito senza la necessità di nessun server centrale. E’ un protocollo pubblico utilizzato anche da Gnutella. Ciascun peer *da solo* indicizza i file che rende disponibili per la condivisione. Questo file viene mantenuto solamente in locale.

Ogni peer si collega a circa una decina di peer vicini per mantenere attiva la connessione e peter ricevere richieste dalla rete.

Ogni volta che si cerca un file viene eseguita una query (molto simile a quella del DNS idealmente): ogni peer se può rispondere con delle informazioni sul file cercato allora risponde, se non può allora rimanda la richiesta ai suoi nodi con la speranza che uno contenga questo file.

Di solito c’è il cosidedetto *raggio limitato* ovvero il segnale si può trasmettere solo ad un  numero di salti (ovvero la query non può circolare continuamente nella rete, ma una volta raggiunto un certo numero di hosts si fermerà). Questa protezione serve a evitare che la rete sia ovunque piena di query, oppure che una richiesta di un file non esistente continui a propagarsi per la rete. Per questa struttura di richiesta la rete viene appunto chiamata **query floating** a raggio limitato(ovvero si “inonda” la rete di richieste per trovare il file).

Una volta che la query ottiene un risultato, allora si crea una via unica scelta in modo che sia più veloce possibile. Questa via diventerà il tunnel che permetterà il completo scaricamento dei file (logicamente anche in questo caso questi valori sono divisi in chunk).

Per mantenere la copertura al meglio, allora ogni nodo è:

* leader
* assegnato a un leader

In questo modo, il nodo leader riesce a mantenere le informazioni che contengono i suoi nodi “figli” in modo opaco. Facendo così le informazioni vengono avvicinate sempre più e quindi si permette di ridurre l’overhead della richiesta sulla rete e soprattutto di avere più possibilità di ottenere una risposta.

Questo modello permette di ridurre le persone che conoscono tutti i dati, che è l’informazione più sensibile, ma allo stesso tempo fornire un servizio completamente distribuito e puntuale nel servire i file.



### Skype

Questo protocollo non è di pubblico dominio, ma è stato dedotto attraverso l’analisi del traffico che questa applicazione crea. 

Come gli esempi precedenti il modello di base su cui si basa il suo funzionamento è quello di fornire un modello P2P per la comunicazione fra utenti e un server centrale per indicare (come un indice) gli indirizzi dei singoli host con cui ci si vuole collegare. La copertura di questo indice viene mantenuta avendo un rete P2P non piatta, ma con alcuni nodi leader che si chiamano **super nodi**. Questi permettono ai vari nodi dispersi sulla rete di essere il più reattivi possibili nelle risposte e (ancora una volta) di poter evitare eventuali colli di bottiglia.

Il server centralizzato che gestisce l’indice è anche quello responsabile delle azioni di login degli utenti.

Un problema però che ha avuto questa applicazione è la sempre più frequente presenza di un NAT sulle reti domestiche. Il **NAT (Network address translation)** è un sistema utilizzato per limitare che host esterni alla rete domestica si colleghino direttamente a un host interno (sfruttando il concetto di IP pubblico e IP privato/interno). In questo modo gli host presenti all’interno della rete sono “mascherati” dall’unico host di uscita che li raggruppa tutti (metodo efficiente per la sicurezza di una rete e per altri motivi che si vedranno più avanti). 

Questo sistema, nonostante i molti lati positivi, impedisce di fatto la creazione di questo sistema, perché gli hosts privati non riescono a esporre al pubblico il proprio indirizzo IP. Questo problema Skype lo ha risolto con la creazione di *relay*. I relay (spesso situati anche in prossimità dei supernodi) permette a due hosts privati di connettersi tra loro: il primo host si connette al relay, il secondo host si connette al relay e il relay si occupa di connettere questi due utenti. Di fatto il relay agisce da *bridge* e sono situate in posizioni importanti della rete per permettere di evadere il NAT. 

Per i blocchi imposti dal NAT e la necessità di utilizzare il ralay in corrispondenza del supernodo, se il nostro supernodo di riferimento cade, allora cade tutta la connesisone e non si riesce a ricreare se non utilizzando un altro route.

[^Gabri dice di inserire queste parole.]: La lavatrice parla con internet io mi fumo l’alloro.



## 2.7 Cloud Computing

Il cloud computing è un estremizzazione del modello client-server, con una rivoluzione della gestione del server. 

Per questo discorso si utilizzano dei termini specifici: 

* gestore dei server o amministratore: è la persona o (più spesso) l’azienda che ha l’accesso ai servizi fisici e fornisce la manutenzione fisica del datacenter, oltre che supporto in caso di problemi tecnici.
* cliente amministratore: persona o azienda che affitta i server di un datacenter per fornire la propria applicazione.
* utente finale: si interfaccia con l’applicazione senza notare la differenza di macchina fisica sul quale sta lavorando oppure notando la differenza di carico che un determinato servizio sta subendo.

Le idee che stanno alla base di questo modello sono:

* le aziende che possiedono il datacenter dispongono di un grandissimo numero di server reali, di solito con un architettura a alta affidabilità e posizionati nei punti più favorevoli della rete in modo da avere la rete di accesso migliore possibile.
* il gestore del server espone i servizi attraverso un layer di applicazione che permette il funzionamento dell’elastic cloud, ovvero:
  * la possibilità di aggregare in maniera virtuale una serie di servizi in base alla capacità che si vuole fornire ai propri clienti, oppure (caso molto più probabile) in base alla necessità e al numero degli users richiedenti il servizio.
  * questo sistema permette di disattivare questi spazi virtuali se in numero troppo elevato rispetto agli utenti, ma permettono di crearne in  in caso di un picco sostenuto di traffico 
  * l’applicazione di questo servizio è fatta in modo che il cliente amministratore non si accorga della differenza dal lavorare in un servizio bare-metal a un servizio in cloud.
  * questi elementi permettono di avere dei servizi che sono estremamente scalabili e riescono a distribuire le risorse e il carico in modo estremamente vantaggioso.
* il risultato finale di tutti questi elementi è che l’utente amministratore può selezionare un servizio completamente componibile a cui i clienti possono interfacciarsi con estrema facilità e stabilità.
* Tutte le operazioni che includono il backup periodico, la salvaguardia dei dati, il controllo del benestare dei server, il controllo del corretto funzionamento e la manutenzione in generale è completamente lasciata in mano agli amministratori, togliendo questi problemi dai pensieri del cliente amministratore, che deve soltanto pensare all’effettivo servizio che vuole offrire.



Nonostante i lati positivi indubitabili del sistema in-cloud, ci sono alcune criticità non sottovalutabili che porteranno a un successivo miglioramento di questo servizio: 

* problemi di **sicurezza e privacy**: i dati sono dispersi all’interno del datacenter, non localizzati in modo preciso.

  Nuove tecniche permettono di distribuire i dati solo su una serie di macchine predefinite in partenza oppure scegliere un luogo geografico dal quale non si deve uscire.

* problemi **internazionali di natura economica e politica**: siccome i datacenter diventano dei grandissimi agglomerati di dati, ogni servizio pubblico che si sposta in cloud oppure i singoli cittadini vogliono sapere che i dati siano mantenuti in un luogo che ha delle regole per il mantenimento dei server simili al paese di provenienza. Di fatto l’Europa sta cercando di dotarsi di datacenter completamente europei e quindi sotto le regolazioni del GDPR.

  Allo stesso modo, siccome anche i servizi pubblici è sempre più facile trovarli come servizi in-cloud, i singoli governi (che vogliono mantenere il controllo su queste informazioni sensibili) hanno la necessità di creare datacenter geo-politicamente localizzati.

* continuità del servizio offerto: gli amministratori devono gestire tutte quelle operazioni di manutenzione e backup (come detto appena sopra) per mantenere il servizio online e ben funzionante. 

  Questo tipo di sicurezze dipendono del tutto in base alla compagnia alla quale ci si sta affidando.

* migrazione dai servizi: è sempre più comune la pratica del *vendor-lockin*, ovvero un cliente che ha tutti i sistemi impostati per lavorare con una determinata azienda fornitrice ha difficoltà a cambiare fornitore di servizi perché avrebbe la necessità di rivedere molti aspetti pratici del funzionamento del sistema di elastic-cloud, siccome sono tutte layer applicativi diversi e non compatibili.

  Negli ultimi anni si sta osservando una rivoluzione da questo punto di vista, siccome molti software e tecniche utilizzate stanno venendo generalizzate e standardizzate, sfruttando del codice in comune ai produttori.



Chiaramente il processo di migrazione dal modello client-server al modello in-cloud non è stato immediato, ma ha passato una serie di fasi intermedie che stanno ancora oggi modificandosi e aprono nuove possibilità a questo modello in futuro.

![image-20211018165716454](image/image-20211018165716454.png)

![image-20211018165842169](image/image-20211018165842169.png)

1. Chiaramente all’inizio si utilizzavano una serie di macchine fisiche, dove si facevano girare dei servizi direttamente sul server, come fosse un normale terminale.

2. I servizi forniti attraverso macchina virtuale diventano sempre più utilizzabili perché iniziano a fornire un modello di scalabilità accettabile per il tempo

3. I servizi vengono forniti attraverso i container, ovvero delle macchine virtuali completamente minimizzate e altamente ottimizzate, studiate per contenere una singola istanza dell’applicazione. La creazione di questi container è pressoché immediata e permette la completa duplicazione del servizio.

   Attraverso un load-balancer questi sistemi possono gestire una grandissima mole di traffico sfruttando qualche instanza dell’applicazione lanciata in container.

4. In futuro vedremo delle applicazioni create in tempo reale al momento della richiesta per svolgere una singola funzione in modo completamente ottimizzato (Amazon nel suo cloud AWS permette questo servizio attraverso le *lambda functions*). Questo modello si chiama **serverless** e occupa sul server la potenza necessaria solo per svolgere una singola azione su richiesta.

Chiaramente questo andamento (come si nota anche in figura) ha permesso una scalabilità sempre più granulare e ottimizzata, ma allo stesso tempo ha portato a una grandissima dispersione delle risorse dato dall’overhead di questi programmi per la gestione di container o applicazioni immediate.

La granularità della scalabilità del servizio si nota sopratutto nell’ultimo servizio, il serverless. Questa applicazione permette di avere delle funzioni create ad hoc invocate e create singolarmente ogni volta che sono necessarie, per poi essere distrutte.

Un immagine che sintetizza questo discorso è la seguente:

![image-20211020200020907](image/image-20211020200020907.png)



### La rete del Datacenter

Di un datacenter la parte più complicata è la rete. Questa parte è logicamente la più importante e quella che necessita di più attenzione, anche perché è pensata per avere degli obiettivi diversi da tutte le altre reti esistenti.

I servizi all’interno del datacenter devono avere più banda di accesso possibile e essere sempre online, di fatto quindi questa rete deve fornire numerosi collegamenti di accesso per fare in modo che la linea di collegamento non sia unica. In questo modo le connessioni hanno un backup, riescono a smaltire il traffico attraverso altre vie se si trova un router congestionato nel percorso, inoltre si cerca di avere la banda di accesso più grande possibile per ogni server esposto sul web.

Dall’altra parte il datacenter si basa anche sulla gestione delle tecnologie di virtualizzazione. Queste tecnologie, prima relegate solo agli spazi fisici dei server ad alte prestazioni, stanno arrivando sulla rete attraverso la virtualizzazione delle reti. Queste tecniche permettono di creare, di fatto, una rete virtuale di overlay basata su delle effettive tratte di rete.

Essendo il networking così importante per il datacenter, si ha un organizzazione granulare delle connessioni e dei router principali:

![image-20211018170724860](image/image-20211018170724860.png)

Gestire una rete così grande, evitando blocchi, sovraccarichi o congestioni è veramente complicato. Bisogna aver fatto uno attento studio sull’instradamento per ridurre la possibilità di congestione. Per garantire la continuità del servizio si cerca la ridondanza tra tutti gli apparecchi (motivo per cui i router principali sono tutti collegati tra loro a stella, a differenza delle reti esterne nel quale si cerca di minimizzare le tratte fisiche). L’unico particolare che facilita il lavoro di gestione di una rete così impegnativa è che tutti questi apparecchi sono tutti dello stesso proprietario e useranno per lo più la stessa tecnologia. In questo modo interfacciarsi e intervenire è molto più facile e generalizzabile per i manutentori.

Il datacenter ha un accesso alla rete a alta capacità e banda larga. Questa connessione viene poi distribuita nel datacenter, spesso con dei collegamenti anche più larghi dell’effettiva banda di accesso passando prima per i **core-network** router, che permettono i collegamenti principali con tutti gli altri router. Questi core-networks router si occupano di collegare aggregation-network. Infine di solito ci sono gli access-network router, che spesso si trovano nei rack (gli “armadi”) per i server.



### Video Streaming e CDN

Un esempio di servizi spesso forniti da un datacenter che ha un ingombro di banda il più grande possibile è lo streaming video. Questi servizi hanno delle necessità di traffico e di banda in uscita dal datacenter molto alto, tanto che si stima che l’80% del traffico di rete mondiale ormai sia prodotto da servizi di streaming come YouTube, Netflix o AmazonPrime.

Per gestire un servizio del genere una sfida è essere capaci di scalare le risorse in modo automatico in base al numero di utenti, in modo da fornire un servizio consistente a tutti i clienti.

L’altra sfida è l’eterogeneità delle reti di accesso degli utenti che accedono al servizio. Le bande di accesso sono molto fluttuanti e variabili, possono avere dei rallentamenti e dei colli di bottiglia e soprattutto possono avere caratteristiche diverse. Ad esempio la rete cellulare ha una banda piuttosto larga se non è congestionata, ma è molto instabile e quindi fornisce prestazioni molto variabili. La rete in fibra dall’altra parte fornisce un servizio a banda larga con un estremo grado di affidabilità. La sfida quindi è garantire un servizio molto simile a questi due utenti nonostante le differenze tecniche sostanziali.

Un modo per risolvere questo problema è attraverso l’utilizzo delle **CDN (Content Delivery Network)**. Questa infrastruttura è una rete overlay dedicata di un azienda, oppure fornita a un azienda che ne ha necessità da un terzo e si basa su una rete dedicata di datacenter di dimensioni più ridotte. Questi piccoli datacenter vengono chiamati **content delivery node** e seguono le stesse regole per il posizionamento dei normali datacenter: vicino ai grandi nodi con banda di accesso molto larga. La differenza che contraddistingue i CDN dai normali datacenter è che, essendo più piccoli e più dispersi per il territorio, vengono posizionati in modo da essere il più vicino agli utenti finali possibile. In questo modo si possono bypassare eventuali colli di bottiglia nel percorso dal datacenter all’user e permettere quindi una maggiore velocità di accesso a questi servizi.

Il funzionamento dei CDN è strettamente collegato a quello del datacenter centrale: sui nodi si caricano una serie di file che vanno condivisi. Il client verrà redirezionato verso questi CDN rispetto che al server centrale al momento di necessità di accesso al servizio, facendo in modo che il cliente scarichi direttamente da questi server più vicini.



#### CDN: applicazione

Per fornire una rete di CDN si possono utilizzare più strategie per disporre i server nelle nazioni necessarie. Questo servizio può essere “affittato” da aziende che fanno questo di mestiere, come Akamai, oppure si può decidere di creare un sistema “fatto in casa” come la rete di CDN di Google (che serve anche YouTube).

Le reti di CDN possono essere organizzate in due modi diversi, principalmente:

1. *vai in profondità* (**enter deep**): questa tecnica di fornitura dei server si appoggia direttamente agli ISP di accesso alla rete. Con questi provider ci si accorda per avere dei server cluster disposti nella loro rete di accesso. Si ottiene quindi una rete di server disposti tutti sulle reti di accesso, ma estremamente frazionati (si parla, nel caso di Akamai, pioniere di questa tecnica, di circa 240000 server in più di 120 nazioni). Questi nodi hanno la caratteristica di fornire una banda di accesso e un throughput ai clienti molto basso, ma il fatto che forniscono un servizio così distribuito rende la manutenzione e la gestione un grandissimo problema. Per questo motivo queste aziende hanno servizi di monitoraggio della rete e dei server molto avanzati, con la possibilità di avere dei dati aggiornati in tempo reale e statistiche sull’andamento. Inoltre, siccome un servizio di un azienda è così disperso per il mondo, spesso vengono usati questi server come indicazioni dell’andamento mondiale della connettività, in modo da capire in tempo se una zona geografica è effettivamente disconnessa dalla rete.

2. *porta a casa* (**bring home**) questa seconda filosofia, applicata da altri esperti del settore e fornitori di CDN, si basa sulla creazione di datacenter più piccoli, che di solito vengono connessi dall’ISP o sono posizionati presso grandi POP, ma non lo sfruttano direttamente, posizionandosi completamente all’interno della rete. Questa tecnica si può anche pensare come si avessero dei grandi Exchange Points (IXPs). 

   Questa filosofia, rispetto a quella *enter-deep* permette di avere una necessità di mantenimento e gestione minore, ma allo stesso tempo fornisce agli users un throughput e una banda ridotta rispetto all’altra tecnica.

Il CDN di grandi servizi, che non possono permettersi di avere l’intero database su tutti i nodi funziona molto più come un server di cache. In questo modo, al momento della richiesta da parte del client, si ottiene un *manifest* che indica i server CDN disponibili per lo scambio. Il client identifica il server migliore con una serie di test automatici, e avvia lo streaming video. Se questa risorsa richiesta si trova già sul nodo, allora viene recuperata e manda all’utente. In caso contrario nello stesso momento della fornitura del video ai clienti, una copia viene salvata sul server per successive richieste.

Il funzionamento di queste reti spesso è permesso da una serie di DNS server studiati per direzionare il traffico in base alla localizzazione del pacchetto che richiede. In questo modo, il DNS penserà in automatico a dividere le richieste tra i server CDN più vicini, permettendo una maggiore divisione delle risorse.

##### Study Case

Servizi come **YouTube** che non possono permettersi di avere l’intero database di risorse video che possiedono sulla piattaforma copiata su ogni CDN. Per questo utilizza una serie di tecniche statistiche e di (possiamo dire) azioni simili al funzionamento della cache che permettono di avere in questi nodi solo i video più visti e più cliccati nella regione che questo nodo serve (i video più popolari). In questo modo, i video più popolari al momento nella zona sono anche quelli con velocità di accesso maggiore, mentre invece quelli meno popolari hanno una maggiore latenza.

Altri servizi, non propriamente di streaming, possono sfruttare questo servizio per migliorare la reattività delle loro applicazioni. Ad esempio **FaceBook** ha la sua CDN per fornire video e immagini da risorse più vicine, fornendo un servizio più stabile e reattivo ai suoi clienti. Intanto fornisce i restanti servizi dal server centrale, siccome necessitano di meno banda e sono più veloci sulla rete.



### Multimedia: codifica e flusso di dati del video

Come ogni tipo di file, anche tutta la multimedia viene codificata con determinate regole e modelli. Di fatto il video è una sequenza di immagini visualizzata a frequenza costante, al minimo 24 imm/sec. Le immagini digitali sono costituite da pixel, che hanno una quantità di bit variabile in base alla qualità e la quantità di colori rappresentabili. 

Per capire il reale problema di trasferimento di questi file prendiamo come esempio un’immagine di 2MB. Questa immagine è una matrice $2 \times 10^3 \times 10^3$ bit, che indicherebbe per immagine media una quantità di circa $125$ pixel. Se questa immagine avesse la necessità di essere in un video, allora per avere una fluidità dell’immagine e continuità di visualizzazione, si necessiterebbe di una banda di accesso di `400Mbit/sec`. 

Logicamente questi parametri non possono essere raggiunti da tutte le linee di accesso, per questo motivo si è cercato delle tecniche che diminuissero questo ingombro di banda. Si ottengono queste caratteristiche con degli algoritmi di compressione che possono lavorare in modi diversi:

* possono ridurre la quantità di informazioni racchiuse in un immagine togliendo tutte le informazioni in eccesso. Questa azione viene svolta molto bene dall’algoritmo `jpeg` che appunto permette di sapere quali sono le zone ridondanti delle immagini.

  Una volta ottenuti questi dati, le zone ripetute vengono mandate una sola volta, all’inizio della sequenza, e poi modificate con delle indicazioni vettoriali specifiche.

  Per questo motivo, se il bit-rate di scambio dati diminuisce, allora si ottiene un’immagine “quadrettata” oppure con delle “scie” di quadretti. Questo avviene perché l’algoritmo non riesce più a ricostruire l’immagine perché ha troppi pochi dati.

* possono (in questo caso solo per applicazioni video) partire da un immagine iniziale e mandare ogni volta solo la differenza che questa possedeva con la precedente. In questo caso si ottiene una larghezza di banda necessaria molto variabile: alta se si necessita di mandare scene in cui c’è molto movimento, bassa se si riproducono scene meno frenetiche e più statiche. 

  Di solito DVD (lettori e codificatori) funzionano in questo modo. Hanno una frequenza di collegamento medio di circa `5Mbit/sec`, che può però variare fino a `10Mbit/sec` circa sulle scene più intense o sui tratti meno compressi. Se questa banda non è disponibile allora si ottiene un’immagine distorta oppure non nella qualità iniziale.

* anche i codificatori più moderni eseguono una compressione delle immagini, ma fanno in modo di mantenere più dati in maniera più consistente, per avere quindi un immagine a qualità più alta.

  Queste tecniche sono capaci di sfruttare la codifica spaziale e temporale di tutte le immagini, immagazzinando più valori (e garantendo una qualità migliore) ma con un’occupazione di spazio accettabile.



Sulla rete nessuno manda dei video in maniera non codificata, perché non è accettabile fornire dei servizi con questa necessità di banda su internet. Inoltre se ci si scambiassero i file multimediali in questo stato non compresso, si otterrebbe una rete molto spesso congestionata.

L’audio di solito è per applicazioni umane, per cui si cerca sempre di sfruttare la sensibilità ai diversi stimoli del corpo umano e in particolare dell’orecchio. Di solito si usano queste variazioni per mandare il segnale nel migliore dei modi, ovvero cancellando tutte quelle informazioni registrate dall’apparecchio che il nostro orecchio non potrebbe comunque avvertire. Inoltre, il nostro cervello riesce a ricostruire alcuni pezzi mancanti di informazione, se ne ha il contorno. Per questo motivo spesso, negli algoritmi di compressione musicale più spinti, si tolgono alcune informazioni, sapendo che il nostro cervello ricompenserà a questa mancanza.

Ci sono due modi per fare codifica dei file video: 

* **CBR (Constant Bit Rate)**: ovvero mandare un video con una quantità fissa di dati. Non è una tecnica utilizzata spesso, però è quella di più facile implementazione.
* **VBR (Variable Bit Rate)**: ovvero il sistema ha capacità di adattarsi e modificare la quantità di bit che scambia tra gli host, in un meccanismo molto simile a quello del DVD. Poi questo concetto viene sviluppato meglio dal protocollo [DASH](###streaming-multimedia:-DASH). 



### Cosa comporta fare streaming

Il flusso di dati necessario a scambiare le informazioni tra client-server sono molti e può succedere che per vari motivi si ottenga una larghezza di banda ridotta (di solito avviene per multiplexing statistico oppure semplicemente per congestione del server). Questo effetto inoltre si amplifica allontanandosi dal server da cui si sta recuperando il file.

Altri problemi di ricezione e invio di questi pacchetti possono essere:

* coda dei pacchetti su un determinato nodo studiata per dare la precedenza a altri tipi di dati
* semplice coda first-in-first-out che impone a questi dati di non essere subito smistati
* altri problemi della rete, che aumentano se si incontrano più colli di bottiglia del normale

Per risolvere in parte questo problema si utilizzano tecniche di streaming di video memorizzato, ovvero si utilizza un buffer prescaricato (una sorta di cache) di pacchetti sul client, in modo da garantire che, anche se la ricezione dei dati non è puntuale (si parla in questo caso di *jitter*, ovvero ritardo della rete variabile), si riesce a ottenere sempre una visualizzazione continua.

Queste tecniche inoltre permettono di avere un fast-forward e un fast-backward sul video molto più reattivo, senza la necessità di richiedere a ogni azione questi dati. Dall’altro lato però queste tecniche vengono interrotte e rese inutili da continui interventi dall’utente. Infatti se si continua a modificare la sezione da riprodurre il video risulterà più lento siccome si dovrà recuperare una sezione diversa da quella già scaricata in cache.

![image-20211019142747519](image/image-20211019142747519.png)



### Streaming multimedia: DASH

Per migliorare lo streaming e la ricezione di file multimediali (i più comuni degli ultimi anni) si hanno inventato delle tecniche diverse e dei protocolli appositi per migliorare l’utilizzo di questo servizio.

Il protocollo **DASH (Dynamic Adaptive Streaming over HTTP)** è stato sviluppato apposta a questo scopo, e siccome ha una serie di necessità di servizio molto strette è stato sviluppato appositamente. Chiaramente è servito attraverso il protocollo TCP siccome fornisce di default il servizio di congestions-control e recupero pacchetti persi, e ha un’altra serie di caratteristiche particolari. Dal lato **server**

* si produce un video viene diviso in *chunk* e si copiano tutti questi oggetti in tutte le qualità (e quindi bit-rate di dati) possibili forniti dal servizio.
* tutti questi dati vengono replicati sulle CDN (sfruttando logicamente la loro logica geografica di raggruppamento dei dati) in modo da avere i dati dal mirror più vicino possibile.
* spesso il server stesso invia nella prima richiesta un file di *manifest* che contiene tutte le informazioni per recuperare i vari chunk e nelle varie qualità

Dal lato **client**:

* si ottiene una stima della banda disponibile per ogni richiesta fatta al server. Questo valore avviene in maniera indiretta sfruttando il tempo di arrivo dei pacchetti e altri fattori specifici.
* al momento di recuperare i dati, si una richiesta che include il valore di banda appena stimata, in modo da avere come risposta il chunk con il rapporto qualità/disponibiltà del servizio più alta possibile. (insiema a questi valori si inseriscono anche altri, ad esempio qualità massima del dispositivo. Questo per fare in modo di non fornire dei dati che non verrebbero utilizzati)
* finita questa richiesta si riinizia questa procedura, con la possibilità di ottenere una stima della banda diversa e quindi ricevere un chunk differente.
* con queste tecniche si cerca inoltre di sfruttare nel modo migliore la cache per ottenere un playout costante.
* chiaramente un chunk di dimensioni minori rispetto a quello di più alta qualità ottiene un immagine peggiore.

Un tipico esempio di applicazione di questo protocollo è YouTube, nel quale si può notare, sopratutto se si sta lavorando su una connessione instabile, come il chunk ritornato dal server possa avere qualità variabile.

Un altro particolare interessante da notare dell’applicazione di questo protocollo è che ancora una volta si posiziona l’intelligenza del servizio sugli *edge-point* della rete, in modo da avere un servizio scalabile e modificabile in base alla disponibilità di banda che la rete in un determinato momento mette a disposizione.



Per aiutare il funzionamento di questo protocollo si utilizzano alcune accortezze pratiche, ad esempio fornire i video più popolari su tutte le CDN, in modo che si bypassino i colli di bottiglia. Chiaramente quando si parla di soddisfare milioni di utenti entra in gioco in modo preponderante questa *delivery-network*. Avere un server centralizzato in questo caso non è la scelta migliore per due motivi principalmente: 

1. le persone vicine ricevono un servizio migliore, rispetto a quelle lontane 
2. si evita di avere un *one-point-of-failure* su tutto il servizio, avendo potenzialmente dei problemi con la totalità degli users in caso di malfunzionamento

## 2.8 Programmazione delle socket (cenni)

Le socket sono il modo principale per chi sviluppa l’applicazione per interfacciarsi con la pila TCP/IP ad alto livello. Questo concetto è stato introdotto negli anni ‘80 e funziona in modalità client-server per creare un interfaccia tra due host.

La socket si richiede al sistema operativo, che in pratica è il vero gestore di questa applicazione. Di fatto si può pensare che si lavori in un modello simile a quello delle API. Si possono richiedere due tipi di socket:

* comunicazione attraverso datagramma inaffidabile (basato sull’UDP)
* comunicazione affidabile, orientata al completo e sicuro trasporto di byte (basato sul protocollo TCP)

Il funzionamento di base si fonda sulla creazione di un buffer mittente che si occupa di inviare le informazioni presenti al suo interno, e un buffer di ricezione che si occupa di accogliere le richieste. In questo modo si lavora a alto livello senza doversi appoggiare a API del kernel a livello molto basso.

Di solito le informazioni necessarie a richiedere una socket sono racchiuse in una tupla, ovvero una coppia di dati non modificabile, che contiene `(indirizzo, porta)`. Queste informazioni sono univoche sia per il ricevente sia per il mittente.

Si possono avere delle connessioni per ogni processo o ogni thread (sottoprocesso), ma di fatto c’è un limite pratico, ovvero che è un informazione di 16bit e quindi di fatto ci possono essere 65000 porte circa. Calcolando che circa le prime 1000 sono riservate al sistema operativo e fino a 5000 ci sono spesso dei servizi comunemente utilizzati, si ottengono circa 60000 porte utilizzabili in parallelo contemporaneamente al massimo. 

In realtà questo dato è falsato: per avere potenza necessaria a utilizzare tutte queste porte si deve avere un computer molto potente che abbia abbastanza memoria per tutti i buffer creati.

Il server risponde sempre da porta 80 quando fornisce un servizio `http`. I messaggi in ingresso si accodano su questo buffer e poi vengono smistati. Spesso si utilizza un load-balancer che lavora su questa porta per dividere il traffico tra più istanze della stessa applicazione che lavorano su delle altre porte. 

Se la banda del server non è abbastanza per mantenere tutto il traffico, ci saranno dei pacchetti persi. Se non c’è abbastanza memoria nel buffer allora ci saranno dei pacchetti persi. In altri casi si può avere anche un crash dell’applicazione se non riesce a reggere il traffico.



(Non necessario per l’esame)

### Programmazione di una socket

![image-20211028123230801](image/image-20211028123230801.png)

Un esempio di programmazione di una socket UDP in python è:

```python
#! Python UDPClient
from socket import *

serverName = ‘hostname’
serverPort = 12000

clientSocket = socket(AF_INET, SOCK_DGRAM)
message = raw_input("Input lowercase sentence:")
clientSocket.sendto(message.encode(),(serverName, serverPort))
modifiedMessage, serverAddress = clientSocket.recvfrom(2048)
print(modifiedMessage.decode())

clientSocket.close()

#! Python UDPServer
from socket import *

serverPort = 12000
serverSocket = socket(AF_INET, SOCK_DGRAM)

serverSocket.bind(('', serverPort))
print(“The server is ready to receive”)
while True:
	message, clientAddress = serverSocket.recvfrom(2048)
	modifiedMessage = message.decode().upper()
	serverSocket.sendto(modifiedMessage.encode(), clientAddress)
```

Con il TCP, invece si ha una socket in python costruita in questo modo:

```python
#! Python TCPClient
from socket import *

serverName = ’servername’
serverPort = 12000

clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((serverName,serverPort))
sentence = raw_input(‘Input lowercase sentence:’)
clientSocket.send(sentence.encode())
modifiedSentence = clientSocket.recv(1024)
print("From Server:", modifiedSentence.decode())
clientSocket.close()

#! Python TCPServer
from socket import *

serverPort = 12000
serverSocket = socket(AF_INET,SOCK_STREAM)

serverSocket.bind((‘’,serverPort))
serverSocket.listen(1)
print("The server is ready to receive")
while True:
    connectionSocket, addr = serverSocket.accept()
    sentence = connectionSocket.recv(1024).decode()
    capitalizedSentence = sentence.upper()
    connectionSocket.send(capitalizedSentence.encode())
    connectionSocket.close()
```





# Capitolo 3: Livello di Trasporto

L’obiettivo per questa sezione è capire i principi che sono alla base dei servizi del livello di trasporto, come ad esempio:

* multiplexing/demultiplexing
* trasferimento dati affidabile

* controllo di flusso
* controllo di congestione

Per raggiungere questo obiettivo si descriveranno i protocolli del livello di trasporto di internet: 

* UDP: trasporto senza connessione
* TCP: trasporto orientato alla connessione
* controllo di congestione TCP

## 3.1 Servizi e protocolli di trasporto

I servizi e i protocolli di trasporto forniscono la parte logica della comunicazione tra processi applicativi di host differenti, facendo in modo che i processi ottengano un tunnel risultante come se i processi interagissero direttamente tra loro. 

Di fatto il transport layer fa in modo che due processi si parlino attraverso la rete come fossero direttamente collegati.

I protocolli di questo layer servono sia dal lato invio, sia dal lato ricezione:

* all’invio tutti i messaggi vengono rotti in chunk e forniti di header di questo layer della pila di rete. Dopo questo chunk viene passato al protocollo di rete e inviato all’altro host.
* alla ricezione di questi messaggi il protocollo di rete si occupa di ottenere i dati e li fornisce al livello di trasporto, che si occupa di fornire i dati richiesti.

Questo livello e i protocolli a esso collegati lavorano completamente sugli host, e sono i responsabili dello “smistamento” dei pacchetti in arrivo al protocolli di rete. Questi pacchetti infatti contengono le informazioni nell’header che identificano il processo che sta ricevendo informazioni. Per questo motivo questo protocollo non ha la necessità di utilizzare e quindi conoscere l’indirizzo IP della macchina.

Il libro presenta la rete come il servizio postale, dove i postini che si occupano di ricevere e mandare mail tra le varie abitazioni sono i protocolli di rete, mentre le persone che all’interno della casa raccolgono tutte le buste in arrivo e le distribuiscono ai vari inquilini è il protocollo di trasporto. Di fatto quindi questi due livelli, lavorando molto similmente hanno una particolare caratteristica che li distingue: uno lavora sull’host (protocollo di trasporto) , l’altro lavora tra gli host (protocollo di rete).



Il livello di trasporto dispone di due protocolli:

* protocollo **TCP**: fornisce un servizio affidabile e garantisce che nella consegna il messaggio non venga modificato rispetto all’originario. Questo protocollo fornisce:
  * controllo di congestione
  * controllo di flusso
  * setup della connessione
  * affidabilità della connessione. Si può pensare come ci sia un vincolo di consegna.
* protocollo **UDP**: fornisce un servizio di consegna inaffidabile e senz’ordine.
  * estensione del servizio di consegna *best-effort* con nessun tipo di applicazione aggiunta
  * questo servizio non fornisce molte garanzie, ma allo stesso modo fornisce una grande velocità di trasferimento data la mancanza di overload di operazioni da effettuare prima di mandare il pacchetto sulla rete. Per questo motivo è molto utilizzato per mandare grandi quantità di dati oppure dati piuttosto piccoli ma che hanno la necessità di essere recapitati velocemente.
* in entrambi questi protocolli non si ha:
  * la garanzia sui ritardi
  * garanzia sull’ampiezza di banda
  * queste due funzioni possono essere implementate dagli sviluppatori, sia dal lato client che dal lato server (spesso da entrambe le parti). Ma questo non significa che la rete fornisca uno strumento efficace per effettuare queste operazioni direttamente.

Per questo motivo il servizio di consegna fornito dal protocollo IP viene definito non affidabile, siccome non riesce a garantire un servizio consistente sempre nella rete, ma lavora solo con il concetto di **best-effort**.



## 3.2 Multiplexing e demultiplexing

Queste operazioni permettono il corretto invio dei pacchetti e la corretta ricezione, facendo in modo di identificare in modo univoco il processo che richiede questi dati.

Di base il funzionamento molto ad alto livello è questo:

* il layer sottostante passa al layer di trasporto i datagrammi in arrivo sulla macchina
* il layer di trasporto analizza gli header e fornisce a ciascun processo tutte le informazioni a lui indirizzate

Il **demultiplexing** sono tutte le operazioni che permettono la ricezione dei singoli pacchetti ai rispettivi processi applicativi, mentre il **multiplexing** sono tutte quelle operazioni di aggiunta di header al pacchetto e di modifica che permettono il corretto passaggio al layer successivo e di conseguenza l’invio dei dati.

Per fare queste operazioni possiamo partire dal fatto che le socket hanno la necessità di avere indicazioni univoche sulla posizione a cui vanno indirizzati i pacchetti diretti a questi processi. Queste due informazioni sono chiamate **porta di destinazione e porta di origine**. Ogni porta è un valore di 16bit, che quindi può variare da 0 a 65535. Le porte da 0 a 1023 sono chiamate *porte conosciute* e servono per applicazioni importanti e di solito riservate al sistema operativo (infatti per usarle in Linux serve di solito avere i permessi di root). Queste porte sono conosciute attraverso l`RFC 1700` e costantemente aggiornate su [questo sito](http://www.iana.org).

Da notare come l’**indirizzo IP non è in questo header,** siccome non sono nell’ambito di competenza del livello di trasporto.

Ci sono due tipi di multiplexing e demultiplexing nel caso si voglia una connessione con persistenza o senza. Logicamente le casistiche e i protocolli che stanno alla base di questi tipi di connessioni sono diversi e le tecniche di demultiplexing e multiplexing che ne conseguono logicamente sono modificata a loro volta.

Tratteremo prima di queste due operazioni con connessioni senza e con connessione, poi tratteremo i protocolli che stanno alla base di queste operazioni.



### Demultiplexing senza connessione: UDP (User Datagram Protocol)

Il multiplexing e demultiplexing operato dal protocollo UDP è particolarmente lineare. La socket UDP è identificata da due parametri: IP destinazione e porta di destinazione. Il buffer UDP, non avendo connessione, “scarica” tutto sullo stesso buffer, come se consegnasse tutto alla stessa porta. Poi in seguito ogni segmento UDP viene inviato alla socket con il numero di porta di destinazione uguale a quello dell’header del pacchetto.

L’header contiene quindi anche la destinazione, questa opzione permette di avere un pacchetto di ritorno molto più veloce, infatti nel caso basta impostare come tuple `(IP, port)` di destinazione quella di origine nel messaggio ricevuto.



### Demultiplexing con connessione: TCP (Transmission Control Protocol)

La socket necessaria al funzionamento del TCP è identificato da 4 parametri: indirizzo IP di origine, numero di porta di origine, numero IP di destinazione, numero di porta di destinazione.

L’host che riceve questo messaggio utilizza questi parametri per inviare il segmento alla socket appropriata. Inoltre si può notare la prima grande differenza: due pacchetti in arrivo sullo stesso host ma con due IP diversi vengono direzionati su due socket diverse da subito, a differenza di come gestiva i pacchetti in arrivo UDP. Di fatto quindi un host server può servire contemporaneamente più socket TCP identificate da questi quattro parametri. Ogni connessione client con questo server web avrà una diversa socket di riferimento.

Il funzionamento tipo della connessione tramite protocollo TCP è:

* il server TCP ha un messaggio di accoglienza, che aspetta una connessione client con un IP e una porta
* il client TCP crea una socket TCP e invia un messaggio di richiesta di connessione al server
* un messaggio di connessione al server non è altro che un pacchetto TCP con una struttura di bit particolare nell’header (visti più avanti).
* Quando il server viene raggiunto da questa richiesta di connessione, si connette alla socket TCP in ascolto creata precedentemente e accetta la connessione. A questo punto crea una nuova socket per questo collegamento.
* I 4 valori che identificano questa nuova socket vengono condivisi con il client e ogni messaggio in arrivo sul server TCP con questi 4 valori (IP destinazione e origine, porta di destinazione e origine) sono uguali a una delle socket presenti sul server allora il processo di demultiplexing le reinderizza a quel determinato processo.

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107130307279.png" alt="image-20211107130307279" style="zoom:33%;" />

Nei più moderni server TCP si utilizzano i thread al posto di effettivi programmi diversi, in modo da lasciare sempre la porta occupa costante e inoltre ridurre la necessità di tempo e potenza della macchina per creare diversi processi.

Le connessioni HTTP non persistenti invece non si comportano come spiegato prima, ma ottengono una socket che viene aperta e chiusa per ogni messaggio inviato o ricevuto. Il problema di chiudere e aprire socket così velocemente può seriamente modificare le performance del server, ma alcuni sistemi operativi hanno possibilità di mitigare questo problema.



## 3.3 Trasporto senza connessione: UDP

Si basa sul FRC `768` ed è un protocollo non affidabile e senza fronzoli ovvero aggiunge ben poco rispetto al livello di rete (cioè di base solo le informazioni delle porte della socket). Per questo motivo le applicazioni che si possono basare su questo protocollo sono diverse e possono avere diversi scopi. Le caratteristiche di base dell’UDP sono che:

* riesce a trasmettere dati rapidamente e in pacchetti molto semplici (infatti non mantiene lo stato e altre informazioni)
* ha un header ridotto al minimo
* offre una latenza data da overlay di software minimizzata
* è molto utile quando si vogliono passare dati direttamente al livello successivo (di trasporto) oppure al precedente (di applicazione). In questo modo si lascia il compito di controllare i messaggi e fare tutte le operazioni sui pacchetti all’host finale lasciando il passaggio nella pila TCP/IP il più lineare e semplice possibile.

Il protocollo UDP quindi, a parte una piccola sezione di controllo degli errori, non fa nulla che non fa già il protocollo IP: aggiunge un piccolo header al pacchetto prima di mandarlo oppure lo rimuove al momento della ricezione.

Un esempio di applicazione che utilizza il protocollo UDP è il DNS, che sfrutta la mancanza di overlay di questo protocollo per inviare dati e risposte più velocemente. Inoltre il server DNS non dovendo mantenere la connessione con l’host dopo che ha risposto, sfrutta proprio questa caratteristica del protocollo. Il client inoltre se non riceve una risposta può decidere di rimandarla.

Adesso si potrà pensare che sia sempre meglio il TCP, date le sue molte garanzie. Invece con l’UDP si possono ottenere alcuni use case particolarmente efficaci:

* più granulare controllo da parte dell’applicazione di quanti, quali e dove i dati sono mandati. Questo nel TCP non è sempre disponibile, siccome il meccanismo di controllo delle congestioni impone di seguire un rigoroso protocollo che non sempre premette ai pacchetti finali di arrivare a destinazione.
* non si crea una connessione. TCP utilizza una tecnica per ottenere una connessione sicura e confermarla, cosa che l’UDP non esegue. Per questo motivo l’UDP non inserisce nessun delay nel mandare il pacchetto.
* connessioni senza stato. Mentre il TCP utilizza il controllo dello stato per effettuare una serie di controlli sui pacchetti in uscita e in entrata, UDP non fa nessuno di questo mantenimento. Di fatto questo permette solitamente che i server UDP permettano un maggior numero di connessioni in entrata.
* poco overhead sui pacchetti. TCP manda circa 20byte di dati in più ogni pacchetto, mentre UDP solo 8byte.

Molto spesso si utilizza l’UDP in tutte quelle applicazioni che richiedono un grande traffico, ma che può mantenere un buon livello di pacchetti persi.

Un pacchetto UDP invece segue una certa struttura, definita dal `RFC 768`:

* tupla contenente il numero della porta e l’IP

* dati dell’applicazione trasportati nel pacchetto

* lunghezza del messaggio che specifica il numero di bytes nel segmento UDP (header + data). Questo campo è particolarmente importante siccome i pacchetti UDP non hanno una dimensione predefinita.

* checksum: utilizzato per controllare che nel segmento ricevuto non ci siano errori. Questi possono avvenire nel trasporto oppure mentre aspettavano su un router.

  Calcolando che ogni “frase” del pacchetto UDP sia lungo 16bit, allora si incolonnano questi bit del pacchetto tra di loro.

  <img src="./image/image-20211107160908673.png" alt="image-20211107160908673" style="zoom:33%;" />

  Dopo si esegue una semplice operazione XOR, che essendo operazione direttamente sui bit è estremamente efficiente per il processore. Questa processione dei dati può essere vista come un metodo di hashing, ma allo stesso tempo non utilizza nessuna formula matematica, soltanto una formula logica di bit per bit.

  Di solito i pacchetti il cui checksum non corrisponde vengono eliminati e si aspetta che l’host di partenza lo riinvii, infatti le tecniche di recupero errori sono piuttosto complicate. Nonostante questo ci sono alcuni casi in cui invece conviene utilizzare una modalità di recupero errori, ad esempio nelle connessioni satellitari geostazionarie. Queste connessioni hanno un tempo di risposta di circa 3 secondi, motivo per cui conviene impiegare un secondo per recuperare un pacchetto perso piuttosto che aspettarne 6 che questo venga rimandato.

  L’argomento del recupero pacchetti persi o contenenti errori è molto complicato e spesso non applicato solamente alla rete, che preferisce (sia per ragioni di tempo sia per mantenere le cose semplici) di rimandare i pacchetti.

  Queste operazioni di controllo errori nel percorso vengono più volte ripetute in più di un livello, siccome non ci sono garanzie che protocolli sottostanti offrano questo tipo di controllo.

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107081923081.png" alt="image-20211107081923081" style="zoom:33%;" />





## 3.4 Principi del trasferimento dati affidabile

Il problema dell’affidabilità della rete (**reliable data service** o **rdt**) è molto importante e molto difficile da ottenere. Si torna sempre al solito problema, ovvero che la rete è nata dall’unione di molte reti diverse con diversa struttura e componenti. Per questo motivo avere la garanzia sul trasferimento effettivo dei dati è molto difficile.

Ma in questa azione noi vogliamo essere molto sicuri che: 1) i nostri dati arrivino e 2) i nostri datti arrivino *corretti*. Per fare questo server un tubo che operi questa operazione. Questo però come si diceva prima è un canale inaffidabile. Serve quindi un processo per ottenere questa tecnica, e serve anche che sia accessibile da tutti, siccome non si può pensare che si reimplementino tutti queste tecniche ogni volta che si vuole una connessione affidabile. Per questo motivo si lavora tramite un interfaccia, che permette di interfacciarsi a alto livello e produrre gli effetti desiderati.

Logicamente i luoghi fisici o logici in cui si possono ottenere errori sulla rete sono molteplici e quindi ci sono molteplici casualità che devono essere sempre controllate. Per questo motivo si utilizzano gli automi a stati finiti che connettono degli oggetti con uno stato (situazione del pacchetto, della rete, etc) definiti. Utilizzando questi schemi si ottiene uno schema con la semplificazione di tutte le operazioni che il protocollo effettua nei vari casi.

Logicamente per avere accesso a questa serie di protocolli si devono seguire una serie di passi. Da lato invio abbiamo bisogno di una funzione che invia i dati sul protocollo e consegna i dati da mandare a livello superiore del ricevente. Poi si passa attraverso le funzioni di controllo del protocollo affidabile. Il pacchetto in uscita viene ricevuto dalla funzione `udt_send()` che si occupa del trasferimento dei dati sul canale inaffidabile. 

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107165100602.png" alt="image-20211107165100602" style="zoom:33%;" />



Dal lato ricezione i dati vengono ottenuti attraverso la funzione `rdt_rcv()` che si occupa di recuperarli dal canale inaffidabile e passarli al protocollo di trasferimento affidabile. Una volta che questi protocolli vengono eseguiti e il pacchetto è stato controllato, allora la funzione `deliver_data()` si occupa di passare il pacchetto o i dati al livello superiore.

Ci sono più modelli di trasferimento affidabile, definiti rdt e che introducono miglioramenti partendo dal caso di base a modelli che introducono più errori.



### Rdt1.0: trasferimento affidabile su canale affidabile

Il canale di collegamento è perfettamente affidabile (completamente ideale), non si verificano errori nei bit mandati o ricevuti e non si ha nessuna perdita di pacchetti.

Il mittente invia i dati al canale sottostante, mentre il ricevente ottiene solamente i dati dal canale sottostante. 

Di fatto in questo caso non avviene nulla, da una parte si aspetta che i dati arrivino, dall’altra che arrivi la richiesta dei dati (di solito si dice che il sistema è in “ascolto” per un’eventuale chiamata).

![image-20211026141102197](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211026141102197.png)

L’unico evento in questo caso è che l’applicazione invochi la primitiva `rdt_send()`. Questo è un automa che aspetta dati e pacchettizza sulla rete. Le azioni che vengono svolte in modo continuo sono: creo un nuovo pacchetto con una primitiva, poi chiamo `udt_send(packet)` per inviare il pacchetto.

Al livello di ricezione si ottiene la primitiva che ottiene i dati (`rdt_rcv(packet)`) e poi, siccome il canale è affidabile, toglie l’intestazione e restituisce il pacchetto al livello superiore.



### Rdt2.0: canale con errori nei bit

Il canale preso in considerazione permette una comunicazione affidabile (quindi *non* si perdono pacchetti) ma con errori all’interno dei pacchetti (*errori ai bit del pacchetto*). Questo è il primo *rdt* che introduce delle operazioni sui pacchetti. 

Di base questa versione permette una trasmissione affidabile, introducendo un checksum che permetta di capire se il pacchetto ricevuto è corretto. 

La parte di correzione degli errori non è valida perché bisogna avere un pacchetto ridondante che utilizza di conseguenza tantissima banda. Per questo motivo non viene utilizzato questa funzione di recupero, eccetto su alcuni canali caratterizzati da particolare ritardo di trasporto o propagazione (o con un RTT molto elevato, tipo la connessione satellitare). Normalmente in questi casi si tende a rimandare il pacchetto. 

Logicamente il mittente deve sapere se il pacchetto è arrivato correttamente o no (quindi deve sapere se rimandarlo o meno), per cui si sono introdotti due messaggi di risposta (che sono di fatto dei **feedback** per il mittente):

* mandare una **notifica positiva (ACK)** (acknowledgement): il ricevente comunica espressamente che il pacchetto ricevuto è arrivato correttamente
* mandare una **notifica negativa (NAK)** (negative acknowledgement): il ricevente comunica espressamente al mittente che il pacchetto contiene errori. Dopo questo messaggio il mittente deve ritrasmettere il pacchetto.

![image-20211026141851149](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211026141851149.png)

Questo modello ha un funzionamento a blocchi definibile in questo modo:

* per il mittente (doppio stato):
  * A) ricevo dati al livello applicazione
    * creo il pacchetto da inviare e calcolo in checksum, poi lo invio
  * B) entro nello stato di `attesa di ACK o NACK`, ovvero aspetto la risposta del ricevitore
    * quando ricevo un pacchetto, controllo se è di risposta `NAK`. 
    * se il pacchetto è `NAK` allora rimando il pacchetto
    * se il pacchetto è `ACK` allora torno allo stato di `ATTESA DI CHIAMATA DALL'ALTO`
* per il ricevitore (stato singolo):
  * vado in `ATTESA DI CHIAMATA DAL BASSO` e poi elaboro due transizioni. 
    * Se il pacchetto arrivato non è corretto, allora mando un `ACK` al mittente
    * Se il pacchetto arrivato è corretto, allora mando `NAK` al mittente



### Rdt2.1: errore nei pacchetti di NAK o ACK

Il modello rdt2.0 ha un errore importante perché lascia la comunicazione di ritorno come *sempre* affidabile. Invece come posso avere errori nei dati posso avere errori nella trasmissione dei feedback (ACK, NAK).

Per applicare questo rdt ci si deve dotare di un protocollo di **stop & wait**, ovvero bisogna aspettare la risposta alla comunicazione di un pacchetto prima di poter mandare il prossimo. 

Per avere a disposizione tutte le informazioni per determinare uno stato finito in questo modello è necessario introdurre più stati.

IQuesto modello evita la **produzione di pacchetti duplicati**: trasmettendo un pacchetto che è già stato mandato perché il messaggio di avvenuta consegna non è arrivato correttamente fa in modo che il destinatario restituisca al livello successivo due volte gli stessi dati, producendo effetti potenzialmente indesiderati. Si introduce il concetto di **numerazione dei pacchetti** (in questo caso può essere 0 o 1).

![image-20211026143210458](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211026143210458.png)

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107172718209.png" alt="image-20211107172718209"  />

I due automi (nelle due slide, prima mittente poi ricevente):

* mittente:

  * creo un pacchetto numerato o 0 o 1. Il pacchetto viene mandato sulla rete

  * aspetto la ricezione del ricevente

  * Questo pacchetto di risposta può essere ACK/NAK valido (ovvero pacchetto corretto con lo stesso valore numero del pacchetto). 

    Come prima in questo stato si decide se rimandare o meno il pacchetto. 

* ricevente

  * questo valore deve sapere se aspettare il pacchetto 0 o il pacchetto 1
  * se ricevo un pacchetto corretto (il checksum non rivela errori), allora estraggo i dati e li consegno
  * creo il pacchetto nell’ACK con il valore di 0. 
  * Inizio a aspettare per il pacchetto 1

  Il mittente può avere un pacchetto ACK che confermi la corretta ricezione dello 0, perchè se no il trasmettitore non avanza. 

Ancora una volta però si ha un problema infatti aggiungere solo il numero 1 o 0 al pacchetto per definire la sequenza non è sufficiente per ottenere un ordine ben definito. Inoltre si inserisce anche il problema che bisogna controllare se i pacchetti di ACK e NAK sono danneggiati. 

Altro problema è che lo stato deve terne conto dello stato dei pacchetti, aggiungendo overhead di memoria per il determinato pacchetto. 

Dalla parte del ricevente invece si hanno degli altri problemi: si deve controllare se il pacchetto ricevuto è duplicato, e l’unica informazione disponibile per questo controllo è se la sequenza debba essere 1 o 0. Inoltre non può sapere se il il suo ultimo ACK/NAK è stato ricevuto correttamente dal mittente.



### Rdt2.2: un protocollo senza NAK

Questo protocollo ha le stesse funzionalità del rdt2.1, utilizzando soltanto gli ACK, usando il numero per rappresentare il NAK. Viene applicato al caso specifico in cui né il mittente né il ricevente ricevono pacchetti completi, ma solo frammenti. 

Se io mando un pacchetto 0 e ricevo un ACK 0, allora è intuibile che è stato ricevuto tutto correttamente. Se invece si manda un pacchetto 1 e si riceve un ACK 0, allora è come mandare un NAK, e di conseguenza il mittente può rimandare il pacchetto.

Ci sono quindi due tipi di ACK, quello cor retto e quello sbagliato (uno con il numero di sequenza corretto e l’altro con il numero di sequenza sbagliato).

Questo protocollo viene rappresentato a stati come: 

![image-20211107181512766](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107181512766.png)

Le modifiche in rosso permettono di inserire la possibilità che sia indice 1 o indice 



### Rdt3.0: canali con errori e perdite

Si lavora a questo punto su una nuova ipotesi, ovvero che il canale sottostante può anche smarrire i pacchetti (sia di dati che di ACK). Questo blocca il sistema rdt2.2. Nessuna azione viene attivata se non si riceve risposta, ma anzi si continua a aspettare, congelando il sistema.

Per ovviare questo problema si modifica il grafo a stati finiti da parte del mittente introducendo il meccanismo di **countdown timer**. (Questa pratica è sempre utile quando si lavora con la rete o in generale quando si parla di eventi, in modo da sbloccare lo stato anche se non avvengono delle operazioni).

Di solito si parla di perdita di pacchetti molto casuali, perché le perdite ci sono ma di solito sono abbastanza randomico. Alcune tecnologie di comunicazione permettono una minore perdita di pacchetti, mentre invece altre tecnologie hanno più problemi di questo tipo (soprattuto nei protocolli wireless). Di solito comunque i pacchetti persi di una linea considerata “buona” è di $10^{-3}$ solitamente. Se si ha una perdita media maggiore di questo valore il canale non è ritenuto ottimale.

Esiste anche il caso in cui il paccheto viene bloccato per congestione su un router che ha un buffer completamente pieno, ma di solito questi avvenimenti non vengono presi in considerazione dalla statistica.

Il funzionamento di questo protocollo è:

* per il mittente:
  * ogni pacchetto viene creato con numero di controllo e checksum.
  * una volta che il pacchetto viene inviato viene fatto partire un counter. In questo tempo posso:
    * ricevere pacchetti con errori o NAK (ACK con numero sbagliato) allora rinvio il pacchetto
    * se ricevo un pacchetto ACK corretto, allora mando il prossimo pacchetto e resetto il timer.
    * Se il countdown si ferma, allora faccio ripartire il pacchetto.
* per il ricevente viene mantenuto il diagramma a stati di prima



![image-20211107182117298](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107182117298.png)

![image-20211107182131564](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107182131564.png)

![image-20211107182148253](/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211107182148253.png)

Questa secondo schema evidenzia come il timer livello ricezione non è necessario, perchè si ha un controllo duplicati, che fa in modo di garantire il corretto arrivo dei pacchetti.

Nell’esempio *d)* invece si può notare che un timer troppo stretto rispetto al tempo di percorrenza del pacchetto fa in modo di bloccare tutto il diagramma a stati finiti siccome continuerà a mandare lo stesso pacchetto e il ricevente continuerà a tagliare pacchetti all’arrivo perché trova un duplicato.



### Rdt3.0: Prestazioni

Il protocollo è corretto dal punto di vista logico, siccome inserisce tutti i controlli necessari a avere un tipo di connessione e di trasmissione dei pacchetti corretto. Il problema che ci sono delle prestazioni molto basse, siccome ci si basa solamente sul concetto di *stop&wait*. Quindi il sistema è **robusto** ma **non efficace**.

Il protocollo infatti rischia di fornire un troughtput molto basso. Se si nota, dalle slides sopra, questo valore è rappresentato dai pacchetti blu e il fatto di aspettare la conferma fa bloccare tutta la comunicazione.

Assumento di avere una linea di 1Gbps, con ritardo di 15ms e pacchetti da 1KB. Con questi valore si ha:
$$
T_{trasm} = \frac{L(\text{lunghezza pacchetto in bit})}{R (\text{tasso trasmissivo in bps})} = \frac{8 \space Kb/pacc}{10^9 \space b/sec} = 8 \space microsec
$$
A questo valore di tempo di trasmissione si deve aggiungere la frazione di tempo in cui l’utente è impiegato nell’invio dei bit:
$$
U_{mitt} = \frac{L/R}{RTT + L/R} = \frac{0,008}{30,008} = 0,00027
$$
Questo vuol dire che si invia un pacchetto di 1KB ogni 30 msec, che di fatto rende il valore del throuthput di 33kB/sec in un collegamento da 1Gbps.

Questo significa che il protocollo di trasporto **limita le risorse fisiche**. Questo concetto è fondamentale, siccome su tutta la pila protocollare si può introdurre un valore che rallenta tutta la connessione. Logicamente il protocollo TCP fornisce un obbligo piuttosto grande sulla quantità di pacchetti scambiati e inviati sulla rete.

Questo problema è accettabile, perché si sta dando la precedenza alla sicurezza, ma non in questa quantità. Di fatto quindi servono altre tecniche per ovviare a questo basso bitrate di trasmissione lavorando sul concetto di *stop&wait*. Questo impone che il RTT sia:

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111180313993.png" alt="image-20211111180313993" style="zoom:50%;" />

### Protocolli con il pipeline

Un metodo per risolvere il problema della poca banda risultante dovuta all’overhead di comunicazione del TCP è il **pipeline**. Il concetto che sta alla base di questa idea è che ci sia più di un pacchetto in transito sulla linea di comunicazione, in modo da occupare il tempo sul canale di attesa di risposta con l’invio di altri pacchetti. I pacchetti al ricevitore potranno arrivare con errori o essere corretti, ma intanto si ha diminuito drasticamente il packet-rate della connessione, aumentando di fatto il collo di bottiglia che il protocollo *stop&wait* opera.

Logicamente questo tecnica è migliore perchè permette di avere un RTT sul canale del pacchetto singolo praticamente uguale, ma la banda risulterà divisa in questo modo:

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111181023022.png" alt="image-20211111181023022" style="zoom:50%;" />

In questo esempio si utilizzano solo 3 pacchetti in sequenza, e solo questa aumenta considerevolmente il tempo di emissione dei pacchetti. 

Logicamente, se riuscissimo a mandare tanti pacchetti da riempire completamente il canale, si ottiene un valore di $U_{mitt}$ notevole. Per questo motivo si eseguono dei controlli ogni certo tempo per capire quanti pacchetti si possono mandare oppure si utilizza un valore medio standard.

Da questo concetto risultano due applicazioni (tutti due derivati dal protocollo rdt3.0):

1. **Go-back-N**:

   $N$ sono il numero di pacchetti in sequenza senza attendere i rispettivi ACK. Aspetto il pacchetto di conferma solo dopo che questi siano stati inviati. 

   Il ricevente risponde solo con **ACK comulativi**: se si inviano $N=3$ pacchetti, e si ottiene una risposta con un ACK di valore 3, allora significa che tutti e tre i pacchetti sono stati ricevuti correttamente.

   Il mittente ha un timeout associato al più vecchio pacchetto senza che sia stato tornato un ACK. Questo significa che se il timer scade allora verrano rimandati tutti i pacchetti sequenzialmente dopo questo. Questo metodo è più facile da implementare.

2. **Ripetizione selettiva**:

   Il mittente può trasmettere fino a $N$ pacchetti diversi senza richiedere un ACK di risposta. Il ricevente fornisce un pacchetto ACK per ogni singolo pacchetto. 

   Il mittente stabilisce un timer per ogni pacchetto inviato che non ha ancora ricevuto un ACK, quando scade allora ritrasmette solo il pacchetto che non ha ricevuto ACK.

Di solito queste due tecniche sono entrambe valide e correttamente funzionanti. Inoltre hanno una notevole quantità di sotto-protocolli derivati. Spesso si ha una soluzione di questo tipo: il primo pacchetto indicava al server che protocollo utilizzare e veniva mantenuto per tutta la connessione. Fino a poco tempo fa il default era il *Go-back-N*. Adesso sia per questioni di efficienza che di costo (soprattutto nel wifi, che ha un livello di perdita pacchetti notevole) si preferisce la ripetizione selettiva. 

Entrambi i modelli hanno punti di forza e criticità e per questo vengono mantenuti entrambi per avere una migliore applicazione nei diversi use-cases.

Una visualizzazione di questi pacchetti si può trovare [qui](https://www2.tkn.tu-berlin.de/teaching/rn/animations/gbn_sr/).

##### Go-Back-N:

Dal lato del mittente si devono emettere un valore $N$ continuo di pacchetti. Questo valore poi può essere stimato in vari modi che verranno visti più avanti.

Dal punto di vista del **mittente** si ha una coda così costituita:

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111185120689.png" alt="image-20211111185120689" style="zoom:50%;" />

Il funzionamento di base è che il puntatore a `base` indica il pacchetto più vecchio senza una conferma, il puntatore a `nextseqnum` indica il prossimo pacchetto da mando dei pacchetti ACK duplicati.

I pacchetti arrivati fuori sequenza vengono scartati, quindi non c’è buffering.are, invece il puntatore che conta `base+N` oggetti serve per obbligare il mittente a non mandare troppi pacchetti sulla rete senza aspettare conferma. 

Di fatto questa coda indica che: 

* alcuni pacchetti sono stati mandati e è stato ricevuta conferma della ricezione.

* poi seguono $N$ pacchetti che si dividono in:

  * gialli: pacchetti che sono stati inviati ma non è ancora stata ricevuta la conferma. Hanno un timer attivo sul pacchetto inviato per primo e vengono mantenuti nel buffer finché non si ottiene conferma. Questo per evitare di doverli ricaricare nel caso di timer scaduto.
  * blu: pacchetti pronti per essere inviati e in attesa di lasciare l’host.

  Se questa sezione di sequenza è completamente gialla, allora si sta aspettando per un messaggio di conferma e si ferma. Infatti questo è anche un controllo sul numero di pacchetti emessi.

* poi la coda continua con i pacchetti in sequenza che aspettano di essere preparati per l’invio.

Il funzionamento generale è che nella finestra di controllo degli $N$ pacchetti si mantiene il puntatore `nextseqnum` che indica il prossimo pacchetto da mandare, mentre invece si ha il puntatore `base` che indica il pacchetto di cui si sta mantenendo il timer.

Quando un pacchetto ACK comulativo arriva tutta la finestra gialla viene confermata, diventando verde. La sezione di $N$ pacchetti si sposta avanti di tanti pacchetti quanti sono stati confermati (ovvero il primo pacchetto giallo sarà il `nextseqnum`). 

In questo caso si presume che ci sia un timer separato per ciascun pacchetto (l’implementazione è più faciel e permette una semplicità maggiore nella gestione del reset del counter), che impone allo scoccare del *timeout* di rimandare tutti i pacchetti successivi a quello a cui è scaduto il tempo. 

L’automa a stati di questo protocollo dalla parte del **mittente** è quindi il seguente (che può sembrare più facile, ma in realtà ha molta più logica):

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111190833599.png" alt="image-20211111190833599" style="zoom:50%;" />

Dalla parte del **ricevente**:

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111213306363.png" alt="image-20211111213306363" style="zoom:50%;" />

Questo modello dalla parte del ricevente permette di capire che il sistema emette un ACK corretto solo se si ottiene un numero più alto in sequenza, rispetto al contatore `expectedseqnum`, ovvero il contatore dei pacchetti arrivati correttamente. Questo permette una corretta gestione dei pacchetti ricevuti correttamente, ma c’è la possibilità che si creino dei pacchetti ACK duplicati.

I pacchetti arrivati fuori sequenza vengono scartati, quindi non c’è buffering.

Ecco un esempio di questo protocollo in azione:

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111214142466.png" alt="image-20211111214142466" style="zoom:50%;" />

Si può notare che:

* ci sono più pacchetti ACK1 mandati, infatti il mittente continua a rispondere con questo pacchetto finchè il mittente rimanda il `pkt1`.
* Questo modello inoltre fa vedere come tutti i pacchetti arrivati dopo il `pkt1` siano stati scartati siccome fuori ordine, e di come poi il mittente abbia dovuto mandare anche pacchetti che erano arrivati correttamente al ricevente.

##### Ripetizione selettiva:

Il modello della ripetizione selettiva è molto simile a quello del GBN, ma con la differenza che si crea un pacchetto di riscontro specifico per ogni pacchetto ricevuto correttamente. Questa pratica permette di controllare singolarmente la correttezza dei pacchetti e, in caso, salvarli in un buffer se sono fuori sequenza. Questa tecnica permette di salvare tempo e banda, in modo che non si debba rimandare tutti i pacchetti dopo un singolo errore come avveniva nel GBN.

IL mittente riceve quindi molti ACK, e ritrasmette solamente i paccheti di cui non riceve ACK. Logicamente, per applicare questa tecnica, è necessario utilizzare un timer su ogni pacchetto inviato. In questo modo se avviene il timeout, il pacchetto viene rimandato direttamente.

Questo protocollo utilizza comunque la tecnica di multipli invii di pacchetti (sempre $N$). Il problema che si può avere in questo caso è che il primo pacchetto non abbia ricevuto conferma, mentre tutti gli altri della finestra invece si. Questo blocca completamente la sequenza di invii, riducendo l’effettività di questo protocollo.

Lo schema di mittente (sopra) e ricevente(sotto) è riporato in questo lucido:

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211111220002146.png" alt="image-20211111220002146" style="zoom:50%;" />

Il funzionamento generale è così spiegabile:

* dalla parte del mittente e del ricevente si crea una finestra di $N$ pacchetti che verranno mandati/ricevuti in contemporanea.
* allo stesso modo si crea una finestra di $N$ pacchetti in attesa di essere ricevuti

Di fatto le azioni principali fatte dal **mittente** sono:

* se si dispone di pacchetti successivi pronti per l’invio all’interno della finestra di $N$ elementi, allora si può procedere all’invio
* se un pacchetto inviato va in timeout, allora si procede a rimandare il pacchetto. La finestra non avanza se l’ultimo pacchetto ha ancora attivo il timer.
* se riceve un ACK, marca il numero di pacchetto corrispondente a come arrivato correttamente e fa avanzare la finestra se questo è l’ultimo

Dalla parte del **ricevente** invece avviene che:

* se si ottiene un pacchetto corretto allora invia il pacchetto ACK corrispondente
* se si ottiene un pacchetto fuori sequenza si bufferizza il contenuto, ma si invia lo stesso il pacchetto ACK. Se questo pacchetto è l’ultimo della finestra si ottiene un’avanzamento di tutta la sezione di $N$ elementi.
* se si ricevono pacchetto fuori dalla finestra di controllo allora vengono ignorati



La ripetione selettiva però ha un **problema** che ne limita l’affidabilità. Questa azione però, siccome è conosciuta può essere limitata e arginata.

Se si utilizzano dei numeri di frequenza di un certo valore e si ottiene che, per errori vari le sequenze di controllo non siano mai state recapitate al mittente e quindi si ottiene che le due finestre siano sincronizzate con il numero di sequenza ma in realtà sono sbagliate di una finestra completa.

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211114163554551.png" alt="image-20211114163554551" style="zoom:50%;" />

In questo modo il pacchetto con numero di sequenza 0 viene inteso dal mittente come pacchetto iniziale, mentre dal lato ricezione viene inteso come il secondo 0 della serie. 

Facendo così si distrugge completamente l’affidabilità del trasferimento, siccome si sta di fatto duplicando un oggetto rispetto al valore di partenza. 

Di fatto questa situazione fa capire che bisogna necessariamente **utilizzare una numerazione di controllo opportuna**, ovvero di solito almeno due volte il valore della finestra di controllo.



In teoria la ripetizione selettiva è quella che offre le performance maggiori. 

Nella versione di TCP studiata in questo corso di solito si utilizza il GBN, ma sempre più spesso è facile trovare sistemi (soprattutto wireless, che hanno una grande perdita di pacchetti) utilizzare la ripetizione selettiva, in modo da minimizzare il tempo di trasferimento file.

In realtà tutti i protocolli TCP sono retrocompatibili, in modo da rendere la comunicazione sempre disponibile in base al protocollo utilizzato, che viene di solito imposto dal server e che viene corrisposto dal client. Questo impone che ogni pila disponga di più protocolli TCP al suo interno, tutti però con lo stesso funzionamento ad alto livello, ovvero garantire una comunicazione sicure e affidabile dei file scambiati sulla rete.

Nella realtà dimensione della finestra e del timeout quindi sono dei parametri modificabili. Di base comunque il TCP ha dei contatori di finestra un valore numerico anche fino a 32bit.



## 3.5 Trasporto orientato alla connessione: TCP

Il protocollo per garantire l’affidabilità e la comunicazione garantita sulla rete si utilizza il TCP. Questo protocollo implementa un misto delle tecniche viste precedentemente per garantire il corretto scambio di pacchetti. Per questo motivo il TCP è un protocollo “poliedrico” (ovvero che non è un unico protocollo, ma ci sono diverse opzioni e implementazioni). Gli RFC di base sono `RFC 792, 1122, 1323, 2018, 2581`, dove la RFC principale stabilisce il protocollo di base, poi le altre o utilizzano delle varianti e o introducono dei miglioramenti. Tutte le varianti però sono retrocompatibili e utilizzano l’intestazione standard del TCP studiato e implementato 30 anni fa.

In generale questo protocollo è l’implementazione dell’**Rdt3.0 con il pipelining**, ovvero utilizza:

* numerazione dei segmenti

* riscontri dei segmenti
* timeout
* pipeline (GBN o ripetizione selettiva)

Inoltre il TCP:

* fornisce connessione punto a punto: si può fare il collegamento solo fra una coppia di host

* è affidabile, ovvero crea un flusso che porta al trasporto di tutti i byte dal server al client in modo che non ci sia nessun tipo di modifica da quello originale. Si può pensare come un meccanismo che prende dal buffer di partenza e copia sul buffer di ricezione senza nessun tipo di perdita.

* utilizza la pipeline:

  * due algoritmi che sono responsabili della creazione della finestra di scambio dati dalla parte client e dalla parte server. Questi algoritmi spesso si scontrano, e servono per ottenere il controllo di flusso e il controllo di congestione, due caratteristiche tipo della connessione TCP.
  * il **controllo del flusso** dati serve per adattare la propria frequenza trasmissiva alla propria capacità e a quelle della rete, in modo da non sovraccaricare mai né destinatario né router.

* è **full duplex**: fornisce un protocollo completamente bidirezionale. Di solito si stima un client e un server che comunicano tra di loro attraverso la stessa linea e quindi le risposte del server sono mandate attraverso lo stesso canale/socket. 

* ha una dimensione massima di pacchetto (**MSM, maximum segment size**)

* è orientato alla connessione. Per garantire questo fatto all’apertura della connessione si utilizzano una serie di messaggi di controllo tra mittente e ricevente che servono a garantire la sicurezza degli scambi di file successivi. Questo scambio è chiamato **handshake** e viene sempre fatto prima dell’inizio dell’invio dei file. 

  Queste azioni servono per concordare il numero di sequenza dei messaggi (che logicamente deve essere uguale) e il metodo di comunicazione (Go-back-N o selective repeat, oppure varianti).

Il protocollo TCP è molto comodo sulla rete e da utilizzare perché fornisce una serie di controlli di default (come visto sopra). Questa cosa è molto utile se si vuole avere una connessione sicura e affidabile su un applicazione, senza dover reimplementare tutti i controlli su UDP, rendendo più veloce l’applicazione. Allo stesso modo però limita il controllo della connessione da parte dell’utente, con possibili ritardi dovuti a dei controlli sui pacchetti o sul flusso dei dati, che può portare a un ritardo di invio o ricezione dei dati.

Inoltre il procollo TCP crea dei pacchetti con un header molto più grande di un più semplice UDP (32bit rispetto a 8bit).

### Struttura dei pacchetti TCP

<img src="/home/giovannifoletto/Documents/UNI/reti/appuntireti/image/image-20211116162756764.png" alt="image-20211116162756764" style="zoom:50%;" />

Il pacchetto TCP è costituito da almeno 5 righe da 32bit ciascuna. Poi seguono dei campi opzionali che possono contenere una serie di informazioni che utente e ricevente si devono scambiare utili al protocollo. I dati sono l’ultima sezione e hanno lunghezza variabile fino a raggiungere la lunghezza massima del pacchetto. Tutti i valori inseriti all’interno di un pacchetto sono costituiti da righe di 32bit.

La struttura dell’header di un pacchetto TCP è così costituito:

* *I riga*:  **numero porta di origine e numero porta di destinazione** (due campi da 16bit)

  Questo valore è uguale al pacchetto UDP e indica le informazioni per trasferire i dati da livello applicativo di un host a un’altro. 

* *II  e III riga*: **numero di sequenza e numero di riscontro** (due campi da 32bit)

  Questi due valori servono al protocollo per garantire lo scambio affidabile dei dati, ovvero indicano con questi valori che dato dovrà comunicare il successivo ACK di conferma.

  La numerazione di questi valori è trattata nel paragrafo successivo.

* *IV riga*: (da tre sezioni: una da 10bit, una da 6bit e una da 16)

  La seconda sezione è chiamata anche **FLAG field**. Questi sono dei valori che possono essere veri o falsi, in base a se il bit è 1 o 0.

  * *prima sezione:* **lunghezza dell’intestazione** (4bit)

    Indica il numero di righe dell’intestazione (chiamate word e costituite da 32bit), per informare del numero di campi opzionali presenti nell’header. In questo modo il ricevente sa già la grandezza del payload in ricezione. 

    Se gli header opzionali sono stati lasciati vuoti, allora questo valore è 20byte.

  * *prima sezione*: **unused** (bit variabili)

    Per mantenere le righe del pacchetto sempre di lunghezza 32bit questo valore viene riempito di bit nulli.

  * *flag field*: **ACK**

    Indica se il numero di riscontro nell’intestazione è valido o meno, cioè se si sta strasportando un ACK valido (*peaky-bekking*, ovvero la pratica di utilizzare un pacchetto dati come ACK di un altro pacchetto)

  * *flag field*: **RST**, **SYN** e **FIN**

    Sono pacchetti utilizzati per il setup della connessione TCP e successivamente per la chiusura.

  * *flag field*: **CWR** e **ECE**

    Sono utilizzati in esplicite situazioni di congestione. Raramente utilizzati (infatti con questi due parametri il flag field diviene di 8bit).

  * *flag field*: **PSH**

    Indica che i dati ricevuti vanno passati subito al layer successivo

  * *flag field*: **URG**

    Indica che il pacchetto mandato è stato segnalato dall’applicazione mandante o ricevente come urgente.

  * *terza sezione*: **finestra di ricezione**

    Questo parametro indica il numero di byte che il ricevitore conferma di poter ricevere, ovvero la dimensione di $N$ in byte che il ricevitore ha a disposizione per la connessione.

    Questo viene fatto in byte perchè permette una dimensione dinamica della finestra di recezione del ricevitore oppure può indicare la capacità di buffering del server.

* *V riga*:

  * *prima sezione*: **checksum** 

    Questa sezione indica il valore di controllo (calcolato come il pacchetto UDP) con la funzione di controllare l’integrità dei dati scambiati.

  * *seconda sezione*: **puntatore ai dati urgenti**

    Questa non è una funzionalità molto utilizzata, ma serve a indicare che una certa parte del payload è urgente. 

    Questa segnalazione non serve a TCP, ma serve semplicemente alle applicazioni che utilizzano questo protocollo, che otterranno un valore di dati segnalato come urgenti. 

    I dati di questo pacchetto viaggiano allo stesso modo e vengono trattati dal TCP allo stesso modo, con l’unica differenza che l’applicazione che riceve questo pacchetto ha un puntatore diretto a questo tipo di informazioni.

* **options**: 

  Campo di lunghezza variabile di valore `lenght` nella *IV riga*.

  Questo campo è utilizzato da mittente e ricevente per negoziare il MSS (maximum segment size) o come fattore di scala per delle reti ad altra performance.

  Questo segmento contiene anche il time-stamp. 

  Ulteriorio informazioni di questo parametro sono definite nel `RFC 854` e `RFC 1323`.

* **data**

  Contiene tutti i dati che il pacchetto vuole trasportare.

  Ad esempio nel caso di un pacchetto TCP di una comunicazione HTTP a questo punto inizia l’header di HTTP.

#### Numeri di sequenza e ACK nel TCP

Il numero di sequenza e il numero di ACK in realtà non sono sequenze come indicate negli schemi (quindi ad esempio 1-2-3 ...). Questi valori di solito indicano dei valori di byte.

Ad esempio, prendiamo un pacchetto di 400byte diviso in 4 segmenti da 100byte, il numero di sequenza consecutiva sarà:

1. `#100`, perché decido di chiamarlo così. Questo pacchetto ha lunghezza di `1000byte`.
2. `#1100` (perchè è il valore `#100` del primo e il dato di lunghezza del pacchetto), con lunghezza ancora di `1000byte`.
3. `#2100` (perchè `1100+1000`), sempre di lunghezza di `1000 byte`
4. `#3100` per concludere.

Allo stesso modo gli ACK:

1. Ricevo il pacchetto `#100`, con una lunghezza di `1000byte`. Il corrispondente ACK è `ACK=1100`.
2. Questo sarà `#2100` (`1100` di prima, più la lunghezza del payload ricevuto)
3. Sarà `#3100` 
4. `#4100`

Questo esempio quindi rappresenta uno scambio di questo tipo (dove il *flag: A* è disattivato, quindi si ottiene un pacchetto di ACK singolo per confermare ogni risposta, a differenza dell’esempio successivo che lo scambio di dati deve essere bidirezionale e quindi l’ACK di conferma è sempre inserito in un pacchetto di dati con un *flag: A* attivo):

```mermaid
%% Example of sequence diagram
  sequenceDiagram
    HostA->>HostB: SeqNum=100, ACK=0, lenght=1000
    HostB->>HostA: SeqNum=0, ACK=1100, lenght=1000
    HostA->>HostB: SeqNum=1100, ACK=0, lenght=1000
    HostB->> HostA: SeqNum=0, ACK=2100, lenght=1000
    HostA->>HostB: SeqNum=2100, ACK=0, lenght=1000
    HostB->>HostA: SeqNum=0, ACK=2100, lenght=1000
    HostA->>HostB: SeqNum=3100, ACK=0, lenght=1000
    HostB->>HostA: SeqNum=0, ACK=3100, lenght=1000
    HostA->>HostB: SeqNum=3100, ACK=0, lenght=1000
    HostB->>HostA: SeqNum=0, ACK=4100, lenght=1000
```





Di fatto quindi si ottiene uno schema di sequenze numeriche e ACK di questo tipo (Calcolando la lunghezza dei pacchetti sempre di `1000byte`:

```mermaid
%% Example of sequence diagram
  sequenceDiagram
    HostA->>HostB: SeqNum=42, ACK=79, data=`C`
    HostB->>HostA: SeqNum=79, ACK=43, data=`C`
    HostA->>HostB: SeqNum=43, ACK=80
```

Da questo schema si riesce a capire come si ottengono i valori di sequenza dei pacchetti e i rispettivi ACK:

1. Si ha un ACK, un SeqNum e un payload (data)

   I numeri che andiamo a utilizzare li inseriamo nell’ACK e nel numero di riscontro. Il primo indica dove è posizionato il buffer (SeqNum), il secondo indica fino a che punto il ricevitore ha ottenuto i dati in maniera corretta (ACK). In fondo al pacchetto logicamente c’è il payload.

   Inoltre il dato di ACK indica anche se il ricevitore è in difficoltà, siccome indica qual’è ultimo pacchetto che ha ricevuto correttamente.

2. Il secondo pacchetto ha attivo il *flag: A*, ovvero si indica che questo pacchetto sta anche confermando un pacchetto precedente. 

   Questo significa che per convertire il pacchetto come risposta, si inverte ACK e SeqNum e poi si aggiunge `+1` all’ACK.

3. Il terzo pacchetto non vuole più pacchetti, allora riinvia lo stesso senza problemi



La gestione dei **segmenti out-of-order** dipende completamente dall’implementazione del ricevitore, siccome il suo comportamento non è in nessun modo definito da RFC. Chi trasmette logicamente non deve occuparsi di questa problematica.

Le differenze di gestione di questi pacchetti si avvertono solo nel caso di applicazione del protocollo di *selective repeat*, che sfrutta proprio la bufferizzazione dei pacchetti arrivati precedentemente fuori ordine per ottenere una rete risultante più veloce. Altre implementazioni invece possono optare per distruggere questi pacchetti e mandare ACK negativo come risposta, mantenendo quindi solo i pacchetti arrivati nell’ordine corretto.



### Stima del RTT (round trip time)

La stima di questo valore serve per avere un valore di **timeout** più corretto possibile (questa funzione è necessaria per il corretto funzionamento della rete, siccome permette il riinvio dei pacchetti in caso di perdita o mancata consegna), infatti se questo valore viene impostato senza questa stima:

* se valore troppo piccolo: ritrasmetto il pacchetto nonostante (forse) non sia stato perso il pacchetto, che potrebbe essere ancora in transito
* se il pacchetto è troppo grande: potrebbe andare bene (time out di partenza di solito sono un paio di secondi), ma attenzione che più si aumenta questo valore si aumenta il tempo di reazione per la perdita di pacchetti (quindi ci metto più tempo a ottenere il valore che voglio)

Questo valore può cambiare anche sulla stessa rete nella stessa condizione, anzi è un comportamento che il TCP si aspetta siccome utilizza anche questo valore come modo per limitare il traffico in uscita (controllo del flusso). Di base, questo tempo aumenta se la rete è congestionata o se l’host è molto lontano.

Il TCP utilizza i pacchetti di dati che sta inviando sulla rete come modello, senza inserire dei pacchetti ad hoc e quindi evitando di utilizzare banda per pacchetti “inutili”. 

Per sapere quanto impiega un pacchetto a essere ricevuto dal server (durata del RTT) si può dedurre attraverso la finestra impiegata dal client a ricevere il pacchetto di conferma da quando ha inviato il pacchetto inizialmente. Tutti i calcoli vegono fatti da lato client. Questo valore viene chiamato `SampleRTT`.

Chiaramente questo modello di misurazione non garantisce un valore preciso, ma permette comunque di avere un idea abbastanza corretta di un RTT normale. Inoltre se si cerca di capire l’RTT i pacchetti ritrasmessi vanno ignorati. Questo valore viene poi utilizzato come primo valore del RTT, e in seguito corretto per renderlo più vicino al reale possibile con la tecnica della **media mobile esponenziale ponderata**, che è rappresentata in questa formula:
$$
\text{EstimateRTT} = (1- \alpha) * \text{EstimatedRtt} + \alpha * \text{SampleRTT}
$$
Questo calcolo indica che la stima del RTT è la stima precedente (quindi la formula è *ricorsiva*) e quindi significa che faccio la media tra il campione di RTT creato adesso e il valore precedente, poi ci aggiungo $\alpha * SampleRTT$. Questo valore viene aggiunto per evitare delle oscillazioni molto grosse dovute alla media tra valori possibilmente molto variabili. In questo modo invece ottengo una media mobile (perchè ho una storico di tutta la connessione), ma i pacchetti precedenti hanno sempre meno peso.

Il valore tipico di $\alpha = 0,125$.

Un esempio di questo valore come cambia (e soprattutto come variano i valori nello storico è):


$$
EstimatedRTT(t)=(7/8)*EstimatedRTT(t-1)+1/8*SampleRTT(t) \\ EstimatedRTT[0]=SampleRTT[0] \\ 
EstimatedRTT[1]=7/8*EstimatedRTT[0]+1/8*SampleRTT[1]= =7/8*SampleRTT[0]+1/8*SampleRTT[1] 
\\ EstimatedRTT[2]=7/8*EstimatedRTT[1]+1/8*SampleRTT[2]= =7/8*(7/8*SampleRTT[0]+1/8*SampleRTT[1])+1/8*SampleRTT[2]
$$
Da questi calcoli si capisce come il peso dei valori rimanga sempre, ma di fatto più il campione diventa vecchio più il suo valore si approssima allo zero. Di solito si può stimare il valore alla media dei primi 15 valori, mentre gli altri nello storico sono stati resi approssimabili allo zero.

Chiaramente in tutti questi calcoli si ignorano i valori di ritrasmissione, in modo da non ottenere dei valori di RTT troppo bassi rispetto all’effettiva operabilità della rete.

Di base questo valore è molto variabile, quindi si cerca di ottenere la *deviazione del RTT*, ovvero di quanto il valore ottenuto si discosta dal valore atteso. Questo valore si ottiene con questa formula:

$$
DevRTT= (1 - \beta)*DevRTT + \beta*|SampleRTT - EstimatedRTT|
$$
Tipicamente $\beta = 0,25 = 1/4 = 2^{-2}$ (si utilizza questo valore perché essendo multiplo di due è molto comodo).

Per impostare l’**intervallo di timeout** quindi:
$$
TimeOutInterval = EstimatedRTT + 4* DevRTT
$$
Questa formula è *empirica* e con questi test la formula si è dimostrata efficace al massimo.

Questa formula è piuttosto interessante siccome in modo indiretto ottiene una serie di informazione sulla rete (in particolare al flusso di pacchetti e allo stato di congestione), senza aver bisogno di pacchetti aggiuntivi. In questo modo si permette di avere una serie di statistiche necessarie al funzionamento della rete senza però introdurre ritardi oppure diminuire banda (siccome si introducono pacchetti sulla rete).

Inoltre questo valore è interessante siccome si ottiene una buona media tra un valore che permette di reagire prontamente alla perdita di un pacchetto senza però introdurre troppo ritardo  di recupero della connessione nel caso di perdita di pacchetto. Di fatto in questo modo:

* se si ha un timeout troppo grande non si reagisce alle perdite di pacchetto abbastanza prontamente, quindi la rete risulterà rallentata
* se si ha un timeout troppo piccolo allora si tenderà a rimandare una serie di pacchetti più volte, senza aspettare la necessaria conferma, e di nuovo quindi ottenendo una rete risultante rallentata

![image-20211206211620058](image/image-20211206211620058.png)

Di base tutto il calcolo del timeout presentato è lo standard di calcolo per il TCP, mentre invece si può impostare un valore di base deciso dall’utente, ma si otterrà probabilmente una perdita di prestazione.



### Principi di trasferimento dei dati affidabile

 Il protocollo che permette il trasferimento affidabile di dati su una rete inaffidabile fornita dal protocollo IP è chiamato **TCP**, e ha diverse caratteristiche:

* pipeline dei segmenti

* ACK cumulativi

* TCP utilizza un solo timer di ritrasmissione

* Le ritrasmissioni sono avviate da: 

  * eventi di timeout (GBN e altre tecniche)

  * ACK duplicati

    Se si utilizzano ACK cumulativi, ricevere un ACK duplicato è come ricevere un NAK. Questo di fatto fa assumere al protocollo TCP che il pacchetto non sia stato correttamente consegnato e quindi il mandante cerca di rimandarlo prima di ottenere un timeout.

* inizialmente utilizziamo un mittente TCP semplificato: 

  * ignoriamo gli ACK duplicati
  * ignoriamo il controllo di flusso e il controllo di congestione (queste tecniche servono per non creare blocchi e scarso funzionamento dal lato server)

* si introduce il controllo di flusso e di congestione. In questo modo si cerca di evitare di distruggere o rallentare la rete con l’immissione di troppi pacchetti.

Di fatto il TCP nelle prime versioni aveva solo il controllo di flusso, poi nelle versioni successive è stato introdotto anche il controllo della congestione. Di base in queste prime sezioni si stanno prendendo in considerazione solo le prime specifiche del protocollo.

#### TCP: eventi del mittente

Il caso più semplice è quando un applicazione chiede a TCP di creare dati. In questo caso il TCP considera la stringa di dati che ottiene e crea dei segmenti in sequenza (è il valore del primo byte del segmento nel flusso di byte). Una volta che il pacchetto viene mandato allora si avvia il timeout (se non è già in funzione).

In questo caso l’operazione di invio dei pacchetti viene interrotta solamente se si ottiene un `TimeOutInterval`, che ha l’effetto di resettare il timer e ritrasmettere il segmento che ha causato l’evento. 

Questo evento non rimanda tutti i segmenti consecutivi al pacchetto che ha ottenuto il timeout, perché se è successo che solo quel pacchetto è andato perso, allora rimandarli tutti non è efficiente.



Di base quindi l’algoritmo che si ottiene rispetto a questo modello è così formulato (pseudocodice):

```c++
while(true){
    switch(evento){
            evento: "i dati dell'applicazione crano il segmento TCP con numero di sequenza NextSeqNum",
            	if( "timer not working yet"){
                	startTimer();
            	}
            	passa il segmento al livello inferiore (IP);
            	NextSeqNum = NextSeqNum+(lunghezza dati);
        	
            evento: "timeout del timer",
            	"rtrasmetti il segmento non ancora riscontrato con il più piccolo numero di sequenza"
                startTimer();
            
            evento: "ACK ricevuto, campo ACK pari a y",
            	if(y > SendBase){
                    SendBase = y;
                    if("esistono ancora segmenti non riscontrati"){
                        startTimer();
                    }
                }
    }
}// end loop

/*	In questo esempio:
	SendBase -1 : ultimo byte comulatamente riscontrato
*/  
```



#### TCP: Scenari di connessione

<img src="image/image-20211206215827582.png" alt="image-20211206215827582" style="zoom:50%;" />

<img src="image/image-20211206215648099.png" alt="image-20211206215648099" style="zoom:50%;" />

In questi scenari si può vedere come è possibile l’applicazione del protocollo TCP nel caso di perdita del riscontro, nel caso di timeout prematuro, nel caso di riscontro cumulativo, e nel caso non si ottenga nulla dall’altra parte.

Di fatto questi schemi indicano come la funzione timeout permetta di risolvere principalmente tutti i problemi che si possono riscontrare durante una connessione.

Inoltre tutti questi controlli vengono fatti dalla parte del client, di fatto quindi si ottiene un modello della gestione completamente staccato dal server che punta a ridurre la banda necessaria per il trasferimento dei pacchetti e l’accumulo dei pacchetti nella rete.

#### TCP: generazione di ACK

Questi scenari sono descritti nel protocollo dal`RFC 1122` e dal `RFC 2581`.

Queste specifiche permettono di ridurre il numero di ACK emessi per evitare di non avere un’accumulo di pacchetti sulla rete, ma allo stesso modo di non perdere l’affidabilità della rete.

In questa sezione del protocollo vengono definiti una serie di eventi dalla parte del destinatario che corrispondono a un azione intrapresa:

* Arrivo di un segmento con **numero di sequenza atteso**. Tutti i dati fino al numero di sequenza ricevuto sono stati riscontrati. 

  * Si suggerisce l’**ACK ritardato**: aspettare 500ms affinché arrivi il prossimo segmento. Se il segmento non arriva, allora si invia un ACK per il pacchetto riscontrato.

    Questo significa che otterrò un ACK bufferizzato per 500ms, ma se si ottiene un secondo ACK allora si può mandare un unico ACK comulativo, ottenendo una minor occupazione della banda.

    Non si può tenere l’ACK più di questo tempo nel buffer o si otterrebbe un notevole errore nel calcolo del RTT del client, che risulterebbe in una rete più lenta.

* Si ottiene un pacchetto con numero di sequenza atteso e un altro ACK è in attesa di essere mandato.

  * Si crea un pacchetto cumulativo che indichi l’avvenuta ricezione di entrambi i pacchetti.ù

* Arrivo **non ordinato** con un numero di sequenza superiore a quello atteso. Viene rilevato un buco.

  * Invia un ACK duplicato indicando il valore del pacchetto atteso.

* Arrivo di un pacchetto che colma parzialmente o completamente il buco

  * invia immediatamente un ACK, ammesso che il frammento inizi dall’estremità inferiore del buco



C’è un algoritmo che permette di avere una ritrasmissione rapida dei pacchetti con ACK sbagliato. Questo pseudocodice non fa altro che aggiungere un evento di questo tipo:

```c++
evento: "ACK ripetuto, con valore del campo ACK pari a y"
    if(y > SendBase){
        SendBase = y;
        if("esistono attualmente segmenti non ancora riscontrati"){
            startTimer();
        }
    } else { // ACK duplicato per un segmento già riscontrato
        "incrementa il numero di ACK duplicati ricevuti per y"
            if("numero di ACK duplicati ricevuti per y=3"){ // ritrasmissione in caso di 3 ACK duplicati, ottenendo quindi una ritrasmissione rapida
                "rispedisci il numero di sequenza y"
            }
    }
```



#### TCP: controllo di flusso

Il controllo di flusso nella rete serve per evitare che il buffer del ricevitore non vada mai in overflow per colpa dei pacchetti dei mittente.

Il concetto di base di questo tipo di controllo è che il buffer della socket fornita al livello applicazione non sia mai pieno, e che quindi possa continuamente accettare pacchetti in arrivo. Di fatto quindi serve che le applicazioni che devono leggere questi dati li elaborino più velocemente di quanti questi arrivino, in modo che non si trovi mai un accumulazione esagerata (situazione non funzionale, siccome si perdono dati all’interno della pila protocollare, che è orribile, siccome di solito si da per scontato che si perdano pacchetti solo sulla rete, mai sugli host).

Per ottenere questo controllo si utilizza il campo del pacchetto TCP chiamato *receive windows* (se vuoi vedere la sezione clicca [qui](#Struttura dei pacchetti TCP)). Di base quindi il ricevitore dice al mittente il valore di dati che ancora può ricevere, permettendo quindi di non superare mai la soglia critica. (Ancora una volta quindi si utilizza un valore definito nell’header per permettere una comunicazione tra ricevente e mittente).

Il buffer di questa socket è creata dal sistema operativo e quindi ha uno specifico spazio a disposizione. Il calcolo di questo valore quindi è semplicemente la differenza tra il buffer TCP e la finestra in attesa.

Di base quindi questo meccanismo è funzionale siccome indica al mittente quanto spazio può ancora utilizzare.

Il ricevitore può indicare anche valore zero di questo pacchetto e in questo caso il mittente:

* spegne il pipelining
* smette di mandare pacchetti sulla rete indirizzati a quella socket
* inizia a aspettare il primo ACK (per forza duplicato) che indicherà una finestra di socket di nuovo libera.

Questa protezione è necessaria siccome sulla rete sono presenti tutti i tipi di host disponibili, e quindi non si ha il controllo o la percezione di quanto la socket ricevente sia grande. In questo modo il TCP automatizza il controllo assicurando ancora una volta un corretto scambio di pacchetti.

Questo modello è molto efficace, ma di fatto non è privo di errori. Infatti si ha una latenza dovuta allo scambio di pacchetti, ma in qualsiasi caso se il ricevitore perde dei pacchetti allora si ottiene un effetto risultante come quello di perdita di pacchetti sulla rete (ovvero non si ottiene un ACK di conferma).

Di base questo protocollo ha la capacità di stimare la dimensione del buffer in qualsiasi momento con un semplice calcolo, utilizzando solo la dimensione della finestra disponibile ottenuta nel pacchetto di risposta:
$$
RcvWindow = RcvBuffer - [LastbyteRcvd - LastByteRead]
$$


#### TCP: gestione della connessione (apertura)

La gestione della connessione nel TCP è la parte principale dell’affidabilità della connessione. Infatti i pacchetti che vengono persi è sempre un lato negativo, ma ci sono modalità per limitarne gli effetti e ridurre l’impatto che questo ha sulla connessione. Il fatto principale della connessione TCP invece è proprio che mittente e ricevente si accordino su dei parametri utilizzabili poi per implementare i meccanismi di affidabilità. 

Tutte queste azioni e variabili vengono definite nella primissima parte della connessione, attraverso l’**handshake a tre vie** (“una stretta di mano con tre messaggi”). 

Il **3-way handshake** viene svolto in questo modo:

```mermaid
%% Sequence diagram	
	sequenceDiagram
Client->>Server: SYN;
Server->>Client: SYNACK;
Client->>Server: ACK;
```

* *primo passo*:
  * Il client inizializza la connessione, manda un messaggio `SYN` al server
  * specifica il numero di sequenza iniziale (il numero da cui inizia a contare i pacchetti)
  * ha attivo il bit di flag `SYN`.
* *secondo passo*:
  * Il server riceve il pacchetto del client, e risponde con un segmento `SYNACK`
  * specifica il numero di sequenza iniziale che utilizzerà il server
  * ha attivi due bit di flag: `SYN` e `ACK`
* *terzo passo*:
  * il client riceve il pacchetto, risponde con un `ACK`
  * specifica la conferma della ricezione del numero di sequenza in utilizzo dal server
  * ha attivo solo il bit di flag `ACK`.
  * questo pacchetto, per aumentare l’efficienza della connessione (siccome dopo 3 pacchetti non è ancora stato mandato nessun dato) può contenere anche un payload di dati.

Questa azione è stata implementata con tre messaggi perché c’è la possibilità di avere un apertura di connessione non corretta se si utilizzassero solo due messaggi. (Il problema sta nella ricezione del pacchetto `SYNACK` che potrebbe portare alla conferma di un numero di ricezione errato del SeqNum del server da parte del client).

Un esempio di handshake a tre vie può essere:

```mermaid
%% Example of sequence diagram
  sequenceDiagram
    Client->>Server: Syn 3546
    Server->>Client: Syn 7654, ACK 3547
    Client->>Server: ACK 7655
```

Da notare che il client conta da `3546` e il server conta da un altro numero, di solito non collegati e quindi fuori da ogni successione. Il valore di *SeqNum* del client è confermato dal server attraverso il messaggio `ACK 2547`.

Il motivo per cui questo modo per impostare il setup della connessione si ha la necessità di avere 3 messaggi viene dimostrato con questo esempio:

```mermaid
%% Example of sequence diagram
  sequenceDiagram
  	Client->>Server: SYN 9999
    Client->>Server: Syn 3546
    Server->>Client: Syn y, ACK 10000
    Client->>Server: ACK y+1, RST
    Server->>Client: SYN 7654, ACK 3547
    Client->>Server: ACK 7655
```

* nel passato il client ha mandato un pacchetto `SYN` numerato `9999` che viene ricevuto ora dal server
* il client riceve anche il pacchetto `SYN` dallo stesso host numerato diversamente.
* A questo punto il client risponde con il suo SeqNum, ma aggiunge anche il bit flag `RST` ovvero il bit che indica il reset della connessione. Questo avviene perché il client ha notato che il server non sta rispondendo al messaggio corretto.
* Il pacchetto `RST` indica al server che sto rifiutando il pacchetto `SYN y, ACK 10000`. 
* Dopo il messaggio di reset inviato dal client il server riinvia il secondo messaggio, aspettando risposta positiva

Di fatto il pacchetto che contiene il flag `RST` annulla tutti i pacchetti che non seguono l’ultima richiesta di connessione, senza valutarne il contenuto.

Un pacchetto si può ottenere anche a metà di una connessione, ma di solito è associato a un problema non prevedibile con la connessione e viene solitamente risolto con un annullamento della connessione e un nuovo handshake.



#### TCP: gestione della connessione (chiusura)

Di solito il client è il responsabile della chiusura della connessione, come l’apertura. Di base questo modello è molto simile all’apertura della connessione.

La connessione non essendo affidabile può anche saltare in qualsiasi momento. In questo caso la disconnessione avviene in modo non controllato e si lasciano tutti i pacchetti pendenti come persi. 

Nel caso normale invece che si ha finito di aver bisogno di una connessione e si vuole procedere a una disconnessione si può operare una serie di azioni che permettono di essere sicuri di chiudere la comunicazione dopo che tutti i pacchetti sono transitati.

```mermaid
sequenceDiagram
Client->>Server: FIN
Server->>Client: ACK
Server-->Client: chiusura della connessione del server
Server->>Client: FIN
Server-->Client: attesa temporizzata
Client->>Server: ACK
```

Di fatto quindi le azioni possono essere così descritte:

* *primo passo*: il client invia un pacchetto `FIN` al server, indicando che si ha intenzione di chiudere la connessione.
* *secondo passo*: il server conferma la ricezione del pacchetto del client con un `ACK`. Una volta che il server è pronto a chiudere la connessione allora invia a sua volta un pacchetto `FIN`
* *terzo passo*: il client riceve un `FIN` dal server quando questo ha terminato la connessione
  * inizia un attesa temporizzata che serve a recuperare tutti gli eventuali segmenti che possono essere ancora in traffico sulla rete. Il client risponde `ACK` a tutti i `FIN` che riceve.
* *quarto passo*: il server riceve un `ACK` dal client e chiude la connessione



Un riassunto attraverso diagrammi a stati per l’apertura la chiusura può essere rappresentata così:

![image-20211207153608391](image/image-20211207153608391.png)



## 3.6 Principi del controllo di congestione

La rete ha da sempre il problema della congestione siccome è creata da molte tecnologie e capacità diverse. Questo significa che si possono creare degli accumuli di pacchetti in passaggio per una determinata zona della rete. Nel caso gli host o i router non riescano più a contenere i pacchetti in arrivo allora può avvenire una situazione di congestione critica, ovvero possibilità di aver perdita di pacchetti e tempo di trasmissione aumentato notevolmente.

Questa situazione può essere risolta in due modi: la rete smette di accettare pacchetti oppure gli host che immettono pacchetti “controllano” lo stato di salute della rete prima di iniziare una comunicazione. Come più volte visto l’approccio applicato è quello di risolvere i problemi ai bordi, quindi si utilizza un controllo da parte degli host attraverso il protocollo TCP.

Le prime versioni del TCP non aveva controllo di congestione, ma solo di flusso (che è un’altra cosa).

Solitamente una situazione di congestione critica viene anticipata da un periodo di tempo in cui:

*  pacchetti vengono persi (per l’overflow dello spazio a disposizione sugli host)
*  lunghi ritardi nella trasmissione

Spesso i router preferisce avere sempre la possibilità di poter scegliere di tenere o buttare via il pacchetto. Per ottenere questa possibilità fa in modo di avere sempre il buffer abbastanza vuoto da poter accettare sempre i pacchetti in arrivo, ovvero iniziando a eliminare i pacchetti prematuramente per evitare che al bisogno ci sia necessità di spazio. Spesso inoltre si possono avere delle regole che stabiliscono che in determinate situazioni alcuni tipi di pacchetti vengano eliminati di default.

Il funzionamento del controllo di congestione si basa sul concetto che i pacchetti vengano buttati via. Questo può avvenire sia per una congestione critica, sia per triggerare il controllo sulle congestioni degli host quando il nodo decide di aver raggiunto una quantità di pacchetti nel buffer abbastanza consistente per iniziare a “proteggersi”.

Quando il nodo si accorge di iniziare a perdere dei pacchetti l’host attiva il controllo di congestione e quindi riduce i pacchetti inseriti nella rete nel tentativo di migliorare la situazione di congestione.

Il problema della stabilità e della congestione della rete è uno dei 10 grandi problemi della rete, e viene tuttora studiato. Di solito l’approccio più utilizzato è quello di iniziare a “sfiatare” i pacchetti prima che si raggiunga la soglia critica di congestione, in modo da mantenere sempre la rete operativa sopra un certo livello.

Inoltre il controllo di congestione è la funzione che da la forma al traffico internet, infatti i pacchetti verranno emessi conseguentemente a questo controllo, che regola non solo il numero di pacchetti emesso, ma anche la finestra di controllo del protocollo. Questo flusso non è consistente, ma è molto spezzato, inoltre non cerca neanche mai di ottenere un livello di finestra costante siccome la rete è molto variabile.

Di fatto questi comportamenti si possono notare sulla rete soprattutto quando ci sono degli down/up-load di file molto grandi, che hanno appunto un comportamento di questo tipo.

Inoltre va notato che il mittente è completamente responsabile della limitazione sulla connessione, attraverso questa operazione:
$$
LastByteSent - LastByteAcked \le CongesionWindow
$$
E di fatto quindi si ottiene una frequenza d’invio dei pacchetti di approssimativamente:
$$
SendFreq = \frac{CongestionWindow}{RTT} byte/sec
$$
Un esempio di utilizzo di questo valore può essere:

<img src="image/image-20211208101828087.png" alt="image-20211208101828087" style="zoom:50%;" />

Per forza di cose da questa formula si capisce diminuire il $RTT$ permette di aiutare il TCP a funzionare meglio. Da questo si capisce anche come le CDN siano molto utili e permettano un sostanziale aumento delle performance. 

Inoltre notare che la funzione che indica la finestra di congestione è dinamica rispetto al valore della congestione percepita dalla rete.



### Cause e costi della congestione DA FARE

Si possono avere degli approcci alla risoluzione della congestione diversi, ad esempio:

* Il throughtput non può mai superare la capacità

  <img src="image/image-20211207161427857.png" alt="image-20211207161427857" style="zoom:50%;" />

  In questo caso si ha una quantità di pacchetti sempre in aumento linearmente. In questo modello il tempo d

* il ritardo aumenta all’aumentare della congestione (si ottiene un grafico con un asindoto, non si arriverà mai al valore di arrivo). Il ritardo di consegna in questo caso è che il tempo di aspetto per il pacchetto è infinitamente grande (nonostante sia un asindoto nella visione teorica, ma prima o poi arriva lo stesso o viene droppato)

* perdite e ritrasmissioni riducono il trhoughtput. Questo significa che si utilizza tutta la capacità ma alcuni pacchetti vengono mandati più volte, quindi si ottiene un valore di dati scambiati minore.

Nella realtà si ottiene un grafico diverso (grafico 4 sulla slide di cause e costi della congestione).

Nella realtà si sommano una serie di effetti e in più si aggiunge il concetto che superata una certa soglia non i recupera più.

C’è un livello che è un filo prima del gomito in cui si arriva al limite della congestione, a questo punto si ottiene il valore massimo del throughput, ma se si continua a questo livello si potrebbe congestionare la rete, quindi si ottiene una congestione. 

I router non riescono più a gestire il traffico, ovvero non possono più scegliere di accettare un pacchetto rispetto al rifiuto di un altro. Questo è lo stesso effetto Un pacchetto si può ottenere anche a metà di una connessione, ma di solito è associato a un problema non prevedibile con la connessione e viene solitamente risolto con un annullamento della connessione e un nuovo handshake.

Se si ottiene un pacchetto del genere a metà della connessione di solito è avvenuto un problema non prevedibile e viene riannullata la connessione e si rifà di nuovo l’handshake.che avviene nell’idraulica. Si ottiene che si può buttare più acqua in un certo tubo, se si butta troppa acqua il tubo non riesce più a svuotare la vasca.

Questo è uno dei motivi per cui la rete IP è complicata e sensibile.

Ci sono due cose da dire, una è che chi costruisce un isola della rete deve dimensionarla nel modo giusto, e se il traffico tipo è modellizzato correttamente, allora questa opzione non avviene facilmente (che si arrivi al gomito). di soltio è molto più facile che si ottenga uno strozzamento a un certo punto nel sending dei pacchetti.

Questo valori sono più pensate a una rete IP pura, infatti in questo caso questo grafico è quello che meglio sintetizza il comportamento di questa rete. 



## 3.7 Controllo di congestione TCP

A questo problema si cerca di risolvere in due modi: controllare la congestione dall’interno della rete o distribuire il controllo ai lati della rete. Come al solito si è scelto di portare la complessità ai lati della rete. Questi due modelli sono:

* **controllo di congestione punto-punto**

  è un modello di controllo di congestione che cerca di capire tutte le informazioni necessarie a evitare la congestione senza nessun aiuto dalla rete. Di fatto quindi tutti gli host e i nodi sono autonomi e da soli riescono a capire in che stato di congestione si trova la rete e di conseguenza a gestire l’immissione dei pacchetti sulla rete. Questo è il metodo applicato dal protocollo TCP.

  Il funzionamento di questo modello è molto facile: utilizza una valutazione su tempi di risposta e sulle perdite per valutare lo stato della rete.

  

* **controllo di congestione assistito dalla rete**

  Questo meccanismo è stato studiato perché teoricamente possibile. Di fatto non è mai stato applicato perché serviva che tutti i nodi della rete fossero predisposti a avere delle funzionalità di collaborazione tra di loro. Di fatto l’implementazione di questo metodo è stata teorizzata troppo tardi e troppo hardware era già in circolazione per permettere a questo modello di essere applicato su larga scala.

  Il funzionamento di questo processo di controllo è attraverso dei flag inseriti nei pacchetti modificabili dai nodi attraverso cui passano. Questi flag possono essere di più tipi (`SNA`, `DECbit`, `TCP/IP ECN` e `ATM`). Di fatto però questo metodo infrange la stratificazione della pila protocollare, siccome di fatto il pacchetto viene modificato.

  Ma di fatto questo modello non è funzionale per due motivi in particolare:

  * i router sono stateless: non dovrebbero ricordare i pacchetti che sono passati.
  * Questo principio necessita di sapere che flusso va avvisato, e per questo motivo avvisa solo il flusso che sta creando problemi

Tutte funzioni responsabili del controllo congestione si basano su delle informazioni ottenute dalla rete in modo indiretto, questi possono essere:

* un *evento di timeout*: scatta un timeout su un pacchetto. Questo evento viene sempre preso dal client come indicazione di una congestione e agisce subito per limitarla.
* *ricezione di 3 ACK duplicati*: il client in questo modo capisce che c’è stata una perdita, e di fatto ritrasmette dall’ultimo pacchetto confermato. Questo evento è un warning per il protocollo, infatti vengono effettuate operazioni anche in questo caso.

Ci sono **tre meccanismi** utilizzati per ridurre la finestra di congestione:

* partenza lenta
* AIMD
* reazione agli eventi di timeout

(tutti definiti più avanti)

La versione utilizzata di TCP è **newReno**, che è il riferimento di standard e viene utilizzato come limite minimo delle performance. Di fatto ad oggi il migliore è stato presentato da Google chiamato **BBR**. Questo modello è stato ottimizzato utilizzando tantissimi dati elaborati sulle performance di rete.

Tutto il controllo di trasmissione viene ottenuto con il controllo delle finestre di invio dei messaggi. Quindi questa finestra viene utilizzata nello stesso momento dal controllo di congestione e dal controllo di flusso. Di fatto prende il valore minimo tra i due ottenuti dai due controlli.



### Partenza lenta

Quando la connessione viene effettuata si ottiene un valore di dimensione massima del pacchetto (**MSS, maximum segment size**), solitamente $1500 byte$. Con questo valore si ottiene il primo frammento che viene mandato, e di fatto si può mandare questa quantità di dati in un RTT. 

Questa prima parte della connessione è caratterizzata da un frequenza iniziale di invio dei pacchetti molto ridotta e di fatto la finestra è molto piccola. Ogni RTT però il valore della finestra viene raddoppiato, ottenendo una funzione esponenziale rispetto al numero di pacchetti mandati sulla rete.

Questo modello di aumento pacchetti però è particolarmente aggressivo e quindi ha bisogno di essere fermato appena si verifica un evento particolare che provoca il raggiungimento della fase di affinamento.

Questo cambio può essere causato da due motivi principalmente, che hanno degli effetti diversi:

* Dopo 3 ACK duplicati:

  * la finestra di `CongWin` viene dimezzata
  * da ora in poi la finestra viene fatta aumentare linearmente (metodo di regime, che punta a continuare a oscillare sempre intorno a questo valore)

* Dopo un evento di timeout:

  * la finestra di `CongWin` viene impostato a 1 MSS

  * la finestra viene nuovamente aumentata in modo esponenziale.

    Questo evento però fa anche in modo di avere un valore di soglia impostato alla metà della frequenza raggiunta prima di un evento di timeout. In questo modo quando si ottiene nuovamente questo valore si passa alla fase di affinamento, prima di rischiare di stressare la rete ulteriormente. 

Queste azioni vengono diversificate siccome un evento di 3 ACK duplicati indica che la capacità massima della rete è stata ottenuta, mentre invece un timeout è considerato un evento grave di congestione. Infatti questo evento indica che il client non ha più ricevuto nulla, indicando di fatto che la rete è completamente bloccata. Per questo motivo si ritorna alla tecnica di partenza lenta. 

<img src="image/image-20211208111316351.png" alt="image-20211208111316351" style="zoom:67%;" />

In questo grafico si ottiene il valore di finestra massimo ottenuto nelle varie fasi della connessione. Inoltre si vede anche il confronto con due protocolli TCP diversi: *Tahoe* e *Reno*. Il secondo più avanzato implementa la crescita lineare dopo il valore di soglia, come indicato nelle specifiche scritte qui sopra.

Nelle connessioni brevi (chiamate anche *mouse connection*) non si riesce a terminare questa fase, e di fatto si ottiene un comportamento molto aggressivo per tutta la durata della connessione, mantenendo sempre il comportamento a crescita esponenziale.

Normalmente si potrebbe pensare che in questo caso sarebbe meglio avere un punto di partenza per il primo pacchetto più alto, in modo da avere subito tutti i pacchetti da mandare. Questo nella realtà poi non funziona, siccome con un comportamento del genere questo flusso si riesce a “infilare” in mezzo alle altre connessioni, sfruttando tutti i buchi lasciati dalle altre connessioni e di fatto ottenendo una comunicazione risultante più veloce.

Nelle connessioni più lunghe invece (chiamate anche *elephant connection*) si termina la fase di affinamento tranquillamente e si procede nelle fasi successive, ottenendo un andamento diverso nel corso del tempo.

Il grafico inoltre rappresenta il caso teorico in cui si valuta un funzionamento della rete completamente corretto sempre e con sempre dati disponibili a essere mandati.

Un riassunto di questi comportamenti e eventi è rappresentata in questa tabella:

![image-20211208112615969](image/image-20211208112615969.png)

Invece il funzionamento di questo modello è descritto con questo modello a stati:

![image-20211208113246833](image/image-20211208113246833.png)



### AIMD: incremento additivo e decremento moltiplicativo

Questa sezione della connessione viene ottenuta dopo la sezione di affinamento e viene applicata solo durante delle connessioni lunghe. Lo scopo di questo comportamento è quello di avere un valore di frequenza di pacchetti inviati sempre ondeggiante sul limite di capacità della connessione.

La soluzione per risolvere questo problema è l’AIMD, ovvero **additive increase multiplicative decrease**. Questo modello viene applicato in una connessione prolungata, non viene applicata subito.

<img src="image/image-20211207183206710.png" alt="image-20211207183206710" style="zoom:67%;" />

Idealmente il concetto che è dietro questa applicazione è che vogliamo trovare il throughput esattamente uguale alla nostra capacità di trasmissione per ottenere le migliori performance. Di fatto quindi si ottiene una spezzata che ha dei valori all’interno di un range vicino a questo valore (che non conosciamo perché la rete non ce lo fornisce). 

Questo modello fa in modo di:

* crescere linearmente: quando non si hanno perdite di pacchetti e non si hanno ritardi (ovvero quando la connessione viene rilevata come in buone condizioni)

* decresce dimezzandosi: c’è stato un evento che ha fatto capire al client di aver superato il valore di capacità. Allora a questo punto per prevenire la congestione viene dimezzata la finestra di invio. 

  La finestra viene brutalmente dimezzata perché in questo modo si ha la possibilità che i buffer dei router si svuotino o che comunque abbiano alcuni momenti per recuperare un pochino e rimettersi in pari sulla quantità di pacchetti in entrata (infatti se si è avvertita una perdita, vuol dire che la congestione è già in corso).

Questo modello permette quindi di fare **probing** della rete: fornisce alla rete una quantità di pacchetti sempre maggiore, annotandosi se riesce questa riesce a accettarli. Appena si accorge che la rete è satura allora dimezza completamente la quantità di pacchetti inoltrati. In questo modo si lascia alla rete il tempo di ricominciare a scorrere e per questo motivo il grafico prende questa forma a dente di sega.

Questa modalità è molto utile anche perché la capacità non è mai costante, e in questo modo quindi si punta a ottenere il massimo throughput rispetto alla capacità disponibile.

Questo modello è anche il primo presentato per risolvere questo problema. Di fatto ad oggi si utilizza un protocollo più avanzato sviluppato da Google che cerca attivamente di prevenire la congestione.



### TCP: throughput

Siccome la frequenza dei pacchetti immessi sulla rete è completamente dipendente dal protocollo TCP. Questo significa che il valore di throughput è completamente definibile da TCP stesso.

I protocolli TCP, eccetto le finestre di apertura della connessione, tende a mantenere un valore medio massimizzato, in modo da ottenere il massimo numero di pacchetti inviati sulla rete senza disturbare la rete. 

Di fatto se prendiamo $W$ la dimensione della finestra quando si verifica una perdita, allora il throughput è quantificabile come:
$$
\frac{W}{RTT}
$$
Subito dopo la perdita invece il throughput, dipendente dalla finestra dimezzata (quindi $W/2$), prende il valore di:
$$
\frac{W}{2RTT}
$$
Questo non è il modello migliore, infatti per capire il limite della rete si ottiene la finestra $W$ quando si sta già parzialmente creando una congestione. Ci sono metodi che puntano a stressare meno possibile la rete, come il nuovo standard di TCP presentato da Google, ottenendo quindi una prestazione migliore e più stabile. 

Chiaramente questa modifica non cambia radicalmente le performance di una semplice azienda, invece possono cambiare notevolmente in applicazioni come streaming e video consuming, infatti applicando il nuovo protocollo si ottengono meno perdite di pacchetti, siccome ci si ferma prima di ottenere una congestione.

Il throughput medio è $0,75 W/RTT$, generalmente.



### TCP: il suo futuro

Si vuole cercare di massimizzare i pacchetti mandati su una rete da $10Gbps$, con dimensione di segmenti da $1500byte$ e  RTT di $100ms$.

La finestra per utilizzare tutta questa capacità di rete è $W = 83.333$ di segmenti in transito.

La perdita di pacchetti nella realtà invece modifica notevolmente quando si applica in funzione dello smarrimento dei pacchetti:
$$
L = \frac{1.22*MSS}{RTT\sqrt{L}} = 2 \times 10^{-10}
$$
calcolato con una serie di modelli avanzati e calcoli specifici e, in alcuni casi, empirico-statistico. Questo valore è la probabilità di perdita di un pacchetto necessario per ottenere il throughput abbastanza grande da saturare la connessione con i valori precedentemente definiti. Questa probabilità di errore è infinitesima, ma non si può ottenere neanche sulla rete migliore possibile (la fibra ha la proprietà di errore intorno a $10^{-8}/10^{-9}$). 

Da questi calcoli si capisce che il problema di queste versioni di TCP ha un tempo di reazione alla perdita troppo grande e quindi è necessario studiare **nuove versioni di TCP**, che siano pensate per migliorare la connessione, studiando il caso in cui avvengono perdite nella connessione.

Lo studio si basa principalmente su questi due grafici che rappresentano il funzionamento della connessione (ideale):

<img src="image/image-20211208120059382.png" alt="image-20211208120059382" style="zoom:50%;" />

Nel grafico vediamo 3 fasi rappresentate al variare tra il numero di pacchetti immessi sulla rete e il throughput:

* *nella parte blu*:
  * il RTT è costante perché non ho congestione e la rete è in buono stato, e di fatto tutti i pacchetti vengono consegnati nello stesso tempo.
  * il throughput invece prende la forma lineare siccome è l’inverso del RTT
* *nella parte gialla*:
  * il throughput non riesce più a aumentare siccome si raggiunge la capacità massima della connessione
  * il RTT aumenta, siccome si sta appesantendo la rete. infatti la banda è completamente occupata e si otterrà un accodamento sempre maggiore nei buffer (i pacchetti non vengono buttati via, ma la componente di tempo di accodamento diventa non indifferente)
* *nella parte rossa*: ottengo un blocco della rete, siccome ho raggiunto la capacità massima e anche il valore massimo dei buffer dei router, quindi non riesco più a accumulare pacchetti.
  * il throughput rimane controllato, siccome ero già al valore massimo
  * il RTT potenzialmente è molto maggiore siccome si hanno molto pacchetti persi.

Il protocollo **TCP NewReno** trova la sua applicazione tra il settore giallo e quello rosso, siccome sollecita al limite della perdita e a quel punto viene dimezzata la finestra. Questo modello si chiama **lost-based congestion control**, ovvero si ottiene una modifica della finestra dovuta all’evento di perdita creato apposta per ottenere il valore di congestione della rete.

Il protocollo TCP proposto da Google (chiamato **BBR**) si impone di lavorare tra la sezione blu e gialla, ovvero nel momento di massimo funzionamento della rete, siccome si ha il minor RTT possibile e il maggiore throughput. L’unico modo per capire quando questa soglia viene superata però è il controllo sul RTT, valore che inizia a aumentare proprio dopo la sezione blu. Il protocollo inizia a diminuire la finestra quando si accorge che il RTT sta iniziando a aumentare mentre il valore di throughput rimane costante. 

In passato questa tecnica era stata già presentata e studiata, ma di fatto tutti gli altri protocolli, essendo più aggressivi, facevano in modo di “fagocitare” le connessioni in uscita da questo nuovo approccio. Infatti solitamente sulla rete se due protocolli competono per la banda, allora vince quella che diventa più aggressiva. Questo concetto crea il problema di applicazione di questo protocollo, che non essendo applicabile su ogni host, allora tutte le volte che si applica questo modello si ottiene di fatto una connessione più lenta. Google per la prima volta ha ottenuto un protocollo che lavora in questa zona tra blu e giallo ma nello stesso modo non si fa distruggere dagli altri protocolli e a sua volta non è troppo superiormente avanzato da essere lui il protocollo che prevale. Tutto ciò significa che Google ha presentato un protocollo che funziona meglio di TCP NewReno, che tenta di evitare completamente la perdita di pacchetti con lo scopo di avere una connessione più reattiva e che allo stesso modo riesce a convivere con tutte le versioni di TCP esistenti.



La tendenza di oggi su questi protocolli è quello di abbandonare il TCP. Infatti i prerequisiti di throughput della rete fa in modo che sia necessario un controllo diverso della frequenza di pacchetti inoltrati. La tendenza è quindi quella di abbandonare TCP e iniziare a utilizzare UDP con delle accortezze. 



### TCP: il passato

Prima che Google presentasse il suo protocollo evidentemente superiore, c’erano due versioni utilizzate maggiormente.

Il primo è **TCP Cubic** che è di default in Linux. Ha un comportamento di questo tipo:

<img src="image/image-20211208122128394.png" alt="image-20211208122128394" style="zoom:50%;" />

Su Windows invece è presente un approccio diverso (**TCP Compund**), che utilizza dei moltiplicatori quando si trova una finestra migliore di trasmissione. Di fatto questo protocollo non è completamente studiabile, siccome è uno standard chiuso, ma ha un comportamento di questo tipo:

<img src="image/image-20211208122249674.png" alt="image-20211208122249674" style="zoom:50%;" />

Di fatto questi protocolli poi vengono chiaramente battuti come efficienza dal protocollo di Google, qui sotto in confronto:

<img src="image/image-20211208122340837.png" alt="image-20211208122340837" style="zoom:50%;" />

Chiaramente il grafico è idealizzato e server a rappresentare la capacità end-to-end, e di fatto si può capire che:

* il BBR (verde) tende a stabilizzarsi *esattamente* sul limite della capacità. Questo significa che tende sempre a stare sul limite di dimensione dei buffer senza stressarli.

  Di fatto dal grafico sembra che il protocollo si stabilizzi sempre su quel valore, siccome la capacità non sarai mai così stabile. In base a questo valore allora si ottiene un grafico leggermente diverso, in cui la parte di *platue* coincide alla capacità massima.

  Inoltre si noti che ci sono delle sezioni che comunque diminuiscono, dando lo spazio ai buffer di svuotarsi.

  Per rendere questo protocollo competitivo rispetto agli altri inoltre hanno aggiunto alcuni trucchetti molto sviluppati, non trattati in questo corso.

* il CUBIC (rosso) tende sempre a stare molto sopra il limite di capacità. Questo significa che tende sempre a dare un certo stress alla rete, lavorando sempre al limite, nonostante il protocollo funzionasse molto bene.

* il COMPOUND invece ha un comportamento leggermente diverso, che lavora sulla linea del limite e ha crescita lineare finché non arriva al limite della connessione, portando quindi sempre dello stess alla rete.



### TCP: Equità

Il problema del TCP di oggi è che avendo tantissimi protocolli disponibili con un sacco di applicazioni e implementazioni diverse si ha il problema che non tutti i protocolli daranno delle performance diverse.

Su una connessione ci sono dei link condivisi. Quando la rete ha equità ogni connessione ha esattamente $Banda/numeroHostConnessi$ come valore di banda.

Di solito quindi si capisce se un protocollo è equo attraverso delle simulazioni o dei test, siccome dimostrazioni matematiche di questi concetti è molto complicato.

Il TCP NewReno è equo, siccome permette a tutti di condividere più o meno le performance. Chiaramente questo fatto dipende notevolmente in funzione del RTT, siccome logicamente queste connessioni si comporteranno in modo più aggressivo.

Di fatto ci sono dei protocolli che hanno un metodo di decisione della finestra di controllo calcolata attraverso delle formule matematiche e che quindi riesce a ottenere un modello equo anche nel caso di variazione del RTT.

<img src="image/image-20211209115127103.png" alt="image-20211209115127103" style="zoom:50%;" />



Ci sono due altri aspetti di equità che sono ugualmente importanti: l’equità tra UDP e TCP. Questi due flussi di dati tendono a avere un comportamento vincente rispetto a UDP. Infatti, siccome UDP non ha controlli automatizzati, la perdita di pacchetti non triggera nessun evento che limita il throughput del canale. 

Nel TCP alla prima perdita di un pacchetto si ottiene un throughput dimezzato. 

Ad oggi le applicazioni di solito vengono fatte su TCP perché le applicazioni si adattano alla capacità della connessione senza sovraccaricare la rete. Inoltre spesso i router commerciali hanno delle regole per bloccare i flussi UDP, siccome gli host che emettono UDP non hanno azioni secondarie. Questo fa in modo che i router droppino i pacchetti quando ci si avvicina a uno stato di rischio di connessione. 

Per evitare problemi con la rete comunque, le applicazioni che utilizzano UDP so applicano con un modello definito TCP friendly, ovvero aggiungono una serie di controlli dal lato mittente che permette di ridurre/bloccare l’emissione di pacchetti in casi particolari.

Per tutti questi motivi si può dire che UDP non è equo rispetto TCP.



Un altro modello è quello utilizzato alle applicazioni che permettono di avere un accelleratore di download. Questi client aprirono più connessioni in parallelo in modo da ottenere il file in spezzoni separati. In pratica il comportamento di questo host però è un comportamento di ogni connessioni uguale al TCP:

*  slow start,
*  crescita lineare e perdita moltiplicativa

Il throughput di tutti i canali sarà costante e equo. Ma di fatto in questo modello il client che emette più connessioni parallele avrà un throughput maggiore per forza di cose (ad esempio: se si vuole scaricare lo stesso file con due host diversi sulla stessa condizione di rete, uno dei quali però impiega 9 connessioni parallele, allora questo sarà 9 volte più veloce).

Questo modello permette si di sfruttare l’equità del TCP (che quindi assegna un valore di banda diviso tra le connessioni che richiedono) per aumentare il throughput personale. Però è molto rischioso avere un applicazione di questo protocollo in parallelo molto spesso, perché mina la stabilità della rete. Infatto si stressa molto più del previsto il buffer dei router, perché la crescita lenta avviene $n$ volte più velocemente. Questo significa che un host cerca di prendere più posto nei buffer possibile per avere la precedenza. 

Inoltre questo modello funziona solo in un momento transitorio (non continuativo), siccome poi è un gioco al rialzo che porta a una congestione sulla rete tra gli host (se il secondo host inizia a aprire più connessioni in parallelo delle nostre si ottiene un ulteriore rallentamento). Il risultato finale è una serie di connessioni sempre più aggressive sulla stessa rete che stressano esageratamente il buffer di un router e quindi un appesantimento di tutta la rete.



### TCP: versioni specifiche

Ci sono alcune applicazioni che non riescono ad utilizzare le versioni disponibili di TCP, perché si hanno dei problemi sul modo in cui è studiata la rete. 

Ad esempio sulle reti satellitari nessuna connessione TCP funziona completamente bene, e quindi si utilizza un TCP modificato appositamente. Un altro esempio è nei datacenter. Di solito si potrebbe pensare che quello fosse il modello più ottimale per il funzionamento dei protocolli di rete. Di fatto però la bassima latenza che questi luoghi offrono mettono in crisi il TCP, siccome il datacenter ha dei pattern di traffico molto particolari e quindi si possono ottenere delle congestioni molto spinte in casi molto particolari.

Di base ad oggi si utilizza però il TCP in quasi tutte le applicazioni di base, mentre invece UDP viene utilizzato in contesti particolari.

##### QUIC

Le ultime proposte per ridurre alcuni problemi sulla rete, soprattutto sul web in generale. Quindi hanno proposto un nuovo modello che richiede di non utilizzare il TCP per le connessioni HTTP. Questa scelta è stata fatta anche in visione del HTTPS, dove c’è anche bisogno di gestire la crittografia e quindi molto scambi client-server per fare il setup di queste funzioni.

Il nuovo protocollo HTTP/3 si propone di utilizzare l’UDP. Questa scelta però non permette né l’introduzione del layer di sicurezza, né quello di controllo di congestione e di flusso.

La soluzione è stata trovata nel protocollo **QUIC (quick udp internet connection)**, sviluppato da Google e in fase di testing. Questo protocollo permette di utilizzare UDP per l’HTTPS in modo da gestire meglio le connessioni in parallelo, la gestione della sicurezza e del congestion control. Questo modello lavora su uno stack applicativo di questo tipo:

<img src="image/image-20211209121806752.png" alt="image-20211209121806752" style="zoom:50%;" />

Il protocollo quindi implementa alcuni tipi di applicativi:

* **controllo di errori e congestione**: algoritmi e controlli molto simili a quelli del TCP

* modello di **apertura della connessione**: questo processo si occupa di aprire la connessione, per assicurare l’affidabilità della connessione, controllo della congestione, autenticazione, crittografia. 

  Il protocollo inoltre utilizza una tecnica per aprire la connessione e la connessione sicura in un modello chiamato $0RTT$, ovvero che inizializza tutto in un solo RTT.

* inoltre si ha la possibilità di **multiplexing sulla singola applicazione**, ovvero avere un unica connessione che permette di avere una serie di flussi che consentano lo scambio di pacchetti multipli sulla stessa connessione.

* no **HOL blocking**: di fatto questo protocollo permettendo il multiplexing, permette di non avere il problema del HOL blocking, siccome un errore di comunicazione di un pacchetto causa lo stop di un solo flusso di dati.

La differenza di questi modelli si può trovare in questi schemi:

<img src="image/image-20211209122519623.png" alt="image-20211209122519623" style="zoom:50%;" />





# Capitolo 4: Livello di Rete

Questo livello è molto importante, siccome permette di instradare i pacchetti sulla rete.

Capire i principi che stanno dietro i servizi del livello di rete:

* modelli di servizio del livello di rete
* funzioni di inoltro e di instrademento
* come funziona un router
* instradamento (scelta del percorso)
* scalabilità
* argomenti avanzati: IPv6, mobilità

inoltre si tratterà l’implementazione nell’Internet.

## 4.1 Introduzione

Come tutti i livelli ci sono alcune funzioni chiave: in questo il compito principale è quello di muovere i pacchetti dall’host mittente a quello ricevente. Per ottenere questo effetto ci sono principalmente due funzioni:

* il **forwarding (inoltro)**: permette di trasferire i pacchetti dall’input di una connessione all’output del link appropriato. Di fatto quindi si tratta di spostare un pacchetto da una porta a un’altra all’interno del nodo.

  Generalmente i pacchetti in questo stato possono essere bloccati (se ad esempio una regola impone di bloccare il pacchetto, oppure la destinazione non esiste o ancora se è inoltrato su un host non raggiungibile) o può essere mandato a più link moltiplicandolo.

* **routing (instradamento)**: determina il percorso seguito dai pacchetti dall’origine alla destinazione. 

  In questa sezione si ottengono anche una serie di algoritmi per definire queste traiettorie, chiamati *routing algoritms*. 

Di solito questi termini sono spesso utilizzati in modo intercambiabile, ma in realtà hanno dei significati leggermente diversi. Il termine *forwarding* si riferisce a un azione che avviene localmente nel router e è assegnato all’azione di trasferire i pacchetti dal link in input al link in output appropriato. Solitamente (soprattutto per le soluzioni più high end) questi metodi sono implementati via hardware e svolgono queste operazioni in tempi nella scala dei nanosecondi. 

Il termine *routing* si riferisce invece a un azione che avviene nella totalità della rete e sono tutte le azioni che permettono di definire un passaggio continuo attraverso i nodi della rete fatti in modo da permettere il passaggio dei datagrammi dal mittente al ricevente.

Si può pensare a questi due termini come fossero delle strade reali. Il percorso completo è definito nell’instradamento con il GPS che indica tutto il percorso da casa alla destinazione. La decisione invece che indica di uscire sulla destra durante il viaggio si ottiene nella sezione di inoltre (ovvero una funzione svolta dai singoli router).

Il principio chiave del funzionamento di questo processo è basato sulle **tabelle di inoltro** presenti su ogni nodo della rete (ovvero su ogni router). Queste tabelle associano a un valore di un header di pacchetto in arrivo a una determinata porta a cui inoltrare il pacchetto. Il confronto di questi header e la comprensione di dove questi pacchetti debbano andare è la chiave di tutta l’operazione di inoltro svolta sul router. 

![image-20211209132639508](image/image-20211209132639508.png)

Il contenuto di questa tabella è calcolato e mantenuto attraverso gli algoritmi di routing. Un altro caso (non consigliato, molto complicato e molto raramente applicato) è quello di creare una tabella di inoltro a mano, in modo da impostare dove i pacchetti debbano andare in modo manuale.

Di fatto il modello attraverso cui ogni router è direttamente collegato ai router vicini è quello più utilizzato al momento. Non per questo significa che sia l’unico. Un modello che permette di avere un servizio centralizzato che fornisce i dati delle tabelle di inoltro per tutta la rete è possibile e viene di solito utilizzato per le **reti software-defined (SDN)**, che per definizione hanno questa caratteristica.

Di fatto prima che i datagrammi fluiscano sulla rete, è necessario che i due host (mittente e ricevente) e i router stabiliscano una connessione virtuale. In questo processo vengono coinvolti i router.



Siccome ci sono molte similitudini tra il livello di rete e quello di trasporto ricordare che:

* il livello di trasporto crea una connessione tra due host
* il livello di rete crea una connessione tra due processi



## 4.2 Reti a circuito virtuale e datagramma

Questo livello della pila protocollare può offrire una serie di modelli di servizio che chiaramente hanno dei funzionamenti e delle garanzie fornite ai pacchetti transitanti diversi.

I servizi che una rete può decidere di fornire di base possono essere:

* **garanzie nella ricezione dei dati**: la rete offre la conferma che il pacchetto arrivi a destinazione (tralasciando il tempo necessario per ottenere questo trasferimento, dice solo che prima o poi arriverà a destinazione)
* **garanzia di ricezione dei dati in un delay preciso**: il servizio garantisce non solo che il pacchetto verrà consegnato, ma anche che verrà consegnato esattamente in un certo valore di tempo ben definito.
* **consegna dei pacchetti in ordine**: il servizio garantisce che i pacchetti arrivino all’host a cui sono mandati esattamente nell’ordine con cui sono emessi dal mittente.
* **garanzia di banda minima**: questa garanzia imita il modello di consegna dei pacchetti telefonici, che “utilizzano” un certo valore di banda durante tutta la connessione su tutti i collegamenti che necessitano.
* **sicurezza**: il servizio permette di avere una connessione sicura e criptata.

Le reti possono, di fatto, essere una qualsiasi combinazione di queste garanzie oppure seguire il modello (poi diventato quello della rete internet) di **best-effort**, siccome ha dimostrato di essere accettabilmente buono da garantire una rete consistente nel servizio offerto agli hosts.



Di fatto comunque le reti che ad oggi si possono trovare sono di due tipi, entrambi con un funzionamento a commutazione di pacchetti:

* Reti a circuito virtuale
* Reti a datagrammi

Questi due modelli di rete hanno delle analogie sul funzionamento del livello di trasporto, ma:

* offrono un servizio da host a host
* si può scegliere uno solo di questi servizi, non entrambi nello stesso momento (o servizio con connessione o servizio senza connessione)
* le implementazioni di questi protocolli sono fondamentalmente diverse

Uno schema riassuntivo di questi modelli di rete:

![image-20211211003644516](image/image-20211211003644516.png)

### Reti a circuito virtuale

Le reti di questo tipo hanno un comportamento molto simile a quelle dei circuiti telefonici per prestazioni fornite e coinvolgimento della rete durante il percorso tra sorgente e destinazione.

In questo senso si potrà notare una richiesta alla rete per una connessione, che può non andare a buon fine. Se invece si ottiene una connessione allora si hanno anche le garanzie di banda definita e “bloccata” dal determinato collegamento.

Di fatto questo flusso di pacchetti è analogo a quello creato nella rete telefonica: tutti i pacchetti che viaggiano al suo interno avranno lo stesso percorso attraversando gli stessi nodi. Se uno di questi durante la connessione venisse a mancare tutto il collegamento verrebbe distrutto.

Questa rete però non è completamente instabile: dovendo preservare un certo valore di banda per ogni flusso si ottiene un controllo di congestione automatico, con la possibilità di evitare la comunicazione a un numero di host superiore a quello possibile sulla determinata tratta.

Il lato negativo di questo modello della rete sono i costi. Si hanno dei costi di creazione dell’hardware necessario a queste reti molto alti. Inoltre non si otterranno le stesse prestazioni, infatti ogni nodo di accesso della rete dovrà svolgere una serie di controlli ulteriori rispetto al semplice processo di inoltro.

Il funzionamento a basso livello di questa rete è uguale al un normale router, quindi legge l’intestazione del pacchetto e in base a quello decide dove inoltrarlo. L’unica differenza è che in questo modello sull’intestazione del pacchetto non c’è l’indicazione del ricevente, ma di fatto si indica il flusso (ovvero il percorso logico dei nodi della rete) sul quale deve essere consegnato. Questo valore viene assegnato al pacchetto prima di iniziare la comunicazione. Tutti i pacchetti che condividono la stessa indicazione di flusso vengono instradati sullo stesso percorso.

Il circuito virtuale creato consiste in un certo numero di router che vengono coinvolti nella connessione e che formano un percorso logico tra i nodi della rete. Questo flusso è sempre ottenibile sulla rete se sono disponibili gli stessi nodi, oppure viene ricreato su un altro percorso con altri nodi. Se qualsiasi router della rete andasse in crash, ogni percorso andrebbe ricalcolato. 

Inoltre il fatto che una sezione di rete venga riservata per questa connessione significa che prima che i datagrammi fluiscano sulla rete, la rete stessa riesce a creare un percorso virtuale (quindi simile alla rete telefonica, ma virtuale, ovvero può non corrisponde a nessun allocamento di spazio sulla connesssione).

Queste reti offrono un servizio solo con connessione, motivo per cui i router che sono presenti in questa rete sono stateful e riescono a ricordare i flussi che li attraversano.

Un esempio di una rete con questo funzionamento è l’ **ATM (asyncronus transfer mode)**. La rete era stata studiata molto attentamente per fornire con una serie di tecniche a sfruttare il modello di conversione di circuito per fornire una serie di garanzie (che si possono leggere nella tabella sopra).

Queste connessioni possono usare diverse tecniche che possono fornire:

*  **CBR (constant bitrate)**: connessioni a frequenza costante. Si ottengono garanzie su banda, ordinamento e temporizzazione di consegna.
*  **VBR (variable bitrate)**: connessioni a frequenza variabile. Si ottengono delle reti con le garanzie precedenti, però con la banda di accesso variabile (e che quindi viene descritta nella prima connessione e poi mantenuta). Questa rete veniva utilizzata molto per i contenuti video.

Per il funzionamento di un circuito a commutazione di pacchetto a circuito virutale (ovvero una rete tipo ATM) è necessaria una **chiamata di inizializzazione del percorso**, ovvero la richiesta dell’host alla rete per richiedere il circuito necessario a collegare mittente e ricevente. Queste operazioni hanno una serie di fasi:

1. Si esegue la chiamata di inizializzazione da parte del mittente verso la rete
2. Si ottiene la chiamata in ingresso dal lato ricevente
3. Si esegue l’accettazione della chiamata, oppure la si rifiuta
4. In caso di chiamata accettata, si connettono i due host e inizia la chiamata
5. Si fa partire il flusso di dati

<img src="image/image-20211209160851750.png" alt="image" style="zoom:50%;" />

Applicazioni della rete ATM oggi possono essere diverse (ma di solito limitate sia nel numero di hosts partecipanti e nelle performance fornite) come ad esempio i circuiti per fornire la connessione ADSL. Inoltre è spesso utilizzata all’interno delle aziende per migliorare le prestazioni, infatti si possono applicare dei protocolli di conversione che permettano la traduzione dei pacchetti sul protocollo IP.

L’ATM in certi aspetti può essere anche considerata una tecnologia di livello 2.



### Reti a datagrammi

In questi modelli di rete non si ha un’impostazione di chiamata a livello di rete, inoltre i router non conservano informazioni sullo stato dei circuiti virtuali e non c’è il concetto di connessione a livello di rete.  

Tutti gli inoltri su ogni nodo avvengono utilizzando l’indirizzo dell’host del destinatario. In questo modo i pacchetti passano per una serie di router che utilizzano gli indirizzi di destinazione per inviarli e che quindi possono passare da percorsi diversi.

![image-20211211104305446](image/image-20211211104305446.png)

Questo modello è quello applicato alla rete internet. Di fatto si definisce la rete **best-effort** che significa che la rete non fornisce nessuna garanzia rispetto a banda, consegna, ordinamento, temporizzazione e indicazione di congestione.

Inoltre i datagrammi scambiati su questa rete è completamente stateless, ovvero ogni pacchetto è unico per la rete e non vengono mantenute informazioni sul percorso dei pacchetti della stessa coppia host-host. Questo permette di avere dei pacchetti in arrivo sugli stessi estremi, senza che però abbiano svolto lo stesso percorso.

Il lato positivo di questa tecnica è che spostando tutte le tecniche di gestione della connessione e della rete agli hosts esterni, il cuore della rete rimane molto semplice e efficiente.

Il lato negativo di questa tecnica è che si perdono tutte le garanzie e tutto il controllo possibile.

Il funzionamento anche in questo caso è attraverso una **tabella di inoltro**, che però in questo modello viene ottenuta con una serie di algoritmi. In questa rete si possono avere diversi pacchetti mandati attraverso dei collegamenti diversi, oppure si può decidere di dividere il carico tra più link diversi.

Di fatto una tabella in questi router prende una forma simile a questa:

<img src="image/image-20211211105721813.png" alt="image-20211211105721813" style="zoom:50%;" />



Gli indirizzi utilizzati in questa rete sono IPv4. Questo significa che sono dei valori organizzati in 3 numeri da 0 a 255, ovvero 4 valori da 8bit. Di fatto questo permette la creazione di 4 miliardi di possibili indirizzi.

La tabella così costruita inoltre è molto pesante per il router, siccome per ogni pacchetto il router deve scorrerla e ottenere un risultato. Per ridurre il tempo necessario a ottenere una risposta in questo modello si utilizzano una serie di tecniche come ad esempio il raggruppamento geografico e/o la corrispondenza di prefisso. 

Questi metodi permettono di avere una serie di IP sequenziali vicini, riducendo il numero di informazioni che ogni router deve possodere:

* gli IP che indicano host vicini geograficamente sono molto specifici
* gli IP che indicano host piuttosto lontani sono molto poco specifici, ovvero generalmente si raggruppa in un range di indirizzi e si forwarda tutti questi pacchetti nella stessa direzione. 

Il concetto che sta alla base di questa modalità è che ogni router conosce molto bene la rete nelle sue vicinanze, mentre invece quando ottiene un indirizzo molto lontano rispetto alla sua posizione delega a un altro nodo che sa essere più vicino.

Il raggruppamento geografico è stato ottenuto grazie alla compravendita degli indirizzi IP. Ogni ISP ha negli anni comprato un certo range di indirizzi (di fatto gli indirizzi disponibili su IPv4 sono terminati) determinati normalmente da un bit specifico nel valore. In questo modo si ottengono una serie di indirizzi compresi in determinati valori che condividono gli stessi valori a sinistra, ma che hanno diversi valori sulla destra.

Il miglior modo per ridurre le righe della tabella di instradamento appunto è attraverso questa tecnica di *range based address*, per cui si hanno una serie di regole che indicano i valori di sinistra in modo poco specifico, mentre invece ci sono regole molto specifiche per una serie di valori di destra.

Questo metodo, applicato da moltissimo tempo, ha dimostrato di essere molto più efficiente che avere ogni router contenente tutti gli indirizzi.



Il fatto che la rete internet oggi sia a datagrammi è dovuta al fatto che:

* negli anni 80 tutte le aziende utilizzavano degli standard propri. Se si comprava una rete allora era chiusa e non aveva necessità di interconnettersi.
* Quando la rete ha dovuto integrare tutte le altre, allora si è arrivati al problema di avere dei vincoli sottostanti: o obbligo una rete globale a circuiti virtuali, allora tutte le mie sottoreti devono avere la capacità di costruire questo circuito.
* si inizia a rilassare i vincoli sulle componenti, ma avere una rete **best effort**. Di fato quindi si avranno delle isole di funzionamento a circuito e delle reti di pacchetto, ma di fatto poi è rimasto il modello a pacchetti siccome aveva meno vincoli sulle reti che poi ne facevano parte.
  * Questo ha facilitato la rete e la scalabilità (il circuito impone un limite alle connessioni disponibili siccome devono dare uno stato alle connessioni)



In questa sezione abbiamo iniziato a parlare di indirizzi IP. Generalmente si utilizza il protocollo IP alla versione 4. Di fatto questo protocollo ha troppi pochi indirizzi (al momento gli indirizzi pubblici sono terminati in realtà) e non riesce a implementare alcuni modelli di sicurezza (nonostante si stia utilizzando IPv4Sec, che implementa alcune versioni di sicurezza e verifica degli host).

Per risolvere questi problemi si è sviluppata il protocollo IP versione 6 che permette di avere molti più indirizzi di rete e che garantisce una versione sicura e di verifica automatica.  

Siccome in molte zone del mondo, il protocollo versione 4 di IP è ancora lo standard ci sono diverse tecniche per moltiplicare gli indirizzi e riutilizzarli.



### Riassunto

Pur restando nella commutazione di pacchetto (ovvero il modello di scambio impone di avere dei dati pacchettizzati), abbiamo due soluzioni:

* modalità a datagrammi, che è il punto di riferimento di internet.
  * è molto più semplice siccome tutta la complessità è relegata agli host ai margini della rete, mantenendo semplice il cuore.
  * le performance sono migliori siccome ogni nodo ha un solo compito: il forwarding dei pacchetti.
  * questa soluzione è molto scalabile e i router sono stateless (ovvero non hanno necessità di mantenere informazioni sui flussi di pacchetti). Di fatto ogni nodo ha la responsabilità di portare un pacchetto al prossimo nodo della rete, semplicemente.
  * Questa rete ha necessità di una serie di controlli dall’esterno perché non si sa autoregolare.
* circuiti virtuali (ATM):
  * evoluzione della rete telefonica come approccio ma che utilizza dei pacchetti (tutta la gestione di banda, percorso e tutti gli altri servizi sono rilasciati e controllati direttamente dalla rete).
  * non ha la stessa capacità di riservare banda
  * con la possibilità di controllare il collegamento prima di iniziare il flusso di traffico, che permette anche di avere un controllo sul flusso e sulle congestioni. è necessario quindi di avere dei router che mantengano lo stato. Infatti:
    * la rete è molto più complessa, siccome i nodi devono fare:
      * forwarding: operazione di comunicazione dei pacchetti
      * consentire di gestire la connessione (applicare i limitatori di banda e operare sulla limitazione dei pacchetti)

Per semplicità e costi si è scelto la rete a datagrammi per internet.



## 4.3 Architettura di un router

Il router ha un architettura logica che consiste in porte in ingresso e porte in uscita. Nella pratica ogni porta è sia di ingresso che di uscita ma logicamente può essere in un’unica direzione alla volta.

Il cuore del router è lo **switching fabric** che è l’oggetto che permette ai pacchetti di essere inoltrati da un porta ad un’altra. La logica secondo la quale il router manda i pacchetti verso un’altra porta è definita nel **routing processor** che appunto ha il compito di programmare la tabella di inoltro.

Il router (per definizione) permette di avere dei pacchetti che raggiungono al massimo livello 3 della pila protocollare e quindi i pacchetti passano attraverso i livelli fisico, di accodamento e di rete.

Una architettura logica di un router è simile a questa:

![image-20211211190010895](image/image-20211211190010895.png)

Il router solitamente ha due funzioni chiave:

* far girare i protocolli/algoritmi di instradamento (*RIP, OSPF, BGP*)
* inoltrare datagrammi dai collegamenti di ingresso a quelli in uscita



### Porte di ingresso

Le porte di ingresso hanno 3 sezioni particolari (nella pila protocollare vanno da livello fisico fino a livello 3):

* *line termination*: è la sezione della porta che si occupa di accettare la linea fisica, ricevere i bit comunicati (oppure i segnali di luce) e convertire questi valori in dati.

* *livello di collegamento*: in questa sezione della porta si può ottenere il de-incapsulamento del pacchetto. Il payload ottenuto in questa fase può essere passato a livello sopra.

* *buffer di lookup & forwarding*: questa sezione è il buffer responsabile della coda (*queuing*) dei pacchetti in entrata. Lo scopo è far aspettare i pacchetti in questa sezione finché lo switching fabric non è disponibile per smistarli.

  Questa sezione della macchina solitamente è meno importante perché, affinché uno switch sia ottimale, è necessario che il rate dei pacchetti in entrata sia uguale alla frequenza di elaborazione dello switching fabric.

  Questa operazione è piuttosto complicata, infatti è difficile trovare dei dispositivi hardware con la capacità di svolgere operazioni di inoltro alla velocità della rete e di fatto ad oggi le soluzioni più performanti permettono di svolgere queste operazioni fino a 100Gbit/sec (logicamente queste performance arrivano con un costo interessante).



### Switching Fabric e le 3 tecniche di commutazione dei pacchetti

Lo switching fabric è di fatto la sezione del router che li differenzia dai computer. Di fatto al momento ci possono essere 3 tipi diversi di oggetti che permettono questa operazione (rappresentate anche nel disegno sotto):

* commutazione attraverso un buffer in memoria
* commutazione attraverso un bus di sistema
* commutazione con metodo crossbar

![image-20211211190122725](image/image-20211211190122725.png)

#### Commutazione in memoria

Questo modello utilizza un architettura *general pourpose*, è uno dei primi modelli di router utilizzati siccome per essere creati è necessario semplicemente riconfigurare un computer con più di un interfaccia di rete. Questo è anche il modello più facile da applicare e implementare.

La commutazione in memoria consiste praticamente nella memorizzazione dei pacchetti in memoria, dove vengono elaborati (ad esempio devono essere svolti dei controlli sul checksum oppure la semplice lettura dell’header) e poi ritrasmessi ad una porta in uscita in base al contenuto.

Questo modello è si il più facile da implementare, ma è anche il meno performante, visto che la capacità di commutazione è limitata dal bus di sistema (il pacchetto deve transitare in questa sezione per due volte) e dalle capacità del computer utilizzato (le performance della memoria per la maggior parte). 

![image-20211211190441668](image/image-20211211190441668.png)

In particolare il bus di comunicazione del computer non può essere utilizzato da due flussi diversi nello stesso momento, quindi la frequenza massima di inoltro è sempre un valore minore di $B/2$ (dove $B$ è la capacità del collegamento del bus).



#### Commutazione tramite bus

Siccome la prima soluzione non forniva un modello abbastanza performante per la capacità della rete, allora si ha iniziato a studiare un nuovo modello.

La commutazione tramite bus di sistema permette di evitare il salvataggio dei pacchetti in memoria, eseguendo lo switching direttamente utilizzando il bus di sistema. In questo modo si riesce a rendere unica la comunicazione svolta sul router.

Il pacchetto una volta entrato nello schema di bus sa già che porta di uscita prendere, rendendo obsoleta la richiesta di memorizzare il pacchetto in memoria. In questo modo la frequenza massima di commutazione di pacchetto riesce (potenzialmente) ad essere raddoppiata, diventando $B$ solamente (infatti viene anche chiamato bus a linea dedicata).

Un problema in questo modello però è la **contesa per il bus**: non può succedere che più pacchetti vogliano accedere al bus nello stesso momento. Per questo motivo è necessario un modello di controllo che gestisca il traffico in entrata.  

Questa architettura rimane ancora molto simile ad un computer, motivo per cui riesce a mantenere dei costi relativamente bassi. Allo stesso modo però fornisce delle prestazioni abbastanza buone per un router su una rete di accesso, ma non resisterebbe nel cuore della rete.

Ad esempio un router Cisco 5600 ha un bus di 36Gbit/s. Questo router riesce tranquillamente a gestire 3 porte da 10Gbit/sec, oppure una trentina di porte da 1Gbit/sec. Per questo motivo si configura perfettamente come router da limiti della rete.



#### Architettura Crossbar

L’architettura di commutazione più performante è la **crossbar** e è stata studiata appositamente per consentire le migliori performance possibili nell’inoltro dei pacchetti. Per questo motivo i router che impiegano questa architettura sono anche i più costosi (impiegano un hardware completamente diverso da un computer), ma permettono di avere delle capacità di switching in grado di gestire i Tbps (Terabit per sec).

Il funzionamento di questo modello è attraverso una serie di bus e logiche che permettono di parallelizzare il traffico in modo da permettere di superare la banda disponibile rispetto a quando si ha solo un bus condiviso. Questo modello permette di sviluppare $2n$ bus che collegano $n$ porte di ingresso a $n$ porte di uscita.

Per favorire la gestione dei pacchetti e dell’intero router questi modelli utilizzano delle task di durata predefinita (in questo particolare si differenziano rispetto al resto dei modelli che lavorano sul modello dell’interrupt). Il funzionamento è simile al clock di un processore, dove ogni task è **sincrona**. Di fatto quindi il flusso dei pacchetti assumerà la forma della funzione di clock. Inoltre per permettere a più pacchetti possibili di circolare sui bus di sistema contemporaneamente si utilizzano un’altra serie di apparati di supporto che organizzano l’ordine in cui mandare i pacchetti affinché si saturino al meglio le linee.

Questi modelli permettono la gestione di grandissimi volumi di traffico e sono applicati nel cuore della rete. Ad esempio lo Switch Cisco 12000 (chiamato solo switch perché in realtà gestisce solo le operazioni di inoltro, quelle di indirizzamento vengono svolte in un apparato esterno) permette di avere una capacità di forwarding fino a 60Gbps



### Porte di uscita

Le **porte di uscite** sono di base la versione specchiata rispetto a quelle di ingresso. Anche questa porta quindi ha accesso solo fino al terzo livello della pila protocollare. 

* *buffer*: quando un pacchetto arriva dall’architettura di smistamento superiore allora raggiunge questo luogo.

  Questo buffer è quello che deve sopportare lo stress in caso di congestione, infatti il tempo di accodamento non è costante, inoltre l’interfaccia può avere dei tempi di accodamento.

  Questa sezione viene spesso accoppiata con uno *scheduler*. Questo componente è capace di definire il meccanismo di gestione della coda e dell’accodamento dei pacchetti in arrivo.

  * Solitamente si ha un modello di accodamento FIFO (*first-in-first-out*)
  * si possono avere dei meccanismi di gestione della coda differenti o differenziati in base al tipo di pacchetto. Ad esempio può creare delle code differenziate e dare la precedenza a un certo tipo di pacchetto o traffico.
  * questa sezione è responsabile del dropping dei pacchetti in caso di buffer troppo pieno e può reagire con diverse regole.
  * Solitamente nei router commerciali le modalità di accodamento disponibili sono codificate nel firmware del router.



#### Dimensione del buffer

Per la gestione dei buffer di accodamento ci sono dei problemi quando si parla delle dimensioni, infatti si hanno lati negativi sia se il buffer è troppo grande o troppo piccolo.

Un router con un buffer troppo piccolo non permette di avere la possibilità di assorbire delle congestioni temporanee e potrebbe dare problemi nell’accodamento in caso di congestione.

Se il buffer è troppo grande si ottiene l’effetto di non rischiare mai la congestione grave, ma si perderebbe la capacità di controllare il ritardo. Infatti si pensi alla possibilità di avere un buffer possibilmente infinito, allora i pacchetti potrebbero sarebbero sempre in attesa, facendo aumentare di molto il carico della rete.

Per risolvere questo problema sono state fatte alcune proposte, la prima delle quali nell’`RFC 3439`. Questo proponeva di avere un buffer disponibile uguale a una media del tempo di andata e ritorno ($RTT$) moltiplicato per la capacità del collegamento ($C$). Si può pensare a questo valore come il calcolo spannometrico del volume del collegamento, immaginando quest’ultimo come un tubo per l’acqua. 

Questa soluzione però inizia a diventare impegnativa soprattutto sui modelli di router molto grandi, che richiedono molta memoria. Quest’ultima è molto costosa siccome deve essere particolarmente affidabile e rispettare una serie di particolari caratteristiche. Per questo motivo, in seguito a degli studi si è arrivati alla soluzione di avere dei buffer dimensionati secondo:
$$
\frac{RTT * C}{\sqrt N}
$$
(La quantità di buffering necessaria per $N$ flussi quindi è il $RTT$ per la capacità del collegamento $C$, tutto fratto $\sqrt{N}$).

La formula è stata trovata in modo abbastanza empirico, siccome $\sqrt{N}$ non ha effettivi fondamenti teorici, ma di fatto permette di non arrivare mai al limite della memoria disponibile.

Applicata nei router con più necessità di memoria inoltre questo modello ha fatto notare un miglioramento delle prestazioni, avendo minor volume di memoria da gestire.



## 4.4 Protocollo Internet (IP)

Non si può parlare di reti senza IP (protocollo internet). Questo protocollo rimane piuttosto semplice siccome utilizza la commutazione di pacchetto a datagrammi.

Questo modello non è complicato e spesso utilizzato in tutti i campi dell’informatica.

All’interno di questo protocollo si trovano le indicazioni:

* su convenzioni sugli indirizzi
* rispetto al formato dei datagrammi di comunicazione
* rispetto alle regole di instradamento e inoltro (che poi si tramuta in programmazione delle regole per la programmazione della tabella di inoltro attraverso questi protocolli).
* protocollo di segnalazione ICMP



### Formato dei datagrammi

L’header dei datagrammi IP sono grandi sempre 20byte e quindi hanno 5 righe di intestazione da 32bit ciascuna (come il TCP). Ulteriormente a questo ci sono i campi opzionali (anche se vengono utilizzati molto poco), nei quali ad esempio si può indicare la registrazione del percorso del pacchetto, segnare i timestamp (una delle modalità per valutare i valori dell’RTT) e segnare l’elenco di router. Infine segue il payload.

![image-20211212122727320](image/image-20211212122727320.png)



La *prima riga* contiene:

* *numero di versione*: la versione di default è 4, ma c’è anche la 6 che è più moderna. 

* *lunghezza di intestazione*: indica il numero delle righe di intestazione (di solito 5)

* *tipo di servizio*: questo campo è stato studiato per avere un certo scopo su alcuni tipi di reti (per valutarne il funzionamento), ma di fatto non viene utilizzato sulla rete internet.

* *lunghezza del datagramma*: questo campo indica quando è grande il pacchetto. Solitamente la dimensione è $1500byte$. 

  Solitamente è strano vedere pacchetti troppo grandi perché non conviene ai layer della pila protocollare creare dei pacchetti così (nonostante sia un valore a 16bit, e che quindi permette un valore teorico di dimensione all’incirca 65500 byte). Inoltre sulla rete può capitare che dei pacchetti troppo grandi vengano droppati o che il livello sottostante della pila protocollare lo rifiuti direttamente.

  

La *seconda riga* contiene (tutti i parametri che servono a svolgere la frammentazione del pacchetto, vista più avanti):

* identificatore a 16bit
* flag
* spazzamento di framm. a 13bit



La *terza riga* contiene:

* Tempo di vita residuo: tempo di vita previsto (di solito a 64, ma dipende dal sistema operativo) per il pacchetto. Questo valore viene decrementato ad ogni nodo e serve a evitare di avere dei pacchetti dispersi sulla rete che potrebbero creare problemi e congestioni.
* protocollo di livello superiore: questo indica il protocollo di livello superiore che si è incaricato di inserire i pacchetti. Questo campo è importante perché permette di capire se si sono pacchetti TCP o a UDP oppure a altri protocolli (ad esempio ICMP). Di base questo serve a fare multiplexing, oppure a distinguere i pacchetti di instradamento (che solitamente vengono fatti con un pacchetto di livello 3-TCP).
* checksum (solo sull’intestazione): il protocollo IP protegge solo l’intestazione, quindi esegue il checksum dele prime 5 righe (o in più i campi opzionali)



La *quarta e quinta riga* contengono i campi di intestazione per origine (chi ha creato il datagramma) e destinazione (chi ha ricevuto o a chi è indirizzato il datagramma). Questi due campi sono entrambi da 32bit e ne tratteremo più avanti più nello specifico.

Seguono poi dei *campi opzionali*.



### Frammentazione dei datagrammi IP

Ogni rete ha un limite massimo di dimensione di datagrammi che possono essere mandati sulla loro connessione. Questo valore si chiama **MTU (Maximum Transport Unit)** e di fatto indica anche la dimensione massima del pacchetto che accetterà.

Solitamente questo valore è settato a $1500byte$, ovvero lo stesso valore di TCP, ma su alcune reti particolari questo può essere diverso (ad esempio le reti ATM hanno $50byte$)e quindi è necessario avere delle opzioni che ne riducano la dimensione. Questa azione può essere effettuata in due modi:

* la sorgente sa già che il percorso ha un certo MTU. In questo modo imposto un pacchetto per avere una grandezza massima definita a quel valore e pacchettizzo direttamente in modo di non superare mai quel limite.
* lascio al livello di rete di frammentare il mio datagramma

La prima impostazione non ha nessuna operazione diversa dal solito da effettuare, semplicemente che creerà dei datagrammi direttamente più piccoli a livello 3, senza immetterne sulla rete di troppo grandi.

Nel secondo caso avvegono una serie di operazioni automatiche a livello di collegamento attraverso i router, che sanno “rompere” il datagramma in segmenti automaticamente, siccome conoscono il valore di MTU delle reti che collegano.

L’importante di questa tecnica è che ogni datagramma sia valido, nonostante sia solo un frammento di un altro più grande. In questo modo di base si creano più pacchetti che hanno stessa intestazione (eccetto alcuni parametri) ma contengono pezzi di dati diversi. Inoltre la frammentazione così svolta (ovvero mantenendo validi i pacchetti) permette di avere ulteriori riframmentazioni dei pacchetti. 

Questi valori potranno poi essere ricomposti in un secondo momento dal router o direttamente dagli host.

Il lato negativo di questa tecnica è che si aumentano i punti di fallimento, infatti perdendo uno solo di questi frammenti, si ha perso completamente il pacchetto originale e è necessario ritrasmettere completamente tutto il datagramma originale (per questo motivo è preferibile ottenere una frammentazione a livello di trasporto).



L’operazione di frammentazione è piuttosto difficile e opera con diverse informazioni: la lunghezza del pacchetto, le flag di frammentazione e la dimensione di spiazzamento.

Prendiamo un datagramma originale di $4000byte$ su una rete con un MTU=$1500byte$, con questi valori nell’intestazione (sono presi dalle prime due righe del datagramma IP presentato sopra. Rappresento solo le informazioni importante per questa operazione):  

```html
Lunghezza = 4000
Flag = 0
Spiazzamento = 0
```

Siccome la rete ha MTU minore della lunghezza del pacchetto, allora è necessario procedere alla frammentazione. L’idea di fondo è di riempire ogni pacchetto al massimo e lasciare il rimanente nell’ultimo.

Tutti i pacchetti creati hanno una serie di caratteristiche:

* hanno una lunghezza uguale a MTU (ad eccezione dell’ultimo pacchetto).
* Flag settata a:
  * un valore uguale a zero se è l’ultimo pacchetto della frammentazione
  * un valore diverso da zero e crescente in base alla frammentazione, ovvero:
    * ogni volta che il pacchetto viene frammentato questa flag assume il valore di $\text{FlagPrecedente} +1$. Quindi se si esegue una sola frammentazione questo valore è esattamente $1$
    * Se si frammenta ulteriormente un pacchetto (già frammentato quindi) si ottiene $\text{FlagPrecedente}+1 = 1+1 = 2$.
    * Poi si continua con questa logica
    * Questi casi avvengono quando si passa su una serie di router che hanno valori di MTU sempre più piccoli. Così facendo si ottengono dei valori di flag di frammentazione sempre maggiori.
  * *è necessario* che in ogni operazione di frammentazione si ottenga l’ultimo frammento con valore 0, siccome indica che la comunicazione del pacchetto originale è terminata e si può iniziare a ricostruire.
* Dimensione di spiazzamento:
  * Questo parametro indica il valore del pacchetto in modo da permettere di riordinare tutti i frammenti. 
  * per risparmiare dei byte, questo parametro si presenta sempre con un valore diviso per $8$. Questo valore si tiene come puntatore e indica i byte del pacchetto in base alla posizione in quello originale. Ad esempio vediamo un caso in cui si ha un pacchetto di dimensione  $1480byte$ ($20byte$ di header IP + $1460byte$ di payload). Lo spiazzamento viene calcolato in questo modo:
    * la prima sezione ha lunghezza: $20 H + 1480 P$. Spiazzamento = $0$
    * la seconda sezione ha lunghezza: $20 H + 1480 P$. Spiazzamento = $ 1480/8 = 185$
    * la terza sezione ha lunghezza: $20H + (3980 - 2960)P = 1040 byte$. Spiazzamento $2960/8 = 370$



Solitamente non si frammenta facilmente, infatti TCP pacchettizza a $1500byte$ che è il MTU della maggior parte delle reti, inoltre la soluzione affidata al livello di trasporto è molto più funzionale e performante. Inoltre avendo così tanti frammenti, i pacchetti sono molto più lunghi da mandare e si introduce anche un certo costo di tempo di elaborazione da entrambe le parti (mittente e ricevente) per la frammentizzazione e la ricomposizione.

Per tutti questi motivi e anche per alcuni altri nella versione 6 del protocollo IP questa funzione viene tolta completamente. 

La frammentazione dei pacchetti nel protocollo IPv6 avviene solamente nel livello di trasporto, mai nel livello di rete, in modo da richiedere meno tempo al router per svolgere questa operazione. Per segnalare che un pacchetto è troppo grande e che l’MTU della rete è minore si è elaborato un altro segnale di risposta.



### Indirizzamento IPv4

L’indirizzo IP è un valore globalmente univoco e viene associata a qualsiasi interfaccia (sia host che router) visibili in rete. 

Esempio di assegnazione di indirizzo IPv4:

<img src="image/image-20211213010421635.png" alt="image-20211213010421635" style="zoom:50%;" />

Di fatto la rete è un unione di sottoreti. Le sottoreti sono definite dalla presenza del router, che lavorando a livello 3 impone la presenza di un interfaccia diversa per ogni sottorete.

Nella foto si può notare che i primi 3 raggruppamenti di valori sono uguali per tutti gli host e anche per il router centrale alla rete, mentre invece i router finali sono diversi per ogni host nella stessa sottorete.

I primi 3 gruppi di bit sono uguali in modo da permettere una facile comprensione di quali host siano nella stessa rete e quali invece siano esterni. Questa informazione è necessaria per l’indirizzamento del pacchetto iniziale, siccome è necessario capire se va trasmesso a un host sulla stessa rete (operazione svolta direttamente dall’host) o se invece va indirizzato al router perché si deve comunicare con un host distante. 

Questa operazione di controllo è molto facile per il computer, infatti viene fatto un controllo attraverso l’operazione binaria `&` tra gli indirizzi. 

Per definire una sottorete è necessario utilizzare una scrittura particolare che indica i **campi di indirizzi**, ad esempio `192.168.1.1/24`. Questa scrittura indica che i primi $24bit$ da destra sono l’indirizzo della sottorete (ovvero questi bit sono condivisi tra tutti gli host sulla rete), mentre gli altri sono il valore degli host sulla sottorete. Il valore dopo la barra può avere qualsia valore da 1 a 32. Il `/24` è il campo più diffuso.

Di fatto anche i collegamenti diretti sono delle sottoreti, e quindi hanno un campo di indirizzi (ad esempio la comunicazione tra due router). Solitamente per questo scopo si utilizza un campo `/30`.

Alcuni indirizzi **non si possono utilizzare** perché riservati per scopi particolari:

* l’indirizzo `0.0.0.0` (ovvero quello con tutti i bit settati a 0) rappresenta **la rete stessa**
* l’indirizzo `255.255.255.255` (ovvero quello con tutti i bit settati a 1) che è l’**indirizzo di broadcast**.

Il broadcast è un operazione che permette di mandare un pacchetti a tutti gli host della rete e per questo motivo non può mai essere effettuata su tutta la rete. Viene invece spesso effettuata una comunicazione del genere su delle sottoreti. I router vengono anche qui in aiuto: limitando la sottorete, si possono avere alcune comunicazioni in broadcast su una specifica rete studiando il posizionamento dei router tra gli host. Per questo motivo spesso i router vengono posizionati ad arte, in modo da spezzare le comunicazioni in broadcast e poi se necessario riunirle con il protocollo IP. 

I router non possono emettere nessun pacchetto in broadcast.

Generalmente gli utilizzi di una comunicazione in broadcast possono essere:

* un host vuole indicare la sua presenza sulla rete (ad esempio una stampante sulla rete locale)
* raggiungere un host di cui non conosco l’indirizzo

Alcuni mezzi di comunicazione (ad esempio il wifi) funzionano comunicando sempre in broadcast, ma in questo caso si sta parlando di un tipo di broadcast diverso, effettuato a livello fisico.

Un altro oggetto che utilizza il broadcast come mezzo di comunicazione dei dati sono gli access point. Questi oggetti però funzionano a livello 2 e hanno come unico scopo quello di arrivare al router più vicino, senza alcuna capacità di indirizzamento e/o forwading. 







La strategia per assegnare gli indirizzi tra gli host è chiamata **CIDR (Classless InterDomain Routing)**. Questa strategia di assegnazione degli indirizzi IP è fatta in modo da non sapere a priori quale sia la parte dell’host e la parte della rete. In pratica sui 32bit dell’indirizzo di rete, si ottiene un valore di bit, partendo da sinistra che indica quelli che possono indicare l’host, mentre invece i rimanenti sono comuni su tutta la rete. 

Questo tipo di indirizzo prende una forma del genere `a.b.c.d/x` dove `x` è il valore di bit che si riservano per l’host. 

<img src="image/image-20211213181233501.png" alt="image-20211213181233501" style="zoom:50%;" /> 

Questo valore (`x`) non deve per forza essere un multiplo di 8 ma può prendere qualsiasi valore possibile.

In questa sezione si deve fare la divisione tra il concetto di **indirizzo della sottorete** e **netmask della sottorete**. Praticamente l’indirizzo della rete è un valore che contiene i bit relativi alla sottorete costanti, mentre invece varia i bit della parte di host. La maschera di sottorete invece è un indirizzo che rappresenta la stessa cosa, ma si lasciano a `1` i bit della parte di sottorete, mentre invece si lasciano a zero i bit della parte relativa all’host.

Di fatto quindi un indirizzo è indicabile univocamente in due modi, sulla stessa sottorete: 

* maschera della sottorete (esempio: `255.255.254.0`)
* indirizzo della sottorete (esempio: `200.23.16.0/23`)

Questi modelli non si utilizzano mai assieme ma o uno o l’altro.

Questo modello è molto comodo e efficace siccome permette di comprendere subito se il pacchetto va consegnato nella stessa sottorete, oppure se deve uscire.  Inoltre essendo un operazione binaria, si può fare un confronto molto veloce (attraverso un semplice `&` binario). In questo modo si può subito capire se mandare a un host interno alla rete o affidarlo al router perché più lontano.



### Assegnazione di un indirizzo a un host

L’assegnazione di un indirizzo è sempre ottenibile in modo manuale, ma di solito si utilizza un metodo automatico. 

Generalmente la configurazione manuale dell’IP è utile per i server.

Di solito sulle reti è disponibile il **DHCP (Dynamic Host Configuration Protocol)**. Lo scopo di questo protocollo è di rendere “plug-and-play” l’ingresso in una rete, ovvero rende automatico l’assegnazione di un IP a un host sulla rete.

Questo protocollo è quello che permette di non aver quasi mai da gestire una rete. 

L’obiettivo è quello di consentire a un host di entrare in una rete, siccome l’assegnazione di un indirizzo è la base per ottenere una comunicazione sulla rete. 

Di solito il provider utilizzano spesso degli indirizzi dinamici, perché non si hanno troppi indirizzi. Questo approccio permette un ottimo utilizzo di questo protocollo.

Il principio è molto semplice, sono solo 4 messaggi:

*  l’host invia un messaggio di broadcast chiamato *DCHP discover*
*  il server DHCP risponde con un messaggio chiamato *DHCP request* (questo messaggio viene ottenuto solo se si trova un server sulla rete)
*  l’host richiede l’indirizzo attraverso un *DHCP request*
*  il server conferma l’indirizzo con un *DHCP Ack*



Affinché il funzionamento del protocollo DHCP sia corretto si deve avere il server sulla rete locale. Solitamente nelle case si ottiene in un unico pacchetto NAT, server DHCP e router, con magari anche il router WI-FI.

IL DHCP è diverso dal servizio di NAT. Il DHCP è comodo perché non si deve autoconfigurare l’indirizzo IP. Generalemente il DHCP di una sottorete da un indirizzo privato. Il DHCP può essere anche un server che asssegna indirizzi pubblici, ad esempio i server DHCP dei provider forniscono valori pubblici. Se il server fa parte di una rete allora si assegnano gli indirizzi privati della sottorete. 

Come si fa a collegarsi a una certa rete? Di solito con il cavo ethernet ci si collega, ma di solito richiede un autenticazione dell’utente (noi diamo per scontato che questa azione sia già stata fatta) e che si svolgano le azioni necessarie a ottenere l’indirizzo IP.

Le procedure di seguito sono tutte quelle che avvengono appena ci si è autenticati sulla rete.

<img src="image/image-20211213223124564.png" alt="image-20211213223124564" style="zoom:50%;" />  

* mando il protocollo dall’host (porta 68) al server DHCP (porta 67)
  * questo messaggio viene inviato come fosse un broadcast generalizzato
  * l’unico modo è mandare un messaggio composto da tutti 1 in modo che il pacchetto venga consegnato a tutti, sapendo che il router non lascerebbe uscire il messaggio dalla sottorete
* si utilizza anche un transaction ID, per distinguere le richieste
* si ha un indirizzo di sorgente, un destinatario e poi le porte di destinatario e sorgente. Dentro il payload poi si ottiene la risposta dell’indirizzo offerto dal server DHCP. Questo è il momento in cui il server rilascia un possibile indirizzo e un tempo di vita (il lifetime viene messo perché quando questo valore scade, allora l’host rifà la richiesta. Di solito in questo caso il DHCP ridà l’indirizzo precedente).
* si conferma la ricezione dell’indirizzo proposto e poi conferma del DHCP.
* una volta ultimata queste operazioni allora si ottiene un indirizzo IP per l’host. Spesso poi nell’ultimo pacchetto vengono date altre informazioni (ad esempio l’indirizzo IP del router per uscire dalla sottorete e la maschera di sottorete)



Come ottenere un blocco di indirizzo IP. 

Ad oggi ogni blocco di indirizzi è affittabile dal provider, siccome tutti gli indirizzi sono occupati, al momento. I provider, finché non è stato proposto l’approccio di avere indirizzi privati, avevano la necessità di assegnare un indirizzo pubblico a ogni utente. Di solito per un utente è abbastanza avere un solo indirizzo, mentre le aziende possono avere la necessità di avere più di un indirizzo.

La compravendita degli indirizzi consiste nel comprare dei bit sull’indirizzo ovvero un bit della rete e tutti i valori sotto quell’indirizzo. 

La divisione degli indirizzi si ottiene in modo molto facile: si vuole dividere in un certo valore di indirizzi, mettiamo 8 ($2^3$). Bisogna allungare la maschera di rete di altri 3bit. Ad esempio si ha una maschera di rete `200.23.16.0/20` sull’ISP. Si vogliono fornire indirizzi a 8 organizzazioni, quindi si divide in 8 parti uguali, aggiungendo 3bit alla maschera di rete: `200.23.16.0/23`, `200.21.18.0/23`, `200.23.20.0/23`, `200.23.22.0/23`, `200.23.24.0/23`, `200.23.26.0/23`, `200.23.28.0/23`, `200.23.30.0/23`. Di fatto questo genera i nuovi prefissi delle reti. Ognuna di queste reti ha 9bit di indirizzi per contenere tutti gli host, ovvero hanno $2^9 = 512$ indirizzi, a cui però vanno tolti il tutto $0$ e il tutto $1$. Quindi questo significa che ci sono $509$ hosts.

![image-20211213230658945](image/image-20211213230658945.png)

Di fatto nei casi pratici si può avere una netmask più restrittiva con `/30`. Quella con tutti zero e quella con tutti uni è la più restrittiva. Di solito questo tipo di connessioni avviene nelle connessioni tramite cavo, che sono necessari solo 2 indirizzi.



**MANCA INDIRIZZAMENTO GERARCHICO PIÚ PRECISO**



C’è un pochino di centralizzazione sulla rete internet, infatti **ICANN** (stessa corporazione che risolve i nuovi nomi di dominio e cose varie)  ha la responsabilità di gestire e vendere gli indirizzi online. Di fatto questo valore non può essere distribuito e quindi è necessario essere centralizzati.



### Traduzione degli indirizzi di rete (NAT) Network Address Translator 

Questo oggetto è utilizzato in connessione casalinga, per separare la rete aziendale/domestica da quella pubblica. Di fatto significa che da un lato è su internet (da qui partono i pacchetti verso la rete internet e quindi ha un indirizzo IP pubblico) e poi dall’altra parte ha un indirizzo riservato (per definizione infatti non sono privati, ma sono riservati. Erano nati con l’idea di usarli per esperimenti e test.). Possono essere `10.0.0.4` oppure del tipo `192.168.1.1`. Di fatto questi indirizzi poi non sono più stati utilizzati sulla rete pubblica. Lo spazio di indirizzi utilizzabili sulla rete privata è grande quando serve, infatti di solito non c’è nessun problema a aumentare il numero di host sulla rete.

Il NAT fa in modo di convertire i pacchetti in uscita e in ingresso in modo che gli host in rete locale siano raggiungibili attraverso l’IP pubblico del NAT.

Chiaramente questa operazione ha alcuni vantaggi:

* è economico (per farlo funzionare è necessario un unico indirizzo IP). Più di un indirizzo inizia a diventare costoso.

* Si possono cambiare tutti gli indirizzi privati dell’abitazione senza influenzare la rete esterna o senza essere in qualsiasi modo bloccato.

* Posso cambiare l’ISP senza cambiare il resto della rete

* (poi diventato uno dei motivi principali per utilizzare il NAT). I dispositivi dietro il NAT non sono esplicitamente indirizzabili e visibili dal mondo esterno, quindi rende la rete interna decisamente più sicura (di fatto questo significa che i computer interni alla rete non sono direttamente raggiungibili.)

  Esempio: la stampante in rete può accedere a internet, ma di fatto mentre lavora nella casa è disponibile solo alla rete interna, non è visibile all’esterno.



L’implementazione del NAT è piuttosto semplice, ma richiede di rompere la divisione dei layer della pila protocollare.

Infatti affinché il NAT funzioni allora è necessario che il router possa gestire gli indirizzi di trasporto (cambiare le porte degli indirizzi di fatto, cosa che in teoria sarebbe vietata dalla stratificazione protocollare). Per questo motivo e per un altro paio di altri il NAT è stato discusso: di fatto interviene su una situazione molto delicata. Questo è necessario perché: 

* quando un router NAT riceve un datagramma allora il NAT crea un nuovo numero di porta in uscita
* sostituisce l’indirizzo privato dell’host con il proprio indirizzo **WAN (Wide Area Network) - Indirizzo Pubblico** e sostituisce il numero di porta di orgine iniziale con il nuovo numero.

Praticamente quello che viene fatto è che sostituisce l’indirizzo del pacchetto con il proprio indirizzo pubblico (in modo che poi possa transitare sulla rete). Questo però genera un problema: il router NAT deve sapere a chi inoltrare la risposta. Per raggiungere questo scopo si utilizza anche il numero di porta, che serve quindi a identificare l’indirizzo di porta originario.

L’immagine sotto identifica meglio lo scenario:

![image-20211214101834551](image/image-20211214101834551.png)



Di fatto la tabella di traduzione NAT serve a “spiegare” al router dove i pacchetti debbano essere inoltrati e di fatto permette quindi il trasporto dei pacchetti dentro e fuori la rete pubblica.

Il NAT in questo modo permette di avere un numero di indirizzi IP potenzialmente infinito (poi in realtà non così tanto). Inoltre permette di avere un range di IP privati molto grande. Inoltre non ci accorge della sua presenza se non per l’indirizzo privato della connessione. 

Se si è dietro il router NAT per sapere il proprio indirizzo IP pubblico è necessario appoggiarsi a qualche servizio che indica l’indirizzo pubblico presente sulla rete.

Il NAT si è diffuso molto per la sicurezza che fornisce e per il moltiplicatore di indirizzi IP che permette di fare. Il blocco di questo modello è il numero di porta (di fatto impone il numero di host fornibili con questi indirizzi fino al numero di porte del NAT, quindi meno di 65000). 

Questo modello è stato contestato dai puristi della rete, siccome infrange il modello della stratificazione della rete.

Inoltre si parla di **argomento punto-punto**: siccome il NAT deve scrivere nell’intestazione del pacchetto di trasporto, questo viola l’argomento punto-punto, perché di fatto quello che viene fatto dal livello di trasporto in su non dovrebbe avere interferenze della rete. Questo è un rischio siccome se il NAT avesse dei problemi, allora si perderebbero dei pacchetti.

Inoltre questa argomentazione direbbe che passando a IP versione 6 (che il numero di indirizzi è potenzialmente infinito e non esisterebbero queste necessità di moltiplicazione delle connessioni). 

Ci sono altri problemi che non si possono risolvere in maniera molto elegante. Il client fa in modo di creare indirizzamento pubblico-privato. Ma di fatto come si fa se il client è fuori dalla rete e si vuole dare accesso a qualcuno che non è in indirizzo privato. Il client non potrà raggiungere il server dietro il NAT finchè la connessione non è inizializzata dalla rete internamente, siccome nella tabella di traduzione NAT non è presente ancora nessun valore. Questo problema ha alcune soluzioni:

* SOLUZIONE 1: si inserisce la corrispondenza sulla tabella degli indirizzi NAT a mano sul NAT in modo da permettere di accedere sempre dall’esterno.
  * Questo modello ha alcuni problemi: 
    * L’ISP può fornire un router che non permette questa azione
    * si possono avere dei problemi di configurazione e/o sbagliare
* SOLUZIONE 2: Universal Plug and Play (**UPnP**) Internet Gateway Device (IGD) Protocol. 
  * Questa azione permette, quando si accende l’host, di richiedere un qualche tipo di mapping per un qualsiasi tipo di porta. 
  * Questo modello si utilizza per un sacco di oggetti privati. Questo può succedere che le porte siano state assegnate e di solito spegnere e riaccendere il router, e di solito questo funziona siccome fa resettare la tabella NAT e quindi in questo modo gli oggetti smart possono richiedere di nuovo la porta e prenotarsela, in modo da avere l’accesso a internet. 
  * Questo si utilizza spesso per i dispositivi che sono oggetti (non computer), siccome è una soluzione abbastanza elegante ed è plug-and-play.
* SOLUZIONE 3: questa è una soluzione estrema e di fatto si utilizza quando le altre non funzionano o non sono applicabili. Questa viene fatto quando ho un software dietro il NAT allora si utilizza una connessione a un nodo esterno che permetta di mantenere la entry sul NAT, poi rimango connesso con il super-nodo e al quando vengo ricercato allora il relay si occupa di connettermi con l’host attraverso la connessione impostata tramite NAT. Questo funziona siccome finché non si fa traffico sulla rete allora non si crea la entry dietro al NAT. Se io devo utilizzare un software e so che verrà spesso utilizzato dietro il NAT allora devo trovare subito una soluzione.
  * Questa soluzione è quella utilizzata da Skype (questa funziona perché il servizio ha dei nodi divisi abbastanza bene su tutta la popolazione)
  * Spesso il telefono VoIP (Voice Over IP) utilizza questo modello: la gente per chiamare deve utilizzare una tecnica di questo tipo. In questo modello di solito si mette uno scatolotto sull’internet pubblica (come fosse un centralino). Di solito chi vende il servizio VoIP vende questo scatolotto che permette di essere esterno al NAT e mantenere la connessione.



### ICMP (Internet Control Message Protocol)

ICMP è un protocollo di servizio per reti a pacchetto che si occupa di trasmettere informazioni riguardanti malfunzionamenti, informazioni di controllo o messaggi tra i vari componenti di una rete di calcolatori.

Questo modello viene utilizzato da host e router per scambiarsi informazioni. Il servizio, nonostante sia un servizio necessario al livello 4, però utilizza dei pacchetti di livello 3.

I messaggi ICMP sono piuttosto semplici: campo tipo, campo codice e un frammento di datagramma.

I messaggi ICMP più utilizzati sono:

<img src="image/image-20211214104609372.png" alt="image-20211214104609372" style="zoom:50%;" />

Poi ci sono una serie di messaggi che servono al client e che servono al router (ad esempio che il router non riesce a connettersi all’host). TTL scaduto se i pacchetti devono essere mollati. Errata intestazione IP serve per la mobilità, per cui serve un router particolare che supporta la versione mobile e in questo caso è necessario cercare il router quando c’è.

Traceroute funziona utilizzando i messaggi ICMP in questo modello:

<img src="image/image-20211214105028892.png" alt="image-20211214105028892" style="zoom:50%;" />

### IPv6

La versione 6 di IP è stata teorizzata nel 2000 per un esigenza che era già chiara. Di fatto utilizzare gli indirizzi a 32bit è un modello di indirizzi troppo restrittivo, nonostante all’inizio sembrassero troppi.

Di fatto si è creato un nuovo protocollo IP, ma siccome si aveva studiato molto il protocollo IPv4 allora si voleva inserire alcune modifiche per renderlo migliore:

* cercare di avere un formato con meno campi, in modo da migliorare la velocità del servizio

* frammentazione lato router non è veramente necessaria e fa solo perdere tempo

  

* Esigenza principale: il numero di indirizzi è troppo piccolo 

  * Adesso si hanno $2^{128}$ indirizzi, questo significa che potenzialmente il campo indirizzi può descrivere un numero di host infinito.

  * con questo modello quindi si risolvere il problema del numero di indirizzi

    

* il formato d’intestazione di questo nuovo modello rende più veloce la connessione e i processi di elaborazione e inoltro

L’intestazione della parte obbligatoria diventa 40byte e lunghezza fissa (il protocollo IP versione 4 ha lunghezza 20byte). Di fatto anche se il pacchetto è più grande le informazioni contenute sono minori, infatti ci sono meno campi contenuti in questo modello, ma si è data la priorità a contenere indirizzi molto vasti.



<img src="image/image-20211214105705587.png" alt="image-20211214105705587" style="zoom:50%;" />

L’intestazione è sempre a 32bit, ma ci sono meno campi siccome il *source address* e il *destination address* sono 4 righe per campo entrambe.

* campo versione: che indica la versione 6 del protocollo IP
* *priorità di flusso*: attribuisce una certa priorità a dei campi determinati di flusso

* *etichetta di flusso* (**flow label**) attribuisce priorità a determinati datagrammi di un flusso. Questo è un modello per indicare che una certa serie di pacchetti sono dello stesso flusso di dati e quindi non è necessario che il router faccia un elaborazione.
* *payload len*: lunghezza del payload, che indica la lunghezza del pacchetto
* *intestazione successiva* (**next hdr**): indica il protocollo a cui verranno consegnati i contenuti del datagramma
  * di fatto questo campo server a indicare delle eventuali parti opzionali (sempre ridotte al minimo) all’interno del pacchetto.



#### Rappresentazione di questi indirizzi

Solitamente questo indirizzo si scrive in esadecimale => impossibile ricordarsi a memoria gli indirizzi degli host. Quando si vedono i doppi punti allora vuol dire che c’è un gruppo di zeri.

```http
fe80::1400::81c6::a5f1::dd6c%en0
```

Di solito questo indirizzo si concatena con il valore dell’interfaccia (`en0`). 

Logicamente in questa versione del protocollo i dns e tutti questi servizi diventeranno necessari, siccome ricordare questo campo di indirizzi sarà praticamente impossibile.



#### Transizione da Version 4 a Versione6

Il problema è che la maggior parte dei router di accesso sono in versione 4 e non è possibile aggiornare allo stesso momento tutti i router. Di fatto ci sono alcuni servizi che non si possono fermare completamente per un certo tempo. Inoltre deve essere un operazione che deve essere fatte a passi.

Nonostante questo ad oggi molte reti sono alla versione 6, e le connessioni di alcuni paesi anche. 

In pratica per far coesistere le due versioni si utilizza il cosiddetto **tunneling**: si prende un pacchetto ad un certo livello e lo si utilizza come payload di un livello che non è successivo. Utilizzando questo modello tra due punti è come se si facesse passare una connessione dentro l’altra.

<img src="image/image-20211214111446403.png" alt="image-20211214111446403" style="zoom:50%;" />

Questo modello di fatto fa in modo di inserire un pacchetto Ipv6 all’interno di un pacchetto IPv4, oppure viceversa (un pacchetto IPv4 incapsulato dentro un IPv6).

La vista fisica e logica di questo modello è sviluppata in questo modo:

<img src="image/image-20211214111855428.png" alt="image-20211214111855428" style="zoom:50%;" />

Di fatto i router sul confine devono saper gestire entrambi i protocolli, in modo da poter istanziare il tunnel e permette l’inoltro in questo tunnel. Logicamente il funzionamento lavora anche nell’altra direzione.

Questa modalità permette di mantenere la versione 4 utilizzabile, mentre tutta la rete si aggiorna.



La rete IPv6 com’è messa? Secondo Google il 30% dei router di accesso supporta le connessioni IPv6, ovvero che la gente accede direttamente in IPv6. Si possono vedere statistiche aggiornate [qui](https://www.google.com/intl/en/ipv6/statistics.html).

La lentezza di questo cambiamento sono più di una:

* i servizi non sono facilissimi da applicare
* i router di bordo che devono instanziare il tunneling non sono molto economici



Di fatto però la completa conversione permetterebbe di avere una rete più uniforme e più reattiva.



## 4.5 Algoritmi di instradamento

Studiamo quali sono le procedure per trovare il percorso in una rete. Le capacità di inoltro sono sviluppare nel router per mandare i pacchetti da una porta all’altra.

Ci serve ancora conoscere come si fa a trovare una strada su internet. Di fatto ogni router ha una tabella di instradamento locale/inoltro che permetterà al router di inoltrare pacchetti.

Il compito dell’algoritmo di instradamento è il responsabile della popolazione delle tabelle di instradamento locale.

Questo modello è basato su un algoritmo matematico che deve poi però essere ritrasposto in un protocollo applicabile.

Si ha la necessità di distinguere l’indirizzo della sottorete, ma l’indirizzo dell’host è chiaramente unico.

Una sottorete ha necessità di aver associata la rete a un valore di indirizzi variabili.

Studiamo quali sono le procedure per trovare il percorso in una rete. Le capacità di inoltro sono sviluppare nel router per mandare i pacchetti da una porta all’altra.

Ci serve ancora conoscere come si fa a trovare una strada su internet. Di fatto ogni router ha una tabella di instradamento locale/inoltro che permetterà al router di inoltrare pacchetti.

Il compito dell’algoritmo di instradamento è il responsabile della popolazione delle tabelle di instradamento locale. E’ importante trovare dei meccanismi che permettano di costruire queste tabelle anche per reti molto grandi.

### Il modello

Questo modello è basato su un algoritmo matematico che permette di trovare i percorsi, che di appoggia alla minimizzazione di una funzione. Poi dobbiamo trasformarlo in qualcosa che consenta ai nodi di parlarsi e di scambiarsi informazioni in maniera standard.

Il modo con cui la rete è collegata in un astrazione matematica e su cui poi si possono pensare gli algoritmi di instradamento è il grafo. In questo modello non andremo molto in profondità allo strumento matematico. Di fatto utilizziamo il grafo siccome utilizziamo la nozione di insiemi di nodi e insieme di archi che collegano gli host. 

Abbiamo i nodi della rete che vengono collegati, poi abbiamo gli archi che sono i collegamenti diretti tra i router, ovvero che hanno tra di loro un collegamento di livello 2 oppure di livello fisico.

Di fatto per capire il grafo è importante sapere cos’è un router e quali sono gli host poi. Di fatto poi attraverso questo metodo è possibile visualizzare bene la rete, poi non pone molto vincoli sulla rappresentazione della rete, ma permette anche di avere una serie di nodi in tutte le configurazioni.

Un esempio di grafo è:

![image-20211215095610546](image/image-20211215095610546.png)

<img src="image/image-20211215095632658.png" alt="image-20211215095632658" style="zoom:50%;" />

Da questo grafico si può capire il costo di collegamento per ogni ramo. Esempio: $c(w, z) = 5$. Generalmente non ci sono costi negativi nelle reti che affronteremo noi. Una volta definiti i costi dei collegamenti diretti, bisogna trovare un metodo per sommare i collegamenti. In questo modo riusciamo a calcolare i costi per andare da un nodo a un altro non direttamente collegato, allora sommiamo i costi dei singoli collegamenti. In base a costi dei collegamenti, la formula per trovare il collegamento migliore bisogna definire il modo per trovare il costo migliore.

Il funzionamento ad alto livello di questo modello è che: si trovano tutti i percorsi possibili, si trova il costo unitario di ogni percorso e si sceglie il percorso migliore selezionando quello con costo minore. Il costo del collegamento di solito è associato dal gestore della rete, che si occupa di assegnare un valore a ogni collegamento. Di solito se i costi sono tutti uguali l’algoritmo troverà il percorso con meno salti (siccome tutti costano uguale, cerco di minimizzare il numero di percorsi che devono essere fatti).

Quindi di solito si riesce a ottenere il funzionamento di questi algoritmi attraverso questi valori di costo assegnati a ogni collegamento. Di fatto più si vuole il collegamento inutilizzato, più si assegna un valore alto al collegamento, in questo modo verrà scelto solo poche volte.

Logicamente l’assegnazione dei costi deve essere fatta in modo coscienzioso, in questo modo si può dare la priorità a alcuni collegamenti migliori di altri, oppure si farà in modo che si preferisca un dato collegamento rispetto a un altro.

### Globale o Centralizzato, Statico o Dinamico

Ci sono due algoritmi di questo tipo, uno che ha necessità di avere tutte le informazioni prima (ha necessità di una visione globale), un altro modello permette di avere una conoscenza distribuita.

I modelli che funzionano a livello globale vengono chiamati **link-state algorithm** oppure **algoritmi a stato di collegamento**. Siccome questo modello è globale, quindi significa che per lanciare questo algoritmo è necessario conoscere tutte le informazioni dei collegamenti della rete.

Il modello a funzionamento decentralizzato sono chiamati **algoritmi a vettore distanza** oppure **DV, distance-vector algorithms**.  I costi di collegamento vengono fatti da ogni nodo in modo distribuito attraverso una stima del collegamento con i nodi vicini. In questo modo il cammino a costo minimo viene calcolato in modo distribuito e iterativo, in modo da trovare le situazioni migliori.

Poi di algoritmi poi ce ne sono tantissimi, migliori/peggiori in prestazioni. Poi le implementazione dei due modelli possono essere equivalenti nei costi, ma l’applicazione è diversa in base agli scopi che si vogliono raggiungere.



Un instradamento statico permette di fare calcoli su dei cammini che cambiano molto raramente. Questo significa che una volta che si elaborano i valori di instradamento allora rimangono costanti per un valore di tempo. Chiaramente questi algoritmi vengono ricalcolati una volta passato un certo valore di tempo, ma di fatto rimangono costanti sulla connessione.

Instradamento dinamico è applicato a delle reti che continuano a cambiare (ad esempio delle reti di veicoli). In questo modello la rete è sempre in un processo di modifica, quindi è necessario che gli algoritmi reagiscano molto frequentemente (anche, ad esempio, che la rete perda un qualche nodo che si disconnette). Altri tipi di reti di questo tipo possono reagire a un volume di traffico diverso o altri cambiamenti della topologia della rete. Questi valori sono costanti durante l’applicazione dell’algoritmo, siccome in caso contrario sarebbe difficile anche effettuare il calcolo. Spesso questa mobilità viene applicata alle reti di accesso che per fornire flessibilità agli host permetto un continuo cambio di valori degli host e delle posizioni degli host.

Questa mobilità non deve essere confusa con il modello di connessione in mobilità che, ad esempio, fornisce il wifi. Questa rete infatti è si senza fili e permette la mobilità degli host, ma di fatto ogni host connesso a un modello di rete di questo tipo ha una topologia costante.

Quando si parla di topologia della rete si parla di grafico. Ad esempio da stella diventa circolare oppure altre modifiche del genere.



### Algoritmo Dijkstra

Questo algoritmo è un algoritmo a **stato di collegamento**.

Per ottenere questo algoritmo è necessario che tutti i nodi conoscano i costi di ogni link. Questo stato dei dati si ottiene attravero un *link-state broadcast*, ovvero tutti i nodi comunicano ai vicini le informazioni dei collegamenti che possiedono, in modo che tutti i nodi abbiano la visione completa. Questa operazione è costosa dal punto computazionale e di banda, infatti si inonda la rete con un sacco di pacchetti di stato tra i nodi della rete, motivo per cui di solito è meglio lanciare questo algoritmo poche volte e soprattutto quando la rete è più scarica. Inoltre se la rete è stabile non è necessario continuare a lanciarlo.

Questo algoritmo funziona attraverso il calcolo svolto su un nodo di tutti i percorsi, quindi di fatto la spiegazione viene fatta da un unico nodo. Di base viene fatto un calcolo del costo di ogni collegamento da un nodo iniziale a tutti gli altri.

Una volta sviluppati questi algoritmi si salvano tutti i migliori collegamenti che serviranno poi a costruire la **tabella di inoltro**.

Questo algoritmo indica che se c’è un certo numero di nodi su questa rete, allora esiste un certo numero di direzioni. 

Questo algoritmo è lineare in funzione dei nodi della rete di cui dobbiamo calcolare il collegamento. Infatti la $k$-esima iterazione di questo algoritmo permette a $k$ nodi di conoscere i costi minimi delle sue connessioni.

Per spiegare questo algoritmo è necessario definire dei valori:

* $c(x, y)$: costo dei collegamenti dal nodo $x$ al nodo $y$. Questo costo è uguale a $\infty$ se i nodi non sono adiacenti, altrimenti il valore è un numero intero.

* $D(v)$ costo del cammino dal nodo di origine alla destinazione $v$ rispetto all’iterazione dell’algoritmo corrente. 

  Questo costo varierà in base all’interazione, quindi finché l’algoritmo non è terminato si avranno una serie di valori parziali.

* $p(v)$: questo valore indica il predecessore del nodo di arrivo $v$.

* $N’$ è il sottoinsieme di $N$ (dove $N$ è l’insieme di tutti i nodi della rete su cui si sta lanciando l’algoritmo) per cui il cammino a costo minimo dall’origine è definitivamente noto.

  Infatti poi vedremo come ad ogni passo questo algoritmo sappia identificare un percorso che ha costo unitario minimo tra tutti i percorsi.



```pseudocode
// inizializzazione
// inserimento del nodo U nella rete
N_primo = {U}
// da questo momento si calcola il costo di ogni collegamento diretto
// indichiamo con V un nodo di U
while(V)
	if(V.isDirect(U)):
		D(V) = c(U, V);
	else:
		D(V) = INF;
		
// inizia il ciclo => Continua finché non si svolge il calcolo su tutti i nodi
while(N != N_primo) 
	// determina un nodo W, non in N_primo tale che D(W) sia minimo
	// aggiungi W a N_primo
	min = W_1;
	forEach(W)
		if(W < W_1):
			min = W
	N_primo.add(W);
	
	// Aggiorno D(v) per ogni nodo V adiacente a W e non in N_primo
	D(V) = min( D(V), D(W)+c(w, v));
    // questo significa che il *nuovo costo del percorso verso V* è:
    // - il vecchio percorso verso V
    // (oppure)
    // - il costo del cammino noto minimo (D(W)) sommato al costo del percorso da W a V

```

Un esempio di questa applicazione può essere:

![image-20211215115215084](image/image-20211215115215084.png)

Chiaramente il risultato sarà visualizzato in funzione del nodo che ha richiamato l’algoritmo, e in questo modo riesce a calcolare la via più veloce per raggiungere tutti gli host della rete, avendo come punto di partenza se stesso. L’algoritmo lanciato in $U$ infatti restituisce questi valori (che contribuiscono alla creazione della tabella di inoltro):

![image-20211215115859738](image/image-20211215115859738.png)







Una visualizzazione e spiegazione visuale dell’algoritmo in 3 minuti è visibile a [questo algoritmo](https://www.youtube.com/watch?v=_lHSawdgXpI).

La complessità di questo algoritmo, implementato nel modello più efficace possibile è $O(nlogn)$.



Si noti una cosa: in questo algoritmo si utilizzano i costi del collegamento come indicazione della quantità di traffico. Di base questa pratica non è *mai* consigliata siccome:

1. per definizione dell’algoritmo si sceglierà la sezione di rete sempre meno carica

2. la rete sarà carica in base alle scelte precendenti, quindi si otterrà un continuo cambiamento di routing rispetto all’istradamento iniziale, che potrebbe causare instabilità alla rete.

   ![image-20211215120554969](image/image-20211215120554969.png)

   Questo fatto non sarebbe un problema, se non fosse per l’RTT del collegamento così variabile. Infatti, soprattutto TCP (che ha necessità di fare un calcolo dell’RTT) potrebbe avere dei problemi di stabilità e di funzionamento, siccome si otterrebbero dei valori teorici molto falsati che non rispecchiano sempre la stessa rete fisica e che quindi sono soggetti a più errori. Questo potrebbe causare eccessivi timeout, oppure un throughtput molto ridotto a causa di perdita di pacchetti frequenti.

### Algoritmo con vettore distanza

Questo algoritmo proviene dalla programmazione dinamica: si risolve un problema attraverso la scomposizione in problemi più piccoli e più facilmente risolvibili distribuiti su più agenti.

L’algoritmo che prevede questa applicazione si chiama **Bellman-Ford**. Per capire questo algoritmo è necessario sapere che $d_x(y)$ è il costo del percorso a costo minimo dal nodo $x$ al nodo $y$. Il secondo concetto è il calcolo di questo valore: $d_x(y) = min_v(c(x, v)+d_v(y))$.

Questo valore deve essere il minimo tra tutti i vicini di $y$.

Immaginare che tutti i router vicini informino il nodo richiedente del valore di risposta di un determinato nodo. In questo modo, attraverso questa formula voglio trovare il migliore tra i collegamenti, sommando il valore dei vari nodi, facendo però una richiesta a ogni pezzo della rete.

Questo algoritmo lavora in maniera distribuita, ovvero fa i calcoli per se stesso e poi propaga le informazioni. Piano piano ogni nodo conosco anche le informazioni per i nodi più lontani, in questo modo posso sapere anche le informazioni dei vicini. 

Questo funzionamento fa in modo da richiedere il valore del percorso minimo per un determinato tragitto ai vicini e poi sommare il proprio costo per raggiungere il vicino, infine minimizza.

Uno dei vantaggi di questo algoritmo funziona attraverso questo modello distribuito, facendo così ogni nodo calcola per se stesso e poi informa i vicini. Così facendo la potenza computazionale necessaria è minore e distribuita su tutta la rete, e a nessun nodo è richiesto informarsi su tutti i nodi.

In questo modo inoltre si diminuiscono anche i pacchetti da mandare sulla rete, infatti la rete non ha bisogno di mandare tutti i percorsi, ma solo i minimi. Inoltre questo scambio di pacchetti si ferma automaticamente quando tutti i nodi hanno ottenuto il valore minimo.


Con questo algoritmo vengono condivise solo le informazioni che permettono di avere la tratta minore. Inoltre i costi di questi circuiti vengono aggiornati (diminuendo il costo del collegamento) allora questa informazione probabilmente influenza un sacco di percorsi e viene condivisa con gli altri nodi velocemente, invece se una distanza vettoriale aumenta, allora più difficilmente i percorsi possono cambiare. Così facendo questo modello punta sempre a scegliere i costi migliori tra tutti i percorsi. L’unico modo per far viaggiare velocemente anche le informazioni negative sulla rete allora è necessario impostare un valore massimo della distanza di collegamento. In questo modo quando si raggiunge quel valore allora il percorso viene definito come $\infty$ e quindi tutto il percorso cade e viene annullato, portando a cercare un alternativa.



Link della visualizzazione di questo algoritmo a questo [link](https://www.youtube.com/watch?v=9PHkk0UavIM).



### Confronti tra i due algoritmi

Complessità dei messaggi:

* LS (Dijkstra): durante la fase bisogna mandare un numero di massaggi nel valore di $O(nE)$ ($n$ nodi e $E$ collegamenti). Questo valore è molto elevato (soprattutto se la rete è molto grande).

* DV (Bellman-Ford): siccome il tempo di convergenza è variabile questa fase può portare alla produzione di un numero di messaggi variabile e dipende dalla velocità con cui si propagano queste informazioni. 

  Questo significa anche che la situazione potrebbe essere più instabile per più tempo.

Velocità di convergenza:

* LS: l’algoritmo ha complessità $O(n^2)$ e richiedere di mandare $O(nE)$ messaggi, per cui ci possono essere oscillazioni di velocità.

  Non può presentare cicli di instradamento (quindi se un valore è circolare allora si otterrà un valore continuamente variabile fra due) e di fatto la rete non sarà mai stabile.

* DV: la convergenza può avvenire molto più lentamente e può presentare cicli di instradamento.

  Può presentare il problema del conteggio dell’infinito, ovvero non dando un upperbound allora è difficile che la peggiore strada possibile venga identificata e eliminata causando una modifica dei percorsi istantanea.

Robustezza: (cosa succede se un router funziona male)

* LS: se il router funziona male allora il router rappresenta un errore solo per se stesso, infatti tutti i calcoli per tutti i nodi vengono fatti in locale, creando un problema di instradamento sbagliato solo sul suo nodo.
* DV: in teoria il nodo che fornisce informazioni sbagliate potrebbe portare all’indirizzamento sbagliato di molti nodi della rete, siccome la sua informazione sarebbe ridistribuita su tutta la rete.



### Instradamento Gerarchico

Il problema degli algoritmi di instradamento è che sono molto dispendiosi sulle reti molto grandi, motivo per cui è necessario trovare un modello che ne riduca la complessità.

Inoltre si avrebbe un problema di archiviazione, infatti non si potrebbero immagazzinare tutti i dati di tutti gli host su ogni nodo della rete siccome sarebbero troppe informazioni.

Questo modello viene trovato con il concetto di internet per definizione, ovvero che la rete è una rete di reti.

Esiste quindi una serie di algoritmi che permettono di fare queste operazioni su delle isole interne. Poi esistono una serie di router che conoscono i metodi di implementazione per interfacciarsi sulla rete esterna e che quindi hanno necessità di conoscere tutti gli algoritmi di routing.

Ogni isola in questo schema si chiama *sistema autonomo (AS - **autonomus system**)* e permette di avere delle organizzazioni interne ed esterne alla rete. Di solito questa entità eseguono lo stesso algoritmo di instradamento, che infatti vengono divisi tra **intra-AS** (protocolli di instradamento all’interno del sistema autonomo). Router appartenenti a diversi AS possono eseguire protocolli di instradamento intra-AS diversi (potenzialmente si possono scrivere dei protocolli di instradamento completamente proprietari, se si vuole ottenere un determinato obiettivo). 

**Inter-AS** sono i protocolli di instradamento *tra* i sistemi autonomi permettono di collegare i sistemi autonomi. Questi router, chiamati **router gateway** hanno la possibilità di far girare internamente il sistema di instradamento interno, all’esterno la IETF ha ideato un protocollo che permette di collegare le diverse isole di traffico, chiamato **BGP**.

Di fatto questo algoritmo permette di capire tra quali isole di traffico far passare il pacchetto in modo che sia il più veloce possibile, calcolo che è rimandato all’algoritmo inter.



#### Protocolli IGP (Inter Gateway Protocol)

Questi protocolli sono conosciuti anche come intra-AS. Servono quindi a gestire l’instradamento del traffico interno a una rete.

I protocolli intra-AS più comuni sono:

* **RIP**: routing informazion protocol
* **OSPF**: open shortest path first
* **IGRP**: interior gateway routing protocol



##### RIP (Routing Information Protocol)

è un protocollo a vettore distanza. Utilizza come metrica il numero di hop che l’host deve fare, e inoltre è di default su UNIX BSD dal 1982 (quindi piuttosto antico).

Ogni collegamento ha costo unitario (1) e quindi i collegamenti diretti saranno solo $1$ hop. Il massimo numero di hop contabili è 15, quindi è impostato l’infinito a 16 salti. In questo modo non si ha mai un collegamento che fa più di 15 salti di connessioni tra i nodi.

In questo protocollo i nodi si scambiano un messaggio di risposta RIP (RIP advertisement, oppure messaggio di annuncio RIP). Questo pacchetto indica le distante relative ad un massimo di 25 sottoreti di destinazione all’interno del sistema autonomo.

Guasto di comunicazione e advertisement: quando il nodo adiacente non risponde per un intervallo di 180 secondi (ovvero 6 advertisement) allora il nodo adiacente o il collegamento vengono ritenuti spenti o guasti, e di fatto si ricalcola la tabella di routing locale. Dopodichè propaga l’informazione mandando annunci ai router vicini, che mandano nuovi messaggi se la loro tabella d’instradamento è cambiata a sua volta. Di fatto l’informazione di propagazione di un router guasto si propaga velocemente su tutta la rete. Si può utilizzare anche la tecnica dell’**inversione avvelenata**, ovvero si indica quel collegamento come numero di hop infinito (16hop), in questo modo questo percorso non può essere scelto da nessuno e si procede al ricalcolo di tutti i percorsi.

Questo calcolo viene fatto da un processo, chiamato **routed** all’interno del sistema operativo UNIX. Questo processo è quello che implementa RIP. Questo è un processo particolare che ha i privilegi per scrivere nelle tabelle di inoltro (cosa particolare, siccome sono solitamente riservati a livello tre questi privilegi). Questo modello esce leggermente dal contesto del terzo livello, ma permette di avere una comunicazione su dei pacchetti UDP standard. Questa soluzione è abbastanza interessante, siccome non necessita di nessun altro protocollo di comunicazione, ma si appoggia a questo già esistente.



##### OSPF (Open Shortest Path First)

Questo algoritmo esegue un implementazione di DJikstra. Il protocollo è definito *open* siccome tutte le specifiche e il codice sono disponibili pubblicamente. é un algoritmo molto utilizzato nella realtà.

Questo protocollo utilizza il *flooding* di informazioni di stato del collegamento per trasferire tutte le informazioni di stato su tutti i nodi.

Quando un router si accorge che in una connessione ci sono costi diversi, allora informa in broadcast tutti gli altri nodi della rete, in questo modo la modifica dei percorsi avviene in seguito a ogni modifica dei percorsi e fa in modo di modificare solo i nodi che questa modifica potrebbe cambiare il percorso minimo. Di fatto questo farà il triggering di un nuovo Dijkstra. Il flooding avviene solo all’inizio, quando viene lanciato la prima volta oppure viene lanciato manualmente. 

Questo protocollo intra-AS tutti i messaggi sono fatti all’interno del AS (non si tratta di un broadcast vero e proprio, siccome possono attraversare i router, senza essere bloccati alla sottorete). Tutte le comunicazioni avvengono attraverso il protocollo IP vero e proprio, senza dei protocolli di livello 3 (UDP o TCP). 

Ci sono alcuni vantaggi a utilizzare OSPF:

* utilizza la variante sicura di IP, di fatto quindi questo protocollo permette l’autenticazione dei router. In questo modello quindi si riesce a raggiungere i router in modo autenticabile, verificandone l‘autenticità

* quando più percorsi hanno lo stesso costo, questo algoritmo permette di utilizzarli senza doverne scegliere uno in particolare, cosa che invece avveniva nel RIP

* ulteriormente all’associazione costo-collegamento, si può avere un associazione relativa al **TOS (type-of-service)**, ovvero associare un costo del servizio in base al tipo di servizio.

  Per ottenere questo modello si utilizzano dei campi flag di IP, che permettono di indicare il tipo di serivizio e quindi associarci il costo della linea, in questo modo posso avere dei costi diversi rispetto al tipo di comunicazione/flusso dei pacchetto, ovvero che posso avere un costo relativo al tipo di applicazione che sta mandando flussi di dati (ad esempio posso dare una bassa priorità ai flussi normali, mentre un alta priorità ai flussi di dati real-time).

*  ha un supporto integrato diretto per unicast e multicast.

  * *unicast* è una connessione che va da punto a punto
  * *multicast* è una connessione che va da un punto a molti (ad esempio un applicazione di video streaming). Solitamente c’è una lista di host da raggiungere e vengono connessi (solitamente) con una struttura ad albero. Questo tipo di collegamento viene ottenuto con l’instradamento MOSPF (multicast OSPF). Di fatto questa funziona cerca di costruire un percorso in cui cerca di evitare la duplicazione dei dati, ovvero fa del traffico unico finché non si deve dividere. 

* supporto alle gerarchie di instradamento:

  * OSPF, essendo più complicato rispetto al protocollo RIP, permette di avere una costruzione gerarchica. 

  * molto utile quando si lavora in una SA molto grande, in modo da strutturare le flooding e permettere di avere troppo traffico sulla stessa SA relativo all’instrademento.

    ![image-20211216100935124](image/image-20211216100935124.png)

    Per fare questo bisogna definire questi *area borders routers*, che permette l’unione di tutte le varie aree. Inoltre è necessario avere dei router *backbone*, ovvero i router necessari a riunire le aree.



#### Protocolli BGP (Border Gateway Protocol)

Questi protocolli sono responsabili di permettere l’instradamento tra le reti inter-AS, permettendo le reti intra-AS.

Di fatto il protocollo BGP è lo standard *de facto*, infatti è stato sviluppato e applicato prima di essere stato standardizzato effettivamente. In seguito si è dimostrato particolarmente efficace, quindi è rimasto.

Questo protocollo unisce delle reti, facendo in modo che ogni AS permetta l’implementazione del proprio protocollo di instradamento. Il problema di questo tipo di rete è che poi tutti queste sottoreti devono essere collegate in maniera univoca, in modo che siano tutte compatibili.

La granularità di questi sistemi però è molto minore, infatti non si indica tutta la strada all’interno dell’AS, ma semplicemente che si possono raggiungere definiti AS. Logicamente deve trovare il percorso buono (non ottimi o a costo minimo), infatti l’obiettivo di questo protocollo non è prestazionale, a differenza di RIP e OSPF, deve solo trovare la strada da percorrere. 

I collegamenti tra questi router di solito avvengono tramite accordi tra gli ISP, e questi accordi vanno rispettate in maniera prioritaria. Questo significa che quando due provider si accordano per far passare il traffico da una parte all’altra della loro rete. In questo modo RIP e OSPF sono molto diversi, infatti forniscono semplicemente il percorso più breve dal punto di vista matematico.





BGP viene messo a disposizione dei vari AS, e fornisce una serie di servizi:

* ottenere informazioni sulla raggiungibilità delle sottoreti da parte delle AS confinanti, ovvero indicano che AS sono raggiungibili attraverso questo AS.
* propagare le informazioni di raggiungibilità a tutti i router interni di un AS
* determinare percorsi “buoni” verso le sottoreti sulla base delle informazioni di raggiungibilità secondo le politiche dell’AS. Questo significano che hanno delle operazioni da scegliere per decidere in quale isola passare. In questo algoritmo quindi è necessario un compromesso tra politiche e i percorsi realmente fattibili.
  * Di fatto questo permette di avere dei percorsi scelti in modi diversi: si possono avere delle politiche in modo da scegliere diversi percorsi rispetto ad altri. Questo modello di solito si basa su dei contratti vantaggiosi tra i provider. Il percorso in questo caso non sarà mai quello a costo minore, ma quello che secondo le politiche ha costo minore. 



I nodi di questa rete sono chiamati **peer BGP** e sono i router esterni delle reti intra-AS.

Nonostante BGP serve per interconnettere due protocolli intra-AS, ha due varianti che di fatto lavorano internamente e esternamente alla rete:

* sessione eBGP:
  * connessioni di raggiungibilità e di routing effettivamente
* sessione iBGP
  * sono delle connessioni BGP internet
  * Sono dei collegamenti interni alla rete che fanno sapere ai router dove inoltrare i pacchetti nel caso di comunicazioni indirizzate a zone esterne alle intra-AS. 
  * Queste informazioni riflettono gli accordi presi tra gli ISP, in modo che i router conoscano solo le vie disponibili fornite dai gestori della rete per comunicare. In questo modo non utilizzeranno che quelle.
  * Di fatto quindi questo protocollo non permette di avere più di un possibile tragitto percorribile, ma solo la decisione finale. Questo annuncio di rotta è fatto in modo che ogni router informato di questa rotta possa dirigere i pacchetti che necessitano quella connessione in quella direzione, in base alle informazioni che possiedono i vari router

Come funziona il BGP:

* quando un router annuncia un prefisso per una sessione BGP, include anche un certo numero di attributi BGP. Questi di solito contengono i prefissi di sottorete, in più una serie di attributi e una rotta.

* Questi attributi possono essere:

  * *AS-PATH* indica i sistemi autonomi attraverso cui si è venuti a conoscenza dell’informazione, attraverso quali è passato l’annuncio del prefisso.

  * *NEXT-PATH*: quando si inoltra un pacchetto tra due AS, qual’è, da parte del mittente, il prossimo router a cui mandare i dati per raggiungere quel router di quel prefisso. In questo modo si può sapere qual’è il router gateway a cui indirizzare i pacchetti in uscita.

    Indica il router che può portare i pacchetti all’esterno del AS. Chiaramente questi percorsi possono essere importati oppure no.

* quando un router riceve un annuncio utilizza le **politiche di importazione** per decidere se accettare o filtrare la rotta. ogni informazione di rotta, il router decide se filtrare via la connessione oppure se importare la rotta.

  * Questa politica significa anche che il router, accettando la rotta della connessione, si impegna a risolvere il percorso che porta al router gateway. Di fatto quindi si può anche decidere di importare la rotta e non pubblicizzarla, oppure non importala proprio, oppure ancora importarla e pubblicizzarla (se si vuole che altri router utilizzino quest’ultimo per mandare pacchetti all’esterno dell’AS).
  * Di solito quando viene recapitata una regola di comunicazione, il router ha necessità di capire quale scegliere. Per fare questa operazione vengono applicate le regole di eliminazione:
    * Alle rotte viene assegnato come attributo un valore preferenza locale. Si selezionano solo i percorsi con valore di preferenza maggiore
    * Si seleziona la rotta con valore AS-PATH più breve (uso la sequenza più breve). Più questa informazione è alta più il router è lontano
    * Se no si sceglie il router *NEXT-HOP* più vicino, ovvero si minimizza il numero di salti. Questo modello si chiama instradamento **patata bollente**.
    * se rimane ancora più di una rotta, allora il router si basa sugli identificatori BGP.



I messaggi BGP vengono mandati attraverso il TCP (quindi di fatto è un protocollo modello applicazione). Il fatto di usare TCP permette di usare le socket sicure (TCP-TLS), limitando quindi la possibilità di avere intromissione fra i nodi.

Messaggi:

* *OPEN*: apre la connessione e autentifica il mittente
* *UPDATE*: annuncia un nuovo percorso (o cancella quello vecchio)
* *KEEP ALIVE*: serve per non far andare in timeout la connessione TCP in mancanza di altri pacchetti
* *NOTIFICATION*: riporta degli errori del precedente messaggio o utilizzato per chiudere la connessione.



Le politiche di instradamento BGP sono diverse, siccome la rete è molto diversificata in se stessa:

![image-20211220102055763](image/image-20211220102055763.png)

![image-20211220102120453](image/image-20211220102120453.png)





Le reti stub sono delle reti di accesso che possono (*X*) anche avere dei diversi collegamenti. Di fatto i router di bordo sono programmati per informare le sottoreti. Inoltre gli accordi di BGP possono essere stipulati con i vari ISP, in questo modo si possono usare dei router di bordo come router BGP per backup oppure per altre informazioni.



La differenze tra INTRA e INTER AS:

*  Politiche:
  * inter: il controllo amministrativo vuole avere il completo controllo su come il traffico viene instradato e su chi instrada attraverso le sue reti.
  * intra: un unico controllo ammministrativo, di fatto quindi le politiche amministrative hanno meno peso e rispetto allo scegliere delle rotte veloci all’interno della connessione.
* Scala:
  * l’instradamento gerarchico fa risparmiare sulle tabelle d’istradamento, riducendo il traffico dovuto all’aggiornamento di queste.
* Prestazioni:
  * Intra-AS: è prensato per avere massime prestazioni all’interno della rete, siccome è necessario ci sia il percorso più breve in assoluto all’interno della rete
  * inter-AS: è pensato per dare primo aiuto alle politiche di instradamento dei provider, e quindi lasciare le prestazioni al secondo posto.



# Capitolo 5: Livello di collegamento e Reti Locali

Questo livello 2 è caratterizzato da alcune similitudini con il livello 3, ma l’ambito è diverso. Questo livello è sotto il livello 3. Di fatto al livello 2 si può avere qualsiasi protocollo che permette di avere un collegamento con il resto della rete. Di fatto però a livello 3 è necessario avere un protocollo IP. 

Obiettivi:

* comprendere i principi per implementare i servizi di trasmissione dati:
  * condivisione del canale broadcast: solitamente tutti i canali di comunicazione comunicano su dei canali con accesso multiplo (vedi ad esempio il wifi). Il problema diventa capire come fa coesistere più connessioni nello stesso canale senza avere blocchi di condivisione.
  * Indirizzi a livello di collegamento 
  * Trasferimento affidabile dei dati e controllo di flusso, già visto nel livello di trasporto, ma di fatto in questo layer è più semplice.



## 5.1 Livello di collegamento: introduzione e servizi

Il livello di collegamento ha come ambito i collegamenti tra gli hosts, ovvero i link tra i diversi hosts. Questo livello si occupa dei singoli link o delle sottoreti.

Le unità scambiate da questi protocolli a livello di link sono chiamati **frame** (in italiano *trame*, ma mai utilizzato in questo senso).

Il datagramma di livello 3 può essere gestito da protocolli di livello 2 diversi (e anche delle tecnologie di livello 2 differenti), nonostante sia sempre lo stesso datagramma. 

Il livello 2 esegue di base una serie di servizi:

* **Framing**: il livello 2 ha necessità di creare una struttura dati per racchiudere le informazioni del trasporto di dati.
  * generalmente inserisce un valore di header e trailer nel datagramma, in modo da identificare l’inizio della connessione. Di solito questi valori sono delle sequenze di bit fisse e quando questa sequenza viene identificata, si lascia partire la comunicazione.
  * Problema della sincronizzazione della trasmissione: uno dei problemi più difficili da risolvere è sincronizzare il clock del mittente e del ricevente. Di fatto è molto difficile avere un clock perfettamente sincronizzato tra i molti sistemi. Di solito nei sistemi elettronici c’è un clock-master che distribuisce il clock tra tutti gli elementi.
    * Di solito il clock è una onda rettangolare e di base si fa una salita e una discesa, e di solito spesso ha dei drift di frequenza (ovvero ogni tanto è leggermente diversa la frequenza ricevuta rispetto a quella fissa).
    * Avendo il framing il sistema riesce a riaggiustare il proprio sistema di clock rispetto a quello del sistema. Vedendo una sequenza di bit di questo tipo si ricontrolla la correttezza del proprio clock. Altri sistemi trasmettono il proprio clock e quindi controllano in questo modo.
    * Di fatto questi problemi sono stati risolti in molti modi differenti, quindi non è più un grandissimo problema, ma nelle fasi iniziali della rete era effettivamente un problema da non sottovalutare.
* **Consegna affidabile**: c’è la possibilità di avere dei collegamenti affidabili, in modo da ricevere conferma della connessione avvenuta correttamente.
* **controllo di flusso**: evita che il nodo trasmetta troppo
* **rilevazione di errori**: di solito questa azione viene fatta in molti livelli. Di solito in questo livello sono causati da attenuazione del segnale e dal rumore elettromagnetico. Gli errori vengono individuati dal nodo ricevente.
* **correzione degli errori**: questo livello ha anche la possiblità di correggere degli errori, individuando il nodo che ha causato errori e correggendoli.
  * Nella connessione satellitare (caratterizzati da un alto ritardo della connessione) c’è anche la possibilità di avere il recupero dei dati, ovvero il recupero dei dati in caso di corruzione. Questo però viene a scapito della banda, siccome è necessario avere un sacco di bit di contorno per controllo.
* **half-duplex e full-duplex**: nella trasmissione ci possono essere modelli di half-duplex o full-duplex.
  * Nell’half-duplex è possibile avere un solo pacchetto per connessione, ovvero o un host riceve o trasmette, ma non è possibile fare questa operazione in entrambe le direzioni 
  * solitamente nelle reti via cavo si ottiene una connessione full-duplex anche perché si ha il doppio collegamento, oppure (nella fibra ad esempio) il canale permette di avere più segnali al suo interno senza che si ottengano interferenze.

Il livello di collegamento di solito è l’interfaccia tra il software e l’hardware. Questo si nota di solito quando esce un nuovo standard, che non si può utilizzare un nuovo protocollo senza del nuovo hardware. Di solito gran parte delle funzionalità del livello due è programmabile a livello firmware oppure direttamente hardware. 

Questa caratteristica differenzia questo livello dal resto della rete, siccome differenzia tutta la pila di fatto software dal livello hardware.

Alla meglio il livello 2 è una combinazione di software, firmware e hardware. Ad oggi però si sta andando sempre di più per l’estensione del software a sempre più parti della rete, con le considdette **SDR (Software Defined Radios)**. Attraverso queste tecniche si utilizzano degli elementi hardware che interagiscono direttamente via software. Il problema di questi apparati (ovvero attraverso una serie di interfacce software), si hanno dei costi di applicazioni decisamente più elevati. In qualsiasi caso questo significa che nel futuro è possibile che il sistema software di queste tecnologie saranno sempre più disponibili via software, rendendo disponibile un aggiornamento dei protocolli più costante (e soprattutto senza necessità di acquistare ulteriore hardware).



## 5.2 Protocolli di accesso multiplo

Questa sezione del protocollo riguarda due alternative per costruire il collegamento. Queste tecniche dipendono direttamente dal canale che si vuole utilizzare:

* collegamento **punto-punto (PPP)**: ho un mezzo dedicato, e quindi non ho problemi di condivisione di banda o altri mezzo
* collegamento **broadcast** (cavo o canale condiviso): 
  * questi modelli sono applicati a dei collegamenti via cavo o delle reti connesse tramite cavo unico (che quindi prende la forma di un unico bus). In questo modello è possibile solo una comunicazione per volta.
  * è utilizzato in alcuni metodi di livello 2 tradizionali.
  * Il wifi utilizza questa tecnica, e in questo modo si utilizza una banda diversa in base a quale host sta comunicando. Di solito si utilizza una sola banda in base a cosa si vuole fare
  * la prima connessione broadcast è stata fatta tramite collegamento satellitare per l’università delle Hawaii (nel periodo in cui lasciare i cavi sottomarini non era molto comune)



Il protocollo di accesso multiplo viene applicata a un canale broadcast condiviso. In questa modalità di connessione si hanno centinaia o migliaia di nodi in comunicazione diretta sul canale, e si può ottenere un evento di **collisione**, ovvero quando i nodi ricevono due o più frame contemporaneamente

I protocolli di accesso multiplo fissano le modalità con cui i nodi devono organizzarsi. Di fatto questi meccanismi sono studiati per avere un canale fuori banda (ovvero un secondo canale che tutti ascoltano per la coordinazione). Questo canale di controllo è necessario per indicare su alcuni mezzi le comunicazioni necessarie alle comunicazioni. Di solito questo metodo ha un applicazione più facile, ma di fatto spreca molte risorse. Per questi motivi si vuole avere un canale in-band, con connessione unica che permetta di avere un controllo implicito.

Prendiamo per esempio il canale di broadcast del wifi (una versione più antiquata):

* questo canale divide la capacità totale della rete tra gli host che partecipano alla connessione. 
  * Mettendo che il nodo disponga di un tasso trasmissivo di $Rbps$, e $M$ nodi devono inviare dati, allora il tasso trasmissivo è pari a $R/Mbps$.
* Non si vuole un *one-point-of-failure* e quindi si prevede un modello di protocollo decentralizzato in cui
  * non ci sono nodi master che amministrano la rete
  * non c’è la possibilità di sincronizzare i clock degli host che partecipano alla rete.
* il protocollo deve rimanere il più facile possibile



I protocolli ad accesso multiplo sono suddivisibili in 3 categorie:

* protocolli a **suddivisione del canale** (channel partitioning)
  * La suddivisione delle risorse del canale solitamente avviene a gruppi, ovvero in “parti più piccole”, suddivise in slot di tempo, frequenza e codice
* protocolli ad **accesso casuale** (random access)
  * i canali non vengono divisi e, siccome ogni host può comunicare quando vuole, si può sempre verificare una collisione
  * i nodi coinvolti ritrasmettono ripetutamente i pacchetti
* protocolli a **rotazione** (“taking-turn”)
  * ciascun nodo ha il suo turno di trasmissione. A differenza della suddivisione del canale, in questa tecnica c’è un massimo di banda che può utilizzare ogni connessione



La maggiore popolarità tra questi è il modello a rotazione. 

Solitamente la suddivisione delle connessioni del canale sono **TDMA (Time Division Multiplexing Access)**, ovvero si hanno dei turni che si ripetono. A questi turni sono associati degli utenti e i turni si ripetono continuativamente in intervalli di tempo predefiniti. C’è il vantaggio che ogni stazione ha una capacità sempre garantita, ma di fatto ci sono degli effetti collaterali soprattutto se la fonte non è continua. I buchi creati nel diagramma rimangono nella connessione. Questa soluzione funziona bene quindi solo se tutte le stazioni trasmettono con lo stesso bitrate. Di fatto queste connessioni hanno le caratteristiche giuste per la telefonia, infatti riserva la giusta quantità di banda permettendo un collegamento affidabile e costante su tutte le linee. Funziona molto peggio quando si ha dei flussi di dati molto differenziati o di applicazioni digitali che creano dati a bitrate non costanti.

Solitamente funziona bene il **FDMA (Frequency Division Multiplexing Access)** che permette di accedere alla connessione con una frequenza differenziata per ogni host. In questo modo si hanno delle comunicazioni separate in frequenza, a cui ciascuna stazione è assegnata una banda di frequenza prefissata. In questo modo si hanno delle divisioni in frequenza che permettono la divisione della banda totale tra tutti gli hosts. In questo modello però tutte le stazioni dispongono di un sottomultiplo della capacità totale del collegamento. 

Questo collegamento permette di avere dei canali con un bit-rate garantito, inoltre siccome i canali sono divisi è sempre garantito che la determinata stazione abbia accesso a quella determinata banda. In questo modello permette anche un controllo di congestione, infatti o sono disponibili le risorse per accettarvi sulla rete oppure si viene semplicemente tagliati fuori. In questo modo o tutte le risorse sono utilizzate e non si accettano più altri host, oppure si può continuare a accettare host in connessione fino alla saturazione della banda.

Il protocollo di **accesso casuale** non significa che viene lasciato tutto al caso, ma semplicemente che una stazione può accedere senza coordinazione. Di fatto nessun nodo sa degli altri e dello stato della rete, ma quando desidera mandare un pacchetto lo manda in rete utilizzando tutta la capacità del canale. Se:

* il canale è libero, allora la comunicazione ha esito positivo
* il canale non è libero, allora si ottiene una collisione.
  * di solito questo evento riduce i dati a indecifrabili, e quindi non si possono utilizzare i due pacchetti.

Il protocollo quindi ha necessita di capire come rilevare una collisione e indicare il meccanismo di ritrasmissione nel caso si sia verificata una collisione. 

Ci sono 3 tipologie di questi protocolli:

* *slotted ALOHA*
* *ALOHA*
* *CSMA*, *CSMA/CD*, *CSMA/CA*

Dove i primi due sono variazioni dello stesso protocollo, ma sono entrambi primitivi. Sono stati sviluppati dall’università delle Hawaii per consentire la connessione in broadcast su connessione satellitare, ma ad oggi sono per lo più inutilizzati.

L’ultima tipologia è quella tuttora utilizzata ad oggi e ne esistono variazioni.



### Protocollo Slotted ALOHA

Questo protocollo ha delle caratteristiche ben definite:

* tutti i pacchetti hanno le stesse dimensioni
* il tempo è suddiviso in slot, che equivale al tempo di trasmissione di un pacchetto
* i nodi trasmettono solo all’inizio di ogni slot
* i nodi sono sincronizzati
* se avviene la collisione di due pacchetti in uno slot, i nodi coinvolti rilevano l’evento prima del termine dello slot

ll funzionamento è molto facile:

* quando arriva un nuovo pacchetto da inviare sulla rete a un nodo, si attende fino all’inizio dello slot successivo

* se **non** si verifica una collisione il nodo può trasmettere un nuovo pacchetto nello slot successivo

* se **si** verifica una collisione il nodo lo rileva e ritrasmette con una certa probabilità $p$ il suo pacchetto durante gli slot successivi

  * questa probabilità è necessaria perché, se si ha una collisione, allora è necessario aspettare del tempo perché non continui ad avvenire la collisione.
  * Di fatto a un certo punto della connessione si otterrà un caso in cui uno dei due nodi si ritira dalla connessione, e quindi l’altro esegue l’invio. L’altro nodo seguirà poi all’invio della connessione.

  ![image-20211220121636247](image/image-20211220121636247.png)

Questo modello ha:

* **lati positivi**:
  * l’approccio è molto semplice
  * chi riesce a trasmettere trasmette alla massima capacità del canale. Infatti in questo modo utilizzo tutte le frequenze per un certo tempo.
  * è fortemente decentralizzato, infatti conosce l’esistenza degli altri nodi solo in caso di collisione. 

* **lati negativi**:
  * in caso di collisione posso avere:
    * una serie di intervalli vuoti
    * una serie di slot persi
  * poco efficiente come banda utilizzata rispetto a quella disponibile se iniziano ad avvenire collisioni.
    * Il parametro $p$ deve essere scelto molto scientemente, siccome deve essere applicato in base alla rete, al numero di host e altre caratteristiche della rete. Questo valore quindi è molto difficile da trovare

Questo protocollo quindi è molto semplice, ma permette una connessione sempre con minor banda disponibile al crescere del numero degli host sulla rete, infatti in questo caso si avranno così tante collisioni che non si riuscirà mai a trasmettere nulla.

L’efficienza di questo protocollo è calcolata come la frazione di slot vincenti in presenza di un elevato numero di nodi attivi, che hanno sempre un elevato numero di pacchetti da spedire. La massima efficienza di questo protocollo si trova con un semplice modello matematico, ottenendo che la migliore probabilità di successo è il $37\%$. Infatti solo uno slot su tre compie del lavoro utile per la trasmissione.

Questo valore non è l’efficienza media, ma il valore di efficienza *massima*. Questo significa che questo standard non è molto efficiente, anzi non lo è per nulla.



### Protocollo ALOHA puro

Questo protocollo è uguale a quello visto precedentemente, ma senza l’implementazione di slot temporali. 

Questo modello è ancora peggiore, siccome si può avere un interferenza in ogni momento, non solo all’inizio dello slot precedente.

Per calcolare l’efficienza di questo modello bisogna calcolare che si rifiuteranno pacchetti che vengono mandati durante il sending di una altro pacchetto (durante tutta l’operazione). Questo modello di fatto dimezza ancora di più l’efficienza, rendendola al $18\%$. 

Questo protocollo solitamente piace siccome permette di avere un invio dei pacchetti appena questi sono disponibili, ma non si può avere una connessione stabile dovuto alle collisioni continue sulla rete, che riducono notevolmente la capacità della rete.

Questo concetto viene spiegato meglio dal grafico sotto:

![image-20211220150106376](image/image-20211220150106376.png)



### Protocollo CSMA (Carrier Sensing Multiple Access)

Questo protocollo permette di mantenere la sensibilità del protocollo ALOHA, ma con una semplice funzionalità. Non si esegue la divisione in tempo del pacchetti.

Si riesce però a capire quando il canale è libero attraverso la tecnica chiamata **rilevazione di portante (carrier-sensing)**. Con questa tecnica si può sapere se il canale è utilizzato (in questo caso si aspetta un lasso di tempo e si continua poi con la connessione). Nel caso in cui il canale sia libero invece si procede direttamente all’invio del pacchetto. 

Le collisioni possono ancora verificarsi, infatti avendo un certo ritardo di propagazione si può avere un caso in cui i nodi non rilevino la reciproca trasmissione. 

![image-20211220150123090](image/image-20211220150123090.png)

Questo schema fa capire come entrambe le trasmissioni vadano distrutte se il tempo di propagazione del segnale non è abbastanza veloce da avvisare tutti gli altri nodi della rete.

Nelle trasmissioni CSMA quindi il parametro che fa variare la frequenza di collegamento dipende dalla distanza delle stazioni, infatti più queste sono lontane più è facile notare questo effetto. 



### Protocollo CSMA/CD (CSMA Collision Detection)

Siccome il CSMA non sembrava abbastanza, siccome aveva il problema che l’identificazione della collisione dipendeva completamente dal tempo di propagazione del segnale. 

Questo protocollo funziona come il CSMA, ma ha una variante riferita al rilevamento della portanza differito. Praticamente si ha un rilevamento di collisione continuativo nel tempo di trasmissione dei dati, in questo modo:

* rileva la collisione dei pacchetti in poco tempo
* riesce ad annullare una connessione appena si accorge che un’altra è in corso.

In questo modo i tempi di reazione sono più veloci del protocollo CSMA standard.

Il diagramma quindi varia in questo modo:

![image-20211220151837019](image/image-20211220151837019.png)

L’idea di fatto è che, siccome le collisioni non si possono non avere, in questo modo possiamo mantenere l’utilizzo del canale un tempo maggiore.

Questo modello di rilevazione della collisione è facilmente eseguibile su un canale via cavo (LAN Cablate), infatti è inserito nel protocollo Ethernet.

Questa tecnica non funziona particolarmente bene nel wireless siccome ogni stazione riceve tutti i pacchetti. Per questo e altri motivi poi nelle comunicazioni senza fili si utilizza il CSMA/CA.



### Protocolli MAC a rotazione

I protocolli a rotazione erano abbastanza diffusi, poi l’accesso casuale ha vinto come approccio, siccome è più semplice.

Ci sono due meccanismi di **MAC (Medium Access Control)**:

* a **suddivisione del canale**: questa divisione è molto rigida, condividendo il canale equamente e efficientemente con carichi elevati. Diventa inefficiente con carichi poco elevati.
* ad **accesso casuale**: questa suddivisione permette alla rete di essere molto efficiente con carichi non elevati, infatti un singolo nodo riesce ad utilizzare interamente il canale. Allo stesso modo con carichi elevati avviene un eccesso di collisioni, infatti se si incrementa il numero di stazioni che si può trasmettere il livello di collisioni aumenta

I protocolli a rotazione permettono di avere il meglio di entrambi i protocolli:

* essere più flessibili sulla suddivisione del canale
* essere meno saturati dalla quantità di stazioni precedenti

La soluzione sarebbe il **polling**:

* funziona nel metodo *master-slave*: il nodo *master* interroga a turno tutti gli altri nodi, *slave*. 
  * solitamente funziona con il metodo *round-robin*, ovvero a rotazione ogni host ha la possibilità di mandare in rete i frame che ha necessità di inviare.
* questo tipo di meccanismo è per forza di cose centralizzato
* permette l’eliminazione completa delle collisioni
* permette di eliminare slot vuoti
* crea del ritardo, chiamato *ritardo di polling*
* *one-point-of-failure*, infatti se il nodo principale si guasta, allora l’intero canale rimane inattivo.

Siccome questo tipo di protocollo è molto centralizzato, e sulle reti non piace mai questo concetto, si è iniziato a pensare a un nuovo protocollo, chiamato **protocollo token-passing**, ovvero protocollo a passaggio di token.

Questo protocollo esegue le stesse operazioni e gli stessi controlli ma in maniera distribuita:

* il messaggio di controllo (*token*) è anche il meccanismi di accesso al canale
  * il token viene passato tra tutti le stazioni disponibili
  * se la stazione ha dei dati pronti ad essere inviati allora vengono inviati, se non è presente allora passa il token alla stazione successiva.
* Questo meccanismo si basa sul fatto che chi ha il token può trasmettere, ma allo stesso modo ha una serie di vincoli:
  * non può tenere il token oltre un certo tempo massimo
  * si dovrebbe liberare del token appena possibile (solitamente appena si finisce di mandare i dati)

In questo modo si hanno effettivamente entrambi i vantaggi, infatti:

* si ha un ordine preciso e periodico per accedere al canale.
* Ogni stazione sa che avrà una parte delle risorse del canale
* ogni nodo che non ha necessità di comunicare semplicemente passa il token



### Protocolli Riepilogo

Con il canale condiviso si ha una serie di caratteristiche:

* **suddivisione del canale** per tempo, frequenza e codice
  * TDM, FDM
* **suddivisione casuale** (oppure dinamica)
  * ALOHA, S-ALOHA, CSMA, CSMA/CD
  * Rilevamento della portante: facile in alcune tecnologie (cablate) e difficile in altre (wireless)
  * CSMA/CD usato in Ethernet
  * CSMA/CA usato in 802.11
* **a rotazione**
  * Polling con un nodo principale o a passaggio di testimone
  * Bluetooth, FDDI, IBM Token Ring



Ad oggi molto standard utilizzano un protocollo del tipo CSMA/CD. Le reti cellulari invece utilizzano dei metodi di prenotazione del canale più particolari.



## 5.3 Indirizzi a livello di collegamento

L’indirizzamento di livello 2 è uno degli indirizzi disponibili di un host. Solitamente ogni host ha 3 indirizzi: 

* indirizzo di porta
* indirizz dell’host
* indirizzo **MAC** (chiamato anche indirizzo LAN oppure indirizzo Ethernet)

L’indirizzo di rete ha la necessità di essere unico su ogni sottorete, in modo da permettere di identificare l’host univocamente. Questo indirizzo è di $32bit$ e se n’è parlato [qui](### Indirizzamento IPv4).

L’indirizzo **MAC** è un valore a $48bit$ e è affidato a ogni interfaccia al momento della “costruzione”. Questo indirizzo non cambia mai, anche se si cambia rete e serve a caratterizzare le sorgenti di livello 2.

Questi indirizzi vengono scritti in valori esadecimali. L’indirizzo di broadcast è `FF-FF-FF-FF-FF-FF`. Questo indirizzo è sempre usato siccome è possibile fare comunicazioni in broadcast su tutte le reti.



### Indirizzi MAC/LAN e Protocollo ARP

Questi indirizzi sono gestiti dal IEEE, che ne sovrintende la gestione.

Per garantire la unicità degli indirizzi, solitamente una società compra un blocco di spazio di indirizzi. I primi bit sono fissati per un certo produttore. Infine gli altri valori di quell’insieme sono riservati per quell’apparato.

Questo viene fatto in modo da risolvere un problema dell’applicazione in maniera pedissequa della pila protocollare: si deve aver un modo per collegare l’indirizzo di rete all’indirizzo di link, siccome il livello che effettivamente consegna il livello 3, che lavora con gli indirizzi di livello 2.

Questo significa che il livello 3 della pila protocollare crea un datagramma con destinatario un indirizzo IPv4/IPv6 e poi passa al livello 2. Il livello due, che effettivamente è quello che si occupa della consegna del frame lavora solo con indirizzi di livello 2. Serve quindi un modo per far comunicare tra di loro questi due protocolli in modo da assicurare una corretta consegna. Di fatto quindi bisogna capire come far corrispondere all’indirizzo fisico di un interfaccia un indirizzo di livello 3.

Per collegare questi due indirizzi che appartengono a due layer diversi viene in aiuto il **protocollo ARP (Address Resolution Protocol)**. Di base questo protocollo crea una *tabella ARP* che contiene la corrispondenza IP e MAC in questo modo: `<indirizzo IP; indirizzo MAC; TTL>` (con un `TTL`, di solito di una 20ina di minuti, in modo da fare il refresh della tabella e quindi avere sempre le corrispondenze corrette).

Questo protocollo è forse il più importante di tutto il livello 2, siccome permette l’effettiva comunicazione sulla rete.

Come funziona la **creazione della tabella ARP**:

* `A` vuole inviare un datagramma a `B` e l’indirizzo MAC di  `B` non è nella tabella ARP di `A`

* `A` trasmette un pacchetto *broadcast* il messaggio di richiesta ARP, contentente l’indirizzo IP di `B`.

  Questo messaggio si ottiene utilizzando come indirizzo MAC del destinatario il valore di broadcast (`FF-FF-FF-FF-FF-FF`). A questo punto tutte le macchine della LAN ricevono una richiesta ARP.

* `B` riceve un pacchetto ARP, risponde ad `A` comunicandogli il proprio indirizzo MAC. Il frame viene mandato direttamente all’indirizzo MAC di `A`.

  Con questo pacchetto ci si può nuovamente costruire una nuova entry sulla tabella ARP.

Da notare che la richiesta ARP è inviata in modalità broadcast, mentre invece il messaggio di risposta ARP è inviata in un pacchetto standard.

ARP è *plug-and-play*:

* la tabella si costruisce da sola, facendo automaticamente le richieste ai nodi
* non deve essere configurata in nessun modo



### Invio di pacchetti verso un nodo esterno della sottorete

![image-20211220161942633](image/image-20211220161942633.png)

* `A` sa che deve inoltrare il pacchetto al router, siccome `B` non è sulla stessa sottorete. Questa scelta viene fatta confrontando gli indirizzi di livello 3.

* `A` invia il pacchetto al router. Per fare questa operazione devo conoscere l’indirizzo locale della porta del router interna (interfaccia sulla sottorete interna). Questa comunicazione inizia con una richiesta di comunicazione ARP, infatti la macchina richieste l’indirizzo di questa interfaccia in broadcast sulla rete se non la possiede già. 

  Una volta che tutte queste azioni vengono completate, `A` può mandare il suo frame al router

  Anche se il router è un indirizzo IP privato le azioni effettuate non cambiano, ma semplicemente la tabella ARP viene riempita con una relazione indirizzo IP-MAC con un indirizzo privato.

* Quando il router riceve il pacchetto, allora viene deincapsulato fino a livello 3 per leggere l’intestazione del protocollo IP. Facendo questa operazione il router scopre che la macchina è collegata alla rete di una sua interfaccia. Di nuovo quindi dovrà popolare su questa interfaccia la propria tabella ARP per cercare l’interfaccia B.

  Questa operazione avviene sempre in internet, ma di solito i valori degli indirizzi di MAC rimangono in memoria per un certo tempo.

Di fatto quindi:

* il protocollo IP si occupa di trovare le direzioni su dove effettuare il collegamento
* tramite l’indirizzo di MAC si esegue l’effettiva comunicazione su una linea del collegamento



## 5.4 Ethernet

Questo metodo di comunicazione detiene la posizione dominante nel mercato delle LAN cablate. Questa tecnologia prende il nome da *ether*, ovvero il cavo che all’inizio era il fondamento di questo modello.

Questa rete è stata la prima LAN ad alta velocità con vasta diffusione. E’ la più semplice, meno costosa dei token ring, FDDI e ATM. 

Ethernet è molto interessante perché è stato uno dei pochi standard che è riuscito a mantenere retrocompatibilità con questi apparati, ma comunque a essere sempre al passo con le necessità dei tempi (ad esempio la rete Ethernet adesso ha capacità anche superiori ai $10Gbits$, logicamente salendo di costo).



Questa tecnica di comunicazione era iniziata con una **topologia a bus**, diffusa fino a metà degli anni 90. Oggi quasi tutte le odierne reti Ethernet sono progettate con una **topologia a stella**:

![image-20211220163836941](image/image-20211220163836941.png)

Solitamente al centro di ogni stella è collegato un hub o commutatore (*switch di livello 2*) che riunisce 4 apparati solitamente.

Ciascun nodo esegue un protocollo Ethernet separato e non entra in collisione con gli altri. 



### Struttura dei pacchetti Ethernet

![image-20211220164156244](image/image-20211220164156244.png)

* il *preabolo*

  Sono 8 byte: sette con questa sequenza di bit `10101010` e l’ultimo con `10101011`. Questi byte non portano nessuna informazione, ma servono solo per sincronizzare il clock, “attivare” gli adattatori dei riceventi e sincronizzare i loro orologi con quello del trasmittente.

* gli indirizzi sono a *6byte* e sono gli indirizzi MAC di prima. Si utilizzano gli indirizzi di destinazione prima perché è più facile eseguire lo switching.

  Il campo di destinazione viene messo davanti agli altri solo in questo protocollo siccome negli anni ‘80 questo modello permetteva di avere le indicazioni di dove mandare il pacchetto come prima informazione letta dal bus e quindi iniziare già a trasportare il frame.

  Questo pacchetto viene allora trasferito a livello di rete.

  I pacchetti con altri indirizzi MAC vengono ignorati.

* *campo tipo* del pacchetto indica il protocollo di rete (in gergo questa funzione permette al protocollo ethernet di supportare vari protocolli di rete, oppure di *multiplexare* i protocolli).

* il *payload* del pacchetto

* *controllo CRC*: questo campo permette all’adattatore ricevente di rilevare la presenza di un errore nei bit del pacchetto.

Il protocollo ethernet è senza connessione e non affidabile. Questo viene fatto siccome si pagherebbe la complessità e meccanismi di trasmissione, oltre che esiste già questa possibilità dal punto di vista software con i protocolli di trasmissione. Inoltre avvicinandosi sempre più all’hardware queste operazioni devono essere fatte sempre più velocemente (meno funzionalità, più semplice e veloce possibile).



Fasi operative della connessione con CSMA/CD:

![image-20211220165332115](image/image-20211220165332115.png) 

Il segnale di disturbo al momento della connessione si manda un segnale diverso (solitamente anche a un voltaggio leggermente superiore) in modo da dire a tutte le trasmissioni di fermarsi. Questo segnale è di 48bit solitamente.

![image-20211220165658307](image/image-20211220165658307.png)

Se io continuo a collidere, il massimo che posso aspettare è $50msec$. Questo valore è la conseguenza di questa slide sopra.



### Efficienza di Ethernet

Questo modello è un CSMA/CD leggermente modificato. L’efficienza di questo protocollo quindi viene trovato con:
$$
efficienza = \frac{1}{1+5t_{prop}/t_{trasm}}
$$
Da questa formula si capisce che:

* se $t_{prop} \to 0$ allora $efficienza \to 1$.
* al crescere di $t_{trans}$, l’$efficienza to 1$

Questo modello è più efficiente di ALOHA: decentralizzato, semplice e poco costoso.



Ethernet è un protocollo molto utilizzato, ma cambia il mezzo fisico e in base a quello la nomenclatura:

![image-20211220170154783](image/image-20211220170154783.png)

### Codifica Manchester

Questa modalità viene utilizzata nella comunicazione in protocollo Ethernet `10BaseT`. Questa codifica si distanzia leggermente dal modello binario utilizzato solitamente nella comunicazione:

![image-20211220170445039](image/image-20211220170445039.png)

Questo modello utilizza delle “mezze forme d’onda” nel quale:

* $1$: il segnale parte dall’alto e scende verso il basso
* $0$: il segnale parte dal basso e sale verso l’alto.

Questo modello permette di fatto di avere una transizione ogni ricezione di ciascun bit e permette di sincronizzare gli orologi degli adattatori trasmittenti e riceventi, permettendo quindi di non avere la sincronizzazione del clock tra i nodi

Questa operazione è una codifica che avviene a livello fisico (hardware). 

Logicamente questa soluzione non funziona con il metodo di comunicazione in fibra, siccome utilizza degli impulsi di luce.



## 5.5 Switch a livello di collegamento

Il protocollo Ethernet è uno standard nato *bus*, ma poi trasformato in forma a *stella*. 

Per fare questa transizione è necessario introdurre un nuovo dispositivo, l’**hub**:

* questo dispositivo è piuttosto stupido, infatti all’arrivo di un bit lo riproduce incrementando l’energia attraverso tutte le interfacce. (lavora a livello fisico)
* non implementa nessun logica né nessun tipo di controllo
* Di fatto trasforma i 4 doppini in un unico mezzo broadcast

Lo **switch** è un dispositivo molto più interessante:

* questo dispositivo lavora a livello di link, quindi riesce a svolgere un ruolo attivo:

  * filtra e inoltra i pacchetti Ethernet

  * esamina gli indirizzi di destinazione e lo invia all’interfaccia corrispondente alla sua destinazione

  * quando il pacchetto è stato inoltrato nel segmento utilizza CSMA/CD per accedere al segmento.

    Su ogni canale è in grado di ricevere e trasmettere come se fosse una stazione normale

* è un dispositivo *trasparente*: gli host sono inconsapevoli della presenza di switch, infatti non serve saperne l’esistenza ne comunicare direttamente con lui

* è un oggetto *plug-and-play* e *autoapprendimento*: sono due tecniche che permettono agli switch di funzionare senza essere direttamente configurati.

  * Quando un host crea un frame, questo stesso oggetto fornisce una serie di informazioni tra cui l’indirizzo MAC del pacchetto.

  * quando il router riceve un pacchetto lo switch impara l’indirizzo del mittente

  * lo switch registra la coppia mittente/indirizzo nella tabella di commutazione, in questo modo si ottiene una vera tabella di commutazione.

    ![image-20211220172220798](image/image-20211220172220798.png)

    ![image-20211220172233868](image/image-20211220172233868.png)

  * quando la destinazione è nota invece si esegue il cosiddetto *selective send*, ovvero recupera la entry della tabella e ridireziona il pacchetto attraverso il determinato indirizzo MAC

* gli host sono collegati direttamente allo switch. Questo oggetto permette la bufferizzazione dei pacchetti e l’inoltro in base a una tabella di switching. 

Gli switch possono essere interconnessi, ma non delimitano il broadcast come i router. Con il fatto che gli switch 

![image-20211220172609579](image/image-20211220172609579.png)

![image-20211220172619269](image/image-20211220172619269.png)

## Capitolo 5: Riassunto

Solitamente in una rete aziendale ci sono delle reti che possiedono più switch. Di fatto però è difficile avere una rete completamente a livello 2, in modo che non tutte le comunicazioni avvengano ovunque e che le reti di fatto vengano divise.

![image-20211220173215126](image/image-20211220173215126.png)

In realtà spesso le reti sono modificabili a livello software, attraverso le cosiddette *SDN (software define network)*, in cui si riesce a ottenere delle reti virtuali complete via software.

Tutta la gestione della rete invece è visualizzabile attraverso una serie di indirizzi IP che permettono di accedere a questi oggetti per la gestione e per il controllo. Di solito inoltre ogni router ha un interfaccia di management che permette di modificare il comportamento del router dal network manager.

