# appunti reti Secondo laboratorio: Utilizzo di KATHARA

Tutte le informazioni riguardo al software si possono trovare nella documentazione ufficiale. Uitilizzando la VM inizio a provare un attimo.

## Accensione e spegnimento delle macchine

Questo comando fa partire una macchina, chiamata *pc1*.
```bash
kathara vstart -n pc1
```

Questo comando spegne una pacchina specifica (*pc1*):
```bash
kathara vclean -n pc1
```

## Come gestire un laboratorio con Kathara

Un laboratorio sono una serie di file di configurazione, che simulano un insieme di macchine. Un laboratorio viene gestito con i comandi `l-command`.

I file di configurazione necessari sono:
- `lab.conf`: descrive come è composto il laboratorio
- *host descriptor*: kathara crea un file per ogni host che viene generato perché incluso nel lab descriptor.

Alcuni comandi utili per la gestione dell'ambiente di laboratorio:
```
kathara lstart
kathara vconfig
katahara lclean
kathara lrestart
```

(usando ssh per accedere alla macchina virtuale ho settato DISPLAY=:10.0, in modo che si possano vedere le finestre nella macchina virtuale utilizzata solo in visualizzazione, perché un pochino rotta).

Con queste impostazioni le due macchine sono collegate tra di loro e si può notare questa cosa attraverso il comando `ping`.

