# Capitolo 4: Livello di Rete

Questo livello è molto importante, siccome permette di instradare i pacchetti sulla rete.

Capire i principi che stanno dietro i servizi del livello di rete:

* modelli di servizio del livello di rete
* funzioni di inoltro e di instrademento
* come funziona un router
* instradamento (scelta del percorso)
* scalabilità
* argomenti avanzati: IPv6, mobilità

inoltre si tratterà l’implementazione nell’Internet.

## 4.1 Introduzione

Come tutti i livelli ci sono alcune funzioni chiave: in questo il compito principale è quello di muovere i pacchetti dall’host mittente a quello ricevente. Per ottenere questo effetto ci sono principalmente due funzioni:

* il **forwarding (inoltro)**: permette di trasferire i pacchetti dall’input di una connessione all’output del link appropriato. Di fatto quindi si tratta di spostare un pacchetto da una porta a un’altra all’interno del nodo.

  Generalmente i pacchetti in questo stato possono essere bloccati (se ad esempio una regola impone di bloccare il pacchetto, oppure la destinazione non esiste o ancora se è inoltrato su un host non raggiungibile) o può essere mandato a più link moltiplicandolo.

* **routing (instradamento)**: determina il percorso seguito dai pacchetti dall’origine alla destinazione. 

  In questa sezione si ottengono anche una serie di algoritmi per definire queste traiettorie, chiamati *routing algoritms*. 

Di solito questi termini sono spesso utilizzati in modo intercambiabile, ma in realtà hanno dei significati leggermente diversi. Il termine *forwarding* si riferisce a un azione che avviene localmente nel router e è assegnato all’azione di trasferire i pacchetti dal link in input al link in output appropriato. Solitamente (soprattutto per le soluzioni più high end) questi metodi sono implementati via hardware e svolgono queste operazioni in tempi nella scala dei nanosecondi. 

Il termine *routing* si riferisce invece a un azione che avviene nella totalità della rete e sono tutte le azioni che permettono di definire un passaggio continuo attraverso i nodi della rete fatti in modo da permettere il passaggio dei datagrammi dal mittente al ricevente.

Si può pensare a questi due termini come fossero delle strade reali. Il percorso completo è definito nell’instradamento con il GPS che indica tutto il percorso da casa alla destinazione. La decisione invece che indica di uscire sulla destra durante il viaggio si ottiene nella sezione di inoltre (ovvero una funzione svolta dai singoli router).

Il principio chiave de funzionamento di questo processo è basato sulle **tabelle di inoltro** presenti su ogni nodo della rete (ovvero su ogni router). Queste tabelle associano a un valore di un header di pacchetto in arrivo a una determinata porta a cui inoltrare il pacchetto. Il confronto di questi header e la comprensione di dove questi pacchetti debbano andare è la chiave di tutta l’operazione di inoltro svolta sul router. 

![image-20211209132639508](image/image-20211209132639508.png)

Il contenuto di questa tabella è calcolato e mantenuto attraverso gli algoritmi di routing. Un altro caso (non consigliato, molto complicato e molto raramente applicato) è quello di creare una tabella di inoltro a mano, in modo da impostare dove i pacchetti debbano andare in modo manuale.

Di fatto il modello attraverso cui ogni router è direttamente collegato ai router vicini è quello più utilizzato al momento. Non per questo significa che sia l’unico. Un modello che permette di avere un servizio centralizzato che fornisce i dati delle tabelle di inoltro per tutta la rete è possibile e viene di solito utilizzato per le **reti software-defined (SDN)**, che per definizione hanno questa caratteristica.

Di fatto prima che i datagrammi fluiscano sulla rete, è necessario che i due host (mittente e ricevente) e i router stabiliscano una connessione virtuale. In questo processo vengono coinvolti i router.



Siccome ci sono molte similitudini tra il livello di rete e quello di trasporto ricordare che:

* il livello di trasporto crea una connessione tra due host
* il livello di rete crea una connessione tra due processi



## 4.2 Reti a circuito virtuale e datagramma

Questo livello della pila protocollare può offrire una serie di modelli di servizio che chiaramente hanno dei funzionamenti e delle garanzie fornite ai pacchetti transitanti diversi.

I servizi che una rete può decidere di fornire di base possono essere:

* **garanzie nella ricezione dei dati**: la rete offre la conferma che il pacchetto arrivi a destinazione (tralasciando il tempo necessario per ottenere questo trasferimento, dice solo che prima o poi arriverà a destinazione)
* **garanzia di ricezione dei dati in un delay preciso**: il servizio garantisce non solo che il pacchetto verrà consegnato, ma anche che verrà consegnato esattamente in un certo valore di tempo ben definito.
* **consegna dei pacchetti in ordine**: il servizio garantisce che i pacchetti arrivino all’host a cui sono mandati esattamente nell’ordine con cui sono emessi dal mittente.
* **garanzia di banda minima**: questa garanzia imita il modello di consegna dei pacchetti telefonici, che “utilizzano” un certo valore di banda durante tutta la connessione su tutti i collegamenti che necessitano.
* **sicurezza**: il servizio permette di avere una connessione sicura e criptata.

Le reti possono, di fatto, essere una qualsiasi combinazione di queste garanzie oppure seguire il modello (poi diventato quello della rete internet) di **best-effort**, siccome ha dimostrato di essere accettabilmente buono da garantire una rete consistente nel servizio offerto agli hosts.



Di fatto comunque le reti che ad oggi si possono trovare sono di due tipi, entrambi con un funzionamento a commutazione di pacchetti:

* Reti a circuito virtuale
* Reti a datagrammi

Questi due modelli di rete hanno delle analogie sul funzionamento del livello di trasporto, ma:

* offrono un servizio da host a host
* si può scegliere uno solo di questi servizi, non entrambi nello stesso momento (o servizio con connessione o servizio senza connessione)
* le implementazioni di questi protocolli sono fondamentalmente diverse

Uno schema riassuntivo di questi modelli di rete:

![image-20211211003644516](image/image-20211211003644516.png)

### Reti a circuito virtuale

Le reti di questo tipo hanno un comportamento molto simile a quelle dei circuiti telefonici per prestazioni fornite e coinvolgimento della rete durante il percorso tra sorgente e destinazione.

In questo senso si potrà notare una richiesta alla rete per una connessione, che può non andare a buon fine. Se invece si ottiene una connessione allora si hanno anche le garanzie di banda definita e “bloccata” dal determinato collegamento.

Di fatto questo flusso di pacchetti è analogo a quello creato nella rete telefonica: tutti i pacchetti che viaggiano al suo interno avranno lo stesso percorso attraversando gli stessi nodi. Se uno di questi durante la connessione venisse a mancare tutto il collegamento verrebbe distrutto.

Questa rete però non è completamente instabile: dovendo preservare un certo valore di banda per ogni flusso si ottiene un controllo di congestione automatico, con la possibilità di evitare la comunicazione a un numero di host superiore a quello possibile sulla determinata tratta.

Il lato negativo di questo modello della rete sono i costi. Si hanno dei costi di creazione dell’hardware necessario a queste reti molto alti. Inoltre non si otterranno le stesse prestazioni, infatti ogni nodo di accesso della rete dovrà svolgere una serie di controlli ulteriori rispetto al semplice processo di inoltro.

Il funzionamento a basso livello di questa rete è uguale al un normale router, quindi legge l’intestazione del pacchetto e in base a quello decide dove inoltrarlo. L’unica differenza è che in questo modello sull’intestazione del pacchetto non c’è l’indicazione del ricevente, ma di fatto si indica il flusso (ovvero il percorso logico dei nodi della rete) sul quale deve essere consegnato. Questo valore viene assegnato al pacchetto prima di iniziare la comunicazione. Tutti i pacchetti che condividono la stessa indicazione di flusso vengono instradati sullo stesso percorso.

Il circuito virtuale creato consiste in un certo numero di router che vengono coinvolti nella connessione e che formano un percorso logico tra i nodi della rete. Questo flusso è sempre ottenibile sulla rete se sono disponibili gli stessi nodi, oppure viene ricreato su un altro percorso con altri nodi. Se qualsiasi router della rete andasse in crash, ogni percorso andrebbe ricalcolato. 

Inoltre il fatto che una sezione di rete venga riservata per questa connessione significa che prima che i datagrammi fluiscano sulla rete, la rete stessa riesce a creare un percorso virtuale (quindi simile alla rete telefonica, ma virtuale, ovvero può non corrisponde a nessun allocamento di spazio sulla connesssione).

Queste reti offrono un servizio solo con connessione, motivo per cui i router che sono presenti in questa rete sono stateful e riescono a ricordare i flussi che li attraversano.

Un esempio di una rete con questo funzionamento è l’ **ATM (asyncronus transfer mode)**. La rete era stata studiata molto attentamente per fornire con una serie di tecniche a sfruttare il modello di conversione di circuito per fornire una serie di garanzie (che si possono leggere nella tabella sopra).

Queste connessioni possono usare diverse tecniche che possono fornire:

*  **CBR (constant bitrate)**: connessioni a frequenza costante. Si ottengono garanzie su banda, ordinamento e temporizzazione di consegna.
*  **VBR (variable bitrate)**: connessioni a frequenza variabile. Si ottengono delle reti con le garanzie precedenti, però con la banda di accesso variabile (e che quindi viene descritta nella prima connessione e poi mantenuta). Questa rete veniva utilizzata molto per i contenuti video.

Per il funzionamento di un circuito a commutazione di pacchetto a circuito virutale (ovvero una rete tipo ATM) è necessaria una **chiamata di inizializzazione del percorso**, ovvero la richiesta dell’host alla rete per richiedere il circuito necessario a collegare mittente e ricevente. Queste operazioni hanno una serie di fasi:

1. Si esegue la chiamata di inizializzazione da parte del mittente verso la rete
2. Si ottiene la chiamata in ingresso dal lato ricevente
3. Si esegue l’accettazione della chiamata, oppure la si rifiuta
4. In caso di chiamata accettata, si connettono i due host e inizia la chiamata
5. Si fa partire il flusso di dati

<img src="image/image-20211209160851750.png" alt="image" style="zoom:50%;" />

Applicazioni della rete ATM oggi possono essere diverse (ma di solito limitate sia nel numero di hosts partecipanti e nelle performance fornite) come ad esempio i circuiti per fornire la connessione ADSL. Inoltre è spesso utilizzata all’interno delle aziende per migliorare le prestazioni, infatti si possono applicare dei protocolli di conversione che permettano la traduzione dei pacchetti sul protocollo IP.

L’ATM in certi aspetti può essere anche considerata una tecnologia di livello 2.



### Reti a datagrammi

In questi modelli di rete non si ha un’impostazione di chiamata a livello di rete, inoltre i router non conservano informazioni sullo stato dei circuiti virtuali e non c’è il concetto di connessione a livello di rete.  

Tutti gli inoltri su ogni nodo avvengono utilizzando l’indirizzo dell’host del destinatario. In questo modo i pacchetti passano per una serie di router che utilizzano gli indirizzi di destinazione per inviarli e che quindi possono passare da percorsi diversi.

![image-20211211104305446](image/image-20211211104305446.png)

Questo modello è quello applicato alla rete internet. Di fatto si definisce la rete **best-effort** che significa che la rete non fornisce nessuna garanzia rispetto a banda, consegna, ordinamento, temporizzazione e indicazione di congestione.

Inoltre i datagrammi scambiati su questa rete è completamente stateless, ovvero ogni pacchetto è unico per la rete e non vengono mantenute informazioni sul percorso dei pacchetti della stessa coppia host-host. Questo permette di avere dei pacchetti in arrivo sugli stessi estremi, senza che però abbiano svolto lo stesso percorso.

Il lato positivo di questa tecnica è che spostando tutte le tecniche di gestione della connessione e della rete agli hosts esterni, il cuore della rete rimane molto semplice e efficiente.

Il lato negativo di questa tecnica è che si perdono tutte le garanzie e tutto il controllo possibile.

Il funzionamento anche in questo caso è attraverso una **tabella di inolto**, che però in questo modello viene ottenuta con una serie di algoritmi . In questa rete si possono avere diversi pacchetti mandati attraverso dei collegamenti diversi, oppure si può decidere di dividere il carico tra più link diversi.

Di fatto una tabella in questi router prende una forma simile a questa:

<img src="image/image-20211211105721813.png" alt="image-20211211105721813" style="zoom:50%;" />



Gli indirizzi utilizzati in questa rete sono IPv4. Questo significa che sono dei valori organizzati in 3 numeri da 0 a 255, ovvero 4 valori da 8bit. Di fatto questo permette la creazione di 4 miliardi di possibili indirizzi.

La tabella così costruita inoltre è molto pesante per il router, siccome per ogni pacchetto il router deve scorrerla e ottenere un risultato. Per ridurre il tempo necessario a ottenere una risposta in questo modello si utilizzano una serie di tecniche come ad esempio il raggruppamento geografico e/o la corrispondenza di prefisso. 

Questi metodi permettono di avere una serie di IP sequenziali vicini, riducendo il numero di informazioni che ogni router deve possodere:

* gli IP che indicano host vicini geograficamente sono molto specifici
* gli IP che indicano host piuttosto lontani sono molto poco specifici, ovvero generalmente si raggruppa in un range di indirizzi e si forwarda tutti questi pacchetti nella stessa direzione. 

Il concetto che sta alla base di questa modalità è che ogni router conosce molto bene la rete nelle sue vicinanze, mentre invece quando ottiene un indirizzo molto lontano rispetto alla sua posizione delega a un altro nodo che sa essere più vicino.

Il raggruppamento geografico è stato ottenuto grazie alla compravendita degli indirizzi IP. Ogni ISP ha negli anni comprato un certo range di indirizzi (di fatto gli indirizzi disponibili su IPv4 sono terminati) determinati normalmente da un bit specifico nel valore. In questo modo si ottengono una serie di indirizzi compresi in determinati valori che condividono gli stessi valori a sinistra, ma che hanno diversi valori sulla destra.

Il miglior modo per ridurre le righe della tabella di instradamento appunto è attraverso questa tecnica di *range based address*, per cui si hanno una serie di regole che indicano i valori di sinistra in modo poco specifico, mentre invece ci sono regole molto specifiche per una serie di valori di destra.

Questo metodo, applicato da moltissimo tempo, ha dimostrato di essere molto più efficiente che avere ogni router contenente tutti gli indirizzi.



Il fatto che la rete internet oggi sia a datagrammi è dovuta al fatto che:

* negli anni 80 tutte le aziende utilizzavano degli standard propri. Se si comprava una rete allora era chiusa e non aveva necessità di interconnettersi.
* Quando la rete ha dovuto integrare tutte le altre, allora si è arrivati al problema di avere dei vincoli sottostanti: o obbligo una rete globale a circuiti virtuali, allora tutte le mie sottoreti devono avere la capacità di costruire questo circuito.
* si inizia a rilassare i vincoli sulle componenti, ma avere una rete **best effort**. Di fato quindi si avranno delle isole di funzionamento a circuito e delle reti di pacchetto, ma di fatto poi è rimasto il modello a pacchetti siccome aveva meno vincoli sulle reti che poi ne facevano parte.
  * Questo a facilitato la rete e la scalabilità (il circuito impone un limite alle connessioni disponibili siccome devono dare uno stato alle connesioni)



In questa sezione abbiamo iniziato a parlare di indirizzi IP. Generalmente si utilizza il protocollo IP alla versione 4. Di fatto questo protocollo ha troppi pochi indirizzi (al momento gli indirizzi pubblici sono terminati in realtà) e non riesce a implementare alcuni modelli di sicurezza (nonostante si stia utilizzando IPv4Sec, che implementa alcune versioni di sicurezza e verifica degli host).

Per risolvere questi problemi si è sviluppata il protocollo IP versione 6 che permette di avere molti più indirizzi di rete e che garantisce una versione sicura e di verifica automatica.  

Siccome in molte zone del mondo, il protocollo versione 4 di IP è ancora lo standard ci sono diverse tecniche per moltiplicare gli indirizzi e riutilizzarli.



### Riassunto

Pur restando nella commutazione di pacchetto (ovvero il modello di scambio impone di avere dei dati pacchettizzati), abbiamo due soluzioni:

* modalità a datagrammi, che è il punto di riferimento di internet.
  * è molto più semplice siccome tutta la complessità è relegata agli host ai margini della rete, mantenendo semplice il cuore.
  * le performance sono migliori siccome ogni nodo ha un solo compito: il forwarding dei pacchetti.
  * questa soluzione è molto scalabile e i router sono stateless (ovvero non hanno necessità di mantenere informazioni sui flussi di pacchetti). Di fatto ogni nodo ha la responsabilità di portare un pacchetto al prossimo nodo della rete, semplicemente.
  * Questa rete ha necessità di una serie di controlli dall’esterno perché non si sa autoregolare.
* circuiti virtuali (ATM):
  * evoluzione della rete telefonica come approccio ma che utilizza dei pacchetti (tutta la gestione di banda, percorso e tutti gli altri servizi sono rilasciati e controllati direttamente dalla rete).
  * non ha la stessa capacità di riservare banda
  * con la possibilità di controllare il collegamento prima di iniziare il flusso di traffico, che permette anche di avere un controllo sul flusso e sulle congestioni. è necessario quindi di avere dei router che mantengano lo stato. Infatti:
    * la rete è molto più complessa, siccome i nodi devono fare:
      * forwarding: operazione di comunicazione dei pacchetti
      * consentire di gestire la connessione (applicare i limitatori di banda e operare sulla limitazione dei pacchetti)

Per semplicità, costi si è scelto la rete a datagrammi per internet.



## 4.3 Architettura di un router

Il router ha un architettura logica che consiste in porte in ingresso e porte in uscita. Nella pratica ogni porta è sia di ingresso che di uscita ma logicamente può essere in un unica direzione alla volta.

Il cuore del router è lo **switching fabric** che è l’oggetto che permette ai pacchetti di essere inoltrati da un porta a un’altra. La logica secondo quale il router manda i pacchetti verso un altra porta è definita nel **routing processor** che appunto ha il compito di programmare la tabella di inoltro.

Il router (per definizione) permette di avere dei pacchetti che raggiungono al massimo livello 3 della pila protocollare e quindi i pacchetti passano attraverso i livelli fisico, di accodamento e di rete.

Un architettura logica di un router è simile a questa:

![image-20211211190010895](image/image-20211211190010895.png)

Il router solitamente ha due funzioni chiave:

* far girare i protocolli/algoritmi di instradamento (*RIP, OSPF, BGP*)
* inoltrare datagrammi dai collegamenti di ingresso a quelli in uscita



### Porte di ingresso

Le porte di ingresso hanno 3 sezioni particolari (nella pila protocollare vanno da livello fisico fino a livello 3):

* *line termination*: è la sezione della porta che si occupa di accettare la linea fisica, ricevere i bit comunicati (oppure i segnali di luce) e convertire questi valori in dati.

* *livello di collegamento*: in questa sezione della porta si può ottenere il de-incapsulamento del pacchetto. Il payload ottenuto in questa fase può essere passato a livello sopra.

* *buffer di lookup & forwarding*: questa sezione è il buffer responsabile della coda (*queuing*) dei pacchetti in entrata. Lo scopo è far aspettare i pacchetti in questa sezione finché lo switching fabric non è disponibile per smistarli.

  Questa sezione della macchina solitamente è meno importante perché, affinché uno switch sia ottimale, è necessario che il rate dei pacchetti in entrata sia uguale alla frequenza di elaborazione dello switching fabric.

  Questa operazione è piuttosto complicata, infatti è difficile trovare dei dispositivi hardware con la capacità di svolgere operazioni di inoltro alla velocità della rete e di fatto ad oggi le soluzioni più performanti permettono di svolgere queste operazioni fino a 100Gbit/sec (logicamente queste performance arrivano con un costo interessante).



### Switching Fabric e le 3 tecniche di commutazione dei pacchetti

Lo switching fabric è di fatto la sezione del router che li differenzia dai computer. Di fatto al momento ci possono essere 3 tipi diversi di oggetti che permettono questa operazione (rappresentate anche nel disegno sotto):

* commutazione attraverso un buffer in memoria
* commutazione attraverso un bus di sistema
* commutazione con metodo crossbar

![image-20211211190122725](image/image-20211211190122725.png)

#### Commutazione in memoria

Questo modello utilizza un architettura *general pourpose*, è uno dei primi modelli di router utilizzati siccome per essere creati è necessario semplicemente riconfigurare un computer con più di un interfaccia di rete. Questo è anche il modello più facile da applicare e implementare.

La commutazione in memoria consiste praticamente nella memorizzazione dei pacchetti in memoria, dove vengono elaborati (ad esempio devono essere svolti dei controlli sul checksum oppure la semplice lettura dell’header) e poi ritrasmessi a una porta in uscita in base al contenuto.

Questo modello è si il più facile da implementare, ma è anche il meno performante, siccome la capacità di commutazione è limitata dal bus di sistema (il pacchetto deve transitare in questa sezione per due volte) e dalle capacità del computer utilizzato (le performance della memoria per la maggior parte). 

![image-20211211190441668](image/image-20211211190441668.png)

In particolare il bus di comunicazione del computer non può essere utilizzato da due flussi diversi nello stesso momento, quindi la frequenza massima di inoltro è sempre un valore minore di $B/2$ (dove $B$ è la capacità del collegamento del bus).



#### Commutazione tramite bus

Siccome la prima soluzione non forniva un modello abbastanza performante per la capacità della rete, allora si ha iniziato a studiare un nuovo modello.

La commutazione tramite bus di sistema permette di evitare il salvataggio dei pacchetti in memoria, ma lo switching direttamente utilizzando il bus di sistema. In questo modo si riesce a rendere unico la comunicazione svolta sul router.

Il pacchetto una volta entrato nello schema di bus sa già che porta di uscita prendere, rendendo obsoleto la richiesta di memorizzare il pacchetto in memoria. In questo modo la frequenza massima di commutazione di pacchetto riesce (potenzialmente) ha essere raddoppiata, diventando $B$ solamente (infatti viene anche chiamato bus a linea dedicata).

Un problema in questo modello però è la **contesa per il bus**: non può succedere che più pacchetti vogliano accedere al bus nello stesso momento. Per questo motivo è necessario un modello di controllo che gestisca il traffico in entrata.  

Questa architettura rimane ancora molto simile a un computer, motivo per cui riesce a mantenere dei costi relativamente bassi. Allo stesso modo però fornisce delle prestazioni abbastanza buone per un router su una rete di accesso, ma non resisterebbe nel cuore della rete.

Ad esempio un router Cisco 5600 ha un bus di 36Gbit/s. Questo router riesce tranquillamente a gestire 3 porte da 10Gbit/sec, oppure una trentina di porte da 1Gbit/sec. Per questo motivo si configura perfettamente come router da limiti della rete.



#### Architettura Crossbar

L’architettura di commutazione più performante è la **crossbar** e è stata studiata appositamente per consentire le migliori performance possibili nell’inoltro dei pacchetti. Per questo motivo i router che impiegano questa architettura sono anche i più costosi (impiegano un hardware completamente diverso da un computer), ma permettono di avere delle capacità di switching in grado di gestire i Tbps (Terabit per sec).

Il funzionamento di questo modello è attraverso una serie di bus e logiche che permettono di parallelizzare il traffico in modo da permettere di superare la banda disponibile rispetto a quando si ha solo un bus condiviso. Questo modello permette di sviluppare $2n$ bus che collegano $n$ porte di ingresso a $n$ porte di uscita.

Per favorire la gestione dei pacchetti e dell’intero router questi modelli utilizzano delle task di durata predefinita (in questo particolare si differenziano rispetto al resto dei modelli che lavorano sul modello dell’interrupt). Il funzionamento è simile al clock di un processore, dove ogni task è **sincrona**. Di fatto quindi il flusso dei pacchetti assumerà la forma della funzione di clock. Inoltre per permettere a più pacchetti possibili di circolare sui bus di sistema contemporaneamente si utilizzano un’altra serie di apparati di supporto che organizzano l’ordine in cui mandare i pacchetti affinché si saturino al meglio le linee.

Questi modelli permettono la gestione di grandissimi volumi di traffico e sono applicati nel cuore della rete. Ad esempio lo Switch Cisco 12000 (chiamato solo switch perché in realtà gestisce solo le operazioni di inoltro, quelle di indirizzamento vengono svolte in un apparato esterno) permette di avere una capacità di forwarding fino a 60Gbps



### Porte di uscita

Le **porte di uscite** sono di base la versione specchiata rispetto a quelle di ingresso. Anche questa porta quindi ha accesso solo fino al terzo livello della pila protocollare. 

* *buffer*: quando un pacchetto arriva dall’architettura di smistamento superiore allora raggiunge questo luogo.

  Questo buffer è quello che deve sopportare lo stress in caso di congestione, infatti il tempo di accodamento non è costante, inoltre l’interfaccia può avere dei tempi di accodamento.

  Questa sezione viene spesso accoppiata con uno *scheduler*. Questo componente è capace di definire il meccanismo di gestione della coda e dell’accodamento dei pacchetti in arrivo. 

  * Solitamente si ha un modello di accodamento FIFO (*first-in-first-out*)
  * si possono avere dei meccanismi di gestione della coda differenti o differenziati in base al tipo di pacchetto. Ad esempio può creare delle code differenziate e dare la precedenza a un certo tipo di pacchetto o traffico.
  * questa sezione è responsabile del dropping dei pacchetti in caso di buffer troppo pieno e può reagire con diverse regole
  * Solitamente nei router commerciali le modalità di accodamento disponibili sono codificate nel firmware del router.



#### Dimensione del buffer

Per la gestione dei buffer di accodamento ci sono dei problemi quando si parla delle dimensioni, infatti si hanno lati negativi sia se il buffer è troppo grande o troppo piccolo.

Un router con un buffer troppo piccolo non permette di avere la possibilità di assorbire delle congestioni temporanee e potrebbe dare problemi nell’accodamento in caso di congestione.

Se il buffer è troppo grande si ottiene l’effetto di non rischiare mai la congestione grave, ma si perderebbe la capacità di controllare il ritardo. Infatti si pensi alla possibilità di aver un buffer possibilmente infinito, allora i pacchetti potrebbero sarebbero sempre in attesa, facendo aumentare di molto il carico della rete.

Per risolvere questo problema sono state fatte alcune proposte, la prima delle quali nell’`RFC 3439`. Questo proponeva di avere un buffer disponibile uguale a una media del tempo di andata e ritorno ($RTT$) moltiplicato per la capacità del collegamento ($C$). Si può pensare a questo valore come il calcolo spannometrico del volume del collegamento, immaginando quest’ultimo con un tubo per l’acqua. 

Questa soluzione però inizia a diventare impegnativa soprattutto sui modelli di router molto grandi, che richiedono molta memoria. Quest’ultima è molto costosa siccome deve essere particolarmente affidabile e rispettare una serie di particolari caratteristiche. Per questo motivo, in seguito a degli studi si è arrivati alla soluzione di avere dei buffer dimensionati secondo:
$$
\frac{RTT * C}{\sqrt N}
$$
(La quantità di buffering necessaria per $N$ flussi quindi è il $RTT$ per la capacità del collegamento $C$, tutto fratto $\sqrt{N}$).

La formula è stata trovata in modo abbastanza empirico, siccome $\sqrt{N}$ non ha effettivi fondamenti teorici, ma di fatto permette di non arrivare mai al limite della memoria disponibile.

Applicata nei router con più necessità di memoria inoltre questo modello ha fatto notare un miglioramento delle prestazioni, avendo minor volume di memoria da gestire.



## 4.4 Protocollo Internet (IP)

Non si può parlare di reti senza IP (protocollo internet). Questo protocollo rimane piuttosto semplice siccome utilizza la commutazione di pacchetto a datagrammi.

Questo modello non è complicato e spesso utilizzato in tutti i campi dell’informatica.

All’interno di questo protocollo si trovano le indicazioni:

* su convenzioni sugli indirizzi
* rispetto al formato dei datagrammi di comunicazione
* rispetto alle regole di instradamento e inoltro (che poi si tramuta in programmazione delle regole per la programmazione della tabella di inoltro attraverso questi protocolli).
* protocollo di segnalazione ICMP



### Formato dei datagrammi

L’header dei datagrammi IP sono grandi sempre 20byte e quindi hanno 5 righe di intestazione da 32bit ciascuna (come il TCP). Ulteriormente a questo ci sono i campi opzionali (anche se vengono utilizzati molto poco), nei quali ad esempio si può indicare la registrazione del percorso del pacchetto, segnare i timestamp (una delle modalità per valutare i valori dell’RTT) e segnare l’elenco di router. Infine segue il payload.

![image-20211212122727320](image/image-20211212122727320.png)



La *prima riga* contiene:

* *numero di versione*: la versione di default è 4, ma c’è anche la 6 che è più moderna. 

* *lunghezza di intestazione*: indica il numero delle righe di intestazione (di solito 5).

* *tipo di servizio*: questo campo è stato studiato per avere un certo scopo su alcuni tipi di reti (per valutarne il funzionamento), ma di fatto non viene utilizzato sulla rete internet.

* *lunghezza del datagramma*: questo campo indica quando è grande il pacchetto. Solitamente la dimensione è $1500byte$. 

  Solitamente è strano vedere pacchetti troppo grandi perché non conviene ai layer della pila protocollare creare dei pacchetti così (nonostante sia un valore a 16bit, e che quindi permette un valore teorico di dimensione all’incirca 65500 byte). Inoltre sulla rete può capitare che dei pacchetti troppo grandi vengano droppati o che il livello sottostante della pila protocollare lo rifiuti direttamente.

  

La *seconda riga* contiene (tutti i parametri che servono a svolgere la frammentazione del pacchetto, vista più avanti):

* identificatore a 16bit
* flag
* spazzamento di framm. a 13bit



La *terza riga* contiene:

* Tempo di vita residuo: tempo di vita previsto (di solito a 64, ma dipende dal sistema operativo) per il pacchetto. Questo valore viene decrementato ad ogni nodo e serve a evitare di avere dei pacchetti dispersi sulla rete che potrebbero creare problemi e congestioni.
* protocollo di livello superiore: questo indica il protocollo di livello superiore che si è incaricato di inserire i pacchetti. Questo campo è importante perché permette di capire se si sono pacchetti TCP o a UDP oppure a altri protocolli (ad esempio ICMP). Di base questo serve a fare multiplexing, oppure a distinguere i pacchetti di instradamento (che solitamente vengono fatti con un pacchetto di livello 3-TCP).
* checksum (solo sull’intestazione): il protocollo IP protegge solo l’intestazione, quindi esegue il checksum dele prime 5 righe (o in più i campi opzionali)



La *quarta e quinta riga* contengono i campi di intestazione per origine (chi ha creato il datagramma) e destinazione (chi ha ricevuto o a chi è indirizzato il datagramma). Questi due campi sono entrambi da 32bit e ne tratteremo più avanti più nello specifico.

Seguono poi dei *campi opzionali*.



### Frammentazione dei datagrammi IP

Ogni rete ha un limite massimo di dimensione di datagrammi che possono essere mandati sulla loro connessione. Questo valore si chiama **MTU (Maximum Transport Unit)** e di fatto indica anche la dimensione massima del pacchetto che accetterà.

Solitamente questo valore è settato a $1500byte$, ovvero lo stesso valore di TCP, ma su alcune reti particolari questo può essere diverso (ad esempio le reti ATM hanno $50byte$)e quindi è necessario avere delle opzioni che ne riducano la dimensione. Questa azione può essere effettuata in due modi:

* la sorgente sa già che il percorso ha un certo MTU. In questo modo imposto un pacchetto per avere una grandezza massima definita a quel valore e  pacchettizzo direttamente in modo di non superare mai quel limite.
* lascio al livello di rete di frammentare il mio datagramma

La prima impostazione non ha nessun operazione diversa dal solito da effettuare, semplicemente che creerà dei datagrammi direttamente più piccoli a livello 3, senza immetterne sulla rete di troppo grandi.

Nel secondo caso avvegono una serie di operazioni automatiche a livello di collegamento attraverso i router, che sanno “rompere” il datagramma in segmenti automaticamente, siccome conoscono il valore di MTU delle reti che collegano.

L’importante di questa tecnica è che ogni datagramma sia valido, nonostante sia solo un frammento di un’altro più grande. In questo modo di  base si creano più pacchetti che hanno stessa intestazione (eccetto alcuni parametri) ma contengono pezzi di dati diversi. Inoltre la frammentazione così svolta (ovvero mantenendo validi i pacchetti) permette di avere ulteriori riframmentazioni dei pacchetti. 

Questi valori potranno poi essere ricomposti in un secondo momento dal router o direttamente dagli host.

Il lato negativo di questa tecnica è che si aumentano i punti di fallimento, infatti perdendo uno solo di questi frammenti, si ha perso completamente il pacchetto originale e è necessario ritrasmettere completamente tutto il datagramma originale (per questo motivo è preferibile ottenere una frammentazione a livello di trasporto).



L’operazione di frammentazione è piuttosto difficile e opera con diverse informazioni: la lunghezza del pacchetto, le flag di frammentazione e la dimensione di spiazzamento.

Prendiamo un datagramma originale di $4000byte$ su una rete con un MTU=$1500byte$, con questi valori nell’intestazione (sono presi dalle prime due righe del datagramma IP presentato sopra. Rappresento solo le informazioni importante per questa operazione):  

```html
Lunghezza = 4000
Flag = 0
Spiazzamento = 0
```

Siccome la rete ha MTU minore della lungezza del pacchetto, allora è necessario procedere alla frammentazione. L’idea di fondo è di riempire ogni pacchetto al massimo e lasciare il rimanente nell’ultimo.

Tutti i pacchetti creati hanno una serie di caratteristiche:

* hanno una lunghezza uguale a MTU (ad eccezione dell’ultimo pacchetto).
* Flag settata a:
  * un valore uguale a zero se è l’ultimo pacchetto della frammentazione
  * un valore diverso da zero e crescente in base alla frammentazione, ovvero:
    * ogni volta che il pacchetto viene frammentato questa flag assume il valore di $\text{FlagPrecedente} +1$. Quindi se si esegue una sola frammentazione questo valore è esattamente $1$
    * Se si frammenta ulteriormente un pacchetto (già frammentato quindi) si ottiene $\text{FlagPrecedente}+1 = 1+1 = 2$.
    * Poi si continua con questa logica
    * Questi casi avvengono quando si passa su una serie di router che hanno valori di MTU sempre più piccoli. Così facendo si ottengono dei valori di flag di frammentazione sempre maggiori.
  * *è necessario* che in ogni operazione di frammentazione si ottenga l’ultimo frammento con valore 0, siccome indica che la comunicazione del pacchetto originale è terminata e si può iniziare a ricostruire.
* Dimensione di spiazzamento:
  * Questo parametro indica il valore del pacchetto in modo da permettere di riordinare tutti i frammenti. 
  * per risparmiare dei byte, questo parametro si presenta sempre con un valore diviso per $8$. Questo valore si tiene come puntatore e indica i byte del pacchetto in base alla posizione in quello originale. Ad esempio vediamo un caso in cui si ha un pacchetto di dimensione  $1480byte$ ($20byte$ di header IP + $1460byte$ di payload). Lo spiazzamento viene calcolato in questo modo:
    * la prima sezione ha lunghezza: $20 H + 1480 P$. Spiazzamento = $0$
    * la seconda sezione ha lunghezza: $20 H + 1480 P$. Spiazzamento = $ 1480/8 = 185$
    * la terza sezione ha lunghezza: $20H + (3980 - 2960)P = 1040 byte$. Spiazzamento $2960/8 = 370$



Solitamente non si frammenta frequentemente, infatti TCP pacchettizza a $1500byte$ che è il MTU della maggior parte delle reti, inoltre la soluzione affidata al livello di trasporto è molto più funzionale e performante. Inoltre avendo così tanti frammenti, i pacchetti sono molto più lunghi da mandare e si introduce anche un certo costo di tempo di elaborazione da entrambe le parti (mittente e ricevente) per la frammentizzazione e la ricomposizione.

Per tutti questi motivi e anche per alcuni altri nella versione 6 del protocollo IP questa funzione viene tolta completamente. 

La frammentazione dei pacchetti nel protocollo IPv6 avviene solamente nel livello di trasporto, mai nel livello di rete, in modo da richiedere meno tempo al router per svolgere questa operazione. Per segnalare che un pacchetto è troppo grande e che l’MTU della rete è minore si è elaborato un altro segnale di risposta.



### Indirizzamento IPv4

L’indirizzo IP è un valore globalmente univoco e viene associata a qualsiasi interfaccia (sia host che router) visibili in rete. 

Esempio di assegnazione di indirizzo IPv4:

<img src="image/image-20211213010421635.png" alt="image-20211213010421635" style="zoom:50%;" />

Di fatto la rete è un’unione di sottoreti. Le sottoreti sono definite dalla presenza del router, che lavorando a livello 3 impone la presenza id un interfaccia diversa per ogni sottorete.

Nella foto si può notare che i primi 3 raggruppamenti di valori sono uguali per tutti gli host e anche per il router centrale alla rete, mentre invece i router finali sono diversi per ogni host nella stessa sottorete.

I primi 3 gruppi di bit sono uguali in modo da permettere una facile comprensione di quali host siano nella stessa rete e quali invece siano esterni. Questa informazione è necessaria per l’indirizzamento del pacchetto iniziale, siccome è necessario capire se va trasmesso a un host sulla stessa rete (operazione svolta direttamente dall’host) o se invece va indirizzato al router perché si deve comunicare con un host distante. 

Questa operazione di controllo è molto facile per il computer, infatti viene fatto un controllo attraverso l’operazione binaria `&` tra gli indirizzi. 

Per definire una sottorete è necessario utilizzare una scrittura particolare che indica i **campi di indirizzi**, ad esempio `192.168.1.1/24`. Questa scrittura indica che i primi $24bit$ da destra sono l’indirizzo della sottorete (ovvero questi bit sono condivisi tra tutti gli host sulla rete), mentre gli altri sono il valore degli host sulla sottorete. Il valore dopo la barra può avere qualsia valore da 1 a 32. Il `/24` è il campo più diffuso.

Di fatto anche i collegamenti diretti sono delle sottoreti, e quindi hanno un campo di indirizzi (ad esempio la comunicazione tra due router). Solitamente per questo scopo si utilizza un campo `/30`.

Alcuni indirizzi **non si possono utilizzare** perché riservati per scopi particolari:

* l’indirizzo `0.0.0.0` (ovvero quello con tutti i bit settati a 0) rappresenta **la rete stessa**
* l’indirizzo `255.255.255.255` (ovvero quello con tutti i bit settati a 1) che è l’**indirizzo di broadcast**.

Il broadcast è un operazione che permette di mandare un pacchetti a tutti gli host della rete e per questo motivo non può mai essere effettuata su tutta la rete, mentre è molto utile per comunicazioni sulle sottoreti. I router vengono anche qui in aiuto: limitando la sottorete si possono avere alcune comunicazioni in broadcast su una specifica rete studiando il posizionamento dei router tra gli host. Per questo motivo spesso i router vengono posizionati in modo da spezzare questi tipi di comunicazioni. Inoltre se necessario è possibile riunire questi messaggi tra più sottoreti attraverso il protocollo IP. Logicamente per questo motivo i router non possono emettere nessun pacchetto in broadcast.

Generalmente gli utilizzi di una comunicazione in broadcast possono essere:

* un host vuole indicare la sua presenza sulla rete (ad esempio una stampante sulla rete locale)
* raggiungere un host di cui non conosco l’indirizzo

Alcuni mezzi di comunicazione (ad esempio il wifi) funzionano comunicando sempre in broadcast, ma in questo caso si sta parlando di un tipo di broadcast diverso, effettuato a livello fisico.

Un altro oggetto che utilizza il broadcast come mezzo di comunicazione dei dati sono gli access point. Questi oggetti però funzionano a livello 2 e hanno come unico scopo quello di arrivare al router più vicino, senza alcuna capacità di indirizzamento e/o forwading. 



### Assegnazione di un indirizzo a un host

La strategia per assegnare gli indirizzi tra gli host è chiamata **CIDR (Classless InterDomain Routing)** e permette di assegnare indirizzi IP a degli host in modo da non sapere a priori quale sia la parte dell’host e la parte della rete. In pratica sui 32bit dell’indirizzo di rete, si ottiene un valore di bit, partendo da sinistra che indica quelli che possono indicare l’host, mentre invece i rimanenti sono comuni su tutta la rete. 

Questo tipo di indirizzo prende una forma del genere `a.b.c.d/x` dove `x` è il valore di bit che si riservano per l’host. Questo valore (`x`) non deve per forza essere un multiplo di 8 ma può prendere qualsiasi valore possibile.

<img src="image/image-20211213181233501.png" alt="image-20211213181233501" style="zoom:50%;" /> 

In questa sezione si deve fare la divisione tra il concetto di **indirizzo della sottorete** e **netmask della sottorete**. Praticamente l’indirizzo della rete è un valore che contiene i bit relativi alla sottorete costanti, mentre invece varia i bit della parte di host. La maschera di sottorete invece è un indirizzo che rappresenta la stessa cosa, ma si lasciano a `1` i bit della parte di sottorete, mentre invece si lasciano a zero i bit della parte relativa all’host.

Di fatto quindi un indirizzo è indicabile univocamente in due modi, sulla stessa sottorete: 

* maschera della sottorete (esempio: `255.255.254.0`)
* indirizzo della sottorete (esempio: `200.23.16.0/23`)

Questi modelli non si utilizzano mai assieme ma o uno o l’altro.

Questo modello è molto comodo e efficace siccome permette di comprendere subito se il pacchetto va consegnato nella stessa sottorete, oppure se deve uscire. Inoltre essendo un operazione binaria, si può fare un confronto molto veloce (attraverso un semplice `&` binario). In questo modo si può subito capire se mandare a un host interno alla rete o affidarlo al router perché più lontano.



L’operazione di assegnazione di un indirizzo a un host può anche essere svolta in modo manuale, ma solitamente è preferibile un metodo automatico. Generalmente la configurazione manuale dell’IP è utile per i server, mentre si utilizzano dei modelli automatici sulle reti. 

Infatti di solito sulle reti è disponibile il **DHCP (Dynamic Host Configuration Protocol)**. Lo scopo di questo protocollo è di rendere “plug-and-play” l’ingresso in una rete, ovvero rende automatico l’assegnazione di un IP a un host sulla rete, rendendo di fatto la gestione manuale della rete per lo più inutile/non necessaria.

L’obiettivo è quello di consentire a un host di entrare in una rete nel modo più semplice possibile, siccome l’assegnazione di un indirizzo è la base per ottenere una comunicazione sulla rete. 

Solitamente, soprattutto per gli indirizzi pubblici gestiti dagli ISP, l’assegnazione viene gestita dinamicamente. Questa cosa viene fatta siccome non si dispone quasi mai di troppi indirizzi sulla rete e in questo modo si permette il riutilizzo degli indirizzi.

Per eseguire una richiesta di indirizzo IP sulla rete attraverso questo servizio sono necessari solo 4 messaggi:

```mermaid
sequenceDiagram
	Host-->Server DHCP: DHCP Discover;
	Server DHCP->>Host: DHCP Response;
    Host->>Server DHCP: DHCP Request;
    Server DHCP->>Host: DHCP ACK;
```

*  nella prima parte della connession l’host invia un messaggio di broadcast sulla rete chiamato *DCHP discover*
*  il server DHCP risponde con un messaggio chiamato *DHCP response* (questo messaggio viene ottenuto solo se si trova un server sulla rete)
*  l’host richiede l’indirizzo attraverso un *DHCP request*
*  il server conferma l’indirizzo con un *DHCP ACK*



Affinché il funzionamento del protocollo DHCP sia corretto si deve avere il server sulla rete locale. Solitamente nelle case si ottiene in un unico pacchetto NAT, server DHCP e router, con magari anche il router WI-FI.

IL DHCP è diverso dal servizio di NAT. Il DHCP è comodo perché non si deve autoconfigurare l’indirizzo IP. Generalemente il DHCP di una sottorete da un indirizzo privato. Il DHCP può essere anche un server che asssegna indirizzi pubblici, ad esempio i server DHCP dei provider forniscono valori pubblici. Se il server fa parte di una rete allora si assegnano gli indirizzi privati della sottorete. 

Come si fa a collegarsi a una certa rete? Di solito con il cavo ethernet ci si collega, ma di solito richiede un autenticazione dell’utente (noi diamo per scontato che questa azione sia già stata fatta) e che si svolgano le azioni necessarie a ottenere l’indirizzo IP.

Le procedure di seguito sono tutte quelle che avvengono appena ci si è autenticati sulla rete.

<img src="image/image-20211213223124564.png" alt="image-20211213223124564" style="zoom:50%;" />  

* mando il protocollo dall’host (porta 68) al server DHCP (porta 67)
  * questo messaggio viene inviato come fosse un broadcast generalizzato
  * l’unico modo è mandare con tutti 1 in modo che il pacchetto venga consegnato a tutti, e siccome il router non lascerebbe uscire il messaggio dalla sottorete
* si utilizza anche un transaction ID, per distinguere le richieste
* si ha un indirizzo di sorgente, un destinatario e poi le porte di destinatario e sorgente. Dentro il payload poi si ottiene la risposta dell’indirizzo offerto dal server DHCP. Questo è il momento in cui il server rilascia un possibile indirizzo e un tempo di vita (il lifetime viene messo perché quando questo valore scade, allora l’host rifà la richiesta. Di solito in questo caso il DHCP ridà l’indirizzo precedente).
* si conferma la ricezione dell’indirizzo proposto e poi conferma del DHCP.
* una volta ultimata queste operazioni allora si ottiene un indirizzo IP per l’host. Spesso poi nell’ultimo pacchetto vengono date altre informazioni (ad esempio l’indirizzo IP del router per uscire dalla sottorete e la maschera di sottorete)



Come ottenere un blocco di indirizzo IP. 

Ad oggi ogni blocco di indirizzi è affittabile dal provider, siccome tutti gli indirizzi sono occupati, al momento. I provider, finché non è stato proposto l’approccio di avere indirizzi privati, avevano la necessità di assegnare un indirizzo pubblico a ogni utente. Di solito per un utente è abbastanza avere un solo indirizzo, mentre le aziende possono avere la necessità di avere più di un indirizzo.

La compravendita degli indirizzi consiste nel comprare dei bit sull’indirizzo ovvero un bit della rete e tutti i valori sotto quell’indirizzo. 

La divisione degli indirizzi si ottiene in modo molto facile: si vuole dividere in un certo valore di indirizzi, mettiamo 8 ($2^3$). Bisogna allungare la maschera di rete di altri 3bit. Ad esempio si ha una maschera di rete `200.23.16.0/20` sull’ISP. Si vogliono fornire indirizzi a 8 organizzazioni, quindi si divide in 8 parti uguali, aggiungendo 3bit alla maschera di rete: `200.23.16.0/23`, `200.21.18.0/23`, `200.23.20.0/23`, `200.23.22.0/23`, `200.23.24.0/23`, `200.23.26.0/23`, `200.23.28.0/23`, `200.23.30.0/23`  . Di fatto questo genera i nuovi prefissi delle reti. Ognuna di queste reti ha 9bit di indirizzi per contenere tutti gli host, ovvero hanno $2^9 = 512$ indirizzi, a cui però vanno tolti il tutto $0$ e il tutto $1$. Quindi questo significa che ci sono $509$ hosts.

![image-20211213230658945](image/image-20211213230658945.png)

Di fatto nei casi pratici si può avere una netmask più restrittiva con `/30`. Quella con tutti zero e quella con tutti uni è la più restrittiva. Di solito questo tipo di connessioni avviene nelle connessioni tramite cavo, che sono necessari solo 2 indirizzi.



MANCA INDIRIZZAMENTO GERARCHICO PIù PRECISO



C’è un pochino di centralizzazione sulla rete internet, infatto **ICANN** (stessa corporazione che risolve i nuovi nomi di dominio e cose varie) e ha la responsabilità di gestire e vendere gli indirizzi online. Di fatto questo valore non può essere distribuito e quindi è necessario essere centralizzati.



### Traduzione degli indirizzi di rete (NAT) Network Address Translator 

Questo oggetto è utilizzato in connessione casalinga, per separare la rete aziendale/domestica da quella pubblica. Di fatto significa che da un lato è su internet (da qui partono i pacchetti verso la rete internet e quindi ha un indirizzo IP pubblico) e poi dall’altra parte ha un indirizzo riservato (per definizione infatti non sono privati, ma sono riservati. Erano nati con l’idea di usarli per esperimenti e test.). Possono essere `10.0.0.4` oppure del tipo `192.168.1.1`. Di fatto questi indirizzi poi non sono più stati utilizzati sulla rete pubblica. Lo spazio di indirizzi utilizzabili sulla rete privata è grande quando serve, infatti di solito non c’è nessun problema a aumentare il numero di host sulla rete.

Il NAT fa in modo di convertire i pacchetti in uscita e in ingresso in modo che gli host in rete locale siano raggiungibili attraverso l’IP pubblico del NAT.

Chiaramente questa operazione ha alcuni vantaggi:

* è economico (per farlo funzionare è necessario un unico indirizzo IP). Più di un indirizzo inizia a diventare costoso.

* Si possono cambiare tutti gli indirizzi privati dell’abitazione senza influenzare la rete esterna o senza essere in qualsiasi modo bloccato.

* Posso cambiare l’ISP senza cambiare il resto della rete

* (poi diventato uno dei motivi principali per utilizzare il NAT). I dispositivi dietro il NAT non sono esplicitamente indirizzabili e visibili dal mondo esterno, quindi rende la rete interna decisamente più sicura (di fatto questo significa che i computer interni alla rete non sono direttamente raggiungibili.)

  Esempio: la stampante in rete può accedere a internet, ma di fatto mentre lavora nella casa è disponibile solo alla rete interna, non è visibile all’esterno.



L’implementazione del NAT è piuttosto semplice, ma richiede di rompere la divisione dei layer della pila protocollare.

Infatti affinché il NAT funzioni allora è necessario che il router possa gestire gli indirizzi di trasporto (cambiare le porte degli indirizzi di fatto, cosa che in teoria sarebbe vietata dalla stratificazione protocollare). Per questo motivo e per un altro paio di altri il NAT è stato discusso: di fatto interviene su una situazione molto delicata. Questo è necessario perché: 

* quando un router NAT riceve un datagramma allora il NAT crea un nuovo numero di porta in uscita
* sostituisce l’indirizzo privato dell’host con il proprio indirizzo **WAN (Wide Area Network) - Indirizzo Pubblico** e sostituisce il numero di porta di orgine iniziale con il nuovo numero.

Praticamente quello che viene fatto è che sostituisce l’indirizzo del pacchetto con il proprio indirizzo pubblico (in modo che poi possa transitare sulla rete). Questo però genera un problema: il router NAT deve sapere a chi inoltrare la risposta. Per raggiungere questo scopo si utilizza anche il numero di porta, che serve quindi a identificare l’indirizzo di porta originario.

L’immagine sotto identifica meglio lo scenario:

![image-20211214101834551](image/image-20211214101834551.png)



Di fatto la tabella di traduzione NAT serve a “spiegare” al router dove i pacchetti debbano essere inoltrati e di fatto permette quindi il trasporto dei pacchetti dentro e fuori la rete pubblica.

Il NAT in questo modo permette di avere un numero di indirizzi IP potenzialmente infinito (poi in realtà non così tanto). Inoltre permette di avere un range di IP privati molto grande. Inoltre non ci accorge della sua presenza se non per l’indirizzo privato della connessione. 

Se si è dietro il router NAT per sapere il proprio indirizzo IP pubblico è necessario appoggiarsi a qualche servizio che indica l’indirizzo pubblico presente sulla rete.

Il NAT si è diffuso molto per la sicurezza che fornisce e per il moltiplicatore di indirizzi IP che permette di fare. Il blocco di questo modello è il numero di porta (di fatto impone il numero di host fornibili con questi indirizzi fino al numero di porte del NAT, quindi meno di 65000). 

Questo modello è stato contestato dai puristi della rete, siccome infrange il modello della stratificazione della rete.

Inoltre si parla di **argomento punto-punto**: siccome il NAT deve scrivere nell’intestazione del pacchetto di trasporto, questo viola l’argomento punto-punto, perché di fatto quello che viene fatto dal livello di trasporto in su non dovrebbe avere interferenze della rete. Questo è un rischio siccome se il NAT avesse dei problemi, allora si perderebbero dei pacchetti.

Inoltre questa argomentazione direbbe che passando a IP versione 6 (che il numero di indirizzi è potenzialmente infinito e non esisterebbero queste necessità di moltiplicazione delle connessioni). 

Ci sono altri problemi che non si possono risolvere in maniera molto elegante. Il client fa in modo di creare indirizzamento pubblico-privato. Ma di fatto come si fa se il client è fuori dalla rete e si vuole dare accesso a qualcuno che non è in indirizzo privato. Il client non potrà raggiungere il server dietro il NAT finchè la connessione non è inizializzata dalla rete internamente, siccome nella tabella di traduzione NAT non è presente ancora nessun valore. Questo problema ha alcune soluzioni:

* SOLUZIONE 1: si inserisce la corrispondenza sulla tabella degli indirizzi NAT a mano sul NAT in modo da permettere di accedere sempre dall’esterno.
  * Questo modello ha alcuni problemi: 
    * L’ISP può fornire un router che non permette questa azione
    * si possono avere dei problemi di configurazione e/o sbagliare
* SOLUZIONE 2: Universal Plug and Play (**UPnP**) Internet Gateway Device (IGD) Protocol. 
  * Questa azione permette, quando si accende l’host, di richiedere un qualche tipo di mapping per un qualsiasi tipo di porta. 
  * Questo modello si utilizza per un sacco di oggetti privati. Questo può succedere che le porte siano state assegnate e di solito spegnere e riaccendere il router, e di solito questo funziona siccome fa resettare la tabella NAT e quindi in questo modo gli oggetti smart possono richiedere di nuovo la porta e prenotarsela, in modo da avere l’accesso a internet. 
  * Questo si utilizza spesso per i dispositivi che sono oggetti (non computer), siccome è una soluzione abbastanza elegante e è plug-and-play.
* SOLUZIONE 3: questa è una soluzione estrema e di fatto si utilizza quando le altre non funzionano o non sono applicabili. Questa viene fatto quando ho un software dietro il NAT allora si utilizza una connessione a un nodo esterno che permetta di mantenere la entry sul NAT, poi rimango connesso con il super-nodo e al quando vengo ricercato allora il relay si occupa di connettermi con l’host attraverso la connessione impostata tramite NAT. Questo funziona siccome finché non si fa traffico sulla rete allora non si crea la entry dietro al NAT. Se io devo utilizzare un software e so che verrà spesso utilizzato dietro il NAT allora devo trovare subito una soluzione.
  * Questa soluzione è quella utilizzata da Skype (questa funziona perché il servizio ha dei nodi divisi abbastanza bene su tutta la popolazione)
  * Spesso il telefono VoIP (Voice Over IP) utilizza questo modello: la gente per chiamare deve utilizzare una tecnica di questo tipo. In questo modello di solito si mette uno scatolotto sull’internet pubblica (come fosse un centralino). Di solito chi vende il servizio VoIP vende questo scatolotto che permette di essere esterno al NAT e mantenere la connessione.



### ICMP (Internet Control Message Protocol)

Ad esempio questo servizio viene fatto ad esempio con il ping oppure quando i router avvisano che hanno dovuto buttare via alcuni pacchetti per congestione allora questo viene di fatto creato un modello di messaggistica di controllo.

Questo modello viene utilizzato da host e router per scambiarsi informazioni. Il servizio, nonostante sia un servizio necessario al livello 4, però utilizza dei pacchetti di livello 3.

I messaggi ICMP sono piuttosto semplici: campo tipo, campo codice e un frammento di datagramma.

I messaggi ICMP più utilizzati sono:

<img src="image/image-20211214104609372.png" alt="image-20211214104609372" style="zoom:50%;" />

Poi ci sono una serie di messaggi che servono al client e che servono al router (ad esempio che il router non riesce a connettersi all’host). TTL scaduto se i pacchetti devono essere mollati. Errata intestazione IP serve per la mobilità, per cui serve un router particolare che supporta la versione mobile e in questo caso è necessario cercare il router quando c’è.

Traceroute funziona utilizzando i messaggi ICMP in questo modello:

<img src="image/image-20211214105028892.png" alt="image-20211214105028892" style="zoom:50%;" />

### IPv6

La versione 6 di IP è stata teorizzata nel 2000 per un esigenza che era già chiara. Di fatto utilizzare gli indirizzi a 32bit è un modello di indirizzi troppo restrittivo, nonostante all’inizio sembrassero troppi.

Di fatto si è creato un nuovo protocollo IP, ma siccome si aveva studiato molto il protocollo IPv4 allora si voleva inserire alcune modifiche per renderlo migliore:

* cercare di avere un formato con meno campi, in modo da migliorare la velocità del servizio

* frammentazione lato router non è veramente necessaria e fa solo perdere tempo

  

* Esigenza principale: il numero di indirizzi è troppo piccolo 

  * Adesso si hanno $2^{128}$ indirizzi, questo significa che potenzialmente il campo indirizzi può descrivere un numero di host infinito.

  * con questo modello quindi si risolvere il problema del numero di indirizzi

    

* il formato d’intestazione di questo nuovo modello rende più veloce la connessione e i processi di elaborazione e inoltro

L’intestazione della parte obbligatoria diventa 40byte e lunghezza fissa (il protocollo IP versione 4 ha lunghezza 20byte). Di fatto anche se il pacchetto è più grande le informazioni contenute sono minori, infatti ci sono meno campi contenuti in questo modello, ma si è data la priorità a contenere indirizzi molto vasti.



<img src="image/image-20211214105705587.png" alt="image-20211214105705587" style="zoom:50%;" />

L’intestazione è sempre a 32bit, ma ci sono meno campi siccome il *source address* e il *destination address* sono 4 righe per campo entrambe.

* campo versione: che indica la versione 6 del protocollo IP
* *priorità di flusso*: attribuisce una certa priorità a dei campi determinati di flusso

* *etichetta di flusso* (**flow label**) attribuisce priorità a determinati datagrammi di un flusso. Questo è un modello per indicare che una certa serie di pacchetti sono dello stesso flusso di dati e quindi non è necessario che il router faccia un elaborazione.
* *payload len*: lunghezza del payload, che indica la lunghezza le pacchetto
* *intestazione successiva* (**next hdr**): indica il protocollo a cui verranno consegnati i contenuti del datagramma
  * di fatto questo campo server a indicare delle eventuali parti opzionali (sempre ridotte al minimo) all’interno del pacchetto.



#### Rappresentazione di questi indirizzi

Solitamente questo indirizzo si scrive in esadecimale => impossibile ricordarsi a memoria gli indirizzi degli host. Quando si vedono i doppi punti allora vuol dire che c’è un gruppo di zeri.

```http
fe80::1400::81c6::a5f1::dd6c%en0
```

Di solito questo indirizzo si concatena con il velore dell’interfaccia (`en0`). 

Logicamente in questa versione del protocollo i dns e tutti questi servizi diventeranno necessari, siccome ricordare questo campo di indirizzi sarà praticamente impossibile.



#### Transizione da Version 4 a Versione6

Il problema è che la maggior parte dei router di accesso sono in versione 4 e non è possibile aggiornare allo stesso momento tutti i router. Di fatto ci sono alcuni servizi che non si possono fermare completamente per un certo tempo. Inoltre deve essere un operazione che deve essere fatte a passi.

Nonostante questo ad oggi molte reti sono alla versione 6, e le connessioni di alcuni paesi anche. 

In pratica per far coesistere le due versioni si utilizza il cosiddetto **tunneling**: si prende un pacchetto ad un certo livello e lo utilizza come payload di un livello che non è successivo. in questo modello se si fa tra due punti allora p come se si facesse passare una connessione dentro l’altra.

<img src="image/image-20211214111446403.png" alt="image-20211214111446403" style="zoom:50%;" />

Questo modello di fatto fa in modo di inserire un pacchetto Ipv6 all’interno di un pacchetto IPv4, oppure viceversa (un pacchetto IPv4 incapsulato dentro un IPv6).

La vista fisica e logica di questo modello è sviluppata in questo modo:

<img src="image/image-20211214111855428.png" alt="image-20211214111855428" style="zoom:50%;" />

Di fatto i router sul confine devono saper gestire entrambi i protocolli, in modo da poter istanziare il tunnel e permette l’inoltro in questo tunnel. Logicamente il funzionamento lavora anche nell’altra direzione.

Questa modalità permette di mantenere la versione 4 utilizzabile, mentre tutta la rete si aggiorna.



La rete IPv6 com’è messa? Secondo Google il 30% dei router di accesso supporta le connessioni IPv6, ovvero che la gente accede direttamente in IPv6. Si possono vedere statistiche aggiornate [qui](https://www.google.com/intl/en/ipv6/statistics.html).

La lentezza di questo cambiamento sono più di una:

* i servizi non sono facilissimi da applicare
* i router di bordo che devono instanziare il tunneling non sono molto economici



Di fatto però la completa conversione permetterebbe di avere una rete più uniforme e più reattiva.



## 4.5 Algoritmi di instradamento

Studiamo quali sono le procedure per trovare il percorso in una rete. Le capacità di inoltro sono sviluppare nel router per mandare i pacchetti da una porta all’altra.

Ci serve ancora conoscere come si fa a trovare una strada su internet. Di fatto ogni router ha una tabella di instradamento locale/inoltro che permetterà al router di inoltrare pacchetti.

Il compito dell’algoritmo di instradamento è il responsabile della popolazione delle tabelle di instradamento  locale.

Questo modello è basato su un algoritmo matematico che deve poi però essere ritrasposto in un protocollo applicabile.

Necessità di distinguere l’indirizzo della sottorete, ma l’indirizzo dell’host è chiaramente unico.

Una sottorete ha necessità di aver associata la rete a un valore di indirizzi variabili.

## 4.5 Algoritmi di instradamento

Studiamo quali sono le procedure per trovare il percorso in una rete. Le capacità di inoltro sono sviluppare nel router per mandare i pacchetti da una porta all’altra.

Ci serve ancora conoscere come si fa a trovare una strada su internet. Di fatto ogni router ha una tabella di instradamento locale/inoltro che permetterà al router di inoltrare pacchetti.

Il compito dell’algoritmo di instradamento è il responsabile della popolazione delle tabelle di instradamento  locale. è importante trovare dei meccanismi che permettano di costruire queste tabelle anche per reti molto grandi.

Questo modello è basato su un algoritmo matematico che permette di trovare i percorsi, che di appoggia alla minimizzazione di una funzione. Poi dobbiamo trasformarlo in qualcosa che consenta ai nodi di parlarsi e di scambiarsi informazioni in maniera standard.

Il modo con cui la rete è collegata in un astrazione matematica e su cui poi si possono pensare gli algoritmi di instradamento è il grafo. In questo modello non andremo molto in profondità allo strumento matematico. Di fatto utilizziamo il grafo siccome utilizziamo la nozione di insiemi di nodi e insieme di archi che collegano gli host. 

Abbiamo i nodi della rete che vengono collegati, poi abbiamo gli archi che sono i collegamenti diretti tra i router, ovvero che hanno tra di loro un collegamento di livello 2 oppure di livello fisico.

Di fatto per capire il grafo è importante sapere cos’è un router e quali sono gli host poi. Di fatto poi attraverso questo metodo è possibile visualizzare bene la rete, poi non pone molto vincoli sulla rappresentazione della rete, ma permette anche di avere una serie di nodi in tutte le configurazioni.

Un esempio di grafo è:

![image-20211215095610546](image/image-20211215095610546.png)

<img src="image/image-20211215095632658.png" alt="image-20211215095632658" style="zoom:50%;" />

Da questo grafico si può capire il costo di collegamento per ogni ramo. Esempio: $c(w, z) = 5$. Generalmente non ci sono costi negativi nelle reti che affronteremo noi. Una volta definiti i costi dei collegamenti diretti, bisogna trovare un metodo per sommare i collegamenti. In questo modo riusciamo a calcolare i costi per andare da un nodo a un altro non direttamente collegato, allora sommiamo i costi dei singoli collegamenti. In base a costi dei collegamenti, la formula per trovare il collegamento migliore bisogna definire il modo per trovare il costo migliore.

Il funzionamento ad alto livello di questo modello è che: si trovano tutti i percorsi possibili, si trova il costo unitario di ogni percorso e si sceglie il percorso migliore selezionando quello con costo minore. Il costo del collegamento di solito è associato dal gestore della rete, che si occupa di assegnare un valore a ogni collegamento. Di solito se i costi sono tutti uguali l’algoritmo troverà il percorso con meno salti (siccome tutti costano uguale, cerco di minimizzare il numero di percorsi che devono essere fatti).

Quindi di solito si riesce a ottenere il funzionamento di questi algoritmi attraverso questi valori di costo assegnati a ogni collegamento. Di fatto più si vuole il collegamento inutilizzato, più si assegna un valore alto al collegamento, in questo modo verrà scelto solo poche volte.

Logicamente l’assegnazione dei costi deve essere fatta in modo coscienzioso, in questo modo si può dare la priorità a alcuni collegamenti migliori di altri, oppure si farà in modo che si preferisca un dato collegamento rispetto a un altro.

### Globale o Centralizzato, Statico o Dinamico

Ci sono due algoritmi di questo tipo, uno che ha necessità di avere tutte le informazioni prima (ha necessità di una visione globale), un altro modello permette di avere una conoscenza distribuita.

I modelli che funzionano a livello globale vengono chiamati **link-state algorithm** oppure **algoritmi a stato di collegamento**. Siccome questo modello è globale, quindi significa che per lanciare questo algoritmo è necessario conoscere tutte le informazioni dei collegamenti della rete.

Il modello a funzionamento decentralizzato sono chiamati **algoritmi a vettore distanza** oppure **DV, distance-vector algorithms**.  I costi di collegamento vengono fatti da ogni nodo in modo distribuito attraverso una stima del collegamento con i nodi vicini. In questo modo il cammino a costo minimo viene calcolato in modo distribuito e iterativo, in modo da trovare le situazioni migliori.

Poi di algoritmi poi ce ne sono tantissimi, migliori/peggiori in prestazioni. Poi le implementazione dei due modelli possono essere equivalenti nei costi, ma l’applicazione è diversa in base agli scopi che si vogliono raggiungere.



Un instradamento statico permette di fare calcoli su dei cammini che cambiano molto raramente. Questo significa che una volta che si elaborano i valori di instradamento allora rimangono costanti per un valore di tempo. Chiaramente questi algoritmi vengono ricalcolati una volta passato un certo valore di tempo, ma di fatto rimangono costanti sulla connessione.

Instradamento dinamico è applicato a delle reti che continuano a cambiare (ad esempio delle reti di veicoli). In questo modello la rete è sempre in un processo di modifica, quindi è necessario che gli algoritmi reagiscano molto frequentemente (anche, ad esempio, che la rete perda un qualche nodo che si disconnette). Altri tipi di reti di questo tipo possono reagire a un volume di traffico diverso o altri cambiamenti della topologia della rete. Questi valori sono costanti durante l’applicazione dell’algoritmo, siccome in caso contrario sarebbe difficile anche effettuare il calcolo. Spesso questa mobilità viene applicata alle reti di accesso che per fornire flessibilità agli host permetto un continuo cambio di valori degli host e delle posizioni degli host.

Questa mobilità non deve essere confusa con il modello di connessione in mobilità che, ad esempio, fornisce il wifi. Questa rete infatti è si senza fili e permette la mobilità degli host, ma di fatto ogni host connesso a un modello di rete di questo tipo ha una topologia costante.

Quando si parla di topologia della rete si parla di grafico. Ad esempio da stella diventa circolare oppure altre modifiche del genere.



### Algoritmo Dijkstra

Questo algoritmo è un algoritmo a **stato di collegamento**.

Per ottenere questo algoritmo è necessario che tutti i nodi conoscano i costi di ogni link. Questo stato dei dati si ottiene attravero un *link-state broadcast*, ovvero tutti i nodi comunicano ai vicini le informazioni dei collegamenti che possiedono, in modo che tutti i nodi abbiano la visione completa. Questa operazione è costoso dal punto computazionale e di banda, infatti si inonda la rete con un sacco di pacchetti di stato tra i nodi della rete, motivo per cui di solito è meglio lanciare questo algoritmo poche volte e soprattutto quando la rete è più scarica. Inoltre se la rete è stabile non è necessario continuare a lanciarlo.

Questo algoritmo funziona attraverso il calcolo svolto su un nodo di tutti i percorsi, quindi di fatto la spiegazione viene fatta da un unico nodo. Di base viene fatto un calcolo del costo di ogni collegamento da un nodo iniziale a tutti gli altri.

Una volta sviluppati questi algoritmi si salvano tutti i migliori collegamenti che serviranno poi a costruire la **tabella di inoltro**.

Questo algoritmo indica che se c’è un certo numero di nodi su questa rete, allora esiste un certo numero di direzioni. 

Questo algoritmo è lineare in funzione dei nodi della rete di cui dobbiamo calcolare il collegamento. Infatti la $k$-esima iterazione di questo algoritmo permette a $k$ nodi di conoscere i costi minimi delle sue connessioni.

Per spiegare questo algoritmo è necessario definire dei valori:

* $c(x, y)$: costo dei collegamenti dal nodo $x$ al nodo $y$. Questo costo è uguale a $\infty$ se i nodi non sono adiacenti, se no il valore è un numero intero.

* $D(v)$ costo del cammino dal nodo di origine alla destinazione $v$ rispetto all’iterazione dell’algoritmo corrente. 

  Questo costo varierà in base all’interazione, quindi finché l’algoritmo non è terminato si avranno una serie di valori parziali.

* $p(v)$: questo valore indica il predecessore del nodo di arrivo $v$.

* $N’$ è il sottoinsieme di $N$ (dove $N$ è l’insieme di tutti i nodi della rete su cui si sta lanciando l’algoritmo) per cui il cammino a costo minimo dall’origine è definitivamente noto.

  Infatti poi vedremo come ad ogni passo questo algoritmo sappia identificare un percorso che ha costo unitario minimo tra tutti i percorsi.



```pseudocode
// inizializzazione// inserimento del nodo U nella reteN_primo = {U}// da questo momento si calcola il costo di ogni collegamento diretto// indichiamo con V un nodo di Uwhile(V)	if(V.isDirect(U)):		D(V) = c(U, V);	else:		D(V) = INF;		// inizia il ciclo => Continua finché non si svolge il calcolo su tutti i nodiwhile(N != N_primo) 	// determina un nodo W, non in N_primo tale che D(W) sia minimo	// aggiungi W a N_primo	min = W_1;	forEach(W)		if(W < W_1):			min = W	N_primo.add(W);		// Aggiorno D(v) per ogni nodo V adiacente a W e non in N_primo	D(V) = min( D(V), D(W)+c(w, v));    // questo significa che il *nuovo costo del percorso verso V* è:    // - il vecchio percorso verso V    // (oppure)    // - il costo del cammino noto minimo (D(W)) sommato al costo del percorso da W a V
```

Un esempio di questa applicazione può essere:

![image-20211215115215084](image/image-20211215115215084.png)

Chiaramente il risultato sarà visualizzato in funzione del nodo che ha richiamato l’algoritmo, e in questo modo riesce a calcolare la via più veloce per raggiungere tutti gli host della rete, avendo come punto di partenza se stesso. L’algoritmo lanciato in $U$ infatti restituisce questi valori (che contribuiscono alla creazione della tabella di inoltro):

![image-20211215115859738](image/image-20211215115859738.png)







Una visualizzazione e spiegazione visuale dell’algoritmo in 3 minuti è visibile a [questo algoritmo](https://www.youtube.com/watch?v=_lHSawdgXpI).

La complessità di questo algoritmo, implementato nel modello più efficace possibile è $O(nlogn)$.



Si noti una cosa: in questo algoritmo si utilizzano i costi del collegamento come indicazione della quantità di traffico. Di base questa pratica non è *mai* consigliata siccome:

1. per definizione dell’algoritmo si sceglierà la sezione di rete sempre meno carica

2. la rete sarà carica in base alle scelte precendenti, quindi si otterrà un continuo cambiamento di routing rispetto all’istradamento iniziale, che potrebbe causare instabilità alla rete.

   ![image-20211215120554969](image/image-20211215120554969.png)

   Questo fatto non sarebbe un problema, se non fosse per l’RTT del collegamento così variabile. Infatti, soprattutto TCP (che ha necessità di fare un calcolo dell’RTT) potrebe avere dei problemi di stabilità e di funzionamento, siccome si otterrebbero dei valori teorici molto falsati che non rispecchiano sempre la stessa rete fisica e che quindi sono soggetti a più errori. Questo potrebbe causare eccessivi timeout, oppure un throughtput molto ridotto a causa di perdita di pacchetti frequenti.

### Algoritmo con vettore distanza

Questo algoritmo proviene dalla programmazione dinamica: si risolve un problema attraverso la scomposizione in problemi più piccoli e più facilmente risolvibili distribuiti su più agenti.

L’algoritmo che prevede questa applicazione si chiama **Bellman-Ford**. Per capire questo algoritmo è necessario sapere che $d_x(y)$ è il costo del percorso a costo minimo dal nodo $x$ al nodo $y$. Il secondo concetto è il calcolo di questo valore: $d_x(y) = min_v(c(x, v)+d_v(y))$.

Questo valore deve essere il minimo tra tutti i vicini di $y$.

Immaginare che tutti i router vicini informino il nodo richiedente del valore di risposta di un determinato nodo. In questo modo, attraverso questa formula voglio trovare il migliore tra i collegamenti, sommando il valore dei vari nodi, facendo però una richiesta a ogni pezzo della rete.

Questo algoritmo lavora in maniera distribuita, ovvero fa i calcoli per se stesso e poi propaga le informazioni. Piano piano ogni nodo conosco anche le informazioni per i nodi più lontani, in questo modo posso sapere anche le informazioni dei vicini. 

Questo funzionamento fa in modo da richiedere il valore del percorso minimo per un determinato tragitto ai vicini e poi sommare il proprio costo per raggiungere il vicino, infine minimizza.

Uno dei vantaggi di questo algoritmo funziona attraverso questo modello distribuito, facendo così ogni nodo calcola per se stesso e poi informa i vicini. Così facendo la potenza computazionale necessaria è minore e distribuita su tutta la rete, e a nessun nodo è richiesto informarsi su tutti i nodi.

In questo modo inoltre si diminuiscono anche i pacchetti da mandare sulla rete, infatti la rete non ha bisogno di mandare tutti i percorsi, ma solo i minimi. Inoltre questo scambio di pacchetti si ferma automaticamente quando tutti i nodi hanno ottenuto il valore minimo.



Con questo algoritmo vengono condivise solo le informazioni che permettono di avere la tratta minore. Inoltre i costi di questi circuiti vengono aggiornati (diminuendo il costo del collegamento) allora questa informazione probabilmente influenza un sacco di percorsi e viene condivisa con gli altri nodi velocemente, invece se una distanza vettoriale aumenta, allora più difficilmente i percorsi possono cambiare. Così facendo questo modello punta sempre a scegliere i costi migliori tra tutti i percorsi. L’unico modo per far viaggiare velocemente anche le informazioni negative sulla rete allora è necessario impostare un valore massimo della distanza di collegamento. In questo modo quando si raggiunge quel valore allora il percorso viene definito come $\infty$ e quindi tutto il percorso cade e viene annullato, portando a cercare un alternativa.



Link della visualizzazione di questo algoritmo a questo [link](https://www.youtube.com/watch?v=9PHkk0UavIM).



### Confronti tra i due algoritmi

Complessità dei messaggi:

* LS (Dijkstra): durante la fase bisogna mandare un numero di massaggi nel valore di $O(nE)$ ($n$ nodi e $E$ collegamenti). Questo valore è molto elevato (soprattutto se la rete è molto grande).

* DV (Bellman-Ford): siccome il tempo di convergenza è variabile questa fase può portare alla produzione di un numero di messaggi variabile e dipende dalla velocità con cui si propagano queste informazioni. 

  Questo significa anche che la situazione potrebbe essere più instabile per più tempo.

Velocità di convergenza:

* LS: l’algoritmo ha complessità $O(n^2)$ e richiedere di mandare $O(nE)$ messaggi, per cui ci possono essere oscillazioni di velocità.

  Non può presentare cicli di instradamento (quindi se un valore è circolare allora si otterrà un valore continuamente variabile fra due) e di fatto la rete non sarà mai stabile.

* DV: la convergenza può avvenire molto più lentamente e può presentare cicli di instradamento.

  Può presentare il problema del conteggio dell’infinito, ovvero non dando un upperbound allora è difficile che la peggiore strada possibile venga identificata e eliminata causando una modifica dei percorsi istantanea.

Robustezza: (cosa succede se un router funziona male)

* LS: se il router funziona male allora il router rappresenta un errore solo per se stesso, infatti tutti i calcoli per tutti i nodi vengono fatti in locale, creando un problema di instradamento sbagliato solo sul suo nodo.
* DV: in teoria il nodo che fornisce informazioni sbagliate potrebbe portare all’indirizzamento sbagliato di molti nodi della rete, siccome la sua informazione sarebbe ridistribuita su tutta la rete.



### Instradamento Gerarchico

Il problema degli algoritmi di instradamento è che sono molto dispendiosi sulle reti molto grandi, motivo per cui è necessario trovare un modello che ne riduca la complessità.

Inoltre si avrebbe un problema di archiviazione, infatti non si potrebbero immagazzinare tutti i dati di tutti gli host su ogni nodo della rete siccome sarebbero troppe informazioni.

Questo modello viene trovato con il concetto di internet per definizione, ovvero che la rete è una rete di reti.

Esiste quindi una serie di algoritmi che permettono di fare queste operazioni su delle isole interne. Poi esistono una serie di router che conoscono i metodi di implementazione per interfacciarsu sulla rete esterna e che quindi hanno necessità di conoscere tutti gli algoritmi di routing.

Ogni isola in questo schema si chiama *sistema autonomo (AS - **autonomus system**)* e permette di avere delle organizzazioni interne e esterne alla rete. Di solito questa entità eseguono lo stesso algoritmo di instradamento, che infatti vengono divisi tra **intra-AS** (protocolli di instradamento all’interno del sistema autonomo). Router appartenenti a diversi AS possono eseguire protocolli di instradamento intra-AS diversi (potenzialmente si possono scrivere dei protocolli di instradamento completamente proprietari, se si vuole ottenere un determinato obiettivo). 

**Inter-AS** sono i protocolli di instradamento *tra* i sistemi autonomi permettono di collegare i sistemi autonomi. Questi router, chiamati **router gateway** hanno la possibilità di far girare internamente il sistema di instradamento interno, all’esterno la IETF ha ideato un protocollo che permette di collegare le diverse isole di traffico, chiamato **BGP**.

Di fatto questo algoritmo permette di capire tra quali isole di traffico far passare il pacchetto in modo che sia il più veloce possibile, calcolo che è rimandato all’algoritmo inter.



#### Protocolli IGP (Inter Gateway Protocol)

Questi protocolli sono conosciuti anche come intra-AS. Servono quindi a gestire l’instradamento del traffico interno a una rete.

I protocolli intra-AS più comuni sono:

* **RIP**: routing informazion protocol
* **OSPF**: open shortest path first
* **IGRP**: interior gateway routing protocol



##### RIP (Routing Information Protocol)

è un protocollo a vettore distanza. Utilizza come metrica il numero di hop che l’host deve fare, e inoltre è di default su UNIX BSD dal 1982 (quindi piuttosto antico).

Ogni collegamento costano uguale (1) e quindi i collegamenti diretti saranno solo $1$ hop. Il massimo numero di hop contabili è 15, quindi è impostato l’infinito a 16 salti. In questo modo non si ha mai un collegamento che fa più di 15 salti di connessioni tra i nodi.

In questo protocollo i nodi si scambiano un messaggio di risposta RIP (RIP advertisement, oppure messaggio di annuncio RIP). Questo pacchetto indica le distante relative ad un massimo di 25 sottoreti di destinazione all’interno del sistema autonomo.

Guasto di comunicazione e advertisement: quando il nodo adiacente non risponde per un intervallo di 180 secondi (ovvero 6advertisement) allora il nodo adiacente o il collegamento vengono ritenuti spenti o guasti, e di fatto si ricalcola la tabella di routing locale. Dopodichè propaga l’informazione mandando annunci ai router vicini, che mandano nuovi messaggi se la loro tabella d’instradamento è cambiata a sua volta. Di fatto l’informazione di propagazione di un router guasto si propaga velocemente su tutta la rete. Si può utilizzare anche la tecnica dell’**inversione avvelenata**, ovvero si indica quel collegamento come numero di hop infinito (16hop), in questo modo questo percorso non può essere scelto da nessuno e si procede al ricalcolo di tutti i percorsi.

Questo calcolo viene fatto da un processo, chiamato **routed** all’interno del sistema operativo UNIX. Questo processo è quello che implementa RIP. Questo è un processo particolare che ha i privilegi per scrivere nelle tabelle di inoltro (cosa particolare, siccome sono solitamente riservati a livello tre questi privilegi). Questo modello esce leggermente dal contesto del 3livello, ma permette di avere una comunicazione su dei pacchetti UDP standard. Questa soluzione è abbastanza interessante, siccome non necessita di nessun altro protocollo di comunicazione, ma si appoggia a questo già esistente.



##### OSPF (Open Shortest Path First)

Questo algoritmo esegue un implementazione di DJikstra. Il protocollo è definito *open* siccome tutte le specifiche e il codice sono disponibili pubblicamente. é un algoritmo molto utilizzato nella realtà.

Questo protocollo utilizza il *flooding* di informazioni di stato del collegamento per trasferire tutte le informazioni di stato su tutti i nodi.

Quando un router si accorge che in una connessione ci sono costi diversi, allora informa in broadcast tutti gli altri nodi della rete, in questo modo la modifica dei percorsi avviene in seguito a ogni modifica dei percorsi e fa in modo di modificare solo i nodi che questa modifica potrebbe cambiare il percorso minimo. Di fatto questo farà il triggering di un nuovo Dijkstra. Il flooding avviene solo all’inizio, quando viene lanciato la prima volta oppure viene lanciato manualmente. 

Questo protocollo intra-AS tutti i messaggi sono fatti all’interno del AS (non si tratta di un broadcast vero e proprio, siccome possono attraversare i router, senza essere bloccati alla sottorete). Tutte le comunicazioni avvengono attraverso il protocollo IP vero e proprio, senza dei protocolli di livello 3 (UDP o TCP). 

Ci sono alcuni vantaggi a utilizzare OSPF:

* utilizza la variante sicura di IP, di fatto quindi questo protocollo permette l’autenticazione dei router. In questo modello quindi si riesce a raggiungere i router in modo autenticabile, verificandone l‘autenticità

* quando più percorsi hanno lo stesso costo, questo algoritmo permette di utilizzarli senza doverne scegliere uno in particolare, cosa che invece avveniva nel RIP

* ulteriormente all’associazione costo-collegamento, si può avere un associazione relativa al **TOS (type-of-service)**, ovvero associare un costo del servizio in base al tipo di servizio.

  Per ottenere questo modello si utilizzano dei campi flag di IP, che permettono di indicare il tipo di serivizio e quindi associarci il costo della linea, in questo modo posso avere dei costi diversi rispetto al tipo di comunicazione/flusso dei pacchetto, ovvero che posso avere un costo relativo al tipo di applicazione che sta mandando flussi di dati (ad esempio posso dare una bassa priorità ai flussi normali, mentre un alta priorità ai flussi di dati real-time).

*  ha un supporto integrato diretto per unicast e multicast.

  * *unicast* è una connessione che va da punto a punto
  * *multicast* è una connessione che va da un punto a molti (ad esempio un applicazione di video streaming). Solitamente c’è una lista di host da raggiungere e vengono connessi (solitamente) con una struttura ad albero. Questo tipo di collegamento viene ottenuto con l’instradamento MOSPF (multicast OSPF). Di fatto questo funziona cerca di costruire un percorso in cui cerca di evitare la duplicazione dei dati, ovvero fa del traffico unico finché non si deve dividere. 

* supporto alle gerarchie di instradamento:

  * OSPF, essendo più complicato rispetto al protocollo RIP, permette di avere una costruzione gerarchica. 

  * molto utile quando si lavora in una SA molto grande, in modo da strutturare le flooding e permettere di avere troppo traffico sulla stessa SA relativo all’instrademento.

    ![image-20211216100935124](image/image-20211216100935124.png)

    Per fare questo bisogna definire questi *area borders routers*, che permette l’unione di tutte le varie aree. Inoltre è necessario avere dei router *backbone*, ovvero i router necessari a riunire le aree.



#### Protocolli BGP (Border Gateway Protocol)

Questi protocolli sono responsabili di permettere l’instradamento tra le reti inter-AS, permettendo le reti intra-AS.

Di fatto il protocollo BGP è lo standard *de facto*, infatti è stato sviluppato e applicato prima di essere stato standardizzato effettivamente. In seguito si è dimostrato particolarmente efficace, quindi è rimasto.

Questo protocollo unisce delle reti, facendo in modo che ogni AS permetta l’implementazione del proprio protocollo di instradamento. Il problema di questo tipo di rete è che poi tutti queste sottoreti devono essere collegate in maniera univoca, in modo che siano tutte compatibili.

La granularità di questi sistemi però è molto minore, infatti non si indica tutta la strada all’interno dell’AS, ma semplicemente che si possono raggiungere definiti AS. Logicamente deve trovare il percorso buono (non ottimi o a costo minimo), infatti l’obiettivo di questo protocollo non è prestazionale, a differenza di RIP e OSPF, deve solo trovare la strada da percorrere. 

I collegamenti tra questi router di solito avvengono tramite accordi tra gli ISP, e questi accordi vanno rispettate in maniera prioritaria. Questo significa che quando due provider si accordano per far passare il traffico da una parte all’altra della loro rete. In questo modo RIP e OSPF sono molto diversi, infatti forniscono semplicemente il percorso più breve dal punto di vista matematico.





BGP viene messo a disposizione dei vari AS, e fornisce una serie di servizi:

* ottenere informazioni sulla raggiungibilità delle sottoreti da parte delle AS confinanti, ovvero indicano che AS sono raggiungibili attraverso questo AS.
* propagare le informazioni di raggiungibilità a tutti i router interni di un AS
* determinare percorsi “buoni” verso le sottoreti sulla base delle informazioni di raggiungibilità secondo le politiche dell’AS. Questo significano che hanno delle operazioni da scegliere per decidere in quale isola passare. In questo algoritmo quindi è necessario un compromesso tra politiche e i percorsi realmente fattibili.
  * Di fatto questo permette di avere dei percorsi scelti in modi diversi: si possono avere delle politiche in modo da scegliere diversi percorsi rispetto ad altri. Questo modello di solito si basa su dei contratti vantaggiosi tra i provider. Il percorso in questo caso non sarà mai quello a costo minore, ma quello che secondo le politiche ha costo minore. 



I nodi di questa rete sono chiamati **peer BGP** e sono i router esterni delle reti intra-AS.

Nonostante BGP serve per interconnettere due protocolli intra-AS, ha due varianti che di fatto lavorano internamente e esternamente alla rete:

* sessione eBGP:
  * connessioni di raggiungibilità e di routing effettivamente
* sessione iBGP
  * sono delle connessioni BGP internet
  * Sono dei collegamenti interni alla rete che fanno sapere ai router dove inoltrare i pacchetti nel caso di comunicazioni indirizzate a zone esterne alle intra-AS. 
  * Queste informazioni riflettono gli accordi presi tra gli ISP, in modo che i router conoscano solo le vie disponibili fornite dai gestori della rete per comunicare. In questo modo non utilizzeranno che quelle.
  * Di fatto quindi questo protocollo non permette di avere più di un possibile tragitto percorribile, ma solo la decisione finale. Questo annuncio di rotta è fatto in modo che ogni router informato di questa rotta possa dirigere i pacchetti che necessitano quella connessione in quella direzione, in base alle informazioni che possiedono i vari router

Come funziona il BGP:

* quando un router annuncia un prefisso per una sessione BGP, include anche un certo numero di attributi BGP. Questi di solito contengono i prefissi di sottorete, in più una serie di attributi e una rotta.

* Questi attributi possono essere:

  * *AS-PATH* indica i sistemi autonomi attraverso cui si è venuti a conoscenza dell’informazione, attraverso quali è passato l’annuncio del prefisso.

  * *NEXT-PATH*: quando si inoltra un pacchetto tra due AS, qual’è, da parte del mittente, il prossimo router a cui mandare i dati per raggiungere quel router di quel prefisso. In questo modo si può sapere qual’è il router gateway a cui indirizzare i pacchetti in uscita.

    Indica il router che può portare i pacchetti all’esterno del AS. Chiaramente questi percorsi possono essere importati oppure no.

* quando un router riceve un annuncio utilizza le **politiche di importazione** per decidere se accettare o filtrare la rotta. ogni informazione di rotta, il router decide se filtrare via la connessione oppure se importare la rotta.

  * Questa politica significa anche che il router, accettando la rotta della connessione, si impegna a risolvere il percorso che porta al router gateway. Di fatto quindi si può anche decidere di importare la rotta e non pubblicizzarla, oppure non importala proprio, oppure ancora importarla e pubblicizzarla (se si vuole che altri router utilizzino quest’ultimo per mandare pacchetti all’esterno dell’AS).
  * Di solito quando viene recapitata una regola di comunicazione, il router ha necessità di capire quale scegliere. Per fare questa operazione vengono applicate le regole di eliminazione:
    * Alle rotte viene assegnato come attributo un valore preferenza locale. Si selezionano solo i percorsi con valore di preferenza maggiore
    * Si seleziona la rotta con valore AS-PATH più breve (uso la sequenza più breve). Più questa informazione è alta più il router è lontano
    * Se no si sceglie il router *NEXT-HOP* più vicino, ovvero si minimizza il numero di salti. Questo modello si chiama instradamento **patata bollente**.
    * se rimane ancora più di una rotta, allora il router si basa sugli identificatori BGP.



I messaggi BGP vengono mandati attraverso il TCP (quindi di fatto è un protocollo modello applicazione). Il fatto di usare TCP permette di usare le socket sicure (TCP-TLS), limitando quindi la possibilità di avere intromissione fra i nodi.

Messaggi:

* *OPEN*: apre la connessione e autentifica il mittente
* *UPDATE*: annuncia un nuovo percorso (o cancella quello vecchio)
* *KEEP ALIVE*: serve per non far andare in timeout la connessione TCP in mancanza di altri pacchetti
* *NOTIFICATION*: riporta degli errori del precedente messaggio o utilizzato per chiudere la connessione.



Le politiche di instradamento BGP sono diverse, siccome la rete è molto diversificata in se stessa:

![image-20211220102055763](image/image-20211220102055763.png)

![image-20211220102120453](image/image-20211220102120453.png)





Le reti stub sono delle reti di accesso che possono (*X*) anche avere dei diversi collegamenti. Di fatto i router di bordo sono programmati per informare le sottoreti. Inoltre gli accordi di BGP possono essere stipulati con i vari ISP, in questo modo si possono usare dei router di bordo come router BGP per backup oppure per altre informazioni.



La differenze tra INTRA e INTER AS:

*  Politiche:
  * inter: il controllo amministrativo vuole avere il completo controllo su come il traffico viene instradato e su chi instrada attraverso le sue reti.
  * intra: un unico controllo ammministrativo, di fatto quindi le politiche amministrative hanno meno peso e rispetto allo scegliere delle rotte veloci all’interno della connessione.
* Scala:
  * l’instradamento gerarchico fa risparmiare sulle tabelle d’istradamento, riducendo il traffico dovuto all’aggiornamento di queste.
* Prestazioni:
  * Intra-AS: è prensato per avere massime prestazioni all’interno della rete, siccome è necessario ci sia il percorso più breve in assoluto all’interno della rete
  * inter-AS: è pensato per dare primo aiuto alle politiche di instradamento dei provider, e quindi lasciare le prestazioni al secondo posto.

 
