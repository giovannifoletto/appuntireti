# Appunti del corso di RETI (Networking)

> Appunti dalle lezioni di Fabrizio Granelli (UniTN)
> Anno Accademico 2021/2022

Author: Giovanni Foletto
mail:   giovanni.foletto@gmail.com

# PDF Rendering

Il pdf creato da questi appunti è scaricabile/visualizzabile al seguente [link](https://drive.google.com/file/d/1xfGROl73LGpJoCBNhAD0SROVQu2ZxvIh/view?usp=share_link)


## Organizzazione della Repository

La cartella `image` contiene tutte le immagini presenti nei documenti, a cui si fa riferimento attravero un relative path `./image/<nomeImmagine>`.

Tutti i file markdown (`.md`) presenti contengono ognuno un capitolo trattato, eccetto il primo file che contiene anche tutte le informazioni di formattazione, rendering e informazioni, oltre che l'indice. Questa sezione è consigliato lasciarla così com'è.

Capitolo 1 e 2 sono assieme nel file `01_Reti.md`.

Il `Makefile` contiene il comando di *default* per creare il pdf, mentre, se si lancia `make create-latex` si creerà il documento in prerendering in latex (utile per debugging dello stile, ogni tanto rompe qualche immagine).

## Funzionamento

Tutti gli appunti presi in markdown, sono convertiti in `latex` e successivamente in `pdf` dal programma [pandoc](https://pandoc.org/). Questo fornisce anche la creazione dell'indice.
Si utilizza un template chiamato [eisvogel](https://github.com/Wandmalfarbe/pandoc-latex-template) per lo stile del documento.
Lo script di creazione del file si può trovare nel `makefile`.

## Contribuisci

Se si trovassero errori, frasi di difficile comprensione, concetti poco chiari è ben accetta un PR, che verrà visualizzata e accattata il prima possibile. 
Allo stesso modo, se si hanno consigli sull'estensibilità degli appunti, oppure se è necessario fare delle aggiunte saranno trattate allo stesso modo.

