default:
	pandoc 01_Reti.md 03_LivTrasporto.md 04_LivRete.md 05_LivCollegamento.md -o AppuntiReti.pdf --from markdown --template eisvogel --listings --top-level-division=chapter --toc --pdf-engine=xelatex
create-latex:
	pandoc 01_Reti.md 03_LivTrasporto.md 04_LivRete.md 05_LivCollegamento.md -o AppuntiReti.tex --from markdown --template eisvogel --listings --top-level-division=chapter --toc --pdf-engine=xelatex
