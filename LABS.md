# Laboratori di Reti

## INDEX

[TOC]

## Laboratorio 1

### Wireshark

Wireshark è un analizzatore di rete, questo significa che permette:

* di analizzare il traffico di centinaia protocolli
* di effettuare analisi live
* il *dumping* del traffico analizzato, ovvero il salvataggio del traffico in un file leggibile successivamente
* di essere eseguito su qualsiasi macchina, infatto è multipiattaforma

Si sono poi viste il traffico di rete dovuto a diversi protocolli, e qualche funzionalità che offre il lettore di pacchetti Wireshark.

Alcune di queste funzioni sono replicate nel piccolo anche sul browser, attraverso l'esplora risorse di rete.

## Laboratorio 2

Un altra introduzione infinita a docker.

