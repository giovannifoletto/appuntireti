# Capitolo 5: Livello di collegamento e Reti Locali

Questo livello 2 è caratterizzato da alcune similitudini con il livello 3, ma l’ambito è diverso. Questo livello è sotto il livello 3. Di fatto al livello 2 si può avere qualsiasi protocollo che permette di avere un collegamento con il resto della rete. Di fatto però a livello 3 è necessario avere un protocollo IP. 

Obiettivi:

* comprendere i principi per implementare i servizi di trasmissione dati:
  * condivisione del canale broadcast: solitamente tutti i canali di comunicazione comunicano su dei canali con accesso multiplo (vedi ad esempio il wifi). Il problema diventa capire come fa coesistere più connessioni nello stesso canale senza avere blocchi di condivisione.
  * Indirizzi a livello di collegamento 
  * Trasferimento affidabile dei dati e controllo di flusso, già visto nel livello di trasporto, ma di fatto in questo layer è più semplice.



## 5.1 Livello di collegamento: introduzione e servizi

Il livello di collegamento ha come ambito i collegamenti tra gli hosts, ovvero i link tra i diversi hosts. Questo livello si occupa dei singoli link o delle sottoreti.

Le unità scambiate da questi protocolli a livello di link sono chiamati **frame** (in italiano *trame*, ma mai utilizzato in questo senso).

Il datagramma di livello 3 può essere gestito da protocolli di livello 2 diversi (e anche delle tecnologie di livello 2 differenti), nonostante sia sempre lo stesso datagramma. 

Il livello 2 esegue di base una serie di servizi:

* **Framing**: il livello 2 ha necessità di creare una struttura dati per racchiudere le informazioni del trasporto di dati.
  * generalmente inserisce un valore di header e trailer nel datagramma, in modo da identificare l’inizio della connessione. Di solito questi valori sono delle sequenze di bit fisse e quando questa sequenza viene identificata, si lascia partire la comunicazione.
  * Problema della sincronizzazione della trasmissione: uno dei problemi più difficili da risolvere è sincronizzare il clock del mittente e del ricevente. Di fatto è molto difficile avere un clock perfettamente sincronizzato tra i molti sistemi. Di solito nei sistemi elettronici c’è un clock-master che distribuisce il clock tra tutti gli elementi.
    * Di solito il clock è una onda rettangolare e di base si fa una salita e una discesa, e di solito spesso ha dei drift di frequenza (ovvero ogni tanto è leggermente diversa la frequenza ricevuta rispetto a quella fissa).
    * Avendo il framing il sistema riesce a riaggiustare il proprio sistema di clock rispetto a quello del sistema. Vedendo una sequenza di bit di questo tipo si ricontrolla la correttezza del proprio clock. Altri sistemi trasmettono il proprio clock e quindi controllano in questo modo.
    * Di fatto questi problemi sono stati risolti in molti modi differenti, quindi non è più un grandissimo problema, ma nelle fasi iniziali della rete era effettivamente un problema da non sottovalutare.
* **Consegna affidabile**: c’è la possibilità di avere dei collegamenti affidabili, in modo da ricevere conferma della connessione avvenuta correttamente.
* **controllo di flusso**: evita che il nodo trasmetta troppo
* **rilevazione di errori**: di solito questa azione viene fatta in molti livelli. Di solito in questo livello sono causati da attenuazione del segnale e dal rumore elettromagnetico. Gli errori vengono individuati dal nodo ricevente.
* **correzione degli errori**: questo livello ha anche la possiblità di correggere degli errori, individuando il nodo che ha causato errori e correggendoli.
  * Nella connessione satellitare (caratterizzati da un alto ritardo della connessione) c’è anche la possibilità di avere il recupero dei dati, ovvero il recupero dei dati in caso di corruzione. Questo però viene a scapito della banda, siccome è necessario avere un sacco di bit di contorno per controllo.
* **half-duplex e full-duplex**: nella trasmissione ci possono essere modelli di half-duplex o full-duplex.
  * Nell’half-duplex è possibile avere un solo pacchetto per connessione, ovvero o un host riceve o trasmette, ma non è possibile fare questa operazione in entrambe le direzioni 
  * solitamente nelle reti via cavo si ottiene una connessione full-duplex anche perché si ha il doppio collegamento, oppure (nella fibra ad esempio) il canale permette di avere più segnali al suo interno senza che si ottengano interferenze.

Il livello di collegamento di solito è l’interfaccia tra il software e l’hardware. Questo si nota di solito quando esce un nuovo standard, che non si può utilizzare un nuovo protocollo senza del nuovo hardware. Di solito gran parte delle funzionalità del livello due è programmabile a livello firmware oppure direttamente hardware. 

Questa caratteristica differenzia questo livello dal resto della rete, siccome differenzia tutta la pila di fatto software dal livello hardware.

Alla meglio il livello 2 è una combinazione di software, firmware e hardware. Ad oggi però si sta andando sempre di più per l’estensione del software a sempre più parti della rete, con le considdette **SDR (Software Defined Radios)**. Attraverso queste tecniche si utilizzano degli elementi hardware che interagiscono direttamente via software. Il problema di questi apparati (ovvero attraverso una serie di interfacce software), si hanno dei costi di applicazioni decisamente più elevati. In qualsiasi caso questo significa che nel futuro è possibile che il sistema software di queste tecnologie saranno sempre più disponibili via software, rendendo disponibile un aggiornamento dei protocolli più costante (e soprattutto senza necessità di acquistare ulteriore hardware).



## 5.2 Protocolli di accesso multiplo

Questa sezione del protocollo riguarda due alternative per costruire il collegamento. Queste tecniche dipendono direttamente dal canale che si vuole utilizzare:

* collegamento **punto-punto (PPP)**: ho un mezzo dedicato, e quindi non ho problemi di condivisione di banda o altri mezzo
* collegamento **broadcast** (cavo o canale condiviso): 
  * questi modelli sono applicati a dei collegamenti via cavo o delle reti connesse tramite cavo unico (che quindi prende la forma di un unico bus). In questo modello è possibile solo una comunicazione per volta.
  * è utilizzato in alcuni metodi di livello 2 tradizionali.
  * Il wifi utilizza questa tecnica, e in questo modo si utilizza una banda diversa in base a quale host sta comunicando. Di solito si utilizza una sola banda in base a cosa si vuole fare
  * la prima connessione broadcast è stata fatta tramite collegamento satellitare per l’università delle Hawaii (nel periodo in cui lasciare i cavi sottomarini non era molto comune)



Il protocollo di accesso multiplo viene applicata a un canale broadcast condiviso. In questa modalità di connessione si hanno centinaia o migliaia di nodi in comunicazione diretta sul canale, e si può ottenere un evento di **collisione**, ovvero quando i nodi ricevono due o più frame contemporaneamente

I protocolli di accesso multiplo fissano le modalità con cui i nodi devono organizzarsi. Di fatto questi meccanismi sono studiati per avere un canale fuori banda (ovvero un secondo canale che tutti ascoltano per la coordinazione). Questo canale di controllo è necessario per indicare su alcuni mezzi le comunicazioni necessarie alle comunicazioni. Di solito questo metodo ha un applicazione più facile, ma di fatto spreca molte risorse. Per questi motivi si vuole avere un canale in-band, con connessione unica che permetta di avere un controllo implicito.

Prendiamo per esempio il canale di broadcast del wifi (una versione più antiquata):

* questo canale divide la capacità totale della rete tra gli host che partecipano alla connessione. 
  * Mettendo che il nodo disponga di un tasso trasmissivo di $Rbps$, e $M$ nodi devono inviare dati, allora il tasso trasmissivo è pari a $R/Mbps$.
* Non si vuole un *one-point-of-failure* e quindi si prevede un modello di protocollo decentralizzato in cui
  * non ci sono nodi master che amministrano la rete
  * non c’è la possibilità di sincronizzare i clock degli host che partecipano alla rete.
* il protocollo deve rimanere il più facile possibile



I protocolli ad accesso multiplo sono suddivisibili in 3 categorie:

* protocolli a **suddivisione del canale** (channel partitioning)
  * La suddivisione delle risorse del canale solitamente avviene a gruppi, ovvero in “parti più piccole”, suddivise in slot di tempo, frequenza e codice
* protocolli ad **accesso casuale** (random access)
  * i canali non vengono divisi e, siccome ogni host può comunicare quando vuole, si può sempre verificare una collisione
  * i nodi coinvolti ritrasmettono ripetutamente i pacchetti
* protocolli a **rotazione** (“taking-turn”)
  * ciascun nodo ha il suo turno di trasmissione. A differenza della suddivisione del canale, in questa tecnica c’è un massimo di banda che può utilizzare ogni connessione



La maggiore popolarità tra questi è il modello a rotazione. 

Solitamente la suddivisione delle connessioni del canale sono **TDMA (Time Division Multiplexing Access)**, ovvero si hanno dei turni che si ripetono. A questi turni sono associati degli utenti e i turni si ripetono continuativamente in intervalli di tempo predefiniti. C’è il vantaggio che ogni stazione ha una capacità sempre garantita, ma di fatto ci sono degli effetti collaterali soprattutto se la fonte non è continua. I buchi creati nel diagramma rimangono nella connessione. Questa soluzione funziona bene quindi solo se tutte le stazioni trasmettono con lo stesso bitrate. Di fatto queste connessioni hanno le caratteristiche giuste per la telefonia, infatti riserva la giusta quantità di banda permettendo un collegamento affidabile e costante su tutte le linee. Funziona molto peggio quando si ha dei flussi di dati molto differenziati o di applicazioni digitali che creano dati a bitrate non costanti.

Solitamente funziona bene il **FDMA (Frequency Division Multiplexing Access)** che permette di accedere alla connessione con una frequenza differenziata per ogni host. In questo modo si hanno delle comunicazioni separate in frequenza, a cui ciascuna stazione è assegnata una banda di frequenza prefissata. In questo modo si hanno delle divisioni in frequenza che permettono la divisione della banda totale tra tutti gli hosts. In questo modello però tutte le stazioni dispongono di un sottomultiplo della capacità totale del collegamento. 

Questo collegamento permette di avere dei canali con un bit-rate garantito, inoltre siccome i canali sono divisi è sempre garantito che la determinata stazione abbia accesso a quella determinata banda. In questo modello permette anche un controllo di congestione, infatti o sono disponibili le risorse per accettarvi sulla rete oppure si viene semplicemente tagliati fuori. In questo modo o tutte le risorse sono utilizzate e non si accettano più altri host, oppure si può continuare a accettare host in connessione fino alla saturazione della banda.

Il protocollo di **accesso casuale** non significa che viene lasciato tutto al caso, ma semplicemente che una stazione può accedere senza coordinazione. Di fatto nessun nodo sa degli altri e dello stato della rete, ma quando desidera mandare un pacchetto lo manda in rete utilizzando tutta la capacità del canale. Se:

* il canale è libero, allora la comunicazione ha esito positivo
* il canale non è libero, allora si ottiene una collisione.
  * di solito questo evento riduce i dati a indecifrabili, e quindi non si possono utilizzare i due pacchetti.

Il protocollo quindi ha necessita di capire come rilevare una collisione e indicare il meccanismo di ritrasmissione nel caso si sia verificata una collisione. 

Ci sono 3 tipologie di questi protocolli:

* *slotted ALOHA*
* *ALOHA*
* *CSMA*, *CSMA/CD*, *CSMA/CA*

Dove i primi due sono variazioni dello stesso protocollo, ma sono entrambi primitivi. Sono stati sviluppati dall’università delle Hawaii per consentire la connessione in broadcast su connessione satellitare, ma ad oggi sono per lo più inutilizzati.

L’ultima tipologia è quella tuttora utilizzata ad oggi e ne esistono variazioni.



### Protocollo Slotted ALOHA

Questo protocollo ha delle caratteristiche ben definite:

* tutti i pacchetti hanno le stesse dimensioni
* il tempo è suddiviso in slot, che equivale al tempo di trasmissione di un pacchetto
* i nodi trasmettono solo all’inizio di ogni slot
* i nodi sono sincronizzati
* se avviene la collisione di due pacchetti in uno slot, i nodi coinvolti rilevano l’evento prima del termine dello slot

ll funzionamento è molto facile:

* quando arriva un nuovo pacchetto da inviare sulla rete a un nodo, si attende fino all’inizio dello slot successivo

* se **non** si verifica una collisione il nodo può trasmettere un nuovo pacchetto nello slot successivo

* se **si** verifica una collisione il nodo lo rileva e ritrasmette con una certa probabilità $p$ il suo pacchetto durante gli slot successivi

  * questa probabilità è necessaria perché, se si ha una collisione, allora è necessario aspettare del tempo perché non continui ad avvenire la collisione.
  * Di fatto a un certo punto della connessione si otterrà un caso in cui uno dei due nodi si ritira dalla connessione, e quindi l’altro esegue l’invio. L’altro nodo seguirà poi all’invio della connessione.

  ![image-20211220121636247](image/image-20211220121636247.png)

Questo modello ha:

* **lati positivi**:
  * l’approccio è molto semplice
  * chi riesce a trasmettere trasmette alla massima capacità del canale. Infatti in questo modo utilizzo tutte le frequenze per un certo tempo.
  * è fortemente decentralizzato, infatti conosce l’esistenza degli altri nodi solo in caso di collisione. 

* **lati negativi**:
  * in caso di collisione posso avere:
    * una serie di intervalli vuoti
    * una serie di slot persi
  * poco efficiente come banda utilizzata rispetto a quella disponibile se iniziano ad avvenire collisioni.
    * Il parametro $p$ deve essere scelto molto scientemente, siccome deve essere applicato in base alla rete, al numero di host e altre caratteristiche della rete. Questo valore quindi è molto difficile da trovare

Questo protocollo quindi è molto semplice, ma permette una connessione sempre con minor banda disponibile al crescere del numero degli host sulla rete, infatti in questo caso si avranno così tante collisioni che non si riuscirà mai a trasmettere nulla.

L’efficienza di questo protocollo è calcolata come la frazione di slot vincenti in presenza di un elevato numero di nodi attivi, che hanno sempre un elevato numero di pacchetti da spedire. La massima efficienza di questo protocollo si trova con un semplice modello matematico, ottenendo che la migliore probabilità di successo è il $37\%$. Infatti solo uno slot su tre compie del lavoro utile per la trasmissione.

Questo valore non è l’efficienza media, ma il valore di efficienza *massima*. Questo significa che questo standard non è molto efficiente, anzi non lo è per nulla.



### Protocollo ALOHA puro

Questo protocollo è uguale a quello visto precedentemente, ma senza l’implementazione di slot temporali. 

Questo modello è ancora peggiore, siccome si può avere un interferenza in ogni momento, non solo all’inizio dello slot precedente.

Per calcolare l’efficienza di questo modello bisogna calcolare che si rifiuteranno pacchetti che vengono mandati durante il sending di una altro pacchetto (durante tutta l’operazione). Questo modello di fatto dimezza ancora di più l’efficienza, rendendola al $18\%$. 

Questo protocollo solitamente piace siccome permette di avere un invio dei pacchetti appena questi sono disponibili, ma non si può avere una connessione stabile dovuto alle collisioni continue sulla rete, che riducono notevolmente la capacità della rete.

Questo concetto viene spiegato meglio dal grafico sotto:

![image-20211220150106376](image/image-20211220150106376.png)



### Protocollo CSMA (Carrier Sensing Multiple Access)

Questo protocollo permette di mantenere la sensibilità del protocollo ALOHA, ma con una semplice funzionalità. Non si esegue la divisione in tempo del pacchetti.

Si riesce però a capire quando il canale è libero attraverso la tecnica chiamata **rilevazione di portante (carrier-sensing)**. Con questa tecnica si può sapere se il canale è utilizzato (in questo caso si aspetta un lasso di tempo e si continua poi con la connessione). Nel caso in cui il canale sia libero invece si procede direttamente all’invio del pacchetto. 

Le collisioni possono ancora verificarsi, infatti avendo un certo ritardo di propagazione si può avere un caso in cui i nodi non rilevino la reciproca trasmissione. 

![image-20211220150123090](image/image-20211220150123090.png)

Questo schema fa capire come entrambe le trasmissioni vadano distrutte se il tempo di propagazione del segnale non è abbastanza veloce da avvisare tutti gli altri nodi della rete.

Nelle trasmissioni CSMA quindi il parametro che fa variare la frequenza di collegamento dipende dalla distanza delle stazioni, infatti più queste sono lontane più è facile notare questo effetto. 



### Protocollo CSMA/CD (CSMA Collision Detection)

Siccome il CSMA non sembrava abbastanza, siccome aveva il problema che l’identificazione della collisione dipendeva completamente dal tempo di propagazione del segnale. 

Questo protocollo funziona come il CSMA, ma ha una variante riferita al rilevamento della portanza differito. Praticamente si ha un rilevamento di collisione continuativo nel tempo di trasmissione dei dati, in questo modo:

* rileva la collisione dei pacchetti in poco tempo
* riesce ad annullare una connessione appena si accorge che un’altra è in corso.

In questo modo i tempi di reazione sono più veloci del protocollo CSMA standard.

Il diagramma quindi varia in questo modo:

![image-20211220151837019](image/image-20211220151837019.png)

L’idea di fatto è che, siccome le collisioni non si possono non avere, in questo modo possiamo mantenere l’utilizzo del canale un tempo maggiore.

Questo modello di rilevazione della collisione è facilmente eseguibile su un canale via cavo (LAN Cablate), infatti è inserito nel protocollo Ethernet.

Questa tecnica non funziona particolarmente bene nel wireless siccome ogni stazione riceve tutti i pacchetti. Per questo e altri motivi poi nelle comunicazioni senza fili si utilizza il CSMA/CA.



### Protocolli MAC a rotazione

I protocolli a rotazione erano abbastanza diffusi, poi l’accesso casuale ha vinto come approccio, siccome è più semplice.

Ci sono due meccanismi di **MAC (Medium Access Control)**:

* a **suddivisione del canale**: questa divisione è molto rigida, condividendo il canale equamente e efficientemente con carichi elevati. Diventa inefficiente con carichi poco elevati.
* ad **accesso casuale**: questa suddivisione permette alla rete di essere molto efficiente con carichi non elevati, infatti un singolo nodo riesce ad utilizzare interamente il canale. Allo stesso modo con carichi elevati avviene un eccesso di collisioni, infatti se si incrementa il numero di stazioni che si può trasmettere il livello di collisioni aumenta

I protocolli a rotazione permettono di avere il meglio di entrambi i protocolli:

* essere più flessibili sulla suddivisione del canale
* essere meno saturati dalla quantità di stazioni precedenti

La soluzione sarebbe il **polling**:

* funziona nel metodo *master-slave*: il nodo *master* interroga a turno tutti gli altri nodi, *slave*. 
  * solitamente funziona con il metodo *round-robin*, ovvero a rotazione ogni host ha la possibilità di mandare in rete i frame che ha necessità di inviare.
* questo tipo di meccanismo è per forza di cose centralizzato
* permette l’eliminazione completa delle collisioni
* permette di eliminare slot vuoti
* crea del ritardo, chiamato *ritardo di polling*
* *one-point-of-failure*, infatti se il nodo principale si guasta, allora l’intero canale rimane inattivo.

Siccome questo tipo di protocollo è molto centralizzato, e sulle reti non piace mai questo concetto, si è iniziato a pensare a un nuovo protocollo, chiamato **protocollo token-passing**, ovvero protocollo a passaggio di token.

Questo protocollo esegue le stesse operazioni e gli stessi controlli ma in maniera distribuita:

* il messaggio di controllo (*token*) è anche il meccanismi di accesso al canale
  * il token viene passato tra tutti le stazioni disponibili
  * se la stazione ha dei dati pronti ad essere inviati allora vengono inviati, se non è presente allora passa il token alla stazione successiva.
* Questo meccanismo si basa sul fatto che chi ha il token può trasmettere, ma allo stesso modo ha una serie di vincoli:
  * non può tenere il token oltre un certo tempo massimo
  * si dovrebbe liberare del token appena possibile (solitamente appena si finisce di mandare i dati)

In questo modo si hanno effettivamente entrambi i vantaggi, infatti:

* si ha un ordine preciso e periodico per accedere al canale.
* Ogni stazione sa che avrà una parte delle risorse del canale
* ogni nodo che non ha necessità di comunicare semplicemente passa il token



### Protocolli Riepilogo

Con il canale condiviso si ha una serie di caratteristiche:

* **suddivisione del canale** per tempo, frequenza e codice
  * TDM, FDM
* **suddivisione casuale** (oppure dinamica)
  * ALOHA, S-ALOHA, CSMA, CSMA/CD
  * Rilevamento della portante: facile in alcune tecnologie (cablate) e difficile in altre (wireless)
  * CSMA/CD usato in Ethernet
  * CSMA/CA usato in 802.11
* **a rotazione**
  * Polling con un nodo principale o a passaggio di testimone
  * Bluetooth, FDDI, IBM Token Ring



Ad oggi molto standard utilizzano un protocollo del tipo CSMA/CD. Le reti cellulari invece utilizzano dei metodi di prenotazione del canale più particolari.



## 5.3 Indirizzi a livello di collegamento

L’indirizzamento di livello 2 è uno degli indirizzi disponibili di un host. Solitamente ogni host ha 3 indirizzi: 

* indirizzo di porta
* indirizz dell’host
* indirizzo **MAC** (chiamato anche indirizzo LAN oppure indirizzo Ethernet)

L’indirizzo di rete ha la necessità di essere unico su ogni sottorete, in modo da permettere di identificare l’host univocamente. Questo indirizzo è di $32bit$ e se n’è parlato [qui](### Indirizzamento IPv4).

L’indirizzo **MAC** è un valore a $48bit$ e è affidato a ogni interfaccia al momento della “costruzione”. Questo indirizzo non cambia mai, anche se si cambia rete e serve a caratterizzare le sorgenti di livello 2.

Questi indirizzi vengono scritti in valori esadecimali. L’indirizzo di broadcast è `FF-FF-FF-FF-FF-FF`. Questo indirizzo è sempre usato siccome è possibile fare comunicazioni in broadcast su tutte le reti.



### Indirizzi MAC/LAN e Protocollo ARP

Questi indirizzi sono gestiti dal IEEE, che ne sovrintende la gestione.

Per garantire la unicità degli indirizzi, solitamente una società compra un blocco di spazio di indirizzi. I primi bit sono fissati per un certo produttore. Infine gli altri valori di quell’insieme sono riservati per quell’apparato.

Questo viene fatto in modo da risolvere un problema dell’applicazione in maniera pedissequa della pila protocollare: si deve aver un modo per collegare l’indirizzo di rete all’indirizzo di link, siccome il livello che effettivamente consegna il livello 3, che lavora con gli indirizzi di livello 2.

Questo significa che il livello 3 della pila protocollare crea un datagramma con destinatario un indirizzo IPv4/IPv6 e poi passa al livello 2. Il livello due, che effettivamente è quello che si occupa della consegna del frame lavora solo con indirizzi di livello 2. Serve quindi un modo per far comunicare tra di loro questi due protocolli in modo da assicurare una corretta consegna. Di fatto quindi bisogna capire come far corrispondere all’indirizzo fisico di un interfaccia un indirizzo di livello 3.

Per collegare questi due indirizzi che appartengono a due layer diversi viene in aiuto il **protocollo ARP (Address Resolution Protocol)**. Di base questo protocollo crea una *tabella ARP* che contiene la corrispondenza IP e MAC in questo modo: `<indirizzo IP; indirizzo MAC; TTL>` (con un `TTL`, di solito di una 20ina di minuti, in modo da fare il refresh della tabella e quindi avere sempre le corrispondenze corrette).

Questo protocollo è forse il più importante di tutto il livello 2, siccome permette l’effettiva comunicazione sulla rete.

Come funziona la **creazione della tabella ARP**:

* `A` vuole inviare un datagramma a `B` e l’indirizzo MAC di  `B` non è nella tabella ARP di `A`

* `A` trasmette un pacchetto *broadcast* il messaggio di richiesta ARP, contentente l’indirizzo IP di `B`.

  Questo messaggio si ottiene utilizzando come indirizzo MAC del destinatario il valore di broadcast (`FF-FF-FF-FF-FF-FF`). A questo punto tutte le macchine della LAN ricevono una richiesta ARP.

* `B` riceve un pacchetto ARP, risponde ad `A` comunicandogli il proprio indirizzo MAC. Il frame viene mandato direttamente all’indirizzo MAC di `A`.

  Con questo pacchetto ci si può nuovamente costruire una nuova entry sulla tabella ARP.

Da notare che la richiesta ARP è inviata in modalità broadcast, mentre invece il messaggio di risposta ARP è inviata in un pacchetto standard.

ARP è *plug-and-play*:

* la tabella si costruisce da sola, facendo automaticamente le richieste ai nodi
* non deve essere configurata in nessun modo



### Invio di pacchetti verso un nodo esterno della sottorete

![image-20211220161942633](image/image-20211220161942633.png)

* `A` sa che deve inoltrare il pacchetto al router, siccome `B` non è sulla stessa sottorete. Questa scelta viene fatta confrontando gli indirizzi di livello 3.

* `A` invia il pacchetto al router. Per fare questa operazione devo conoscere l’indirizzo locale della porta del router interna (interfaccia sulla sottorete interna). Questa comunicazione inizia con una richiesta di comunicazione ARP, infatti la macchina richieste l’indirizzo di questa interfaccia in broadcast sulla rete se non la possiede già. 

  Una volta che tutte queste azioni vengono completate, `A` può mandare il suo frame al router

  Anche se il router è un indirizzo IP privato le azioni effettuate non cambiano, ma semplicemente la tabella ARP viene riempita con una relazione indirizzo IP-MAC con un indirizzo privato.

* Quando il router riceve il pacchetto, allora viene deincapsulato fino a livello 3 per leggere l’intestazione del protocollo IP. Facendo questa operazione il router scopre che la macchina è collegata alla rete di una sua interfaccia. Di nuovo quindi dovrà popolare su questa interfaccia la propria tabella ARP per cercare l’interfaccia B.

  Questa operazione avviene sempre in internet, ma di solito i valori degli indirizzi di MAC rimangono in memoria per un certo tempo.

Di fatto quindi:

* il protocollo IP si occupa di trovare le direzioni su dove effettuare il collegamento
* tramite l’indirizzo di MAC si esegue l’effettiva comunicazione su una linea del collegamento



## 5.4 Ethernet

Questo metodo di comunicazione detiene la posizione dominante nel mercato delle LAN cablate. Questa tecnologia prende il nome da *ether*, ovvero il cavo che all’inizio era il fondamento di questo modello.

Questa rete è stata la prima LAN ad alta velocità con vasta diffusione. E’ la più semplice, meno costosa dei token ring, FDDI e ATM. 

Ethernet è molto interessante perché è stato uno dei pochi standard che è riuscito a mantenere retrocompatibilità con questi apparati, ma comunque a essere sempre al passo con le necessità dei tempi (ad esempio la rete Ethernet adesso ha capacità anche superiori ai $10Gbits$, logicamente salendo di costo).



Questa tecnica di comunicazione era iniziata con una **topologia a bus**, diffusa fino a metà degli anni 90. Oggi quasi tutte le odierne reti Ethernet sono progettate con una **topologia a stella**:

![image-20211220163836941](image/image-20211220163836941.png)

Solitamente al centro di ogni stella è collegato un hub o commutatore (*switch di livello 2*) che riunisce 4 apparati solitamente.

Ciascun nodo esegue un protocollo Ethernet separato e non entra in collisione con gli altri. 



### Struttura dei pacchetti Ethernet

![image-20211220164156244](image/image-20211220164156244.png)

* il *preabolo*

  Sono 8 byte: sette con questa sequenza di bit `10101010` e l’ultimo con `10101011`. Questi byte non portano nessuna informazione, ma servono solo per sincronizzare il clock, “attivare” gli adattatori dei riceventi e sincronizzare i loro orologi con quello del trasmittente.

* gli indirizzi sono a *6byte* e sono gli indirizzi MAC di prima. Si utilizzano gli indirizzi di destinazione prima perché è più facile eseguire lo switching.

  Il campo di destinazione viene messo davanti agli altri solo in questo protocollo siccome negli anni ‘80 questo modello permetteva di avere le indicazioni di dove mandare il pacchetto come prima informazione letta dal bus e quindi iniziare già a trasportare il frame.

  Questo pacchetto viene allora trasferito a livello di rete.

  I pacchetti con altri indirizzi MAC vengono ignorati.

* *campo tipo* del pacchetto indica il protocollo di rete (in gergo questa funzione permette al protocollo ethernet di supportare vari protocolli di rete, oppure di *multiplexare* i protocolli).

* il *payload* del pacchetto

* *controllo CRC*: questo campo permette all’adattatore ricevente di rilevare la presenza di un errore nei bit del pacchetto.

Il protocollo ethernet è senza connessione e non affidabile. Questo viene fatto siccome si pagherebbe la complessità e meccanismi di trasmissione, oltre che esiste già questa possibilità dal punto di vista software con i protocolli di trasmissione. Inoltre avvicinandosi sempre più all’hardware queste operazioni devono essere fatte sempre più velocemente (meno funzionalità, più semplice e veloce possibile).



Fasi operative della connessione con CSMA/CD:

![image-20211220165332115](image/image-20211220165332115.png) 

Il segnale di disturbo al momento della connessione si manda un segnale diverso (solitamente anche a un voltaggio leggermente superiore) in modo da dire a tutte le trasmissioni di fermarsi. Questo segnale è di 48bit solitamente.

![image-20211220165658307](image/image-20211220165658307.png)

Se io continuo a collidere, il massimo che posso aspettare è $50msec$. Questo valore è la conseguenza di questa slide sopra.



### Efficienza di Ethernet

Questo modello è un CSMA/CD leggermente modificato. L’efficienza di questo protocollo quindi viene trovato con:
$$
efficienza = \frac{1}{1+5t_{prop}/t_{trasm}}
$$
Da questa formula si capisce che:

* se $t_{prop} \to 0$ allora $efficienza \to 1$.
* al crescere di $t_{trans}$, l’$efficienza to 1$

Questo modello è più efficiente di ALOHA: decentralizzato, semplice e poco costoso.



Ethernet è un protocollo molto utilizzato, ma cambia il mezzo fisico e in base a quello la nomenclatura:

![image-20211220170154783](image/image-20211220170154783.png)

### Codifica Manchester

Questa modalità viene utilizzata nella comunicazione in protocollo Ethernet `10BaseT`. Questa codifica si distanzia leggermente dal modello binario utilizzato solitamente nella comunicazione:

![image-20211220170445039](image/image-20211220170445039.png)

Questo modello utilizza delle “mezze forme d’onda” nel quale:

* $1$: il segnale parte dall’alto e scende verso il basso
* $0$: il segnale parte dal basso e sale verso l’alto.

Questo modello permette di fatto di avere una transizione ogni ricezione di ciascun bit e permette di sincronizzare gli orologi degli adattatori trasmittenti e riceventi, permettendo quindi di non avere la sincronizzazione del clock tra i nodi

Questa operazione è una codifica che avviene a livello fisico (hardware). 

Logicamente questa soluzione non funziona con il metodo di comunicazione in fibra, siccome utilizza degli impulsi di luce.



## 5.5 Switch a livello di collegamento

Il protocollo Ethernet è uno standard nato *bus*, ma poi trasformato in forma a *stella*. 

Per fare questa transizione è necessario introdurre un nuovo dispositivo, l’**hub**:

* questo dispositivo è piuttosto stupido, infatti all’arrivo di un bit lo riproduce incrementando l’energia attraverso tutte le interfacce. (lavora a livello fisico)
* non implementa nessun logica né nessun tipo di controllo
* Di fatto trasforma i 4 doppini in un unico mezzo broadcast

Lo **switch** è un dispositivo molto più interessante:

* questo dispositivo lavora a livello di link, quindi riesce a svolgere un ruolo attivo:

  * filtra e inoltra i pacchetti Ethernet

  * esamina gli indirizzi di destinazione e lo invia all’interfaccia corrispondente alla sua destinazione

  * quando il pacchetto è stato inoltrato nel segmento utilizza CSMA/CD per accedere al segmento.

    Su ogni canale è in grado di ricevere e trasmettere come se fosse una stazione normale

* è un dispositivo *trasparente*: gli host sono inconsapevoli della presenza di switch, infatti non serve saperne l’esistenza ne comunicare direttamente con lui

* è un oggetto *plug-and-play* e *autoapprendimento*: sono due tecniche che permettono agli switch di funzionare senza essere direttamente configurati.

  * Quando un host crea un frame, questo stesso oggetto fornisce una serie di informazioni tra cui l’indirizzo MAC del pacchetto.

  * quando il router riceve un pacchetto lo switch impara l’indirizzo del mittente

  * lo switch registra la coppia mittente/indirizzo nella tabella di commutazione, in questo modo si ottiene una vera tabella di commutazione.

    ![image-20211220172220798](image/image-20211220172220798.png)

    ![image-20211220172233868](image/image-20211220172233868.png)

  * quando la destinazione è nota invece si esegue il cosiddetto *selective send*, ovvero recupera la entry della tabella e ridireziona il pacchetto attraverso il determinato indirizzo MAC

* gli host sono collegati direttamente allo switch. Questo oggetto permette la bufferizzazione dei pacchetti e l’inoltro in base a una tabella di switching. 

Gli switch possono essere interconnessi, ma non delimitano il broadcast come i router. Con il fatto che gli switch 

![image-20211220172609579](image/image-20211220172609579.png)

![image-20211220172619269](image/image-20211220172619269.png)

## Capitolo 5: Riassunto

Solitamente in una rete aziendale ci sono delle reti che possiedono più switch. Di fatto però è difficile avere una rete completamente a livello 2, in modo che non tutte le comunicazioni avvengano ovunque e che le reti di fatto vengano divise.

![image-20211220173215126](image/image-20211220173215126.png)

In realtà spesso le reti sono modificabili a livello software, attraverso le cosiddette *SDN (software define network)*, in cui si riesce a ottenere delle reti virtuali complete via software.

Tutta la gestione della rete invece è visualizzabile attraverso una serie di indirizzi IP che permettono di accedere a questi oggetti per la gestione e per il controllo. Di solito inoltre ogni router ha un interfaccia di management che permette di modificare il comportamento del router dal network manager.

